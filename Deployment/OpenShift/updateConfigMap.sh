#! /bin/sh

oc create configmap groundtask-server-env --from-file=src/main/liberty/config/server.env  --dry-run -o yaml | oc apply -f -
oc create configmap groundtask-jvm-options --from-file=src/main/liberty/config/jvm.options  --dry-run -o yaml | oc apply -f -
oc create configmap groundtask-keystore --from-file=src/main/liberty/config/resources/security/key.p12  --dry-run -o yaml | oc apply -f -
oc create configmap groundtask-runtime-props --from-file=src/main/liberty/config/resources/runtime-props/ --dry-run -o yaml | oc apply -f -