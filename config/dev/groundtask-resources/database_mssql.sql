

CREATE TABLE ALERTS (
       ID CHAR(32) NOT NULL
     , EMPLOYEE_NUMBER CHAR(10) NOT NULL
     , TIME DATETIME
     , ALERT_DATA VARBINARY(MAX)
     , PRIMARY KEY (ID)
);
CREATE INDEX IX_ALERTS_EMP_NUM ON ALERTS (EMPLOYEE_NUMBER);
CREATE INDEX IX_ALERTS_TIME ON ALERTS (TIME);

CREATE TABLE EMPLOYEES (
       EMPLOYEE_NUMBER CHAR(16) NOT NULL
     , LAST_REGISTER DATETIME
     , PRIMARY KEY (EMPLOYEE_NUMBER)
);

