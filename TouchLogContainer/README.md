This container is a sidecar container for groundtask. 

Why do we need this? 

The application writes its logs to a shared persistent volume at /logs/groundtask-logs however there seems to be an issue with the logs syncing.  When 1 pod would write to its log file it wouldn't be updated on ther persistent volume.  
I'm not sure of the cause of the issue and it may be a bug or a log4j issue, however if we run the touch command on the file it updates the time stamp on the file and this triggers the file to sync correctly. 

This sidecar container runs in a separate container than groundtask but it shares the same host and the same persistent volume.  This simply runs the touchlog.sh script, which touches the file, sleeps for a given time, then touches it again. 

You should not have to modify this in the future, however if you do need to here are the commands to run

`cd TouchLogContainer`
`docker build . -t docker build . -t us.icr.io/groundtask/touchlog:YYYY-MM-DD`
`docker push us.icr.io/groundtask/touchlog:YYYY-MM-DD`
(You will need to login to ibm cloud from commandline)
`ibmcloud login --sso`
`ibmcloud cr login`
After you do this you should have permission to push it. 


Once this is complete, edit the deployment_template.yml for groundtask to change the tag of `us.icr.io/groundtask/touchlog:YYYY-MM-DD` to the new version. 


That should be all you need to do if you want to update the script for any reason

