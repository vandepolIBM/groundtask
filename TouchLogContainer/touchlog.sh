#! /bin/sh
while :
do
    filenames=$(ls ${LOG_DIR}*${HOSTNAME}*)
    echo "filenames: $filenames"
    if [ -z "$filenames" ] 
    then
        echo "file does not exist -- normal at startup, should not happen shortly after"
    else

        while IFS= read -r file; 
        do
            echo "file do exist: $file -- touching it."
            touch $file
        done <<< "$filenames"
    fi
        
    echo "after:  ${LOG_DIR}*${HOSTNAME}* $(ls -al ${LOG_DIR}*)"
    sleep ${SLEEP_INTERVAL}
done