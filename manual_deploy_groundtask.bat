set ENV=dev
set NAMESPACE=groundtask-dev
set PIPELINE_IMAGE_URL=us.icr.io/groundtask/groundtask:48-master-ca0cf99f-20210406200142
set DEPLOYMENT_TEMPLATE=Deployment/OpenShift/deployment_template.yml
set DEPLOYMENT_FILE=deployment.yaml

oc create configmap groundtask-server-env --from-file=config/%ENV%/server.env -n %NAMESPACE% --dry-run=client -o yaml | oc apply -f -
oc create configmap groundtask-jvm-options --from-file=config/%ENV%/jvm.options -n %NAMESPACE% --dry-run=client -o yaml | oc apply -f -
oc create configmap groundtask-keystore --from-file=config/%ENV%/security/key.p12 -n %NAMESPACE% --dry-run=client -o yaml | oc apply -f -
oc create configmap groundtask-runtime-props --from-file=config/%ENV%/runtime-props/ -n %NAMESPACE% --dry-run=client -o yaml | oc apply -f -
oc create configmap groundtask-resources --from-file=config/%ENV%/groundtask-resources/ -n %NAMESPACE% --dry-run=client -o yaml | oc apply -f -
oc create configmap groundtask-jmstopic-server-xml --from-file=JMSTopicContainer/server.xml -n %NAMESPACE% --dry-run=client -o yaml | oc apply -f -

echo "oc process -p GROUNDTASK_IMAGE=%PIPELINE_IMAGE_URL%  -f %DEPLOYMENT_TEMPLATE% -o yaml > %DEPLOYMENT_FILE%"
oc process -n %NAMESPACE% -p GROUNDTASK_IMAGE=%PIPELINE_IMAGE_URL%   -f %DEPLOYMENT_TEMPLATE% -o yaml> %DEPLOYMENT_FILE%

oc apply -n %NAMESPACE% --overwrite=true -f "%DEPLOYMENT_FILE%" 
