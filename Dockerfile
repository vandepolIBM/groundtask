FROM maven:3.6.1-ibmjava-8 as build

COPY ./src/main/webapp/WEB-INF/lib/dwr-3.0-20081117.222155-1.jar /tmp/dwr-3.0-20081117.222155-1.jar
COPY ./pom-docker.xml pom.xml
COPY ./settings.xml settings.xml
RUN mvn dependency:go-offline -s settings.xml

COPY ./src src
RUN mvn package -P websphere -s settings.xml

#--------------------
FROM ibmcom/websphere-liberty:javaee8-ubi-min

RUN mkdir -p /opt/ibm/wlp/usr/servers/defaultServer/apps
RUN mkdir -p /opt/ibm/wlp/usr/servers/defaultServer/resources/security
COPY --chown=1001:0 ./src/main/liberty/config/ /opt/ibm/wlp/usr/servers/defaultServer/

ARG SSL=false
ARG MP_MONITORING=false
ARG HTTP_ENDPOINT=false

RUN configure.sh

COPY --chown=1001:0 --from=build ./target/ground-task-2.0.war /opt/ibm/wlp/usr/servers/defaultServer/apps/
COPY --chown=1001:0  ./aircanada_favicon.war /opt/ibm/wlp/usr/servers/defaultServer/
