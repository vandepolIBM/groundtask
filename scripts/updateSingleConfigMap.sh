#! /bin/sh + 

usage ()
{
     echo "usage updateSingleConfigMap.sh <environment>"
}
if [ $# == 1 ]; then
    ENV=$1
else
    usage
    exit -1
fi


  
set -e
set -x
oc project $ENV

oc create configmap groundtask-server-env --from-file=$ENV/server.env -n $ENV --dry-run -o yaml | oc apply -f -
oc create configmap groundtask-jvm-options --from-file=$ENV/jvm.options -n $ENV --dry-run -o yaml | oc apply -f -
oc create configmap groundtask-keystore --from-file=$ENV/security/key.p12 -n $ENV --dry-run -o yaml | oc apply -f -
oc create configmap groundtask-runtime-props --from-file=$ENV/runtime-props/ -n $ENV --dry-run -o yaml | oc apply -f -
oc create configmap groundtask-resources --from-file=$ENV/groundtask-resources/ -n $ENV --dry-run -o yaml | oc apply -f -