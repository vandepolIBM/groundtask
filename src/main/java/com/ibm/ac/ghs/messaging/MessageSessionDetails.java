package com.ibm.ac.ghs.messaging;

import java.util.Date;

public class MessageSessionDetails {

	private String sessionId;
	private Date lastAccessedTime;
	private Date creationTime;
	private String userId;
	private long pendingAlerts;

	public MessageSessionDetails(String sessionId, String userId, long lastAccessedTime, long creationTime) {
		this.sessionId = sessionId;
		this.userId = userId;
		this.lastAccessedTime = new Date(lastAccessedTime);
		this.creationTime = new Date(creationTime);
	}
	
	public MessageSessionDetails(String sessionId, String userId, Date lastAccessedTime, Date creationTime) {
		this.sessionId = sessionId;
		this.userId = userId;
		this.lastAccessedTime = lastAccessedTime;
		this.creationTime = creationTime;
	}


	public String getSessionId() {
		return sessionId;
	}

	public String getUserId() {
		return userId;
	}

	public Date getLastAccessedTime() {
		return lastAccessedTime;
	}

	public Date getCreationTime() {
		return creationTime;
	}
	
	public long getPendingAlerts() {
		return pendingAlerts;
	}
	
	public void setPendingAlerts(long pendingAlerts) {
		this.pendingAlerts = pendingAlerts;
	}
	
	@Override
	public String toString() {
		return "[sessionId=" + sessionId + ",userId=" + userId + ",lastAccess=" + lastAccessedTime + "]";
	}
}
