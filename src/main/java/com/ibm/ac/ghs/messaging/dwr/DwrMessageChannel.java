package com.ibm.ac.ghs.messaging.dwr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContext;
import org.apache.logging.log4j.LogManager;
import org.apache.commons.lang.StringUtils;

import org.directwebremoting.Browser;
import org.directwebremoting.ScriptSession;
import org.directwebremoting.ScriptSessions;
import org.directwebremoting.ServerContext;
import org.directwebremoting.ServerContextFactory;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.event.ScriptSessionEvent;
import org.directwebremoting.event.ScriptSessionListener;
import org.directwebremoting.extend.ScriptSessionManager;
import org.springframework.web.context.ServletContextAware;

import com.ibm.ac.ghs.messaging.MessageChannel;
import com.ibm.ac.ghs.messaging.MessageManager;
import com.ibm.ac.ghs.messaging.MessageSessionDetails;
import com.ibm.ac.ghs.persistence.AlertManager;
import com.ibm.ac.ghs.persistence.LogAction;
import com.ibm.ac.ghs.persistence.LogAction.LogActionType;

public class DwrMessageChannel implements ServletContextAware, ScriptSessionListener, MessageChannel {

	private Logger log = LogManager.getLogger(getClass());

	private ScriptSessionManager scriptSessionManager;

	private MessageManager messageManager;
	

	public void setMessageManager(MessageManager messageManager) {
		this.messageManager = messageManager;
	}
	

	public void setServletContext(ServletContext servletContext) {
		// add a script session listener
		ServerContext serverContext = ServerContextFactory.get();
		scriptSessionManager = (ScriptSessionManager) serverContext.getContainer().getBean(ScriptSessionManager.class.getName());
		scriptSessionManager.addScriptSessionListener(this);
	}

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.messaging.MessageChannel#sendAlert(java.lang.String,
	 *      com.ibm.ac.ghs.tasks.Alert)
	 */
	public void sendGetAlertUpdates(String employeeNumber) {
		sendAlert(employeeNumber, "ghs.srvc.getAlertUpdates", employeeNumber);
	}

	/**
	 * Internal method to add alert reception function call to the script session
	 * @param employeeNumber the employee number
	 * @param obj either an List or single alert object instance
	 */
	private void sendAlert(final String employeeNumber, final String function, final Object obj) {
		Browser.withPage("/gc/ctrl/main.html?empl=" + employeeNumber, new Runnable() {
			public void run() {
				if (log.isDebugEnabled()) {
					log.debug("Adding script, empl=" + employeeNumber + ", script=" + function + ", obj=" + obj);
				}
				ScriptSessions.addFunctionCall(function, obj);
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.messaging.MessageChannel#sendGetTaskUpdates(java.lang.String)
	 */
	public void sendGetTaskUpdates(final String employeeNumber) {
		Browser.withPage("/gc/ctrl/main.html?empl=" + employeeNumber, new Runnable() {
			public void run() {
				String script = "ghs.srvc.getTaskUpdates";
				if (log.isDebugEnabled()) {
					log.debug("Adding script, empl=" + employeeNumber + ", script=" + script + ", obj=" + employeeNumber);
				}
				ScriptSessions.addFunctionCall(script, employeeNumber);
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.messaging.MessageChannel#requestLogAction(com.ibm.ac.ghs.persistence.LogAction)
	 */
	public void sendLogAction(LogAction logAction) {
		if (logAction.getActionType() == LogActionType.RETRIEVE) {
			requestLogs(logAction.getEmployeeNumber(), logAction.getNumRows());
		} else if (logAction.getActionType() == LogActionType.DELETE_ALL) {
			deleteLogs(logAction.getEmployeeNumber());
		}
	}

	/**
	 * Request a specified number of log rows for an employee.
	 * @param employeeNumber
	 * @param numRows
	 */
	private void requestLogs(final String employeeNumber, final int numRows) {
		Browser.withPage("/gc/ctrl/main.html?empl=" + employeeNumber, new Runnable() {
			public void run() {
				if (log.isDebugEnabled()) {
					log.debug("Adding script, empl=" + employeeNumber + ", script=getLogs");
				}
				if (numRows == -1) {
					ScriptSessions.addFunctionCall("ghs.srvc.logs.getAll");
				} else {
					ScriptSessions.addFunctionCall("ghs.srvc.logs.get", numRows);
				}
			}
		});
	}

	/**
	 * Deletes all logs for an employee
	 * @param employeeNumber
	 */
	private void deleteLogs(final String employeeNumber) {
		Browser.withPage("/gc/ctrl/main.html?empl=" + employeeNumber, new Runnable() {
			public void run() {
				if (log.isDebugEnabled()) {
					log.debug("Adding script, empl=" + employeeNumber + ", script=getLogs");
				}
				ScriptSessions.addFunctionCall("ghs.srvc.logs.deleteAll");
			}
		});
	}

	public void sessionCreated(ScriptSessionEvent scriptSessionEvent) {

		// get the employee number
		String employeeNumber = extractEmployeeNumber(scriptSessionEvent.getSession().getPage());

		// log the session details
		if (log.isDebugEnabled()) {
			log.debug("sessionCreatedwithnewchange: empl=" + employeeNumber + ", id=" + scriptSessionEvent.getSession().getId().substring(0, 4));
		}
		
		messageManager.employee_Login(employeeNumber);
		// check if there are any pending alerts in the database
		if (employeeNumber != null) {

			// register the employee with the message manager
			messageManager.addEmployee(employeeNumber, isPageLoadedRequest());
		}
	}

	public List<MessageSessionDetails> getMessageSessionDetails() {

		List<MessageSessionDetails> messageSessionDetails = new ArrayList<MessageSessionDetails>();
		
		/*Collection<ScriptSession> scriptSessions = scriptSessionManager.getAllScriptSessions();
		for (ScriptSession scriptSession : scriptSessions) {
			String empl = extractEmployeeNumber(scriptSession.getPage());
			if (empl != null) {
				messageSessionDetails.add(new MessageSessionDetails(scriptSession.getId(), empl, scriptSession.getLastAccessedTime(), scriptSession.getCreationTime()));
			}
		}*/
		return messageSessionDetails;
	}

	public void sessionDestroyed(ScriptSessionEvent scriptSessionEvent) {
		// get the employee number
		String employeeNumber = extractEmployeeNumber(scriptSessionEvent.getSession().getPage());
		messageManager.employee_Logout(employeeNumber);
		// log the session details
		if (log.isDebugEnabled()) {
			log.debug("sessionDestroyed: empl=" + employeeNumber + ", id=" + scriptSessionEvent.getSession().getId().substring(0, 4));
		}

		if (employeeNumber != null) {
			messageManager.removeEmployee(employeeNumber);
		}
	}

	private boolean isPageLoadedRequest() {
		try {
			if (StringUtils.contains(WebContextFactory.get().getHttpServletRequest().getRequestURL().toString(), "pageLoaded.dwr")) {
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}

	private String extractEmployeeNumber(String url) {
		try {
			String query = StringUtils.substringAfter(url, "?");
			if (query != null) {
				String[] params = query.split("&");
				Map<String, String> map = new HashMap<String, String>();
				for (String param : params) {
					String name = param.split("=")[0];
					String value = param.split("=")[1];
					map.put(name, value);
				}
				return map.get("empl");
			}
		} catch (Exception e) {
		}
		return null;
	}
}
