package com.ibm.ac.ghs.messaging;

import java.util.List;

import com.ibm.ac.ghs.persistence.LogAction;

public interface MessageChannel {

	public void sendGetAlertUpdates(String employeeNumber);
	
	public void sendGetTaskUpdates(String employeeNumber);
	
	public void sendLogAction(LogAction logAction);
	
	public List<MessageSessionDetails> getMessageSessionDetails();
}