package com.ibm.ac.ghs.messaging.dwr;

import org.apache.commons.lang.StringUtils;
import org.directwebremoting.impl.DefaultPageNormalizer;

public class CustomPageNormalizer extends DefaultPageNormalizer {

	private static final String EMPL_PARAM = "empl=";
	
	public String normalizePage(String page) {
		if (page != null) {
			String preQuery = StringUtils.substringBefore(page, "?");
			if (preQuery != null) {
				String empl = getEmplParam(page);
				if (empl != null) {
					page = new StringBuffer().append(preQuery).append("?").append(EMPL_PARAM).append(empl).toString();
				}
			}
		}
		return super.normalizePage(page);
	}

	public String getEmplParam(String url) {
		int startIndex = url.indexOf(EMPL_PARAM);
		if (startIndex == -1) {
			return null;
		}
		int length = EMPL_PARAM.length();
		int endIndex = url.indexOf("&", startIndex + length);
		if (endIndex == -1) {
			endIndex = url.length();
		}
		return url.substring(startIndex + length, endIndex);
	}
}
