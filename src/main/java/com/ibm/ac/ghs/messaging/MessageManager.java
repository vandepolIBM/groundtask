package com.ibm.ac.ghs.messaging;

import java.util.List;

import com.ibm.ac.ghs.persistence.LogAction;
import com.ibm.ac.ghs.tasks.Alert;

public interface MessageManager {

	public boolean addEmployee(String employeeNumber, boolean purge);
	public void removeEmployee(String employeeNumber);
	
	public List<MessageSessionDetails> getMessageSessionDetails();
	
	public void sendAlert(String employeeNumber, Alert alert);
	public void removeAlert(String employeeNumber, String alertId);
	public List<Alert> getAlerts(String employeeNumber);
	
	public void sendGetTaskUpdates(String employeeNumber);
	
	public void sendLogAction(LogAction logAction);
	public void employee_Login(String employeeNumber);
	public void employee_Logout(String employeeNumber);
}
