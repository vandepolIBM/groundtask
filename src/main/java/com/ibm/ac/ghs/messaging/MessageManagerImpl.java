package com.ibm.ac.ghs.messaging;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ibm.ac.ghs.persistence.AlertManager;
import com.ibm.ac.ghs.persistence.LogAction;
import com.ibm.ac.ghs.tasks.Alert;

public class MessageManagerImpl implements MessageManager {

	private Logger log = LogManager.getLogger(getClass());

	private ScheduledExecutorService executor = Executors.newScheduledThreadPool(25);

	private Map<String, EmployeeAlertContainer> employeeMap = new HashMap<String, EmployeeAlertContainer>();

	private MessageChannel messageChannel;

	private AlertManager alertManager;
	
	private static final int RETRY_SYNC_ALERTS_INTERVAL = 120000;

	public void setMessagingChannel(MessageChannel messageChannel) {
		this.messageChannel = messageChannel;
	}

	public void setAlertManager(AlertManager alertManager) {
		this.alertManager = alertManager;
	}
	
	public void employee_Login(String employeeNumber){
		if (log.isDebugEnabled()) {
			log.debug("Enter employee_Login");
		}
		
		alertManager.employee_Login(employeeNumber);
		
		
	}
	public void employee_Logout(String employeeNumber){
		if (log.isDebugEnabled()) {
			log.debug("Enter employee_Logout");
		}
		
		alertManager.employee_Logout(employeeNumber);
		
		
	}

	public synchronized boolean addEmployee(String employeeNumber, boolean purge) {

		// log it
		if (log.isDebugEnabled()) {
			log.debug("Adding/Updating employee=" + employeeNumber + ", purge=" + purge);
		}
		
		// retrieve the employee container
		boolean newContainer = false;
		EmployeeAlertContainer cont = employeeMap.get(employeeNumber);
		if (cont == null) {
			newContainer = true;
			cont = new EmployeeAlertContainer(employeeNumber);			
		} else if (purge) {
			cont.removeAllAlerts();
		}

		boolean newRegistration = false;
		try {
			// register employee
			newRegistration = alertManager.registerEmployeeForAlerts(employeeNumber, purge);
			
			// check if the employee container exists
			// This is intended for fail over, when one server fails, the employee alert queue
			// repopulated with the alerts from the database
			if (newContainer) {
				List<Alert> alerts = alertManager.getAlerts(employeeNumber);
				if (alerts.size() > 0) {
					// repopulate the alert container from alert manager
					for (Alert alert : alerts) {
						cont.addAlert(alert);
					}
				}
			}
			
		} catch (Exception e) {
			// error registering the employee so treat as if its a new registration
			// so a getUpdates will be pushed to the device to retrieve the latest
			// task list
			log.warn("Unable to register empl=" + employeeNumber + "; " + e.getMessage());
			newRegistration = true;
		}

		// register and update the map
		employeeMap.put(employeeNumber, cont.register());
		
		// check if employee is registered
		// if its the pageLoaded request, we dont need to
		// tell the client to update tasks
		// since it will do it automatically after the login.
		if (!purge && newRegistration) {
			cont.syncTasks();
		} else {
			// push the sync
			cont.syncAlerts();
		}

		return true;
	}

	public synchronized void removeEmployee(String employeeNumber) {
		// log it
		if (log.isDebugEnabled()) {
			log.debug("Removing/Updating employee=" + employeeNumber);
		}

		EmployeeAlertContainer cont = employeeMap.get(employeeNumber);
		employeeMap.put(employeeNumber, cont != null ? cont.deregister() : null);
	}

	public void sendAlert(final String employeeNumber, Alert alert) {
		EmployeeAlertContainer cont = employeeMap.get(employeeNumber);
		if (cont != null) {
			cont.addAlert(alert);
			cont.syncAlerts();
		}
	}

	public void removeAlert(String employeeNumber, String alertId) {
		EmployeeAlertContainer cont = employeeMap.get(employeeNumber);
		if (cont != null) {
			cont.removeAlert(alertId);
		}
	}

	public List<Alert> getAlerts(String employeeNumber) {
		EmployeeAlertContainer cont = employeeMap.get(employeeNumber);
		if (cont != null) {
			return cont.getAlerts();
		} else {
			return Collections.EMPTY_LIST;
		}
	}

	public void sendGetTaskUpdates(String employeeNumber) {
		EmployeeAlertContainer cont = employeeMap.get(employeeNumber);
		if (cont != null) {
			cont.syncTasks();
		}
	}

	public void sendLogAction(LogAction logAction) {
		messageChannel.sendLogAction(logAction);
	}

	public List<MessageSessionDetails> getMessageSessionDetails() {
		if (log.isDebugEnabled()) {
			log.debug("Enter getMessageSessionDetails");
		}
		List<MessageSessionDetails> list = alertManager.employee_count();
		
		/*List<MessageSessionDetails> list = messageChannel.getMessageSessionDetails();
		for (MessageSessionDetails msd : list) {
			EmployeeAlertContainer cont = employeeMap.get(msd.getUserId());
			int size = 0;
			if (cont != null) {
				size = cont.getAlerts().size();
			}
			msd.setPendingAlerts(size);
		}*/
		return list;
	}
	
	private class EmployeeAlertContainer {
		private String employeeNumber;
		private int numSessions;
		private SortedSet<Alert> alerts = Collections.synchronizedSortedSet(new TreeSet<Alert>(new AlertIdComparator()));
		private boolean pendingSendAlert = false;
		private boolean pendingVerifyAlert = false;

		public EmployeeAlertContainer(String employeeNumber) {
			this.employeeNumber = employeeNumber;
		}

		public synchronized EmployeeAlertContainer register() {
			// increment sessions
			numSessions++;

			// log it
			if (log.isDebugEnabled()) {
				log.debug("Employee session registered, cont=" + this);
			}

			return this;
		}

		public synchronized EmployeeAlertContainer deregister() {
			// decrement sessions
			numSessions--;

			// log it
			if (log.isDebugEnabled()) {
				log.debug("Employee session deregistered, cont=" + this);
			}

			if (numSessions > 0) {
				return this;
			} else {
				numSessions = 0;
				return null;
			}
		}

		public void addAlert(Alert alert) {
			alerts.add(alert);
		}

		public synchronized void removeAlert(String alertId) {
			for (Iterator<Alert> iter = alerts.iterator(); iter.hasNext();) {
				if (iter.next().getId().equals(alertId)) {
					iter.remove();
				}
			}
		}
		
		public synchronized void removeAllAlerts() {
			alerts.removeAll(alerts);
		}

		public synchronized void syncAlerts() {
			if (pendingSendAlert == false && alerts.size() > 0 && numSessions > 0) {
				pendingSendAlert = true;

				if (log.isDebugEnabled()) {
					log.debug("Sync alerts: scheduling alert notification, cont=" + this);
				}

				// schedule the push to notify of sync
				executor.schedule(new Runnable() {
					public void run() {
						pendingSendAlert = false;
						messageChannel.sendGetAlertUpdates(employeeNumber);
					}
				}, 2000, TimeUnit.MILLISECONDS);

				// verify the push was successful at point in the future
				if (pendingVerifyAlert == false) {
					pendingVerifyAlert = true;
					executor.schedule(new Runnable() {
						public void run() {
							pendingVerifyAlert = false;
							syncAlerts();
						}
					}, RETRY_SYNC_ALERTS_INTERVAL, TimeUnit.MILLISECONDS);
				}
			}
		}

		public synchronized void syncTasks() {

			if (log.isDebugEnabled()) {
				log.debug("Sync tasks fired, cont=" + this);
			}

			ScheduledFuture future = executor.schedule(new Runnable() {
				public void run() {
					messageChannel.sendGetTaskUpdates(employeeNumber);
				}
			}, 2000, TimeUnit.MILLISECONDS);
			
			try {
				future.get(10000, TimeUnit.MILLISECONDS);
				if(future.isDone())
				{
					log.debug("SyncTask is completed successful");
				}
			} catch (Exception e) {
				log.debug("SyncTask interrupted : " + e.getMessage());
				e.printStackTrace();
			}
		}

		public List<Alert> getAlerts() {
			return Collections.unmodifiableList(new ArrayList(alerts));
		}

		@Override
		public String toString() {
			return "[employeeNumber=" + employeeNumber + ",numSessions=" + numSessions + ",numAlerts=" + alerts.size() + "]";
		}
	}

	private class AlertIdComparator implements Comparator<Alert> {
		public int compare(Alert arg0, Alert arg1) {
			// compare in reverse order
			return arg0.getId().compareTo(arg1.getId());
		}
	}
}
