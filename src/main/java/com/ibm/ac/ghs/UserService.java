package com.ibm.ac.ghs;

import java.util.ArrayList;
import java.util.List;


import org.apache.logging.log4j.Logger;
import org.directwebremoting.annotations.RemoteMethod;

import com.ibm.ac.ghs.alerts.RemoteAlertService;
import com.ibm.ac.ghs.logging.LogManager;
import com.ibm.ac.ghs.messaging.MessageManager;
import com.ibm.ac.ghs.persistence.AlertManager;
import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.Alert;
import com.ibm.ac.ghs.tasks.TaskService;

public class UserService {

	private Logger log = org.apache.logging.log4j.LogManager.getLogger(getClass());

	private TaskService taskService;
	private AlertManager alertManager;
	private MessageManager messageManager;
	private LogManager logManager;
	private RemoteAlertService remoteAlertService;

	public UserService(TaskService taskService, AlertManager alertManager) {
		this.taskService = taskService;
		this.alertManager = alertManager;
	}
	@RemoteMethod
	public void setMessageManager(MessageManager messageManager) {
		this.messageManager = messageManager;
	}
	@RemoteMethod
	public void setLogManager(LogManager logManager) {
		this.logManager = logManager;
	}
	@RemoteMethod
	public void setRemoteAlertService(RemoteAlertService remoteAlertService) {
		this.remoteAlertService = remoteAlertService;
	}
	@RemoteMethod
	public boolean alertsExist(String employeeNumber, boolean exist) {
		if (log.isDebugEnabled()) {
			log.debug("alertsExist, empl=" + employeeNumber + ", flag=" + exist);
		}

		// handle the request
		if (exist) {
			remoteAlertService.registerAlert(employeeNumber);
		} else {
			remoteAlertService.clearAlerts(employeeNumber);
		}

		return exist;
	}
	@RemoteMethod
	public List<Alert> getAlerts(String employeeNumber) {
		if (log.isDebugEnabled()) {
			log.debug("getAlerts, empl=" + employeeNumber);
		}

		// handle any exceptions
		try {
			List<Alert> alertList = messageManager.getAlerts(employeeNumber);
			if (log.isDebugEnabled()) {
				log.debug("getAlerts, empl=" + employeeNumber + ", alert-count=" + alertList.size());
			}
			return alertList;
		} catch (Exception e) {
			log.error("Error retrieving alerts, empl=" + employeeNumber, e);
			return new ArrayList<Alert>();
		}
	}
	@RemoteMethod
	public int alertsReceived(String employeeNumber, List<String> alertIdList) {
		if (log.isDebugEnabled()) {
			log.debug("alertsReceived, empl=" + employeeNumber + ", alertIds=" + alertIdList);
		}
		// handle any exceptions
		for (String alertId : alertIdList) {
			internalAlertReceived(employeeNumber, alertId);
		}
		return alertIdList.size();
	}
	@RemoteMethod
	public void alertReceived(String employeeNumber, String alertId) {
		if (log.isDebugEnabled()) {
			log.debug("alertReceived, empl=" + employeeNumber + ", alertId=" + alertId);
		}
		internalAlertReceived(employeeNumber, alertId);
	}

	private void internalAlertReceived(String employeeNumber, String alertId) {
		// remove the alert from the message manager since we've confirmed that
		// its been delivered
		messageManager.removeAlert(employeeNumber, alertId);

		// handle any exceptions
		try {
			// remove the alert from the alert manager (database) since we've
			// confirmed its been delivered
			alertManager.alertReceived(employeeNumber, alertId);
		} catch (Exception e) {
			log.error("Error internalAlertReceived, empl=" + employeeNumber, e);
		}
	}
	@RemoteMethod
	public int acknowledgeTasks(String employeeNumber, List<String> taskIdList) {
		// handle any exceptions
		for (String taskId : taskIdList) {
			acknowledgeTask(employeeNumber, taskId);
		}
		return taskIdList.size();
	}
	@RemoteMethod
	public void acknowledgeTask(String employeeNumber, String taskId) {
		if (log.isDebugEnabled()) {
			log.debug("acknowledgeTask, empl=" + employeeNumber + ", taskId=" + taskId);
		}

		// handle any exceptions
		try {
			taskService.acknowledgeTask(employeeNumber, taskId);
		} catch (Exception e) {
			log.error("Error acknowledging task, empl=" + employeeNumber + ", taskId=" + taskId);
		}
	}
	@RemoteMethod
	public List<AbstractTask> getTasks(String employeeNumber) {
		if (log.isDebugEnabled()) {
			log.debug("getTasks, empl=" + employeeNumber);
		}

		// handle any exceptions
		try {
			return taskService.getTasks(employeeNumber);
		} catch (Exception e) {
			log.error("Error retrieving tasks, empl=" + employeeNumber, e);
			return new ArrayList<AbstractTask>();
		}
	}
	@RemoteMethod
	public void login(String employeeNumber) {
		loginWithOptions(employeeNumber, null);
	}
	@RemoteMethod
	public void loginWithOptions(String employeeNumber, List<String> options) {
		System.out.println("DVP LOGIN: " + employeeNumber);
		if (log.isDebugEnabled()) {
			log.debug("loginWithOptions, empl=" + employeeNumber + ", options=" + options);
		}

		// parse through options
		if (options != null) {
			for (String option : options) {
				if ("ralert".equals(option)) {
					alertsExist(employeeNumber, false);
				}
			}
		}

		// handle any exceptions
		try {
			// login
			taskService.login(employeeNumber);
		} catch (Exception e) {
			log.error("Error logging in, empl=" + employeeNumber, e);
		}
	}
	@RemoteMethod
	public void logoff(String employeeNumber) {
		if (log.isDebugEnabled()) {
			log.debug("logoff, empl=" + employeeNumber);
		}

		// handle any exceptions
		try {
			taskService.logoff(employeeNumber);
		} catch (Exception e) {
			log.error("Error logging off, empl=" + employeeNumber, e);
		}
	}
	@RemoteMethod
	public int startTasks(String employeeNumber, List<String> taskIdList) {
		// handle any exceptions
		for (String taskId : taskIdList) {
			startTask(employeeNumber, taskId);
		}
		return taskIdList.size();
	}
	@RemoteMethod
	public void startTask(String employeeNumber, String taskId) {
		if (log.isDebugEnabled()) {
			log.debug("startTask, empl=" + employeeNumber + ", taskId=" + taskId);
		}

		// handle any exceptions
		try {
			taskService.startTask(employeeNumber, taskId);
		} catch (Exception e) {
			log.error("Error starting task, empl=" + employeeNumber + ", taskId=" + taskId);
		}
	}
	@RemoteMethod
	public int stopTasks(String employeeNumber, List<String> taskIdList) {
		// handle any exceptions
		for (String taskId : taskIdList) {
			stopTask(employeeNumber, taskId);
		}
		return taskIdList.size();
	}
	@RemoteMethod
	public void stopTask(String employeeNumber, String taskId) {
		if (log.isDebugEnabled()) {
			log.debug("stopTask, empl=" + employeeNumber + ", taskId=" + taskId);
		}

		// handle any exceptions
		try {
			taskService.stopTask(employeeNumber, taskId);
		} catch (Exception e) {
			log.error("Error stopping task, empl=" + employeeNumber + ", taskId=" + taskId);
		}
	}
	@RemoteMethod
	public void sendLogs(String employeeNumber, String data, boolean isError) {
		if (log.isDebugEnabled()) {
			log.debug("sendLogs, empl=" + employeeNumber);
		}

		logManager.storeDeviceLog(employeeNumber, data);
	}
	@RemoteMethod
	public void deleteAllAlerts(String employeeNumber) {
		alertManager.deleteAllAlerts(employeeNumber);
	}
	@RemoteMethod
	public void deleteAlertsOlderThan(long seconds) {
		alertManager.deleteAlertsOlderThan(seconds);
	}
}
