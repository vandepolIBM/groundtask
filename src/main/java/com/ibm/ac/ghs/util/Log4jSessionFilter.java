/*
 * Created on Sep 30, 2006
 *  
 */
package com.ibm.ac.ghs.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

/**
 * Servlet filter responsible for putting context information on the NDC/MDC for
 * use by Log4j.
 * @author Jeremy Koch
 */
public class Log4jSessionFilter implements Filter {

	private Logger log = LogManager.getLogger("requests");

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 *      javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		String contextInformation = "";

		// only support http servlet requests
		boolean httpReq = request instanceof HttpServletRequest;
		if (httpReq) {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			contextInformation += httpRequest.getSession().getId();
		}

		// push information
		ThreadContext.push(contextInformation);

		// log it
		if (httpReq) {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			if (log.isTraceEnabled()) {
				String reqUrl = httpRequest.getRequestURL().toString();
				if (reqUrl.indexOf(".dwr") != -1) {
					// dont log reverse ajax polls
					if (reqUrl.indexOf("ReverseAjax") == -1) {
						String type = StringUtils.substringAfterLast(reqUrl, "/");
						String empl = StringUtils.substringAfterLast(httpRequest.getHeader("Referer"), "?");
						log.trace("DWR: type=" + type + ", " + empl);
					}
				} else {
					log.trace(httpRequest.getRequestURL());
				}
			}
		}

		try {
			chain.doFilter(request, response);
		} finally {
			// clean up NDC
			ThreadContext.removeStack();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
	}
}
