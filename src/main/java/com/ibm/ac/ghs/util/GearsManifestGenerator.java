package com.ibm.ac.ghs.util;

/**
 * Generates a gears manifest file from a specified directory
 * 
 * @author M.Kurabi
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;

public class GearsManifestGenerator {

	private static final String fileImport = "<%@ taglib prefix=\"c\" uri=\"http://java.sun.com/jsp/jstl/core\" %>\n";
	private static final String fileHeader = "{\n\t\"betaManifestVersion\": 1,\n\t\"version\": \"%s\",\n\t\"entries\":\n\t[\n";
	private static final String fileFooter = "\t\t{ \"url\": \"<c:url value='/dwr/interface/TaskCreator.js' />\" },\n\t\t{ \"url\": \"<c:url value='/dwr/engine.js' />\" },\n\t\t{ \"url\": \"<c:url value='main.html' />\", \"ignoreQuery\": true }\n\t]\n}";
	private static final String PREFIX = "\t\t{ \"url\": \"<c:url value='";
	private static final String SUFFIX = "' />\" },";
	private static final String[] exclusions = {".", "index.jsp", "thumbs.db", "prototype.js", "https_cert_hack.js","WEB-INF", "META-INF"}; // Exclusions are not case sensative
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Check arguments
		if (args.length != 3) {
			System.out.println("Usage: java GearsManifestGenerator <DIRECTORY_PATH> <CURRENT_VERSION> <OUTPUT_FILE>");
			System.out.println("You might want to use: '../ground-task/src/main/webapp' as directory path.");
			return;
		}
		File dir = new File(args[0]);
		if (!dir.isDirectory()) {
			System.out.println(args[0] + " is not a directory.");
			return;
		}
		// Filter files that start with "." or "WEB-INF" (look at exclusions above!)
	    FilenameFilter filter = new FilenameFilter() {
	        public boolean accept(File dir, String name) {
	        	for (String exclusion : exclusions) {
	        		if (name.toLowerCase().startsWith(exclusion.toLowerCase())) {
	        			return false;
	        		}
	        	}
	        	return true;
	        }
	    };
		
	    // List contents of directory
	    String file = fileImport + fileHeader.format(fileHeader, args[1]) + listDirectoryContents(dir, filter, "") + fileFooter;

	    // Write file
	    try{
	        // Create file 
	        BufferedWriter out = new BufferedWriter( new FileWriter(args[2]) );
	        out.write(file);
	        out.close();
	    } catch (Exception e){//Catch exception if any
	    	System.err.println("Error: " + e.getMessage());
	    }
	    
	}
	
	/**
	 * Lists the contents of the directory and sends to print
	 * @param dir
	 * @param filter
	 * @param path
	 */
	private static String listDirectoryContents(File dir, FilenameFilter filter, String path) {
		String returnString = "";
		for (File file : dir.listFiles(filter)) {
			if (file.isDirectory()) {
				returnString += listDirectoryContents(file, filter, path + "/" + file.getName());
			} else {
				returnString += print(file, path, PREFIX, SUFFIX);
			}
		}
		return returnString;
	}
	
	/**
	 * Prints the prefix + path + file name + prefix to System.out
	 * @param file
	 * @param path
	 * @param prefix
	 * @param suffix
	 */
	private static String print(File file, String path, String prefix, String suffix) {
		return prefix + path + "/" + file.getName() + suffix + "\n";
	}

}
