package com.ibm.ac.ghs.util;

import java.io.IOException;
import java.util.Collections;
import java.util.Properties;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.Resource;

/**
 * A property resource configurer that chooses the property file at runtime
 * based on the runtime environment.
 * <p>
 * Used for choosing properties files based on a specified system property,
 * allowing for movement of the same application between multiple runtime
 * environments without rebuilding.
 * <p>
 * The property replacement semantics of this implementation are identical to
 * PropertyPlaceholderConfigurer, from which this class inherits. <code>
 * <pre>
 * &lt;bean id=&quot;propertyConfigurator&quot; class=RuntimeEnvironmentPropertiesConfigurer&quot;&gt;
 *        &lt;property name=&quot;propertyLocation&quot; value=&quot;/WEB-INF/runtime-properties/&quot; /&gt;
 *        &lt;property name=&quot;environments&quot;&gt;
 *        &lt;set&gt;
 *            &lt;value&gt;production&lt;/value&gt;
 *            &lt;value&gt;staging&lt;/value&gt;
 *            &lt;value&gt;integration&lt;/value&gt;
 *            &lt;value&gt;development&lt;/value&gt;
 *        &lt;/set&gt;
 *        &lt;/property&gt;
 *        &lt;property name=&quot;defaultEnvironment&quot; value=&quot;development&quot;/&gt;
 *        &lt;property name=&quot;environmentKey&quot; value=&quot;runtime.environment&quot;/&gt;
 * &lt;/bean&gt;
 * </code>
 * </pre>
 * 
 * The keys of the environment specific properties files are compared to ensure
 * that each property file defines the complete set of keys, in order to avoid
 * environment-specific failures.
 * 
 */
public class RuntimeEnvironmentPropertiesConfigurer extends PropertyPlaceholderConfigurer implements InitializingBean {

	private Logger log = LogManager.getLogger(getClass());

	private String defaultEnvironment;

	private String environmentKey;

	private Set<String> environments = Collections.emptySet();

	private Resource propertyLocation;

	public void afterPropertiesSet() throws IOException {
		if (!environments.contains(defaultEnvironment)) {
			throw new AssertionError("Default environment '" + defaultEnvironment + "' not listed in environment list");
		}

		String environment = determineEnvironment();
		Resource propertiesLocation = createPropertiesResource(environment);
		setLocation(propertiesLocation);
		validateProperties();
	}

	private boolean compareProperties(Resource resource1, Resource resource2) throws IOException {
		Properties props1 = new Properties();
		props1.load(resource1.getInputStream());

		Properties props2 = new Properties();
		props2.load(resource2.getInputStream());

		Set<Object> outerKeys = props1.keySet();

		boolean missingKeys = false;
		for (Object keyObj : outerKeys) {
			String key = (String) keyObj;
			if (!props2.containsKey(key)) {
				missingKeys = true;
				log.error("Property file mismatch: " + resource1.toString() + " contains key '" + key + "', missing from "
						+ resource2.toString());
			}
		}

		return missingKeys;
	}

	private Resource createPropertiesResource(String environment) throws IOException {
		String fileName = environment.toString().toLowerCase() + ".properties";
		return propertyLocation.createRelative(fileName);
	}

	private String determineEnvironment() {
		String environment = System.getProperty(environmentKey);
		if (environment == null) {
			log.warn("Property '" + environmentKey + "' not found, using default environment '" + defaultEnvironment + "'");
			return defaultEnvironment;
		}
		
		log.debug("Current environment is " + environment.toLowerCase());
		return environment.toLowerCase();
	}

	/**
	 * Sets the system environment key from which to read the runtime environment
	 * name
	 */
	public void setEnvironmentKey(String environmentKey) {
		this.environmentKey = environmentKey;
	}

	/**
	 * Sets the allowed list of runtime environments
	 */
	public void setEnvironments(Set<String> environments) {
		this.environments = environments;
	}

	/**
	 * Sets the directory from which to read environment-specific properties
	 * files; note that it must end with a '/'
	 */
	public void setPropertyLocation(Resource propertyLocation) {
		this.propertyLocation = propertyLocation;
	}

	private void validateProperties() throws IOException {
		boolean missingKeys = false;
		for (String envOuter : environments) {
			for (String envInner : environments) {
				if (!envOuter.equals(envInner)) {
					Resource resource1 = createPropertiesResource(envOuter);
					Resource resource2 = createPropertiesResource(envInner);
					missingKeys |= compareProperties(resource1, resource2);
				}
			}
		}

		if (missingKeys) {
			throw new AssertionError("Missing runtime properties keys (log entries above have details)");
		}
	}

	/**
	 * Sets the default environment name, used when the runtime environment
	 * cannot be determined.
	 */
	public void setDefaultEnvironment(String defaultEnvironment) {
		this.defaultEnvironment = defaultEnvironment;
	}
}
