package com.ibm.ac.ghs.logging;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.appender.FileAppender;


import com.ibm.ac.ghs.persistence.LogAction;
import com.ibm.ac.ghs.persistence.NodeNotificationStrategy;
import com.ibm.ac.ghs.persistence.LogAction.LogActionType;

public class LogManagerImpl implements com.ibm.ac.ghs.logging.LogManager {

	private final static int MAX_DEVICE_LOG_CACHE = 10;

	private List<DeviceLog> deviceLogList = new ArrayList<DeviceLog>();

	private NodeNotificationStrategy notificationStrategy;
	

	public void setNotificationStrategy(NodeNotificationStrategy notificationStrategy) {
		this.notificationStrategy = notificationStrategy;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.logging.LogManager#getServerLogZipFileData()
	 */
	@SuppressWarnings("unchecked")
	public byte[] getServerLogZipFileData() throws IOException {
		// create the byte array output stream
		ByteArrayOutputStream zipData = new ByteArrayOutputStream();
		ZipOutputStream zipOS = new ZipOutputStream(zipData);

		Iterator<Appender> appenders = ((org.apache.logging.log4j.core.Logger)org.apache.logging.log4j.LogManager.getRootLogger()).getAppenders().values().iterator();
		while (appenders.hasNext()) {
			Appender appender = appenders.next();
			if (appender instanceof FileAppender) {
				// create the zip entry
				File file = new File(((FileAppender) appender).getFileName());
				zipOS.putNextEntry(new ZipEntry(file.getName()));
				InputStream in = new FileInputStream(file);
				byte[] buf = new byte[4096];
				while (true) {
					int n = in.read(buf);
					if (n < 0)
						break;
					zipOS.write(buf, 0, n);
				}
				zipOS.closeEntry();
			}
		}

		// close off the zipfile
		zipOS.finish();
		return zipData.toByteArray();
	}

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.logging.LogManager#storeDeviceLog(java.lang.String,
	 *      java.lang.String)
	 */
	public void storeDeviceLog(String employeeNumber, String log) {
		if (deviceLogList.size() >= MAX_DEVICE_LOG_CACHE) {
			deviceLogList.remove(0);
		}
		DeviceLog deviceLog = new DeviceLog(employeeNumber, log);
		deviceLogList.remove(deviceLog);
		deviceLogList.add(deviceLog);
	}

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.logging.LogManager#getDeviceLog(java.lang.String)
	 */
	public DeviceLog getDeviceLog(String employeeNumber) {
		for (DeviceLog deviceLog : deviceLogList) {
			if (deviceLog.getEmployeeNumber().equals(employeeNumber)) {
				return deviceLog;
			}
		}
		return null;
	}

	public List<DeviceLog> getAllDeviceLogs() {
		return deviceLogList;
	}
	
	public void requestDeviceLogs(String employeeNumber, int numRows) {
		if (notificationStrategy != null) {
			notificationStrategy.notifyLogging(LogAction.createLogAction(employeeNumber, LogActionType.RETRIEVE, numRows));
		}
	}

	public void deleteDeviceLogs(String employeeNumber) {
		if (notificationStrategy != null) {
			notificationStrategy.notifyLogging(LogAction.createLogAction(employeeNumber, LogActionType.DELETE_ALL));
		}
	}	
}
