package com.ibm.ac.ghs.logging;

import java.io.IOException;
import java.util.List;

public interface LogManager {

	public byte[] getServerLogZipFileData() throws IOException;

	public void storeDeviceLog(String employeeNumber, String log);

	public DeviceLog getDeviceLog(String employeeNumber);

	public List<DeviceLog> getAllDeviceLogs();
		
	public void requestDeviceLogs(String employeeNumber, int numRows);
	
	public void deleteDeviceLogs(String employeeNumber);	
}