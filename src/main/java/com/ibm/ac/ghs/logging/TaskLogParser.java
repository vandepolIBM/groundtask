package com.ibm.ac.ghs.logging;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;

import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.Alert;

public class TaskLogParser {

	private TimeZone timezone = TimeZone.getTimeZone("Canada/Eastern");

	private Map<DebugTask, List<DebugAlert>> taskAlertMap = new HashMap<DebugTask, List<DebugAlert>>();
	private Map<String, DebugTask> taskMap = new HashMap<String, DebugTask>();
	private Map<String, DebugAlert> alertMap = new HashMap<String, DebugAlert>();
	private List<String> getTasksResponseList = new ArrayList<String>();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String employeeNumber = JOptionPane.showInputDialog(null, "GHS Log Parser", "Enter employee number", JOptionPane.QUESTION_MESSAGE);
		if (employeeNumber.startsWith("AC") || employeeNumber.startsWith("ac")) {
			employeeNumber = employeeNumber.substring(2);
		}

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setMultiSelectionEnabled(true);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		if (fileChooser.showOpenDialog(null) == fileChooser.APPROVE_OPTION) {
			List<File> files = (List) Arrays.asList(fileChooser.getSelectedFiles());
			try {
				new TaskLogParser().parse(employeeNumber, files, System.out);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public TaskLogParser() {
	}

	public TaskLogParser(TimeZone timezone) {
		this.timezone = timezone;
	}

	public void parse(String employeeNumber, List<File> files, OutputStream os) throws Exception {
		PrintStream ps = new PrintStream(os);

		String receivedAlertDebugLine = "Received alert broadcast, empl=";
		String getTaskResponseDebugLine = "GetTasksResponse: employee=";
		String alertsReceivedDebugLine = "alertsReceived, empl=";
		String acknowledgedDebugLine = "acknowledgeTask, empl=";
		String startTaskDebugLine = "startTask, empl=";
		String stopTaskDebugLine = "stopTask, empl=";

		if (employeeNumber != null) {
			employeeNumber = StringUtils.leftPad(employeeNumber, 6, '0');
			receivedAlertDebugLine += employeeNumber;
			getTaskResponseDebugLine += employeeNumber;
			alertsReceivedDebugLine += employeeNumber;
			acknowledgedDebugLine += employeeNumber;
			startTaskDebugLine += employeeNumber;
			stopTaskDebugLine += employeeNumber;
		}

		for (File file : files) {
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new FileReader(file));
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
			String line = reader.readLine();
			while (line != null) {
				// check for recieved alert broadcast debug line
				if (line.indexOf(receivedAlertDebugLine) != -1) {
					Alert alert = parseAlert(line);
					if (!alert.getType().equals("m")) {
						if (!alertMap.containsKey(alert.getId())) {
							alertMap.put(alert.getId(), (DebugAlert) alert);
						}

						alert.setTask(parseTask(line));
						if (!taskAlertMap.containsKey(alert.getTask())) {
							taskMap.put(alert.getTask().getId(), (DebugTask) alert.getTask());
							taskAlertMap.put((DebugTask) alert.getTask(), new ArrayList<DebugAlert>());
						}

						if (!taskAlertMap.get(alert.getTask()).contains(alert)) {
							taskAlertMap.get(alert.getTask()).add((DebugAlert) alert);
						}
					}
				} else if (line.indexOf(getTaskResponseDebugLine) != -1) {
					if (line.indexOf("task=") != -1) {
						DebugTask task = parseTask(line);
						getTasksResponseList.add(getLocaltime(line) + " " + task.getType() + ", " + task.getStartTime() + "-" + task.getEndTime()
								+ "  (" + task.getId() + ")"
								+ (task.getAcknowledgedTimestamp() != null ? " [ack=" + task.getAcknowledgedTimestamp() + "]" : "")
								+ (task.getStartedTimestamp() != null ? " [start=" + task.getStartedTimestamp() + "]" : "")
								+ (task.getCompletedTimestamp() != null ? " [complete=" + task.getCompletedTimestamp() + "]" : ""));
						if (!taskAlertMap.containsKey(task)) {
							taskMap.put(task.getId(), task);
							taskAlertMap.put(task, new ArrayList<DebugAlert>());
						}
					} else {
						getTasksResponseList.add("\n" + getLocaltime(line) + " "
								+ line.substring(line.indexOf(getTaskResponseDebugLine) + getTaskResponseDebugLine.length() + 1));
					}
				} else if (line.indexOf(alertsReceivedDebugLine) != -1) {
					LogLineDetail<String[]> lld = getReceivedAlertIds(line);
					String[] ids = lld.getDetail();
					for (int j = 0; j < ids.length; j++) {
						DebugAlert alert = alertMap.get(ids[j]);
						if (alert != null) {
							alert.setDeviceReceivedTimestamp(lld.getTimestamp());
						}
					}
				} else if (line.indexOf(acknowledgedDebugLine) != -1) {
					LogLineDetail<String> lld = getTaskAcknowledgment(line);
					DebugTask task = taskMap.get(lld.getDetail());
					if (task != null) {
						taskMap.get(lld.getDetail()).setAcknowledgedTimestamp(lld.getTimestamp());
					}
				} else if (line.indexOf(startTaskDebugLine) != -1) {
					LogLineDetail<String> lld = getTaskAcknowledgment(line);
					DebugTask task = taskMap.get(lld.getDetail());
					if (task != null) {
						task.setStartedTimestamp(lld.getTimestamp());
					}
				} else if (line.indexOf(stopTaskDebugLine) != -1) {
					LogLineDetail<String> lld = getTaskAcknowledgment(line);
					DebugTask task = taskMap.get(lld.getDetail());
					if (task != null) {
						task.setCompletedTimestamp(lld.getTimestamp());
					}
				}

				line = reader.readLine();
			}
		}

		ps.println("----------------------------------------");
		ps.println("GetTask Report for Employee # " + employeeNumber);
		ps.println("----------------------------------------");

		for (String tr : getTasksResponseList) {
			ps.println(tr);
		}

		ps.println();
		ps.println("----------------------------------------");
		ps.println("Alert Report for Employee # " + employeeNumber);
		ps.println("----------------------------------------");
		ps.println();

		List<DebugTask> keySet = new ArrayList<DebugTask>(taskAlertMap.keySet());
		Collections.sort(keySet, new TaskTimeComparator());

		for (DebugTask task : keySet) {
			// output the task header
			String flt = StringUtils.isBlank(task.getFlightNum()) ? "NO-FLT" : task.getFlightNum();
			ps.println(flt + ":" + task.getType() + ", " + task.getStartTime() + "-" + task.getEndTime() + "  (taskId=" + task.getId() + ")"
					+ (task.getAcknowledgedTimestamp() != null ? " [ack=" + task.getAcknowledgedTimestamp() + "]" : "")
					+ (task.getStartedTimestamp() != null ? " [start=" + task.getStartedTimestamp() + "]" : "")
					+ (task.getCompletedTimestamp() != null ? " [complete=" + task.getCompletedTimestamp() + "]" : ""));

			// sort the alert list by id
			Collections.sort(taskAlertMap.get(task), new AlertIdComparator());

			// output each alert
			for (DebugAlert alert : taskAlertMap.get(task)) {
				ps.println(alert.getExternalTimestamp() + " " + alert);
			}
			ps.println();
		}

		/*
		 * Set<DebugTask> currentTaskList = new HashSet<DebugTask>();
		 * List<DebugAlert> allAlerts = new
		 * ArrayList<DebugAlert>(alertMap.values()); Collections.sort(allAlerts,
		 * new AlertIdComparator()); for (DebugAlert alert : allAlerts) {
		 * boolean printTaskList = false;
		 * 
		 * if (alert.getType().equals("n")) { printTaskList = true;
		 * currentTaskList.add((DebugTask) alert.getTask()); } else if
		 * (alert.getType().equals("d")) { printTaskList = true;
		 * currentTaskList.remove((DebugTask) alert.getTask()); } else if
		 * (alert.getType().equals("e") &&
		 * currentTaskList.contains(alert.getTask())) {
		 * currentTaskList.add((DebugTask) alert.getTask()); }
		 * 
		 * if (printTaskList) { List<DebugTask> tmpList = new
		 * ArrayList<DebugTask>(currentTaskList); Collections.sort(tmpList, new
		 * TaskTimeComparator()); ps.println("NEW/DELETE, time=" +
		 * alert.getDeviceReceivedTimestamp() + ", " + alert); for (DebugTask
		 * task : tmpList) { ps.println(task.getStartTime() + "-" +
		 * task.getEndTime() + "\t" + (task.getFlightNum() != null ?
		 * task.getFlightNum() : "") + "\t" + task.getType()); } ps.println(); }
		 * }
		 */
	}

	private Alert parseAlert(String line) {
		int alertIndex = line.indexOf("Alert[");
		int taskIndex = line.indexOf(",task=");
		
		int endTaskIndex = line.indexOf("]", taskIndex) + 1;
		//category will be last property for Task type printed in the log, so assume that is the end of toString 
		//representation of the Task else consider it as DirectMessage(no category for it)
		int catagoryIndex = line.indexOf("category=");
		if(catagoryIndex != -1)
			endTaskIndex = line.indexOf("]", catagoryIndex) + 1;
		
		String sub = line.substring(alertIndex + 6, taskIndex) + line.substring(endTaskIndex, line.indexOf("]", endTaskIndex));
//		System.out.println("sub-->"+sub);
		DebugAlert alert = new DebugAlert();
		alert.setId(getPropertyValue(sub, "id"));
		alert.setType(getPropertyValue(sub, "type"));
		alert.setMessage(new Alert.Message());
		try {
			alert.setSeverity(Integer.parseInt(getPropertyValue(sub, "severity")));
		} catch (Exception e) {
		}
		alert.setExternalTimestamp(getLocaltime(line));
		return alert;
	}

	public DebugTask parseTask(String line) 
	{
		//category will be last property for Task type printed in the log, so assume, that is the end of toString 
		//representation of the Task
		String sub = StringUtils.substringBetween(line, "task=", "category=");
		sub = StringUtils.substringAfter(sub, "[");
		if (sub == null) {
			sub = StringUtils.substringAfter(line, "[");
		}
		DebugTask task = new DebugTask();
		task.setId(getPropertyValue(sub, "id"));
		task.setType(getPropertyValue(sub, "type"));
		task.setStartTime(getPropertyValue(sub, "startTime"));
		task.setEndTime(getPropertyValue(sub, "endTime"));
		task.setFlightNum(getPropertyValue(sub, "flightNum"));
		task.setComments(getPropertyValue(sub, "comments"));
		task.setOpsRemarksArr(getPropertyValue(sub, "opsRemarksArr"));
		task.setOpsRemarksDep(getPropertyValue(sub, "opsRemarksDep"));
		return task;
	}

	private String getPropertyValue(String line, String key) {
		if (line == null) {
			return null;
		}

		String lookup = key + "=";
		if (line.indexOf(lookup) != 0) {
			lookup = "," + lookup;
		}

		String val = StringUtils.substringBetween(line, lookup, ",");
		if (val == null) {
			val = StringUtils.substringAfter(line, lookup);
		}

		if (val.equals("null")) {
			return null;
		} else {
			return val;
		}
	}

	private LogLineDetail<String[]> getReceivedAlertIds(String line) {
		String sub = StringUtils.substringBetween(line, "alertIds=[", "]");
		String[] raw = StringUtils.split(sub, ",");
		String[] ids = new String[raw.length];
		for (int i = 0; i < raw.length; i++) {
			ids[i] = StringUtils.trim(raw[i]);
		}

		LogLineDetail<String[]> lld = new LogLineDetail<String[]>();
		lld.setDetail(ids);
		lld.setTimestamp(getLocaltime(line));
		return lld;
	}

	private LogLineDetail<String> getTaskAcknowledgment(String line) {
		LogLineDetail<String> lld = new LogLineDetail<String>();
		lld.setDetail(StringUtils.substringAfter(line, "taskId="));
		lld.setTimestamp(getLocaltime(line));
		return lld;
	}

	private class TaskTimeComparator implements Comparator<AbstractTask> {
		public int compare(AbstractTask o1, AbstractTask o2) {
			return o1.getStartTime().compareTo(o2.getStartTime());
		}
	}

	private class AlertIdComparator implements Comparator<Alert> {
		public int compare(Alert o1, Alert o2) {
			return o1.getId().compareTo(o2.getId());
		}
	}

	private String getLocaltime(String line) {
		try {
			Date date = DateUtils.parseDate(StringUtils.substring(line, 0, 23) + "-0000", new String[] { "yyyy-MM-dd HH:mm:ss,SSSZ" });
			return DateFormatUtils.format(date, "MM-dd-yyyy HH:mm:ss,SSS", timezone);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	private class DebugAlert extends Alert {
		private String externalTimestamp;
		private String deviceReceivedTimestamp;

		public void setDeviceReceivedTimestamp(String received) {
			this.deviceReceivedTimestamp = received;
		}

		public String getDeviceReceivedTimestamp() {
			return deviceReceivedTimestamp;
		}

		public void setExternalTimestamp(String receivedTime) {
			this.externalTimestamp = receivedTime;
		}

		public String getExternalTimestamp() {
			return externalTimestamp;
		}

		public long getAlertToDeviceRecievedDeltaMilli() {
			try {
				Date d1 = null, d2 = null;
				if (externalTimestamp != null) {
					d1 = DateUtils.parseDate(externalTimestamp, new String[] { "yyyy-MM-dd HH:mm:ss,SSS" });
				}
				if (deviceReceivedTimestamp != null) {
					d2 = DateUtils.parseDate(deviceReceivedTimestamp, new String[] { "yyyy-MM-dd HH:mm:ss,SSS" });
				}
				if (d1 != null && d2 != null) {
					return d2.getTime() - d1.getTime();
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return -1;
		}

		@Override
		public int hashCode() {
			return getId().hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			return getId().equals(((DebugAlert) obj).getId());
		}

		@Override
		public String toString() {
			DebugTask task = (DebugTask) getTask();
			return "alertId=" + getId() + ", type=" + getType() + ", taskTime(local)=" + task.getStartTime() + "-"
					+ ((DebugTask) getTask()).getEndTime() + ", silent=" + (getSeverity() == 0)
					+ (!StringUtils.isBlank(task.getComments()) ? ", comments=" + task.getComments() : "")
					+ (getDeviceReceivedTimestamp() != null ? " [dev_rcv=" + getDeviceReceivedTimestamp() + ", diff=" + getAlertToDeviceRecievedDeltaMilli() / 1000 / 60 +"m" + getAlertToDeviceRecievedDeltaMilli() / 1000 % 60 + "s]" : "");
		}
	}

	private class DebugTask extends AbstractTask {

		private String startedTimestamp;
		private String completedTimestamp;
		private String acknowledgedTimestamp;

		public String getStartedTimestamp() {
			return startedTimestamp;
		}

		public void setStartedTimestamp(String startedTimestamp) {
			this.startedTimestamp = startedTimestamp;
		}

		public String getCompletedTimestamp() {
			return completedTimestamp;
		}

		public void setCompletedTimestamp(String completedTimestamp) {
			this.completedTimestamp = completedTimestamp;
		}

		public String getAcknowledgedTimestamp() {
			return acknowledgedTimestamp;
		}

		public void setAcknowledgedTimestamp(String acknowledgedTimestamp) {
			this.acknowledgedTimestamp = acknowledgedTimestamp;
		}

		@Override
		public int hashCode() {
			return getId().hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			return getId().equals(((DebugTask) obj).getId());
		}
	}

	private class LogLineDetail<T> {
		private String timestamp;
		private T detail;

		public String getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(String timestamp) {
			this.timestamp = timestamp;
		}

		public T getDetail() {
			return detail;
		}

		public void setDetail(T detail) {
			this.detail = detail;
		}
	}
}
