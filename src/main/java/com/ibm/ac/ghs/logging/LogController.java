package com.ibm.ac.ghs.logging;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.appender.FileAppender;
import org.apache.logging.log4j.core.appender.RollingFileAppender;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ibm.ac.ghs.messaging.MessageManager;

@Controller
public class LogController {

	// set the default display size to 20K
	private long DEFAULT_DISPLAY_SIZE = 20;

	private static String DEFAULT_TIMEZONE = "EST";
	private static Map<String, TimeZone> timezoneMap = new HashMap<String, TimeZone>();
	static {
		timezoneMap.put("PST", TimeZone.getTimeZone("Canada/Pacific"));
		timezoneMap.put("MST", TimeZone.getTimeZone("Canada/Mountain"));
		timezoneMap.put("CST", TimeZone.getTimeZone("Canada/Central"));
		timezoneMap.put("EST", TimeZone.getTimeZone("Canada/Eastern"));
		timezoneMap.put("NST", TimeZone.getTimeZone("Canada/Newfoundland"));
		timezoneMap.put("AST", TimeZone.getTimeZone("Canada/Atlantic"));
	}

	private Logger log = org.apache.logging.log4j.LogManager.getLogger(getClass());

	private LogManager logManager;

	private MessageManager messageManager;

	@Autowired
	public void setLogManager(LogManager logManager) {
		this.logManager = logManager;
	}

	@Autowired
	public void setMessageManager(MessageManager messageManager) {
		this.messageManager = messageManager;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET, value = "/logging")
	public String menu(HttpServletRequest req, HttpServletResponse resp, ModelMap map) {
		return "admin/log_menu";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/logging/download")
	public void downloadServerLog(HttpServletRequest req, HttpServletResponse resp) {

		// setup the headers, the following fix, when in secure mode (HTTPS),
		// is there to fix an IE bug when streaming PDF/DOC/etc over HTTPS
		// see: http://support.microsoft.com/?id=316431
		if (req.isSecure()) {
			resp.setHeader("Cache-Control", "private");
			resp.setHeader("Pragma", "expires");
		}

		// set the response headers
		resp.setHeader("Content-Disposition", "inline; filename=\"" + "logs.zip" + "\"");
		resp.setDateHeader("Expires", 1L);
		resp.setContentType("application/zip");

		try {
			// write it to the output stream
			FileCopyUtils.copy(logManager.getServerLogZipFileData(), resp.getOutputStream());
			return;
		} catch (Exception e) {
			log.error("Error reading log file.", e);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/logging/display")
	public String displayServerLog() {
		return "admin/output";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/logging/display/search")
	public void regex(HttpServletRequest req, HttpServletResponse resp, @RequestParam(required = false) String name,
			@RequestParam(required = false) Long size, @RequestParam(required = false) String regex) {

		// set the response headers
		resp.setDateHeader("Expires", 1L);
		resp.setContentType("text/plain");

		try {
			if (size == null) {
				size = DEFAULT_DISPLAY_SIZE;
			}

			// if not supplied an appender name, just use first file appender
			List<File> files = findFileAppender(name, true);
			if (files != null) {
				if (StringUtils.isBlank(regex)) {
					regex = ".*";
				}
				regexToOutputStream(files, resp.getWriter(), regex, size * 1000);
			} else {
				resp.getWriter().println("Cannot display logfile.");
			}

		} catch (Exception e) {
			try {
				resp.getWriter().println("Error retrieving log.");
				e.printStackTrace(resp.getWriter());
			} catch (Exception e2) {
			}
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/logging/display/task_log")
	public void displayTaskLogLog(HttpServletRequest req, HttpServletResponse resp,
			@RequestParam(required = false) String employee, @RequestParam(required = false) String timezone,
			@RequestParam(required = false) String name) {

		// set the response headers
		resp.setDateHeader("Expires", 1L);
		resp.setContentType("text/plain");

		try {
			// if not supplied an appender name, just use first file appender
			List<File> files = findFileAppender(name, true);
			if (files != null) {
				if (!StringUtils.isBlank(employee)) {
					TimeZone tz = timezoneMap.get(DEFAULT_TIMEZONE);
					if (!StringUtils.isBlank(timezone) && timezoneMap.containsKey(timezone)) {
						tz = timezoneMap.get(timezone);
					}
					new TaskLogParser(tz).parse(employee, files, resp.getOutputStream());
				} else {
					resp.getWriter().println("No Employee Specified.");
				}
			} else {
				resp.getWriter().println("Cannot display logfile.");
			}

		} catch (Exception e) {
			try {
				resp.getWriter().println("Error retrieving log.");
				e.printStackTrace(resp.getWriter());
			} catch (Exception e2) {
			}
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/logging/display/device")
	public ModelAndView displayDeviceLog(HttpServletRequest req, HttpServletResponse resp, @RequestParam String empl) {

		// set the response headers
		resp.setDateHeader("Expires", 1L);
		resp.setContentType("text/plain");

		try {
			return new ModelAndView("admin/device_log", "deviceLog", logManager.getDeviceLog(empl));
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private List<File> findFileAppender(String name, boolean includePrevious) {
		if (name == null) {
			Iterator<Appender> appenders = ((org.apache.logging.log4j.core.Logger) org.apache.logging.log4j.LogManager
					.getRootLogger()).getAppenders().values().iterator();
			while (appenders.hasNext()) {
				Appender appender = appenders.next();
				if (appender instanceof RollingFileAppender || appender.getName().equals("LogToRollingFile")) {
					String filename = ((RollingFileAppender) appender).getFileName();
					if (!includePrevious) {
						return Arrays.asList(new File(filename));

					} else {
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.DATE, -1);
						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
						String today = dateFormat.format(new Date());
						String yesterday = dateFormat.format(cal.getTime());
						cal.add(Calendar.DATE, -7);

						ArrayList<File> files = new ArrayList<File>();
						File[] allLogFiles = (new File(filename)).getParentFile().listFiles();
						for (int i = 0; i < allLogFiles.length; i++) {
							System.out.println("LOGGING: comparing " + allLogFiles[i].getName() + " to "  + today  + " and : " + yesterday);
							if (allLogFiles[i].getName().startsWith(today)
									|| allLogFiles[i].getName().startsWith(yesterday)) {
								System.out.println("LOGGING: ADDING: " + allLogFiles[i].getAbsolutePath());
								files.add(allLogFiles[i]);
							}
							else if (allLogFiles[i].getName().startsWith("groundtask")){
								System.out.println("LOGGING: handing file: " + allLogFiles[i].getName());
								try {
									Path file = allLogFiles[i].toPath();
									BasicFileAttributes attrs = Files.readAttributes(file, BasicFileAttributes.class);
									FileTime lastModifiedTime = attrs.lastModifiedTime();
									
									LocalDate twodaysago = LocalDate.now().minusDays(2L);
									System.out.println("LOGGING: handing file: " + allLogFiles[i].getName() + " checking if last access time : " + lastModifiedTime.toInstant().toString()  + " is older than 2 days : " + twodaysago.atStartOfDay(ZoneOffset.UTC).toInstant().toString());
									// miliseconds = dtNow.atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli());
									if (lastModifiedTime.toInstant().isBefore(twodaysago.atStartOfDay(ZoneOffset.UTC).toInstant())){
										//Rename so we ignore in future
										FileTime creationTime = attrs.creationTime();
										LocalDate creationDateTime = creationTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
										String creationDateStr = creationDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
										File newFile = new File(allLogFiles[i].getParent() + File.separator + creationDateStr + "-backup-"+allLogFiles[i].getName());
										System.out.println("LOGGING: renameing " + allLogFiles[i].getAbsolutePath() + " to " + newFile.getAbsolutePath() + " to avoid reading again (eventually delete)");
										allLogFiles[i].renameTo(newFile);
									} else {
										System.out.println("LOGGING: ADDING: " + allLogFiles[i].getAbsolutePath());
										files.add(allLogFiles[i]);
									}
								} catch  (IOException e){
									e.printStackTrace();
									System.out.println("LOGGING: exception: " + e.getMessage());
								}
							}
							else {
								try {
									Date filedate = dateFormat.parse(allLogFiles[i].getName().substring(0, 10));
									System.out.println("DVP: checking if log file is old enough to delete: : "
											+ allLogFiles[i].getAbsolutePath() + " date of log file: "
											+ allLogFiles[i].getName().substring(0, 10)
											+ " date of cutoff (1 week ago): " + cal.getTime());
									if (filedate.before(cal.getTime())) {
										System.out.println("LOGGING: Deleting Log file: " + allLogFiles[i].getAbsolutePath() + " because it's over 1 week old");
										allLogFiles[i].delete();
									}
								} catch (Exception e) {
									System.err.println("LOGGING: Error checking if log file can be deleted: " + allLogFiles[i].getAbsolutePath());
								}
							}
						}
						return files;
					}
				}
			}
		} else {
			Appender appender = ((org.apache.logging.log4j.core.Logger) org.apache.logging.log4j.LogManager
					.getRootLogger()).getAppenders().get(name);
			if (appender instanceof RollingFileAppender) {
				return Arrays.asList(new File(((RollingFileAppender) appender).getFileName()));
			}
		}
		return null;
	}

	private void regexToOutputStream(List<File> files, PrintWriter writer, String regex, long max) throws Exception {
		try {
			Pattern pattern = Pattern.compile(regex);
			LinkedList<String> list = new LinkedList<String>();
			long total = 0;

			for (File file : files) {

				FileInputStream in = null;
				try {
					System.out.println(file.getName());
					in = new FileInputStream(file);
					BufferedReader br = new BufferedReader(new InputStreamReader(in));
					// loop and buffer the lines
					String line;
					while ((line = br.readLine()) != null) {
						Matcher m = pattern.matcher(line);
						if (m.find()) {
							list.add(line);

							// check if at max
							total += line.length();
							while (total >= max && list.size() > 0) {
								total -= list.removeFirst().length();
							}
						}
					}
				} catch (Exception e) {
				} finally {
					try {
						if (in != null) {
							in.close();
						}
					} catch (Exception e) {
					}
				}
			}

			// output list
			for (String listLine : list) {
				writer.println(listLine);
			}
			writer.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writer.close();
		}
	}
}