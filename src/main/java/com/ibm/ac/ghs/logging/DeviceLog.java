package com.ibm.ac.ghs.logging;

public class DeviceLog {
	private String employeeNumber;
	private String log;

	public DeviceLog(String employeeNumber, String log) {
		this.employeeNumber = employeeNumber;
		this.log = log;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public String getLog() {
		return log;
	}

	@Override
	public boolean equals(Object obj) {
		return employeeNumber.equals(((DeviceLog) obj).getEmployeeNumber());
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return employeeNumber.hashCode();
	}
}
