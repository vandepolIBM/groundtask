package com.ibm.ac.ghs;

import java.util.List;

import com.ibm.ac.ghs.persistence.AlertManager;
import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.Alert;
import com.ibm.ac.ghs.tasks.mock.TaskCreatorService;
import com.ibm.ac.ghs.tasks.sabre.TestMessagePump;

public class TestService extends UserService {

	private TaskCreatorService taskService;
	private TestMessagePump testMessagePump;	
	
	public TestService(TaskCreatorService taskService, AlertManager alertManager) {
		super(taskService, alertManager);
		this.taskService = taskService;
	}
	
	public void setTestMessagePump(TestMessagePump testMessagePump) {
		this.testMessagePump = testMessagePump;
	}
	
	public AbstractTask getTask(String employeeNumber, String taskId) {
		return taskService.getTask(employeeNumber, taskId);
	}
	
	public List<AbstractTask> getCurrentTasks(String employeeNumber) {
		return taskService.getTasks(employeeNumber);
	}
	
	public void createTask(String employeeNumber, AbstractTask task, boolean silent) {
		taskService.createTask(employeeNumber, task, silent);
	}

	public void updateTask(String employeeNumber, AbstractTask task, boolean silent) {
		taskService.updateTask(employeeNumber, task, silent);
	}
	
	public void deleteTask(String employeeNumber, String taskId) {
		taskService.deleteTask(employeeNumber, taskId);
	}
	
	public List<String> getEmployees() {
		return taskService.getEmployees();
	}
	
	public void createEmployee(String employeeNumber) {
		taskService.createEmployee(employeeNumber);
	}
	
	public void deleteEmployee(String employeeNumber) {
		taskService.deleteEmployee(employeeNumber);
	}
	
	public Alert getAlert(String employeeNumber, String alertId) {
		return taskService.getAlert(employeeNumber, alertId);
	}
	
	public void createAlert(String employeeNumber, String taskId, String message, String type) {
		taskService.createAlert(employeeNumber, taskId, message, type);
	}
	
	public void startMessaging(String employeeNumber, String alertText, int interval) {
		testMessagePump.start(employeeNumber, alertText, interval);
	}
	
	public void stopMessaging(String employeeNumber) {
		testMessagePump.stop(employeeNumber);
	}
	
	public void stopAllMessaging() {
		testMessagePump.stopAll();
	}
	
	public void createDirectMessage(String employeeNumber, String from, String message) {
		taskService.createDirectMessage(employeeNumber, from, message);
	}
}
