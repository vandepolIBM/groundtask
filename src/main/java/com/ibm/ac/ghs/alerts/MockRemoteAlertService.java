package com.ibm.ac.ghs.alerts;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MockRemoteAlertService implements RemoteAlertService {

	private Logger log = LogManager.getLogger(getClass());

	public void registerAlert(String employeeNumber) {
		log.debug("Sending RegisterAlert request, employeeNumber=" + employeeNumber);
	}

	public void clearAlerts(String employeeNumber) {
		log.debug("Sending ClearAlerts request, employeeNumber=" + employeeNumber);
	}

}
