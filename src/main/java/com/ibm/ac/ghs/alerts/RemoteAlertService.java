package com.ibm.ac.ghs.alerts;

public interface RemoteAlertService {

	public void registerAlert(String employeeNumber);
	public void clearAlerts(String employeeNumber);
}
