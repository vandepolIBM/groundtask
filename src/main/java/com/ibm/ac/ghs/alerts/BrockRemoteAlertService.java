package com.ibm.ac.ghs.alerts;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.brock.alert.AlertData;
import com.brock.alert.AlertRequest;
import com.brock.alert.ClearAlerts;
import com.brock.alert.ClearAlertsResponse;
import com.brock.alert.RegisterAlert;
import com.brock.alert.RegisterAlertResponse;

public class BrockRemoteAlertService implements RemoteAlertService {

	private static final int APPLICATION_ID = 1;
	
	private Logger log = LogManager.getLogger(getClass());

	private WebServiceTemplate webServiceTemplate;

	public void setWebServiceTemplate(WebServiceTemplate webServiceTemplate) {
		this.webServiceTemplate = webServiceTemplate;
	}

	public void clearAlerts(String employeeNumber) {
		// log
		if (log.isDebugEnabled()) {
			log.debug("Sending ClearAlerts request, employeeNumber=" + employeeNumber);
		}

		try {
			// create the request
			ClearAlerts req = new ClearAlerts();
			req.setRequest(new AlertRequest());
			req.getRequest().setApplicationId(APPLICATION_ID);
			req.getRequest().setUserId(employeeNumber);

			// send the request
			ClearAlertsResponse resp = (ClearAlertsResponse) webServiceTemplate.marshalSendAndReceive(req);

			// log response details
			log.debug("ClearAlertsResponse: employee="
					+ employeeNumber
					+ ", result="
					+ resp.getClearAlertsResult());

		} catch (Exception e) {
			log.error("Error sending clear alerts request, employee=" + employeeNumber, e);
		}		
	}

	public void registerAlert(String employeeNumber) {
		// log
		if (log.isDebugEnabled()) {
			log.debug("Sending RegisterAlert request, employeeNumber=" + employeeNumber);
		}

		try {
			// create the request
			RegisterAlert req = new RegisterAlert();
			req.setRequest(new AlertRequest());
			req.getRequest().setApplicationId(APPLICATION_ID);
			req.getRequest().setUserId(employeeNumber);
			req.setData(new AlertData());
			req.getData().setMessage(new Date().toString());
			
			// send the request
			RegisterAlertResponse resp = (RegisterAlertResponse) webServiceTemplate.marshalSendAndReceive(req);

			// log response details
			log.debug("RegisterAlertResponse: employee="
					+ employeeNumber
					+ ", result="
					+ resp.getRegisterAlertResult());

		} catch (Exception e) {
			log.error("Error sending register alert request, employee=" + employeeNumber, e);
		}			
	}
}
