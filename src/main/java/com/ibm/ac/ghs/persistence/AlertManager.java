package com.ibm.ac.ghs.persistence;

import java.util.Date;
import java.util.List;

import com.ibm.ac.ghs.messaging.MessageSessionDetails;
import com.ibm.ac.ghs.tasks.Alert;


public interface AlertManager {

	public List<Alert> getAlerts(String employeeNumber);

	public int getTotalNumberOfQueuedAlerts();
	
	public void addAlert(String id, Date timestamp, String employeeNumber, Alert alert);
	
	public void deleteAllAlerts(String employeeNumber);
	
	public void deleteAlertsOlderThan(long seconds);
	
	public void alertReceived(String employeeNumber, String alertId);
	
	public boolean registerEmployeeForAlerts(String employeeNumber, boolean forceAlertPurge) throws RegistrationException;
	
	public List<Employee> getRegisteredEmployees();
	
	public List<Employee> getEmployeesWithQueuedAlerts();

	public void employee_Login(String employeeNumber);
	
	public void employee_Logout(String employeeNumber);
	
	public List<MessageSessionDetails> employee_count();
	
}
