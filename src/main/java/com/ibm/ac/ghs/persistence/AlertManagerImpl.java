package com.ibm.ac.ghs.persistence;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibm.ac.ghs.messaging.MessageSessionDetails;
import com.ibm.ac.ghs.persistence.dao.EmployeeDao;
import com.ibm.ac.ghs.persistence.dao.ServerAlertDao;
import com.ibm.ac.ghs.tasks.Alert;

@Service("alertManager")
public class AlertManagerImpl implements AlertManager, InitializingBean {

	private Logger log = LogManager.getLogger(getClass());

	private ServerAlertDao serverAlertDao;
	private EmployeeDao employeeDao;

	private NodeNotificationStrategy notificationStrategy;

	// set the default expiry to 6 hours (in seconds)
	private int alertMaxAge = 60 * 60 * 6;

	public void afterPropertiesSet() throws Exception {
		createAlertExpiryMaintenanceTimer();
	}

	private void createAlertExpiryMaintenanceTimer() {
		// create a random initial delay < alertMaxAge/4
		int initialDelay = new Random().nextInt((int) alertMaxAge) * 1000;
		int period = (alertMaxAge / 4) * 1000;

		// log
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MILLISECOND, initialDelay);
		log.debug("Scheduling Alert maintenance to run every " + period / 60000 + " minutes.");
		log.debug("Next scheduled execution at " + cal.getTime());

		// start the timer to delete expired alerts
		new Timer().scheduleAtFixedRate(new TimerTask() {
			public void run() {

				try {
					log.debug("Executing expired alert cleanup, pre-total # alerts=" + getTotalNumberOfQueuedAlerts());
					serverAlertDao.deleteByOlderThan(alertMaxAge);
					log.debug("Deleted expired alerts, post-total # alerts=" + getTotalNumberOfQueuedAlerts());
				} catch (Exception e) {
					log.error("Error cleaning up expired alerts.", e);
				}
			}
		}, initialDelay, period);
	}

	@Autowired
	public void setAlertDao(ServerAlertDao serverAlertDao) {
		this.serverAlertDao = serverAlertDao;
	}

	@Autowired
	public void setEmployeeDao(EmployeeDao employeeDao) {
		this.employeeDao = employeeDao;
	}

	@Autowired
	public void setNotificationStrategy(NodeNotificationStrategy notificationStrategy) {
		this.notificationStrategy = notificationStrategy;
	}

	public void setAlertMaxAge(int alertMaxAge) {
		this.alertMaxAge = alertMaxAge;
	}

	public void addAlert(String id, Date timestamp, String employeeNumber, Alert alert) {
		// persist the data
		if (serverAlertDao != null) {
			ServerAlert serverAlert = new ServerAlert();
			serverAlert.setId(id);
			serverAlert.setTimestamp(timestamp);
			serverAlert.setEmployee(new Employee());
			serverAlert.getEmployee().setEmployeeNumber(employeeNumber);
			serverAlert.setAlert(alert);

			try {
				// persist
				serverAlertDao.insert(serverAlert);
			} catch (Exception e) {
				log.warn("Adding alert to database failed, will continue to nofity alert listeners; " + e.getMessage());
			}
		}

		// notify other servers that a message has been received
		if (notificationStrategy != null) {
			notificationStrategy.notifyAlert(employeeNumber, alert);
		}
	}

	public void alertReceived(String employeeNumber, String alertId) {
		if (serverAlertDao != null) {
			try {
				serverAlertDao.delete(alertId);
			} catch (Exception e) {
				log.warn("Unable to delete alert, alertId=" + alertId + "; " + e.getMessage());
			}
		}
	}

	public List<Alert> getAlerts(String employeeNumber) {
		List<Alert> alerts = new ArrayList<Alert>();
		if (serverAlertDao != null) {
			try {
				for (ServerAlert serverAlert : serverAlertDao.findByEmployeeNumber(employeeNumber)) {
					alerts.add(serverAlert.getAlert());
				}
			} catch (Exception e) {
				log.warn("Unable to retrieve alerts for empl=" + employeeNumber + "; " + e.getMessage());
			}
		}
		return alerts;
	}

	public int getTotalNumberOfQueuedAlerts() {
		try {
			return serverAlertDao.getNumAlerts();
		} catch (Exception e) {
			log.warn("Unable to retrieve number of queued alerts;" + e.getMessage());
			return 0;
		}
	}

	public void employee_Login(String employeeNumber){
		log.debug("Enter employee Login");
		employeeDao.insertEmplLoginDtls(employeeNumber);
		//employeeDao.viewEmplLoginDtls();
	}
	
	public void employee_Logout(String employeeNumber){
		log.debug("Enter employee logout, employee="+employeeNumber);
		employeeDao.deleteEmplLoginDtls(employeeNumber);
		
	}
	
	public List<MessageSessionDetails> employee_count(){
		log.debug("Enter employee count");
		List<MessageSessionDetails> messageSessionDetails=new ArrayList<MessageSessionDetails>();
		List<EmployeeLoginDetails> activeUser=	employeeDao.viewEmplLoginDtls();
		List<Employee> pendingAlertsEmplList=employeeDao.getPendingAlertsForActiveUsers();
		log.debug("activeUser size::"+activeUser.size());
		log.debug("pendingAlertsEmplList size::"+pendingAlertsEmplList.size());
		for (EmployeeLoginDetails employeeLoginDetails : activeUser) {
			int pendingAlertCount=0;
			MessageSessionDetails emp=new MessageSessionDetails(null,employeeLoginDetails.getEmployeeNumber(), employeeLoginDetails.getLogin_time(), employeeLoginDetails.getLogin_time());
			for(Employee pendingAlerts:pendingAlertsEmplList){
				//log.debug("inside For Loop"+pendingAlerts.getEmployeeNumber() +"Same as "+ employeeLoginDetails.getEmployeeNumber());
				if(pendingAlerts.getEmployeeNumber().equals(employeeLoginDetails.getEmployeeNumber())){
					log.debug("inside if");
					pendingAlertCount=pendingAlertCount+1;
				}
			}
			emp.setPendingAlerts(pendingAlertCount);
			messageSessionDetails.add(emp);
		}
		log.debug("messageSessionDetails size::"+messageSessionDetails.size());
		return messageSessionDetails;
	}
	
	public boolean registerEmployeeForAlerts(String employeeNumber, boolean forcePurge) throws RegistrationException {

		try {
			// find employee
			Employee employee = employeeDao.findByEmployeeNumber(employeeNumber);

			// check if we need to burn alerts
			if (employee == null || forcePurge) {
				// burn any alerts prior to registering
				serverAlertDao.deleteByEmployeeNumber(employeeNumber);
			}
			
			if (employee == null) {
				// register
				employee = new Employee();
				employee.setEmployeeNumber(employeeNumber);
				employee.setLastRegistered(new Date());
				employeeDao.insert(employee);
				return true;
			} else {
				employee.setLastRegistered(new Date());
				employeeDao.update(employee);
				return false;
			}
		} catch (Exception e) {
			throw new RegistrationException("Error registering empl=" + employeeNumber + "; " + e.getMessage(), e);
		}
	}

	public List<Employee> getRegisteredEmployees() {
		return employeeDao.findAll();
	}

	public List<Employee> getEmployeesWithQueuedAlerts() {
		return employeeDao.findEmployeesWithQueuedAlerts();
	}
	
	public void deleteAllAlerts(String employeeNumber) {
		serverAlertDao.deleteByEmployeeNumber(employeeNumber);
	}
	
	public void deleteAlertsOlderThan(long seconds) {
		serverAlertDao.deleteByOlderThan(seconds);
	}
	
public static void main(String args[]){
		System.out.println("Hello");
	}
}
