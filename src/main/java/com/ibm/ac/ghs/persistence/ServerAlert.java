package com.ibm.ac.ghs.persistence;

import java.io.Serializable;
import java.util.Date;

import com.ibm.ac.ghs.tasks.Alert;

public class ServerAlert implements Serializable {

	private static final long serialVersionUID = 5376696543801931904L;

	private String id;
	private Employee employee;
	private Date timestamp;
	private Alert alert;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Alert getAlert() {
		return alert;
	}
	
	public void setAlert(Alert alert) {
		this.alert = alert;
	}
	
	@Override
	public String toString() {
		return "[id=" + id  + ",emp=" + employee + "]";
	}
}
