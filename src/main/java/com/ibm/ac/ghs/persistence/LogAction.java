package com.ibm.ac.ghs.persistence;

import java.io.Serializable;

public class LogAction implements Serializable {

	public enum LogActionType {
		RETRIEVE,
		DELETE_ALL
	};

	private LogActionType logActionType;

	private String employeeNumber;

	private int numRows;

	private LogAction(String employeeNumber, LogActionType type, int numRows) {
		this.employeeNumber = employeeNumber;
		this.logActionType = type;
		this.numRows = numRows;
	}

	public static LogAction createLogAction(String employeeNumber, LogActionType type) {
		return new LogAction(employeeNumber, type, -1);
	}

	public static LogAction createLogAction(String employeeNumber, LogActionType type, int numRows) {
		return new LogAction(employeeNumber, type, numRows);
	}

	public LogActionType getActionType() {
		return logActionType;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public int getNumRows() {
		return numRows;
	}
	
	@Override
	public String toString() {
		return "[action=" + logActionType + "]";
	}
}
