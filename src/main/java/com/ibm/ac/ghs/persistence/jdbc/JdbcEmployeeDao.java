package com.ibm.ac.ghs.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ibm.ac.ghs.persistence.Employee;
import com.ibm.ac.ghs.persistence.EmployeeLoginDetails;
import com.ibm.ac.ghs.persistence.dao.EmployeeDao;

public class JdbcEmployeeDao implements EmployeeDao {

	private Logger log = LogManager.getLogger(getClass());

	private JdbcTemplate jdbcTemplate;

	private EmployeeRowMapper rowMapper = new EmployeeRowMapper();
	private EmployeeLoginDtlsRowMapper emplRowMapper=new EmployeeLoginDtlsRowMapper();
	private AlertRowMapper alertRowMapper =new AlertRowMapper();

	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public void delete(String employeeNumber) {
		String sql = "delete from employees where employee_number = '" + employeeNumber + "'";
		jdbcTemplate.update(sql);
	}

	public Employee findByEmployeeNumber(String employeeNumber) {
		String sql = "select * from employees where employee_number = '" + employeeNumber + "'";
		try {
			return (Employee) jdbcTemplate.queryForObject(sql, rowMapper);
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Employee> findEmployeesWithQueuedAlerts() {
		String sql = "select distinct alerts.employee_number, employees.last_register from alerts left join employees on alerts.employee_number = employees.employee_number";
		try {
			return jdbcTemplate.query(sql, rowMapper);
		} catch (Exception e) {
			log.error("Error executing SQL query", e);
			return new ArrayList<Employee>();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Employee> findAll() {
		String sql = "select * from employees";
		try {
			return jdbcTemplate.query(sql, rowMapper);
		} catch (Exception e) {
			log.error("Error executing SQL query", e);
			return new ArrayList<Employee>();
		}
	}

	public void insert(final Employee employee) {
		String sql = "insert into employees (employee_number, last_register) values (?, ?)";
		jdbcTemplate.update(sql, new Object[] { employee.getEmployeeNumber(), employee.getLastRegistered() });

	}

	public void update(Employee employee) {
		jdbcTemplate.update("update employees set last_register = ? where employee_number = ?", new Object[] { employee.getLastRegistered(),
				employee.getEmployeeNumber() });
	}
	
	public void insertEmplLoginDtls(String employeeNumber) {
		log.debug("insertEmplLoginDtls:::"+employeeNumber);
		Timestamp timestamp = new Timestamp(new Date().getTime());
		String sql = "insert into EMPLOYEES_LOGIN_DTLS (EMPLOYEE_NUMBER, LOGIN_TIME) values (?, ?)";
		String sql_check = "select * from  EMPLOYEES_LOGIN_DTLS where employee_number = '" + employeeNumber + "'";
		
		try{
		List<EmployeeLoginDetails> emplList= jdbcTemplate.query(sql_check, emplRowMapper);
		if(emplList.size()>0){
			log.debug("insertEmplLoginDtls::"+employeeNumber+ " already exist");
			jdbcTemplate.update("update EMPLOYEES_LOGIN_DTLS set LOGIN_TIME = ? where EMPLOYEE_NUMBER = ?", new Object[] { timestamp, employeeNumber });
		}
		else{
			log.debug("insertEmplLoginDtls::"+employeeNumber+ " doesn't exist already");
		    jdbcTemplate.update(sql, new Object[] { employeeNumber, timestamp });
		}
		
		}
		catch( Exception sqlEx ){
			log.error(" insertEmplLoginDtls:: Error executing SQL query", sqlEx);
		}
			
	}
	public void deleteEmplLoginDtls(String employeeNumber) {
		log.debug("before deletion"+employeeNumber);
			long current_time = new Date().getTime();
			long login_time = 0;
			long time_diff = 0;
			String sql = "delete from EMPLOYEES_LOGIN_DTLS where employee_number = '" + employeeNumber + "'";
			String sql_check = "select * from  EMPLOYEES_LOGIN_DTLS where employee_number = '" + employeeNumber + "'";
			
			try{
				List<EmployeeLoginDetails> emplList= jdbcTemplate.query(sql_check, emplRowMapper);
				if(emplList.size()>0){
					Date login_date = ((EmployeeLoginDetails)emplList.get(0)).getLogin_time();
					if (login_date != null){
						login_time = login_date.getTime();
						time_diff = (((current_time - login_time) / 1000 ) / 60);
					
						if (time_diff >= 5){
							jdbcTemplate.update(sql);
							log.debug("deleted");
						}
					}
				}
			}
			catch( Exception sqlEx ){
				log.error(" deleteEmplLoginDtls:: Error executing SQL query", sqlEx);
			}
			
		
	}
	
	@SuppressWarnings("unchecked")
	public void emplQueuedAlertsCount() {
		String sql = "select * from  alerts";
		try {
			List<EmployeeLoginDetails> emplList= jdbcTemplate.query(sql, emplRowMapper);
			if(emplList.size()>0){
				for (EmployeeLoginDetails employeeLoginDetails : emplList) {
					log.debug(employeeLoginDetails.getEmployeeNumber()+" have Pending Alerts::"+employeeLoginDetails.getPendingAlerts());
					
				}
			}	
			
			else{
				log.debug("No records");
			}
		} catch (Exception e) {
			log.error("Error executing SQL query", e);
			
		}
	}
	
	public List<EmployeeLoginDetails> viewEmplLoginDtls(){
		String sql = "select * from EMPLOYEES_LOGIN_DTLS";
		
		try {
			List<EmployeeLoginDetails> emplList= jdbcTemplate.query(sql, emplRowMapper);
			if(emplList.size()>0){
				for (EmployeeLoginDetails employeeLoginDetails : emplList) {
					log.debug("Employee Number::"+employeeLoginDetails.getEmployeeNumber());
				}
			}	
			
			else{
				log.debug("No records*************");
			}
			return emplList;
			
		} catch (Exception e) {
			log.error("Error executing SQL query", e);
			return new ArrayList<EmployeeLoginDetails>();
		}
	}
	
	public List<Employee> getPendingAlertsForActiveUsers(){
		log.debug("Enter getPendingAlertsForActiveUsers");
		String sql1="select * from alerts";
		String sql ="select alerts.employee_number from alerts INNER JOIN EMPLOYEES_LOGIN_DTLS on alerts.employee_number = employees_login_dtls.employee_number";
		try {
			//List<Employee> count=jdbcTemplate.query(sql1, alertRowMapper);
			//System.out.println("Total new records in alerts************"+count.size());
			//getAlertList();
			return jdbcTemplate.query(sql, alertRowMapper);
		} catch (Exception e) {
			log.error("Error executing SQL query", e);
			return new ArrayList<Employee>();
		}
	}
	
	public void getAlertList(){
		String sql1="select * from alerts";
		List<Employee> count=jdbcTemplate.query(sql1, alertRowMapper);
		log.debug("Total new records in alerts table::"+count.size());
	}

	private class EmployeeRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int i) throws SQLException {
			Employee employee = new Employee();
			employee.setEmployeeNumber(rs.getString("employee_number"));
			
			// check if joining employee table returned data
			Timestamp timestamp = rs.getTimestamp("last_register");
			if (timestamp != null) {
				employee.setLastRegistered(new Date(timestamp.getTime()));
			}			
			return employee;
		}
	}
	
	private class AlertRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int i) throws SQLException {
			Employee employee = new Employee();
			employee.setEmployeeNumber(rs.getString("employee_number"));			
			return employee;
		}
	}
	
	private class EmployeeLoginDtlsRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int i) throws SQLException {
			EmployeeLoginDetails employee = new EmployeeLoginDetails();
			employee.setEmployeeNumber(rs.getString("employee_number"));
			
			// check if joining employee table returned data
			Timestamp timestamp = rs.getTimestamp("LOGIN_TIME");
			if (timestamp != null) {
				employee.setLogin_time(new Date(timestamp.getTime()));
			}	
			
					
			return employee;
		}
	}

	
}
