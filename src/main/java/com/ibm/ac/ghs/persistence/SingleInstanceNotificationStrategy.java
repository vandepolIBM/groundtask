package com.ibm.ac.ghs.persistence;

import org.springframework.beans.factory.annotation.Autowired;

import com.ibm.ac.ghs.messaging.MessageManager;
import com.ibm.ac.ghs.tasks.Alert;

public class SingleInstanceNotificationStrategy implements NodeNotificationStrategy {

	private MessageManager messageManager;

	@Autowired
	public void setMessageManager(MessageManager messageManager) {
		this.messageManager = messageManager;
	}

	public void notifyAlert(String employeeNumber, Alert alert) {
		messageManager.sendAlert(employeeNumber, alert);
	}

	public void notifyLogging(LogAction logAction) {
		messageManager.sendLogAction(logAction);
	}
}
