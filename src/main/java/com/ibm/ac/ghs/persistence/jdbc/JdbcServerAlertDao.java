package com.ibm.ac.ghs.persistence.jdbc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;

import com.ibm.ac.ghs.persistence.Employee;
import com.ibm.ac.ghs.persistence.ServerAlert;
import com.ibm.ac.ghs.persistence.dao.ServerAlertDao;
import com.ibm.ac.ghs.tasks.Alert;

public class JdbcServerAlertDao implements ServerAlertDao {

	private Logger log = LogManager.getLogger(getClass());

	private JdbcTemplate jdbcTemplate;
	private LobHandler lobHandler = new DefaultLobHandler();

	private AlertRowMapper rowMapper = new AlertRowMapper();
	private AlertsRowMapper alertRowMapper=new AlertsRowMapper();

	private String sqlDeleteExpired;
	
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public void setSqlDeleteExpired(String sqlDeleteExpired) {
		this.sqlDeleteExpired = sqlDeleteExpired;
	}
	
	public void delete(String alertId) {
		String sql = "delete from alerts where id = '" + alertId + "'";
		jdbcTemplate.update(sql);
	}

	public void deleteByEmployeeNumber(String employeeNumber) {
		String sql = "delete from alerts where employee_number = '" + employeeNumber + "'";
		jdbcTemplate.update(sql);
	}

	public void deleteByOlderThan(long seconds) {
		String sql = "delete from alerts where datediff('ss', time, current_timestamp) >= ?";
		if (sqlDeleteExpired != null) {
			sql = sqlDeleteExpired;
		} 
		jdbcTemplate.update(sql, new Object[] { seconds });
	}

	public int getNumAlerts() {
		String sql = "select count(*) from alerts";
		return jdbcTemplate.queryForInt(sql);
	}

	@SuppressWarnings("unchecked")
	public List<ServerAlert> findByEmployeeNumber(String employeeNumber) {
		String sql = "select * from alerts left join employees on alerts.employee_number = employees.employee_number where alerts.employee_number = '"
				+ employeeNumber + "'";

		return jdbcTemplate.query(sql, rowMapper);
	}

	public ServerAlert findById(String alertId) {
		String sql = "select * from alerts left join employees on alerts.employee_number = employees.employee_number where alerts.id = '"
				+ alertId + "'";

		return (ServerAlert) jdbcTemplate.queryForObject(sql, rowMapper);
	}

	public void insert(final ServerAlert serverAlert) {
		String sql = "insert into alerts (id, employee_number, time, alert_data) values (?, ?, ?, ?)";
		log.debug("Insert Alert for employee::"+serverAlert.getEmployee().getEmployeeNumber());
		jdbcTemplate.execute(sql, new AbstractLobCreatingPreparedStatementCallback(lobHandler) {
			protected void setValues(PreparedStatement ps, LobCreator lobCreator) throws SQLException {
				ps.setString(1, serverAlert.getId());
				ps.setObject(2, serverAlert.getEmployee().getEmployeeNumber());
				ps.setTimestamp(3, new Timestamp(serverAlert.getTimestamp() != null ? serverAlert.getTimestamp().getTime() : 0));
				lobCreator.setBlobAsBytes(ps, 4, serialize(serverAlert.getAlert()));
			}
		});
		
		//getAlertList();
	}

	public void getAlertList(){
		String sql1="select * from alerts";
		List<Employee> count=jdbcTemplate.query(sql1, alertRowMapper);
		log.debug("Total Alerts at time of insertion::"+count.size());
	}
	
	private byte[] serialize(Object object) {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ObjectOutputStream oout = new ObjectOutputStream(baos);
			oout.writeObject(object);
			oout.close();
		} catch (Exception e) {
			log.error("Serialization error.", e);
		}
		return baos.toByteArray();
	}

	private Object deserialize(byte[] data) {
		final ByteArrayInputStream bais = new ByteArrayInputStream(data);
		try {
			ObjectInputStream ois = new ObjectInputStream(bais);
			return ois.readObject();
		} catch (Exception e) {
			log.error("Deserialization error.", e);
		}
		return null;
	}

	private class AlertRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int i) throws SQLException {
			ServerAlert serverAlert = new ServerAlert();
			serverAlert.setId(rs.getString("id"));
			serverAlert.setTimestamp(new Date(rs.getTimestamp("time").getTime()));
			serverAlert.setAlert((Alert) deserialize(lobHandler.getBlobAsBytes(rs, "alert_data")));
			serverAlert.setEmployee(new Employee());
			serverAlert.getEmployee().setEmployeeNumber(rs.getString("employee_number"));

			// check if joining employee table returned data
			Timestamp timestamp = rs.getTimestamp("last_register");
			if (timestamp != null) {
				serverAlert.getEmployee().setLastRegistered(new Date(timestamp.getTime()));
			}
			return serverAlert;
		}
	}
	
	private class AlertsRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int i) throws SQLException {
			Employee employee = new Employee();
			employee.setEmployeeNumber(rs.getString("employee_number"));			
			return employee;
		}
	}
}
