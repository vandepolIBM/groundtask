package com.ibm.ac.ghs.persistence;

import java.io.Serializable;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.core.JmsTemplate;

import com.ibm.ac.ghs.messaging.MessageManager;
import com.ibm.ac.ghs.tasks.Alert;

public class JmsNotificationStrategy implements NodeNotificationStrategy, MessageListener {

	private Logger log = LogManager.getLogger(getClass());

	private JmsTemplate jmsTemplate;

	private MessageManager messageManager;

	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void setMessageManager(MessageManager messageManager) {
		this.messageManager = messageManager;
	}

	public void notifyAlert(String employeeNumber, Alert alert) {
		try {
			log.debug("*DVP: EMPLOYEE: " + employeeNumber + " on host: " + System.getenv("HOSTNAME") +  " JMSNOTIFICATIONSTRATEGY convertAndSend");
			jmsTemplate.convertAndSend(new AlertWrapper(employeeNumber, alert));
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.error("Unable to broadcast alert to instances, employeeNumber=" + employeeNumber + ", alert=" + alert, e);
			}
		}
	}

	public void notifyLogging(LogAction logAction) {
		try {
			jmsTemplate.convertAndSend(logAction);
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.error("Unable to broadcast log action to instances, employeeNumber=" + logAction.getEmployeeNumber(), e);
			}
		}
	}

	public void onMessage(Message message) {
		ObjectMessage objectMessage = (ObjectMessage) message;
		try {
			if (objectMessage.getObject() instanceof AlertWrapper) {
				// cast the object
				AlertWrapper wrapper = (AlertWrapper) objectMessage.getObject();
				if (log.isDebugEnabled()) {
					log.debug("Received alert broadcast, empl=" + wrapper.getEmployeeNumber() + ", alert=" + wrapper.getAlert());
				}
				log.debug("*DVP: EMPLOYEE: " + wrapper.getEmployeeNumber() + " on host: " + System.getenv("HOSTNAME") +  " onMessage: received message: " + message.getObjectProperty("JMSMessageID"));
				messageManager.sendAlert(wrapper.getEmployeeNumber(), wrapper.getAlert());
			} else if (objectMessage.getObject() instanceof LogAction) {
				// cast the object
				LogAction logAction = (LogAction) objectMessage.getObject();
				// log it
				if (log.isDebugEnabled()) {
					log.debug("Received log action broadcast, empl=" + logAction.getEmployeeNumber() + ", action=" + logAction);
				}				
				// notify the message manager
				messageManager.sendLogAction(logAction);
			}
		} catch (Exception e) {
			log.error("Unable to handle alert broadcast message.", e);
			e.printStackTrace();
		}
	}

	@SuppressWarnings("serial")
	private static class AlertWrapper implements Serializable {

		private String employeeNumber;
		private Alert alert;

		public AlertWrapper(String employeeNumber, Alert alert) {
			this.employeeNumber = employeeNumber;
			this.alert = alert;
		}

		public String getEmployeeNumber() {
			return employeeNumber;
		}

		public Alert getAlert() {
			return alert;
		}
	}
}
