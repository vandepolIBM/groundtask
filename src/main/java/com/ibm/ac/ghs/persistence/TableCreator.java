package com.ibm.ac.ghs.persistence;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ResourceUtils;

public class TableCreator implements InitializingBean {

	private JdbcTemplate jdbcTemplate;

	private boolean createTables;
	private String filename;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public void setCreateTables(boolean createTables) {
		this.createTables = createTables;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public void afterPropertiesSet() throws Exception {
		if (createTables && !StringUtils.isBlank(filename)) {
			File file = ResourceUtils.getFile(filename);
			Reader reader = new InputStreamReader(new FileInputStream(file));
			String sql = FileCopyUtils.copyToString(reader);
			jdbcTemplate.execute(sql);
		}
	}
}
