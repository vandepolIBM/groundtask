package com.ibm.ac.ghs.persistence;

public class RegistrationException extends Exception {

	public RegistrationException() {
		super();
	}
	
	public RegistrationException(String msg) {
		super(msg);
	}

	public RegistrationException(Throwable cause) {
		super(cause);
	}

	public RegistrationException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
