package com.ibm.ac.ghs.persistence;

import java.util.Date;

public class Employee {

	private String employeeNumber;
	private Date lastRegistered;

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public Date getLastRegistered() {
		return lastRegistered;
	}

	public void setLastRegistered(Date lastLogin) {
		this.lastRegistered = lastLogin;
	}

	@Override
	public boolean equals(Object obj) {
		if (employeeNumber != null) {
			return employeeNumber.equals(obj);
		} else {
			return super.equals(obj);
		}
	}

	@Override
	public int hashCode() {
		if (employeeNumber != null) {
			return employeeNumber.hashCode();
		} else {
			return super.hashCode();
		}
	}

	@Override
	public String toString() {
		return "[empNum=" + employeeNumber + ",lastRegistered=" + lastRegistered + "]";
	}
}
