package com.ibm.ac.ghs.persistence;

import java.util.Date;
//Class by Rupal for Active Connection Change
public class EmployeeLoginDetails {
	private String employeeNumber;
	private Date login_time;
	private int pendingAlerts;
	
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public Date getLogin_time() {
		return login_time;
	}
	public void setLogin_time(Date login_time) {
		this.login_time = login_time;
	}
	public int getPendingAlerts() {
		return pendingAlerts;
	}
	public void setPendingAlerts(int pendingAlerts) {
		this.pendingAlerts = pendingAlerts;
	}
	

}
