package com.ibm.ac.ghs.persistence.dao;

import java.util.List;

import com.ibm.ac.ghs.persistence.ServerAlert;


public interface ServerAlertDao {

	public ServerAlert findById(String alertId);
	
	public List<ServerAlert> findByEmployeeNumber(String employeeNumber);

	public void insert(ServerAlert serverAlert);

	public void delete(String alertId);
	
	public void deleteByEmployeeNumber(String employeeNumber);
	
	public void deleteByOlderThan(long seconds);
	
	public int getNumAlerts();
	
	public void getAlertList();
}
