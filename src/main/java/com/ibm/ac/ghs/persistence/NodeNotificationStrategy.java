package com.ibm.ac.ghs.persistence;

import com.ibm.ac.ghs.tasks.Alert;


public interface NodeNotificationStrategy {

	public void notifyAlert(String employeeNumber, Alert slert);
	
	public void notifyLogging(LogAction logAction);
}
