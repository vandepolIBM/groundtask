package com.ibm.ac.ghs.persistence.dao;

import java.util.List;

import com.ibm.ac.ghs.persistence.Employee;
import com.ibm.ac.ghs.persistence.EmployeeLoginDetails;

public interface EmployeeDao {

	public Employee findByEmployeeNumber(String employeeNumber);
	
	public List<Employee> findEmployeesWithQueuedAlerts();

	public List<Employee> findAll();
	
	public void insert(Employee employee);
	
	public void delete(String employeeNumber);
	
	public void update(Employee emplyee);
	
	public void insertEmplLoginDtls(String employeeNumber);
	
	public List<EmployeeLoginDetails> viewEmplLoginDtls();
	
	public void deleteEmplLoginDtls(String employeeNumber);
	
	public List<Employee> getPendingAlertsForActiveUsers();
	
	public void getAlertList();

}
