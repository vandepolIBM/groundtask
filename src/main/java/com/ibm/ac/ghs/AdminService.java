package com.ibm.ac.ghs;

import java.util.List;

import com.ibm.ac.ghs.logging.LogManager;
import com.ibm.ac.ghs.persistence.AlertManager;
import com.ibm.ac.ghs.persistence.Employee;
import com.ibm.ac.ghs.tasks.Alert;

public class AdminService {

	private AlertManager alertManager;
	private LogManager logManager;

	public AdminService(AlertManager alertManager, LogManager logManager) {
		this.alertManager = alertManager;
		this.logManager = logManager;
	}

	public List<Employee> getRegisteredEmployees() {
		return alertManager.getRegisteredEmployees();
	}

	public List<Employee> getEmployeesWithQueuedAlerts() {
		return alertManager.getEmployeesWithQueuedAlerts();
	}

	public List<Alert> getAlerts(String employeeNumber) {
		return alertManager.getAlerts(employeeNumber);
	}

	public int getTotalNumberOfQueuedAlerts() {
		return alertManager.getTotalNumberOfQueuedAlerts();
	}

	public void requestDeviceLogs(String employeeNumber, int numRows) {
		logManager.requestDeviceLogs(employeeNumber, numRows);
	}

	public void deleteDeviceLogs(String employeeNumber) {
		logManager.deleteDeviceLogs(employeeNumber);
	}
}