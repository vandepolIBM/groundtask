package com.ibm.ac.ghs.tasks;

public enum AlertType {

	ADD("n"),
	DELETE("d"),
	EDIT("e"),
	OTHER("o");

	private final String type;

	AlertType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return type;
	}

	public static AlertType fromValue(String v) {
		for (AlertType c : AlertType.values()) {
			if (c.type.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v.toString());
	}
}
