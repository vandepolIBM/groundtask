/**
 * 
 */
package com.ibm.ac.ghs.tasks;

import java.io.Serializable;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class Connection implements Serializable {
	
	private String flightNum;
	private String fin;
	private String etd;
	private String gate;
	private String dest;
	private String bags;
	private String pax;
	private boolean highlightFlight;

	public String getFlightNum() {
		return flightNum;
	}

	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public String getEtd() {
		return etd;
	}

	public void setEtd(String etd) {
		this.etd = etd;
	}

	public String getGate() {
		return gate;
	}

	public void setGate(String gate) {
		this.gate = gate;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getBags() {
		return bags;
	}

	public void setBags(String bags) {
		this.bags = bags;
	}

	public void setPax(String pax) {
		this.pax = pax;
	}

	public String getPax() {
		return pax;
	}
	
	public void setHighlightFlight(boolean highlightFlight) {
		this.highlightFlight =highlightFlight;
	}

	public boolean isHighlightFlight() {
		return highlightFlight;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}