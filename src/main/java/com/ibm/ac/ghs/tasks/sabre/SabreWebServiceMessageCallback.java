package com.ibm.ac.ghs.tasks.sabre;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.transform.TransformerException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;

import com.sabre.streamline.AuthenticationHeader;
import com.sabre.streamline.ObjectFactory;

public class SabreWebServiceMessageCallback implements WebServiceMessageCallback {

	private Logger log = LogManager.getLogger(getClass());
	
	private String username;
	private String password;

	private ObjectFactory factory = new ObjectFactory();
	private JAXBContext jaxbContext;

	public SabreWebServiceMessageCallback(String username, String password) {
		this.username = username;
		this.password = password;

		try {
			jaxbContext = JAXBContext.newInstance("com.sabre.streamline");
		} catch (Exception e) {
			log.error("Error creating JAXBContext", e);
		}
	}

	public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
		try {
			AuthenticationHeader authenticationHeader = factory.createAuthenticationHeader();
			authenticationHeader.setUsername(username);
			authenticationHeader.setPassword(password);

			SoapMessage soapMessage = (SoapMessage) message;
			SoapHeader header = soapMessage.getSoapHeader();
			
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.marshal(factory.createAuthenticationHeader(authenticationHeader), header.getResult());
		} catch (Exception e) {
			log.error("Error adding SOAP authentication header.", e);
		}
	}
}
