package com.ibm.ac.ghs.tasks.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ibm.ac.ghs.persistence.AlertManager;
import com.ibm.ac.ghs.tasks.AboveTheWingArrival;
import com.ibm.ac.ghs.tasks.AboveTheWingDeparture;
import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.Alert;
import com.ibm.ac.ghs.tasks.Alert.Message;
import com.ibm.ac.ghs.tasks.BelowTheWingArrival;
import com.ibm.ac.ghs.tasks.BelowTheWingCnx;
import com.ibm.ac.ghs.tasks.BelowTheWingDeparture;
import com.ibm.ac.ghs.tasks.BelowTheWingLavWater;
import com.ibm.ac.ghs.tasks.BelowTheWingTow;
import com.ibm.ac.ghs.tasks.BelowTheWingTurn;
import com.ibm.ac.ghs.tasks.Cabin;
import com.ibm.ac.ghs.tasks.Connection;
import com.ibm.ac.ghs.tasks.DefaultTask;
import com.ibm.ac.ghs.tasks.DirectMessage;
import com.ibm.ac.ghs.tasks.sabre.resource.ResourceManager;

public class TaskCreatorServiceImpl implements TaskCreatorService {

	private Logger log = LogManager.getLogger(getClass());

	private HashMap<String, List<AbstractTask>> employeeTaskMap = new LinkedHashMap<String, List<AbstractTask>>();

	private AlertManager alertManager;
	
	private ResourceManager resourceManager = new ResourceManager();

	public void setAlertManager(AlertManager alertManager) {
		this.alertManager = alertManager;
	}
	
	private static List<Connection> cnxList = new ArrayList<Connection>();
	
	static {
		Connection cnx = new Connection();
		cnx.setFlightNum("BR320");
		cnx.setFin("901");
		cnx.setEtd("08:50");
		cnx.setGate("322");
		cnx.setDest("YUL");
		cnx.setBags("34");
		cnx.setPax("18");
		cnx.setHighlightFlight(true);
		
		cnxList.add(cnx);
		
		cnx = new Connection();
		cnx.setFlightNum("BM987");
		cnx.setFin("556");
		cnx.setEtd("09:05");
		cnx.setGate("35Z");
		cnx.setDest("YYZ");
		cnx.setBags("124");
		cnx.setPax("70");
		cnx.setHighlightFlight(true);
		
		cnxList.add(cnx);
		
		cnx = new Connection();
		cnx.setFlightNum("AR4233");
		cnx.setFin("921");
		cnx.setEtd("09:20");
		cnx.setGate("234");
		cnx.setDest("YGB");
		cnx.setBags("12");
		cnx.setPax("7");
		cnx.setHighlightFlight(false);
		
		cnxList.add(cnx);
		
		cnx = new Connection();
		cnx.setFlightNum("AC450");
		cnx.setFin("273");
		cnx.setEtd("11:20");
		cnx.setGate("131");
		cnx.setDest("YOW");
		cnx.setBags("8");
		cnx.setPax("7");
		cnx.setHighlightFlight(false);
		
		cnxList.add(cnx);
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.tasks.mock.TaskCreatorService#getTask(java.lang.String)
	 */
	public AbstractTask getTask(String employeeNumber, String taskId) {
		List<AbstractTask> tasks = employeeTaskMap.get(employeeNumber);
		if (tasks != null) {
			for (AbstractTask task : tasks) {
				if (task.getId().equals(taskId)) {
					return task;
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.tasks.TaskService#getTasks(java.lang.String)
	 */
	public List<AbstractTask> getTasks(String employeeNumber) {
		initializeEmployee(employeeNumber);
		List<AbstractTask> list = employeeTaskMap.get(employeeNumber);
		if (list == null) {
			return new ArrayList<AbstractTask>();
		} else {
			return list;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.tasks.mock.TaskCreatorService#getAlert(java.lang.String,
	 *      java.lang.String)
	 */
	public Alert getAlert(String employeeNumber, String alertId) {
		List<Alert> alerts = alertManager.getAlerts(employeeNumber);
		for (Alert alert : alerts) {
			if (alert.getId().equals(alertId)) {
				return alert;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.tasks.TaskService#getAlerts(java.lang.String)
	 */
	public List<Alert> getAlerts(String employeeNumber) {
		return alertManager.getAlerts(employeeNumber);
	}

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.tasks.TaskService#notifyAlertSent(java.lang.String,
	 *      com.ibm.ac.ghs.tasks.Alert)
	 */
	public void alertReceived(String employeeNumber, String alertId) {
		try {
			alertManager.alertReceived(employeeNumber, alertId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.tasks.TaskService#notifyAlertSent(java.lang.String,
	 *      java.util.List)
	 */
	public int alertsReceived(String employeeNumber, List<String> alertIdList) {
		for (String alertId : alertIdList) {
			alertReceived(employeeNumber, alertId);
		}
		return alertIdList.size();
	}

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.tasks.mock.TaskCreatorService#createTask(java.lang.String,
	 *      com.ibm.ac.ghs.tasks.AbstractTask)
	 */
	public void createTask(String employeeNumber, AbstractTask task, boolean silent) {
		List<AbstractTask> tasks = employeeTaskMap.get(employeeNumber);
		if (tasks == null) {
			tasks = new ArrayList<AbstractTask>();
			employeeTaskMap.put(employeeNumber, tasks);
		}
		tasks.add(task);
		createAlert(employeeNumber, task.getId(), "New task: " + task.getType(), Alert.TYPE_NEW, silent);
	}

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.tasks.mock.TaskCreatorService#updateTask(java.lang.String,
	 *      com.ibm.ac.ghs.tasks.AbstractTask)
	 */
	public void updateTask(String employeeNumber, AbstractTask task, boolean silent) {
		List<AbstractTask> tasks = employeeTaskMap.get(employeeNumber);
		if (tasks != null) {
			AbstractTask origTask = null;
			for (Iterator<AbstractTask> iter = tasks.iterator(); iter.hasNext();) {
				AbstractTask abstractTask = iter.next();
				if (abstractTask.getId().equals(task.getId())) {
					origTask = abstractTask;
				}
			}
			if (origTask != null) {
				int index = tasks.indexOf(origTask);
				tasks.remove(origTask);
				tasks.add(index, task);
				createAlert(employeeNumber, task.getId(), "Task " + task.getType() + " has changed.", Alert.TYPE_EDIT, silent);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.tasks.mock.TaskCreatorService#deleteTask(java.lang.String,
	 *      java.lang.String)
	 */
	public void deleteTask(String employeeNumber, String taskId) {
		List<AbstractTask> tasks = employeeTaskMap.get(employeeNumber);
		if (tasks != null) {
			Iterator<AbstractTask> iter = tasks.iterator();
			while (iter.hasNext()) {
				AbstractTask task = iter.next();
				if (task.getId().equals(taskId)) {
					createAlert(employeeNumber, task.getId(), "Task " + task.getType() + " has been removed.", Alert.TYPE_DELETE);
					iter.remove();
					break;
				}
			}
		}
	}

	public List<String> getEmployees() {
		return new ArrayList<String>(employeeTaskMap.keySet());
	}

	public void createEmployee(String employeeNumber) {
		employeeTaskMap.put(employeeNumber, new ArrayList<AbstractTask>());
	}

	public void deleteEmployee(String employeeNumber) {
		employeeTaskMap.remove(employeeNumber);
	}
	
	public void createAlert(String employeeNumber, String taskId, String message, String type) {
		createAlert(employeeNumber, taskId, message, type, false);
	}

	private int nextAlertId = 0;
	public void createAlert(String employeeNumber, String taskId, String message, String type, boolean silent) {
		Alert alert = new Alert();
		alert.setId("" + nextAlertId++);
		alert.setMessage(new Message(message, "(fr) " + message));
		alert.setTask(getTask(employeeNumber, taskId));
		alert.setType(type);
		if (silent) {
			alert.setSeverity(0);
		} else {
			alert.setSeverity(1);
		}
			
		alertManager.addAlert(alert.getId(), new Date(), employeeNumber, alert);
	}

	public void login(String employeeNumber) {
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
		}
	}

	public void logoff(String employeeNumber) {
	}

	public void acknowledgeTask(String employeeNumber, String taskId) {
	}
	
	public int acknowledgeTasks(String employeeNumber, List<String> taskIdList) {
		for (String taskId : taskIdList) {
			acknowledgeTask(employeeNumber, taskId);
		}
		return taskIdList.size();
	}

	public void startTask(String employeeNumber, String taskId) {
	}

	public void stopTask(String employeeNumber, String taskId) {
	}

	private void initializeEmployee(String employeeNumber) {
		if (employeeTaskMap.get(employeeNumber) == null) {
			createEmployee(employeeNumber);
			createTask(employeeNumber, createCabinTask("0", "Turn_1", "13:30", "14:30", "109A", "IM802"), false);
			//createTask(employeeNumber, createCabinTask("1", "O/N_1", "11:00", "12:00", "110", "IM906"), false);
			
			createTask(employeeNumber, createAtwArrivalTask("1", "ARR_MEET", "11:30", "12:30", "118M", "IM512"), false);
			createTask(employeeNumber, createAtwDepartureTask("2", "GT_CTRL", "11:34", "11:50", "125K", "IM5041"), false);
			//createTask(employeeNumber, createAtwDepartureTask("4", "LUNCH", "12:15", "12:45", "CAFE", "IM5555"), false);
			
			createTask(employeeNumber, createBelowTheWingCnxTask("3", "Cnx", "01:32", "02:23", "23BN", "IM123"), false);
			createTask(employeeNumber, createBelowTheWingTurnTask("4", "Turn", "02:34", "02:44", "23NM", "IM234"), false);
			createTask(employeeNumber, createBelowTheWingLavWaterTask("5", "Lav/Water", "05:38", "07:50", "213V", "IM913"), false);
			createTask(employeeNumber, createBelowTheWingTowTask("6", "Tow", "03:38", "03:50", "123G", "IM821"), false);
			createTask(employeeNumber, createBelowTheWingArrivalTask("7", "ON/L", "13:38", "14:34", "129Z", "BM923"), false);
			createTask(employeeNumber, createBelowTheWingDepartureTask("8", "OFF/L", "15:38", "18:34", "244K", "BM9234"), false);
		}
	}

	//Turn_1
	private Cabin createCabinTask(String id, String type, String startTime, String endTime, String location, String flightNum) {
		Cabin cabin = new Cabin();
		cabin.setId(id);
		cabin.setType(type);
		cabin.setSort(Integer.parseInt(startTime.replaceAll("[^0-9]", "")));
		cabin.setStartTime(startTime);
		cabin.setEndTime(endTime);
		cabin.setLocation(location);
		cabin.setCity("YYZ");
		cabin.setFlightNum(flightNum);
		cabin.setArrLoad("80");
		cabin.setAircraftFin("767 / 122");
		cabin.setShowSS(true);
		
		cabin.setNumCrew(22);
//		cabin.setLcscaName("John Doe");
		cabin.setGroundTime("2h 6m");
		
		//PRJ1329 CR13 - Resource Support
		cabin.setResource1("CABRERA CARLOS");
		cabin.setResource2("SHAFIQ AHMAD");
		cabin.setResource3("STARK WILLIAM");		
		cabin.setResource3("ANGELA FORD");
		cabin.setTaskType1("Cabin Crew #1:");
		cabin.setTaskType2("OFF/L Crew:");
		cabin.setTaskType3("Turn Crew:");
		cabin.setTaskType3("Cabin Crew #2:");
				
		
		cabin.setArrFlightNum("IM859");
		cabin.setArrFrom("LHR");
		cabin.setArrGate("170");
		cabin.setArrETA("06:30");
		cabin.setArrSTA("06:34");
		cabin.setArrLoad("86");
		cabin.setArrWchcPax("2");
		
		//PRJ1329 CR13 - ETD/A
		//cabin.setEstimatedTime(cabin.getArrETA());
		//PRJ1329 CR13 Test
		//cabin.setSort(Integer.parseInt(cabin.getArrETA().replaceAll("[^0-9]", "")));
		
		
		cabin.setDepFlightNum("IM406");
		cabin.setDepTo("YUL");
		cabin.setDepGate("131");
		cabin.setDepETD("08:30");
		cabin.setDepSTD("08:32");
		cabin.setDepLoad("22");
		cabin.setDepWchcPax("1");
		
		cabin.setComments("Replace seat K32.");
		
		cabin.setOpsRemarksArr("APU U/S");
		cabin.setOpsRemarksDep("PCH/T - Testx1");
	
		
		return cabin;
	}
	
	private AboveTheWingArrival createAtwArrivalTask(String id, String type, String startTime, String endTime, String location, String flightNum) {
		AboveTheWingArrival atwArr = new AboveTheWingArrival();
		atwArr.setId(id);
		atwArr.setType(type);
		atwArr.setSort(Integer.parseInt(startTime.replaceAll("[^0-9]", "")));
		atwArr.setStartTime(startTime);
		atwArr.setEndTime(endTime);
		atwArr.setGate(location);
		atwArr.setCity("YUL");
		atwArr.setFlightNum(flightNum);
		atwArr.setLoad("90");
		atwArr.setAircraftFin("662 / 156");
		atwArr.setShowSS(false);
		
		atwArr.setPaxF("34"); //PY-Change
		atwArr.setPaxC("12");
		atwArr.setPaxY("56");
		atwArr.setWchr("5");
		atwArr.setWchc("2");
		atwArr.setUm("1");
		atwArr.setBlind("0");
		atwArr.setNumAgents("3");
		atwArr.setAircraftFin("894/298");
		atwArr.setFrom("YUL");
		atwArr.setEta("05:37");
		atwArr.setSta("05:34");
		
		//PRJ1329 CR13 - ETD/A
		//atwArr.setEstimatedTime(atwArr.getEta());
		//PRJ1329 CR13 Test
		//atwArr.setSort(Integer.parseInt(atwArr.getEta().replaceAll("[^0-9]", "")));		
		
		atwArr.setHc(cnxList);
				
		atwArr.setComments("Replace seat K32.");
		
		atwArr.setOpsRemarksDep("APU U/S Dep");
		atwArr.setOpsRemarksArr("PCH/T - Testx2");
		
		return atwArr;
	}
	
	private AboveTheWingDeparture createAtwDepartureTask(String id, String type, String startTime, String endTime, String location, String flightNum) {
		AboveTheWingDeparture atwDep = new AboveTheWingDeparture();
		atwDep.setId(id);
		atwDep.setType(type);
		atwDep.setSort(Integer.parseInt(startTime.replaceAll("[^0-9]", "")));
		atwDep.setStartTime(startTime);
		atwDep.setEndTime(endTime);
		atwDep.setGate(location);
		atwDep.setCity("YNZ");
		atwDep.setFlightNum(flightNum);
		atwDep.setLoad("95");
		atwDep.setAircraftFin("662 / 156");
		atwDep.setShowSS(false);
		
		atwDep.setPaxF("20");//PY-Change
		atwDep.setPaxC("12");
		atwDep.setPaxY("56");
		atwDep.setWchr("5");
		atwDep.setWchc("2");
		
		atwDep.setControlAgent("Jane Doe");
		atwDep.setAssistAgent1("Robert Young");
		atwDep.setAssistAgent2("Susan Walker");
		atwDep.setNumOfGT_SPATT("2");
		atwDep.setUm("1");
		atwDep.setBlind("0");
		atwDep.setAircraftFin("894/298");
		atwDep.setTo("YUL");
		atwDep.setEtd("06:37");   //atwDep.setEtd("05:37");
		atwDep.setStd("06:40");   //atwDep.setStd("05:40");
			
		//PRJ1329 CR13 - ETD/A
		//atwDep.setEstimatedTime(atwDep.getEtd());		
		//PRJ1329 CR13 Test
		//atwDep.setSort(Integer.parseInt(atwDep.getEtd().replaceAll("[^0-9]", "")));		
		
		atwDep.setComments("Replace seat K32.");
		
		atwDep.setOpsRemarksDep("APU U/S Dep");
		atwDep.setOpsRemarksArr("PCH/T - Testx3");
		
		return atwDep;
	}
	
	private BelowTheWingArrival createBelowTheWingArrivalTask(String id, String type, String startTime, String endTime, String location, String flightNum) {
		BelowTheWingArrival task = new BelowTheWingArrival();
		task.setId(id);
		task.setShowSS(true);
		task.setType(type);
		task.setCity("YYZ");
		task.setLocation("23X");
		task.setSort(Integer.parseInt(startTime.replaceAll("[^0-9]", "")));
		task.setStartTime(startTime);
		task.setEndTime(endTime);
		task.setAircraftFin("662 / 156");
		task.setFlightNum(flightNum);
		task.setArrival("YUL");
		task.setBags("2343");
		task.setHcBags("23");
		task.setWchcPax("2");
//		task.setLsaName("John Claude van Dam");
		
		//PRJ1329 CR13 - Resource Support
		task.setResource1("Jane Doe");
		task.setResource2("Robert Young");
		task.setResource3("Susan Walker");		
		task.setTaskType1("OFF/L Crew:");
		task.setTaskType2("OFF/L Assist Crew:");
		task.setTaskType3("Cabin Crew #1:");		
		
		task.setEta("7:41");
		task.setSta("07:43");
		task.setGate("234BC");
		task.setLoad("95");
		
		//PRJ1329 CR13 - ETD/A
		//task.setEstimatedTime(task.getEta());
		//PRJ1329 CR13 Test
		//task.setSort(Integer.parseInt(task.getEta().replaceAll("[^0-9]", "")));		
		
		task.setComments("Double check your work.");
		
		task.setOpsRemarksDep("APU U/S");
		task.setOpsRemarksArr("PCH/T - Testx4");
		
		return task;
	}
	
	private BelowTheWingDeparture createBelowTheWingDepartureTask(String id, String type, String startTime, String endTime, String location, String flightNum) {
		BelowTheWingDeparture task = new BelowTheWingDeparture();
		task.setId(id);
		task.setShowSS(true);
		task.setType(type);
		task.setSort(Integer.parseInt(startTime.replaceAll("[^0-9]", "")));
		task.setStartTime(startTime);
		task.setEndTime(endTime);
		task.setCity("ZWJ");
		task.setLocation("982Z");
		task.setAircraftFin("244 / 324");
		task.setFlightNum(flightNum);
		task.setDeparture("YYZ");
		task.setBags("754");
		task.setHcBags("10");
		task.setWchcPax("2");
//		task.setLsaName("Enobla Hamoraki");
		
		//PRJ1329 CR13 - Resource Support
		task.setResource1("Jane Doe");
		task.setResource2("Robert Young");
		task.setResource3("Susan Walker");		
		task.setTaskType1("ON/L Crew:");
		task.setTaskType2("ON/L Assist Crew:");
		task.setTaskType3("Dep WCHC Crew:");
		
		task.setEtd("19:41");
		task.setStd("19:35");
		task.setGate("924BC");
		task.setLoad("32");
		task.setComments("get it down quick.");
		task.setOpsRemarksDep("APU U/S in the front.");
		task.setOpsRemarksArr("PCH/T - Testx5");
		
		//PRJ1329 CR13 - ETD/A
		//task.setEstimatedTime(task.getEtd());
		//PRJ1329 CR13 Test
		//task.setSort(Integer.parseInt(task.getEtd().replaceAll("[^0-9]", "")));		
		
		return task;
	}
	
	private BelowTheWingCnx createBelowTheWingCnxTask(String id, String type, String startTime, String endTime, String location, String flightNum) {
		BelowTheWingCnx task = new BelowTheWingCnx();
		task.setId(id);
		task.setShowSS(true);
		task.setType(type);
		task.setCity("YYZ");
		
		task.setSort(Integer.parseInt(startTime.replaceAll("[^0-9]", "")));
		task.setStartTime(startTime);
		task.setEndTime(endTime);
		task.setHcBags("23");

		task.setAircraftFin("662 / 156");
		task.setFlightNum(flightNum);
		task.setArrival("YUL");
		task.setGate("234BC");
		task.setEta("8:41");      //task.setEta("7:41");
		task.setSta("08:43");     //task.setSta("07:43");
		
		//PRJ1329 CR13 - ETD/A
		//task.setEstimatedTime(task.getEta());
		//PRJ1329 CR13 Test
		//task.setSort(Integer.parseInt(task.getEta().replaceAll("[^0-9]", "")));		
		
		task.setHc(cnxList);
				
		task.setComments("Double check your work.");
		task.setOpsRemarksDep("APU U/S");
		task.setOpsRemarksArr("PCH/T - Testx6");		
		
		return task;
	}
	
	private BelowTheWingLavWater createBelowTheWingLavWaterTask(String id, String type, String startTime, String endTime, String location, String flightNum) {
		BelowTheWingLavWater task = new BelowTheWingLavWater();
		
		task.setId(id);
		task.setShowSS(true);
		task.setSort(Integer.parseInt(startTime.replaceAll("[^0-9]", "")));
		task.setType(type);
		task.setStartTime(startTime);
		task.setEndTime(endTime);
		task.setCity("YYZ");
		task.setFlightNum(flightNum);
		
		task.setAircraftFin("767 / 122");
		task.setGroundTime("2h 6m");
		
		task.setArrFlightNum("IM859");
		task.setArrFrom("LHR");
		task.setArrGate("170");
		task.setArrETA("09:30");     //task.setArrETA("06:30");
		task.setArrSTA("09:34");     //task.setArrSTA("06:34");
		task.setArrLoad("86");
		
		//PRJ1329 CR13 - ETD/A
		//task.setEstimatedTime(task.getArrETA());
		//PRJ1329 CR13 Test
		//task.setSort(Integer.parseInt(task.getArrETA().replaceAll("[^0-9]", "")));		
		
		task.setDepFlightNum("IM406");
		task.setDepTo("YUL");
		task.setDepGate("131");
		task.setDepETD("10:30");     //task.setDepETD("08:30"); 
		task.setDepSTD("10:33");     //task.setDepSTD("08:33"); 
		task.setDepLoad("22");
		
		task.setComments("Load the pallets 12 in slot D");
		
		task.setOpsRemarksArr("APU U/S");
		task.setOpsRemarksDep("PCH/T - Testx7");
		
		return task;
	}
	
	private BelowTheWingTow createBelowTheWingTowTask(String id, String type, String startTime, String endTime, String location, String flightNum) {
		BelowTheWingTow task = new BelowTheWingTow();
		
		task.setId(id);
		task.setShowSS(true);
		task.setSort(Integer.parseInt(startTime.replaceAll("[^0-9]", "")));
		task.setType(type);
		task.setStartTime(startTime);
		task.setEndTime(endTime);
		task.setCity("YYZ");
		task.setFlightNum(flightNum);
		task.setLocation("75ZX");
		task.setAircraftFin("767 / 122");
		task.setGroundTime("2h 6m");
		
		task.setArrFlightNum("IM859");
		task.setArrFrom("LHR");
		task.setArrGate("170");
		task.setArrETA("16:30");   //task.setArrETA("06:30");
		task.setArrSTA("16:34");   //task.setArrSTA("06:34"); 
		
		//PRJ1329 CR13 - ETD/A
		//task.setEstimatedTime(task.getArrETA());
		//PRJ1329 CR13 Test
		//task.setSort(Integer.parseInt(task.getArrETA().replaceAll("[^0-9]", "")));		
		
		task.setDepFlightNum("IM406");
		task.setDepTo("YUL");
		task.setDepGate("131");
		task.setDepETD("18:30");   //task.setDepETD("08:30"); 
		task.setDepSTD("18:33");   //task.setDepSTD("08:33");
		
		task.setComments("Load the pallets 12 in slot D");
		
		task.setOpsRemarksArr("APU U/S");
		task.setOpsRemarksDep("PCH/T - Testx8");
		
		return task;
	}
	
	private BelowTheWingTurn createBelowTheWingTurnTask(String id, String type, String startTime, String endTime, String location, String flightNum) {
		BelowTheWingTurn task = new BelowTheWingTurn();
		
		task.setId(id);
		task.setShowSS(true);
		task.setSort(Integer.parseInt(startTime.replaceAll("[^0-9]", "")));
		task.setType(type);
		task.setStartTime(startTime);
		task.setEndTime(endTime);
		task.setCity("YYZ");
		task.setFlightNum(flightNum);
		
//		task.setLsaName("Kelvin Glasko");
		task.setArrBags("23");
		task.setDepBags("56");
		task.setArrHcBags("4");
		task.setDepHcBags("2");
		task.setArrWchcPax("2");
		task.setDepWchcPax("0");
		
		task.setAircraftFin("767 / 122");
		task.setGroundTime("2h 6m");
		
		//PRJ1329 CR13 - Resource Support
		task.setResource1("CABRERA CARLOS");
		task.setResource2("SHAFIQ AHMAD");
		task.setResource3("STARK WILLIAM");		
		task.setResource3("ANGELA FORD");
		task.setTaskType1("Turn Crew:");
		task.setTaskType2("Turn Assist Crew:");
		task.setTaskType3("Cabin Crew #1:");
		task.setTaskType3("Dep Dispatch Crew:");
						
		task.setArrFlightNum("IM859");
		task.setArrFrom("LHR");
		task.setArrGate("170");
		task.setArrETA("12:30");     //task.setArrETA("06:30");
		task.setArrSTA("12:34");     //task.setArrSTA("06:34");
		task.setArrLoad("32");
		
		//PRJ1329 CR13 - ETD/A
		//task.setEstimatedTime(task.getArrETA());
		//PRJ1329 CR13 Test
		//task.setSort(Integer.parseInt(task.getArrETA().replaceAll("[^0-9]", "")));		
		
		task.setDepFlightNum("IM406");
		task.setDepTo("YUL");
		task.setDepGate("131");
		task.setDepETD("14:30");      //task.setDepETD("08:30");   
		task.setDepSTD("14:33");      //task.setDepSTD("08:33");
		task.setDepLoad("12");
		
		task.setComments("Load the pallets 12 in slot D");
		
		task.setOpsRemarksArr("APU U/S");
		task.setOpsRemarksDep("PCH/T - Testx9");
		
		return task;
	}
	
	private DefaultTask createLunchTask(String id, String type, String startTime, String endTime, String location, String flightNum) {
		DefaultTask task = new DefaultTask();
		task.setId(id);
		task.setType(type);
		task.setSort(Integer.parseInt(startTime.replaceAll("[^0-9]", "")));
		task.setStartTime(startTime);
		task.setEndTime(endTime);
		task.setLocation(location);
		task.setFlightNum(flightNum);
		task.setShowSS(false);
		task.setComments("Enjoy your lunch!");
		
		return task;
	}
	
	public void createDirectMessage(String employeeNumber, String from, String message) {
		Alert alert = new Alert();
		alert.setId("" + nextAlertId++);
		alert.setMessage(new Message("Message from " + from, "(fr) Message from " + from));
		DirectMessage dm = new DirectMessage();
		dm.setId(alert.getId());
		dm.setFrom(from);
		dm.setTime(new Date().toString());
		dm.setMessage(message);
		alert.setTask(dm);
		alert.setType(Alert.TYPE_DIRECT_MESSAGE);
		alert.setSeverity(1);
		alertManager.addAlert(alert.getId(), new Date(), employeeNumber, alert);
	}
}
