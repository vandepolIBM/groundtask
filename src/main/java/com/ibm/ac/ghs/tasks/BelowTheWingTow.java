package com.ibm.ac.ghs.tasks;

public class BelowTheWingTow extends AbstractTask {

	// Flight Information
	private String aircraftFin;
	private String groundTime;
	
	// Arrival
	private String arrFlightNum;
	private String arrFrom;
	private String arrGate;
	private String arrETA;
	private String arrSTA;
	
	// Departure
	private String depFlightNum;
	private String depTo;
	private String depGate;
	private String depETD;
	private String depSTD;
	
	public BelowTheWingTow() {
		this.setCategory(CategoryType.btw_tow);
	}
	
	public String getAircraftFin() {
		return aircraftFin;
	}
	public void setAircraftFin(String aircraftFin) {
		this.aircraftFin = aircraftFin;
	}
	public String getGroundTime() {
		return groundTime;
	}
	public void setGroundTime(String groundTime) {
		this.groundTime = groundTime;
	}
	public String getArrFlightNum() {
		return arrFlightNum;
	}
	public void setArrFlightNum(String arrFlightNum) {
		this.arrFlightNum = arrFlightNum;
	}
	public String getArrFrom() {
		return arrFrom;
	}
	public void setArrFrom(String arrFrom) {
		this.arrFrom = arrFrom;
	}
	public String getArrGate() {
		return arrGate;
	}
	public void setArrGate(String arrGate) {
		this.arrGate = arrGate;
	}
	public String getArrETA() {
		return arrETA;
	}
	public void setArrETA(String arrETA) {
		this.arrETA = arrETA;
	}
	public String getArrSTA() {
		return arrSTA;
	}
	public void setArrSTA(String arrSTA) {
		this.arrSTA = arrSTA;
	}
	public String getDepFlightNum() {
		return depFlightNum;
	}
	public void setDepFlightNum(String depFlightNum) {
		this.depFlightNum = depFlightNum;
	}
	public String getDepTo() {
		return depTo;
	}
	public void setDepTo(String depTo) {
		this.depTo = depTo;
	}
	public String getDepGate() {
		return depGate;
	}
	public void setDepGate(String depGate) {
		this.depGate = depGate;
	}
	public String getDepETD() {
		return depETD;
	}
	public void setDepETD(String depETD) {
		this.depETD = depETD;
	}
	public String getDepSTD() {
		return depSTD;
	}
	public void setDepSTD(String depSTD) {
		this.depSTD = depSTD;
	}
	
}
