package com.ibm.ac.ghs.tasks.sabre.resource;

public interface RuleChainDao {

	public RuleChain getRuleChain();
}
