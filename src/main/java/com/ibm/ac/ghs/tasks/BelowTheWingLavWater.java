package com.ibm.ac.ghs.tasks;

public class BelowTheWingLavWater extends AbstractTask {

	// Flight Information
	private String aircraftFin;
	private String groundTime;
	
	// Arrival
	private String arrFlightNum;
	private String arrFrom;
	private String arrGate;
	private String arrETA;
	private String arrSTA;
	private String arrLoad;
	
	// Departure
	private String depFlightNum;
	private String depTo;
	private String depGate;
	private String depETD;
	private String depSTD;
	private String depLoad;
	
	public BelowTheWingLavWater() {
		this.setCategory(CategoryType.btw_lav);
	}
	
	public String getAircraftFin() {
		return aircraftFin;
	}
	public void setAircraftFin(String aircraftFin) {
		this.aircraftFin = aircraftFin;
	}
	public String getGroundTime() {
		return groundTime;
	}
	public void setGroundTime(String groundTime) {
		this.groundTime = groundTime;
	}
	public String getArrFlightNum() {
		return arrFlightNum;
	}
	public void setArrFlightNum(String arrFlightNum) {
		this.arrFlightNum = arrFlightNum;
	}
	public String getArrFrom() {
		return arrFrom;
	}
	public void setArrFrom(String arrFrom) {
		this.arrFrom = arrFrom;
	}
	public String getArrETA() {
		return arrETA;
	}
	public void setArrETA(String arrETA) {
		this.arrETA = arrETA;
	}
	public String getArrSTA() {
		return arrSTA;
	}
	public void setArrSTA(String arrSTA) {
		this.arrSTA = arrSTA;
	}
	public String getArrGate() {
		return arrGate;
	}
	public void setArrGate(String arrGate) {
		this.arrGate = arrGate;
	}
	public String getArrLoad() {
		return arrLoad;
	}
	public void setArrLoad(String arrLoad) {
		this.arrLoad = arrLoad;
	}
	public String getDepFlightNum() {
		return depFlightNum;
	}
	public void setDepFlightNum(String depFlightNum) {
		this.depFlightNum = depFlightNum;
	}
	public String getDepTo() {
		return depTo;
	}
	public void setDepTo(String depTo) {
		this.depTo = depTo;
	}
	public String getDepETD() {
		return depETD;
	}
	public void setDepETD(String depETD) {
		this.depETD = depETD;
	}
	public String getDepSTD() {
		return depSTD;
	}
	public void setDepSTD(String depSTD) {
		this.depSTD = depSTD;
	}
	public String getDepGate() {
		return depGate;
	}
	public void setDepGate(String depGate) {
		this.depGate = depGate;
	}
	public String getDepLoad() {
		return depLoad;
	}
	public void setDepLoad(String depLoad) {
		this.depLoad = depLoad;
	}

}
