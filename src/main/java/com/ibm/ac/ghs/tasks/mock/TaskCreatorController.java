package com.ibm.ac.ghs.tasks.mock;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.Cabin;

@Controller
@SessionAttributes("cabin")
public class TaskCreatorController {

	private TaskCreatorService taskCreatorService;

	@Autowired
	public void setTaskCreatorService(TaskCreatorService taskCreatorService) {
		this.taskCreatorService = taskCreatorService;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/task/cabin")
	public String submitCabin(@ModelAttribute("cabin")
	AbstractTask cabin, BindingResult result, SessionStatus status, HttpServletRequest req) {
		if (cabin.getId() == null || cabin.getId().length() == 0) {
			try {
				cabin.setId("" + new Date().getTime());
				cabin.setSort(Integer.parseInt(cabin.getStartTime().replaceAll("[^0-9]", "")));			
			} catch (Exception e) {
			}
			taskCreatorService.createTask("111", cabin, BooleanUtils.toBoolean(req.getParameter("silent")));
		} else {
			taskCreatorService.updateTask("111", cabin, BooleanUtils.toBoolean(req.getParameter("silent")));
		}
		return "redirect:/ctrl/admin/portal";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/task/cabin")
	public String getCabin(@RequestParam(value = "taskId", required = false)
	String taskId, @RequestParam("emp")
	String empNum, ModelMap model) {
		if (taskId != null) {
			AbstractTask task = taskCreatorService.getTask(empNum, taskId);
			model.addAttribute("cabin", task);
		} else {
			model.addAttribute("cabin", new Cabin());
		}
		return "admin/task";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/task/delete")
	public String deleteTask(@RequestParam(value = "taskId")	String taskId, @RequestParam("emp")	String empNum) {
		if (taskId.length() > 0) {
			taskCreatorService.deleteTask(empNum, taskId);
		}
		return "redirect:/ctrl/admin/portal";
	}	
}
