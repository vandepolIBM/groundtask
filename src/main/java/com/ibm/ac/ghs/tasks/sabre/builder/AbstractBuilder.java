package com.ibm.ac.ghs.tasks.sabre.builder;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.commons.lang.time.DateUtils;

import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.Connection;
import com.ibm.ac.ghs.tasks.TaskStatusType;
import com.ibm.ac.ghs.tasks.sabre.resource.ResourceManager;
import com.ibm.ac.ghs.tasks.sabre.resource.ResourceMatchResult;
import com.sabre.streamline.FlightInformation;
import com.sabre.streamline.FlightInformationInboundFlight;
import com.sabre.streamline.Remarks;
import com.sabre.streamline.TaskInformation;
import com.sabre.streamline.TaskStatus;

/**
 * Abstract implementation to convert default task details which are common to
 * every task category.
 * 
 * @author Jeremy Koch
 */
public abstract class AbstractBuilder {

	private ResourceManager resourceManager;

	public void setResourceManager(ResourceManager resourceManager) {
		this.resourceManager = resourceManager;
	}

	/**
	 * Converts the XML time to the format to be displayed on the device.
	 * 
	 * @param xml
	 * @return
	 */
	protected String convertTime(XMLGregorianCalendar xml) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(xml.toGregorianCalendar().getTime());
	}

	/**
	 * Formats the ground time for display as "xh ym".
	 * 
	 * @param biGT
	 *            time in seconds
	 * @return
	 */
	protected String formatGroundTime(BigInteger biGT) {
		if (biGT == null || biGT.intValue() == 0) {
			return "";
		} else {
			return (biGT.intValue() / 3600) + "h " + ((biGT.intValue() / 60) % 60) + "m";
		}
	}

	protected String convertToString(BigInteger bi) {
		return bi != null ? bi.toString() : null;
	}

	/**
	 * Populates the task remarks fields for arrival and departure.
	 * 
	 * <pre>
	 * ATW-AWT
	 * BTW-BTW
	 * TOW-Cabin
	 * OPS-Everybody
	 * Cargo-Ignore
	 * 
	 * Cabin = inbound and outbound
	 * ATW-Arr = inbound
	 * ATW-Dep = outbound
	 * BTW-Arr = inbound
	 * BTW-Dep = outbound
	 * BTW-LAV-water = inbound and outbound
	 * BTW-Connection = inbound
	 * BTW-TOW, = inbound and outbound
	 * BTW-Turn = inbound and outbound
	 * </pre>
	 * 
	 * @param task
	 * @param flight
	 */
	protected void populateOpsRemarks(AbstractTask task, FlightInformation flight) {
		// ensure we have flight details
		if (flight == null) {
			return;
		}

		// grab the remarks
		Remarks remarksArr = flight.getInboundFlight() != null && flight.getInboundFlight().getRemarks() != null ? flight.getInboundFlight().getRemarks() : new Remarks();
		Remarks remarksDep = flight.getOutboundFlight() != null && flight.getOutboundFlight().getRemarks() != null ? flight.getOutboundFlight().getRemarks() : new Remarks();

		switch (task.getCategory()) {
		case cabin:
			task.setOpsRemarksArr(combineOpsRemarks(remarksArr, remarksArr.getTOW()));
			task.setOpsRemarksDep(combineOpsRemarks(remarksDep, remarksDep.getTOW()));
			break;
		case atw_arr:
			task.setOpsRemarksArr(combineOpsRemarks(remarksArr, remarksArr.getATW()));
			break;
		case atw_dep:
			task.setOpsRemarksDep(combineOpsRemarks(remarksDep, remarksDep.getATW()));
			break;
		case btw_arr:
		case btw_cnx:
			task.setOpsRemarksArr(combineOpsRemarks(remarksArr, combineOpsRemarks(remarksArr.getBTW(),remarksArr.getTOW())));
			break;
		case btw_dep:
			task.setOpsRemarksDep(combineOpsRemarks(remarksDep, combineOpsRemarks(remarksDep.getBTW(),remarksDep.getTOW())));
			break;
		case btw_lav:
		case btw_tow:
		case btw_turn:
			task.setOpsRemarksArr(combineOpsRemarks(remarksArr, combineOpsRemarks(remarksArr.getBTW(),remarksArr.getTOW())));			
			task.setOpsRemarksDep(combineOpsRemarks(remarksDep, combineOpsRemarks(remarksDep.getBTW(),remarksDep.getTOW())));
			break;
		}
	}

	/**
	 * Combines OPS and specific remark text into sentences separated by a
	 * period.
	 * 
	 * @param remarks
	 * @param specificRemark
	 * @return
	 */
	private String combineOpsRemarks(Remarks remarks, String specificRemark) {
		StrBuilder sb = new StrBuilder();
		sb.setNullText("");
		sb.append(remarks.getOPS());
		sb.appendSeparator(". ");
		sb.append(specificRemark);
		return StringUtils.trimToNull(sb.toString());
	}

	/**
	 * Combines 2 remark texts into sentences separated by a
	 * period.
	 * 
	 * @param remarks
	 * @param specificRemark
	 * @return
	 */
	private String combineOpsRemarks(String specificRemark1, String specificRemark2) {
		StrBuilder sb = new StrBuilder();
		sb.setNullText("");
		sb.append(specificRemark1);
		sb.appendSeparator(". ");
		sb.append(specificRemark2);
		return StringUtils.trimToNull(sb.toString());
	}
	
	/**
	 * Searches list of resource returned from Sabre against the rules defined
	 * in the resource rule chain.
	 * 
	 * @param taskInformation
	 * @return
	 */
	protected List<ResourceMatchResult> detectResources(TaskInformation taskInformation) {
		return resourceManager.matchResources(taskInformation);
	}

	/**
	 * Builds the list of resource names separated by <code>\<p\></code> tags.
	 * 
	 * @param taskInformation
	 * @return
	 */
	protected String buildResourceNameList(TaskInformation taskInformation) {
		StringBuffer sb = new StringBuffer();
		for (ResourceMatchResult result : detectResources(taskInformation)) {
			sb.append("<p>").append(result.getResource().getEmpName()).append("</p>");
		}
		return StringUtils.trimToNull(sb.toString());
	}

	/**
	 * Sets the home screen city code information depending on task category.
	 * 
	 * @param taskInformation
	 */
	protected void populateCity(AbstractTask task, TaskInformation taskInformation) {
		FlightInformation fltInfo = taskInformation.getFlightInformation();
		switch (task.getCategory()) {
		case atw_dep:
		case btw_dep:
			String outboundCity = (fltInfo != null && fltInfo.getOutboundFlight() != null) ? fltInfo.getOutboundFlight().getDownlineStation() : null;
			task.setCity(outboundCity);
			break;
		default:
			String inboundCity = (fltInfo != null && fltInfo.getInboundFlight() != null) ? fltInfo.getInboundFlight().getUplineStation() : null;
			task.setCity(inboundCity);
			break;
		}
	}

	/**
	 * Build the connection list.
	 * 
	 * @param taskInformation
	 * @return
	 */
	protected List<Connection> buildConnectionList(TaskInformation taskInformation) {
		List<Connection> connectionList = new ArrayList<Connection>();
		FlightInformationInboundFlight flt;
		
		if (taskInformation.getFlightInformation() != null && taskInformation.getFlightInformation().getInboundFlight() != null) {
			flt = taskInformation.getFlightInformation().getInboundFlight();
			XMLGregorianCalendar btwEta = flt.getEstimatedArrivalTime();
			if (flt.getConnectingLoads() != null) {
				for (com.sabre.streamline.Connection sabreCnx : flt.getConnectingLoads().getConnection()) {
					Connection cnx = new Connection();
					cnx.setFlightNum(sabreCnx.getFlightNumber());
					cnx.setFin(sabreCnx.getTailNumber());
					cnx.setEtd(convertTime(sabreCnx.getEstimatedTime()));
					
					try
					{
						if(((sabreCnx.getEstimatedTime().toGregorianCalendar().getTimeInMillis() - 
								flt.getEstimatedArrivalTime().toGregorianCalendar().getTimeInMillis()) / 1000 / 60) < 30) {
							cnx.setHighlightFlight(true);
						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}
					
					if (sabreCnx.getGateInformation() != null) {
						cnx.setGate(sabreCnx.getGateInformation().getGate());
					}
					cnx.setDest(sabreCnx.getStation());
					cnx.setPax(convertToString(sabreCnx.getPax()));
					cnx.setBags(convertToString(sabreCnx.getBags()));
					connectionList.add(cnx);
				}
			}
		}

		return connectionList.size() != 0 ? connectionList : null;
	}

	/**
	 * Initializes the fields of tasks which are common to every task category.
	 * 
	 * @param destTask
	 *            the destination task which is passed to the client
	 * @param srcTask
	 *            the source task from the backend.
	 */
	protected void initialize(AbstractTask destTask, TaskInformation srcTask) {
		// populate the standard fields
		destTask.setId(srcTask.getTask().getSMInstanceId());
		destTask.setType(srcTask.getTask().getTaskType());
		destTask.setSort((srcTask.getTask().getStartTime().toGregorianCalendar().getTime().getTime() / 1000));
		destTask.setStartTime(convertTime(srcTask.getTask().getStartTime()));
		destTask.setEndTime(convertTime(srcTask.getTask().getEndTime()));
		destTask.setLocation(srcTask.getTask().getTaskPosition());
		destTask.setShowSS(BooleanUtils.isTrue(srcTask.getTask().isStartCompleteIndicator()));
		destTask.setComments(StringUtils.defaultString(srcTask.getTask().getComments()));
		destTask.setFlightNum(StringUtils.defaultString(srcTask.getTask().getFlightNumber()));
		
		//PRJ1329 CR13 - Show ETD/A on Task List Screen
		//destTask.setEstimatedTime("");

		// populate the city for the home screen
		populateCity(destTask, srcTask);

		// check whether the task has been previously acknowedged. if it is in
		// start/stop status, then
		// it must have been previously acknowdged.
		TaskStatus stat = srcTask.getTask().getTaskStatus();
		if (stat == TaskStatus.ACKNOWLEDGED || stat == TaskStatus.STARTED || stat == TaskStatus.COMPLETED) {
			destTask.setAckd(true);
		}

		// set the status
		if (stat == TaskStatus.STARTED) {
			destTask.setStatus(TaskStatusType.STARTED);
		} else if (stat == TaskStatus.COMPLETED) {
			destTask.setStatus(TaskStatusType.COMPLETED);
		}

		// populate ops remarks
		populateOpsRemarks(destTask, srcTask.getFlightInformation());
	}

	/**
	 * Builds the client task details which are specific to each category.
	 * 
	 * @param taskInformation
	 *            the source task from the backend.
	 * @return the client task
	 */
	public abstract AbstractTask build(TaskInformation taskInformation);
}
