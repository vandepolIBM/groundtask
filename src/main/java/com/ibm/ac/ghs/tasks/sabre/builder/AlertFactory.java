package com.ibm.ac.ghs.tasks.sabre.builder;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ibm.ac.ghs.tasks.Alert;
import com.ibm.ac.ghs.tasks.DirectMessage;
import com.ibm.ac.ghs.tasks.Alert.Message;
import com.sabre.streamline.StaffManagerMobileMessage;

@Component
public class AlertFactory {

	private static Logger log = LogManager.getLogger(AlertFactory.class);

	private TaskFactory taskFactory; 

	@Autowired
	public void setTaskFactory(TaskFactory taskFactory) {
		this.taskFactory = taskFactory;
	}
	
	public Alert createAlert(StaffManagerMobileMessage message) {

		// create the alert
		Alert alert = new Alert();
		alert.setId(message.getAlertInformation().getAlert().getAlertID());

		// set the default severity
		// 0 = silent
		// 1 = buzz/beep the device
		try {
			if (BooleanUtils.toBoolean(message.getAlertInformation().getAlert().getDisplayAlert())) {
				alert.setSeverity(1);
			} else {
				alert.setSeverity(0);
			}
		} catch (Exception e) {
			log.warn("No alert visibility indicated, assuming visible.");
			alert.setSeverity(1);
		}

		// check for task information
		if (message.getTaskInformation() == null) {
			// since we dont have task info, we consider this a direct
			// message on the client side
			alert.setType(Alert.TYPE_DIRECT_MESSAGE);

			// create the direct message
			DirectMessage dm = new DirectMessage();
			dm.setId(alert.getId());
			dm.setFrom("Resource Manager");
			dm.setTime(DateFormatUtils.format(message.getMessageReference().getTimestamp().toGregorianCalendar().getTime(), "MMM dd, yyyy / HH:mm"));
			dm.setMessage(message.getAlertInformation().getAlert().getAlertString().getEnglishString());
			
			// set the detail we just created
			alert.setTask(dm);

			// set the alert message with hardcoded data until Sabre
			// can send us something instead.
			Message alertMsg = new Message();
			alertMsg.setEn("Message from " + dm.getFrom());
			alertMsg.setFr("Message de " + dm.getFrom());
			alert.setMessage(alertMsg);

		} else {
			// set the task details
			alert.setTask(taskFactory.createTask(message.getTaskInformation()));

			// set the alert message
			Message alertMsg = new Message();
			alertMsg.setEn(StringUtils.defaultString(message.getAlertInformation().getAlert().getAlertString().getEnglishString()));
			alertMsg.setFr(StringUtils.defaultString(message.getAlertInformation().getAlert().getAlertString().getFrenchString()));
			alert.setMessage(alertMsg);

			// set the alert type
			switch (message.getTaskInformation().getTask().getTransactionType()) {
			case ADD:
				alert.setType(Alert.TYPE_NEW);
				break;
			case DELETE:
				alert.setType(Alert.TYPE_DELETE);
				break;
			case UPDATE:
				alert.setType(Alert.TYPE_EDIT);
				break;
			default:
				log.warn("Unknown alert transaction type: " + message.getTaskInformation().getTask().getTransactionType());
				alert.setType(Alert.TYPE_OTHER);
				break;
			}
		}

		return alert;
	}
}
