package com.ibm.ac.ghs.tasks.sabre.resource;

import java.util.ArrayList;
import java.util.List;

public class RuleChain {

	private List<Rule> rules = new ArrayList<Rule>();;

	public List<Rule> getRules() {
		return rules;
	}
	
	public void addRule(Rule rule) {
		rules.add(rule);
	}

	public void addRule(int index, Rule rule) {
		rules.add(index, rule);
	}
}
