package com.ibm.ac.ghs.tasks.sabre.builder;

import org.apache.commons.lang.StringUtils;

import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.BelowTheWingLavWater;
import com.ibm.ac.ghs.tasks.BelowTheWingTow;
import com.sabre.streamline.FlightInformationInboundFlight;
import com.sabre.streamline.FlightInformationOutboundFlight;
import com.sabre.streamline.TaskInformation;

/**
 * Builder responsible for instantiating and population the client-side object
 * which represents a Below the Wing - Tow category task.
 * 
 * @author J.Koch
 */
public class BelowTheWingTowBuilder extends AbstractBuilder {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ibm.ac.ghs.tasks.sabre.builder.AbstractBuilder#build(com.sabre.streamline
	 * .TaskInformation)
	 */
	public AbstractTask build(TaskInformation taskInformation) {
		BelowTheWingTow task = new BelowTheWingTow();

		// setup the default task information
		initialize(task, taskInformation);

		// ensure we have flight information
		if (taskInformation.getFlightInformation() != null) {

			// ensure we have equipment info
			if (taskInformation.getFlightInformation().getEquipmentInformation() != null) {
				// set the aircraft type and fin
				task.setAircraftFin(taskInformation.getFlightInformation().getEquipmentInformation().getAircraftType() + "/"
						+ StringUtils.defaultString(taskInformation.getFlightInformation().getEquipmentInformation().getTailNumber()));

				// set the ground time
				String groundTime = formatGroundTime(taskInformation.getFlightInformation().getEquipmentInformation().getAircraftGroundTime());
				task.setGroundTime(StringUtils.defaultString(groundTime));
			}

			// setup the inbound flight
			FlightInformationInboundFlight inFlt = taskInformation.getFlightInformation().getInboundFlight();
			if (inFlt != null) {
				task.setArrFlightNum(inFlt.getFlightNumber());
				task.setArrFrom(StringUtils.defaultString(inFlt.getUplineStation()));
				task.setArrGate(inFlt.getGateInformation() != null ? StringUtils.defaultString(inFlt.getGateInformation().getGate()) : "");
				task.setArrETA(convertTime(inFlt.getEstimatedArrivalTime()));
				
				//PRJ1329 CR13 - Show ETD/A on Task List Screen
				//task.setEstimatedTime(task.getArrETA());
			}

			// setup the outbound flight
			FlightInformationOutboundFlight outFlt = taskInformation.getFlightInformation().getOutboundFlight();
			if (outFlt != null) {
				task.setDepFlightNum(outFlt.getFlightNumber());
				task.setDepTo(StringUtils.defaultString(outFlt.getDownlineStation()));
				task.setDepGate(outFlt.getGateInformation() != null ? StringUtils.defaultString(outFlt.getGateInformation().getGate()) : "");
				task.setDepETD(convertTime(outFlt.getEstimatedDepartureTime()));
				
				//PRJ1329 CR13 - Show ETD/A on Task List Screen
				//if (task.getEstimatedTime().length() == 0) {
				//	task.setEstimatedTime(task.getDepETD());	
				//}
			}
		}

		return task;
	}
}
