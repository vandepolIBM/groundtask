package com.ibm.ac.ghs.tasks;

public enum CategoryType {
	cabin,
	
	atw_arr,
	atw_dep,
	
	btw_arr,
	btw_dep,
	btw_lav,
	btw_cnx,
	btw_tow,
	btw_turn,

	message,
	other;
}
