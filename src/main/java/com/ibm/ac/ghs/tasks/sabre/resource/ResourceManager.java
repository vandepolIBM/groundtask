package com.ibm.ac.ghs.tasks.sabre.resource;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.ResourceUtils;

import com.sabre.streamline.Resource;
import com.sabre.streamline.Task;
import com.sabre.streamline.TaskInformation;

/**
 * 
 * @author J. Koch
 * 
 */
public class ResourceManager {

	private RuleChain chain;

	public ResourceManager() {
		this(new XmlRuleChainDao());
	}

	public ResourceManager(RuleChainDao dao) {
		this.chain = dao.getRuleChain();
	}

	public List<ResourceMatchResult> matchResources(TaskInformation taskInformation) {
		List<ResourceMatchResult> list = new ArrayList<ResourceMatchResult>();

		// ensure we have data from sabre
		Task task = taskInformation.getTask();
		if (task == null || taskInformation == null || taskInformation.getOtherResources() == null) {
			return list;
		}

		// iterate through the resources and rule chain
		for (Resource resource : taskInformation.getOtherResources().getResource()) {
			for (Rule rule : chain.getRules()) {
				ResourceMatchResult result = rule.matches(task.getTaskCategory(), task.getTaskType(), resource);
				if (result != null) {
					list.add(result);
					break;
				}
			}
		}
		return list;
	}

	public static class XmlRuleChainDao implements RuleChainDao {

		private Logger log = LogManager.getLogger(getClass());

		private static final String DEFAULT_RESOURCE = "classpath:resource_rules.xml";

		private static final String ROOT = "rule";

		private XMLConfiguration configuration;

		public XmlRuleChainDao() {
			this(DEFAULT_RESOURCE);
		}

		public XmlRuleChainDao(String resource) {
			try {
				this.configuration = new XMLConfiguration(ResourceUtils.getFile(resource));
				this.configuration.setReloadingStrategy(new FileChangedReloadingStrategy());
			} catch (Exception e) {
				log.error("Error loading configuration file: " + e.getMessage());
			}
		}

		public RuleChain getRuleChain() {

			RuleChain ruleChain = new RuleChain();
			if (configuration == null) {
				return ruleChain;
			}

			int i = 0;
			boolean next = true;
			do {
				StringBuffer sb = new StringBuffer(ROOT);
				sb.append('(').append(i++).append(')');

				String name = configuration.getString(sb.toString() + ".name");
				if (name != null) {
					String category = configuration.getString(sb.toString() + ".taskCategory");
					String type = configuration.getString(sb.toString() + ".taskType");
					String sabreType = configuration.getString(sb.toString() + ".sabreResourceTaskType");
					DefaultRule rule = new DefaultRule(name, category, type, sabreType);
					ruleChain.addRule(rule);
				} else {
					next = false;
				}
			} while (next);

			return ruleChain;
		}
	}
}
