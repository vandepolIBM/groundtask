package com.ibm.ac.ghs.tasks.sabre.builder;

import java.util.List;
import org.apache.commons.lang.StringUtils;

import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.BelowTheWingDeparture;
import com.ibm.ac.ghs.tasks.sabre.resource.ResourceMatchResult;
import com.ibm.ac.ghs.tasks.sabre.resource.TaskTypeResource;
import com.sabre.streamline.FlightInformationOutboundFlight;
import com.sabre.streamline.TaskInformation;

/**
 * Builder responsible for instantiating and population the client-side object
 * which represents a Below the Wing - Departure category task.
 * 
 * @author J.Koch
 */
public class BelowTheWingDepartureBuilder extends AbstractBuilder {

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.tasks.sabre.builder.AbstractBuilder#build(com.sabre.streamline.TaskInformation)
	 */
	public AbstractTask build(TaskInformation taskInformation) {
		BelowTheWingDeparture task = new BelowTheWingDeparture();
		TaskTypeResource taskType = new TaskTypeResource();
		
		// setup the default task information
		initialize(task, taskInformation);

		// ensure we have flight information
		if (taskInformation.getFlightInformation() != null) {

			// ensure we have inbound flight information
			if (taskInformation.getFlightInformation().getOutboundFlight() != null) {

				// grab the flight
				FlightInformationOutboundFlight flt = taskInformation.getFlightInformation().getOutboundFlight();
				
				// grab the bags information
				if (flt.getBagsInformation() != null ) {
					task.setBags(convertToString(flt.getBagsInformation().getTotalBagCount()));
					task.setHcBags(convertToString(flt.getBagsInformation().getHotCnxBags()));
				}

				// get WCHC
				if (flt.getMiscInformation() != null) {
					task.setWchcPax(convertToString(flt.getMiscInformation().getTotalCarryOnOffWheelChairs()));
				}
				
				// ensure we have equipment info
				if (taskInformation.getFlightInformation().getEquipmentInformation() != null) {
					// set the aircraft type and fin
					task.setAircraftFin(taskInformation.getFlightInformation().getEquipmentInformation().getAircraftType() + "/"
							+ StringUtils.defaultString(taskInformation.getFlightInformation().getEquipmentInformation().getTailNumber()));
				}
				
				// set flight details
				task.setDeparture(StringUtils.defaultString(flt.getDownlineStation()));
				task.setGate(flt.getGateInformation() != null ? StringUtils.defaultString(flt.getGateInformation().getGate()) : "");
				task.setEtd(convertTime(flt.getEstimatedDepartureTime()));
				task.setLoad(flt.getLoadFactor() != null ? flt.getLoadFactor().toPlainString() : "");

				//PRJ1329 CR13 - Show ETD/A on Task List Screen
				//task.setEstimatedTime(task.getEtd());
				
				// TODO 
				task.setStd("");
			}
		}


		//PRJ1329 CR13 - ensure we have resource information
		List<ResourceMatchResult> matches = detectResources(taskInformation);
		for (ResourceMatchResult matchResult : matches) {
			if ("BTWDep".equals(matchResult.getRule().getName())) {
				if (task.getResource1() == null) {
					task.setResource1(matchResult.getResource().getEmpName());
					task.setTaskType1(taskType.getTaskLabel(matchResult.getResource().getTaskType()));					
				} else if (task.getResource2() == null) {
					task.setResource2(matchResult.getResource().getEmpName());
					task.setTaskType2(taskType.getTaskLabel(matchResult.getResource().getTaskType()));
				} else if (task.getResource3() == null) {
					task.setResource3(matchResult.getResource().getEmpName());
					task.setTaskType3(taskType.getTaskLabel(matchResult.getResource().getTaskType()));
				} else if (task.getResource4() == null) {
					task.setResource4(matchResult.getResource().getEmpName());
					task.setTaskType4(taskType.getTaskLabel(matchResult.getResource().getTaskType()));
				}
			}
		}
		
		return task;
	}
	
}
