package com.ibm.ac.ghs.tasks;

public class AboveTheWingArrival extends AbstractTask {

	// Task Information
	private String paxF;//PY-Change
	private String paxC;
	private String paxY;
	private String wchr;
	private String wchc;
	private String um;
	private String blind;
	private String numAgents;
	
	// Flight Information
	private String aircraftFin;
	private String from;
	private String gate;
	private String eta;
	private String sta;
	private String load;
	
	private Object hc;
	
	public AboveTheWingArrival() {
		setCategory(CategoryType.atw_arr);
	}
	
	//PY-Change:start
	public String getPaxF() {
		return paxF;
	}
	public void setPaxF(String paxF) {
		this.paxF = paxF;
	}
	//PY-Change:end


	public String getPaxC() {
		return paxC;
	}
	public void setPaxC(String paxC) {
		this.paxC = paxC;
	}
	public String getPaxY() {
		return paxY;
	}
	public void setPaxY(String paxY) {
		this.paxY = paxY;
	}
	public String getWchr() {
		return wchr;
	}
	public void setWchr(String wchr) {
		this.wchr = wchr;
	}
	public String getWchc() {
		return wchc;
	}
	public void setWchc(String wchc) {
		this.wchc = wchc;
	}
	public String getUm() {
		return um;
	}
	public void setUm(String um) {
		this.um = um;
	}
	public String getBlind() {
		return blind;
	}
	public void setBlind(String blind) {
		this.blind = blind;
	}
	public String getNumAgents() {
		return numAgents;
	}
	public void setNumAgents(String numAgents) {
		this.numAgents = numAgents;
	}
	public String getAircraftFin() {
		return aircraftFin;
	}
	public void setAircraftFin(String aircraftFin) {
		this.aircraftFin = aircraftFin;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getGate() {
		return gate;
	}
	public void setGate(String gate) {
		this.gate = gate;
	}
	public String getEta() {
		return eta;
	}
	public void setEta(String eta) {
		this.eta = eta;
	}
	public String getSta() {
		return sta;
	}
	public void setSta(String sta) {
		this.sta = sta;
	}
	public String getLoad() {
		return load;
	}
	public void setLoad(String load) {
		this.load = load;
	}
	public Object getHc() {
		return hc;
	}
	public void setHc(Object hc) {
		this.hc = hc;
	}
}
