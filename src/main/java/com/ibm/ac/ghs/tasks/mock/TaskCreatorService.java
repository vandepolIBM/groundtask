package com.ibm.ac.ghs.tasks.mock;

import java.util.List;

import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.Alert;
import com.ibm.ac.ghs.tasks.TaskService;

public interface TaskCreatorService extends TaskService {

	public AbstractTask getTask(String employeeNumber, String taskId); 
	
	public void createTask(String employeeNumber, AbstractTask task, boolean silent);

	public void updateTask(String employeeNumber, AbstractTask task, boolean silent);
	
	public void deleteTask(String employeeNumber, String taskId);
	
	public List<String> getEmployees();
	
	public void createEmployee(String employeeNumber);
	
	public void deleteEmployee(String employeeNumber);
	
	public List<Alert> getAlerts(String employeeNumber);
	
	public Alert getAlert(String employeeNumber, String alertId);
	
	public void createAlert(String employeeNumber, String taskId, String message, String type);
	
	public void createDirectMessage(String employeeNumber, String from, String message);
}
