package com.ibm.ac.ghs.tasks.sabre.resource;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TaskTypeResource {

	private static Logger log = LogManager.getLogger(TaskTypeResource.class);
	
	private ResourceBundle resource;
	
	public TaskTypeResource() {
		resource = ResourceBundle.getBundle("tasks", Locale.US);
	}
	
	public String getTaskLabel(String taskType) {
				
	    String result = "Unknown TaskType:";
	    
        try {
            result = resource.getString(taskType) + ":";
        } catch (MissingResourceException e) {
			if (log.isDebugEnabled()) {
				log.debug("Unknown resourceTaskType=" + taskType);   
			}
        }	
        return result;
        }
	
}
