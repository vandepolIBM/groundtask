package com.ibm.ac.ghs.tasks.sabre.builder;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.BelowTheWingTurn;
import com.ibm.ac.ghs.tasks.sabre.resource.ResourceMatchResult;
import com.ibm.ac.ghs.tasks.sabre.resource.TaskTypeResource;
import com.sabre.streamline.FlightInformationInboundFlight;
import com.sabre.streamline.FlightInformationOutboundFlight;
import com.sabre.streamline.TaskInformation;

/**
 * Builder responsible for instantiating and population the client-side object
 * which represents a Below the Wing - Lav/Water category task.
 * 
 * @author J.Koch
 */
public class BelowTheWingTurnBuilder extends AbstractBuilder {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ibm.ac.ghs.tasks.sabre.builder.AbstractBuilder#build(com.sabre.streamline
	 * .TaskInformation)
	 */
	public AbstractTask build(TaskInformation taskInformation) {
		BelowTheWingTurn task = new BelowTheWingTurn();
		TaskTypeResource taskType = new TaskTypeResource();

		// setup the default task information
		initialize(task, taskInformation);

		// ensure we have flight information
		if (taskInformation.getFlightInformation() != null) {

			// ensure we have equipment info
			if (taskInformation.getFlightInformation().getEquipmentInformation() != null) {
				// set the aircraft type and fin
				task.setAircraftFin(taskInformation.getFlightInformation().getEquipmentInformation().getAircraftType() + "/"
						+ StringUtils.defaultString(taskInformation.getFlightInformation().getEquipmentInformation().getTailNumber()));

				// set the ground time
				String groundTime = formatGroundTime(taskInformation.getFlightInformation().getEquipmentInformation().getAircraftGroundTime());
				task.setGroundTime(StringUtils.defaultString(groundTime));
			}

			// setup the inbound flight
			FlightInformationInboundFlight inFlt = taskInformation.getFlightInformation().getInboundFlight();
			if (inFlt != null) {
				// grab the bags information
				if (inFlt.getBagsInformation() != null ) {
					task.setArrBags(convertToString(inFlt.getBagsInformation().getTotalBagCount()));
					task.setArrHcBags(convertToString(inFlt.getBagsInformation().getHotCnxBags()));
				}

				// get WCHC
				if (inFlt.getMiscInformation() != null) {
					task.setArrWchcPax(convertToString(inFlt.getMiscInformation().getTotalCarryOnOffWheelChairs()));
				}
				
				// fill the rest
				task.setArrFlightNum(inFlt.getFlightNumber());
				task.setArrFrom(StringUtils.defaultString(inFlt.getUplineStation()));
				task.setArrGate(inFlt.getGateInformation() != null ? StringUtils.defaultString(inFlt.getGateInformation().getGate()) : "");
				task.setArrETA(convertTime(inFlt.getEstimatedArrivalTime()));
				task.setArrLoad(inFlt.getLoadFactor() != null ? inFlt.getLoadFactor().toPlainString() : "");
				
				//PRJ1329 CR13 - Show ETD/A on Task List Screen
				//task.setEstimatedTime(task.getArrETA());
			}

			// setup the outbound flight
			FlightInformationOutboundFlight outFlt = taskInformation.getFlightInformation().getOutboundFlight();
			if (outFlt != null) {
				// grab the bags information
				if (outFlt.getBagsInformation() != null ) {
					task.setDepBags(convertToString(outFlt.getBagsInformation().getTotalBagCount()));
					task.setDepHcBags(convertToString(outFlt.getBagsInformation().getHotCnxBags()));
				}

				// get WCHC
				if (outFlt.getMiscInformation() != null) {
					task.setDepWchcPax(convertToString(outFlt.getMiscInformation().getTotalCarryOnOffWheelChairs()));
				}
				
				// set the rest
				task.setDepFlightNum(outFlt.getFlightNumber());
				task.setDepTo(StringUtils.defaultString(outFlt.getDownlineStation()));
				task.setDepGate(outFlt.getGateInformation() != null ? StringUtils.defaultString(outFlt.getGateInformation().getGate()) : "");
				task.setDepETD(convertTime(outFlt.getEstimatedDepartureTime()));
				task.setDepLoad(outFlt.getLoadFactor() != null ? outFlt.getLoadFactor().toPlainString() : "");
				
				//PRJ1329 CR13 - Show ETD/A on Task List Screen
				//if (task.getEstimatedTime().length() == 0) {
					//task.setEstimatedTime(task.getDepETD());	
				//}
			}
		}
		//PRJ1329 CR13 - ensure we have resource information
		List<ResourceMatchResult> matches = detectResources(taskInformation);
		for (ResourceMatchResult matchResult : matches) {
			if ("BTW_Turn".equals(matchResult.getRule().getName())) {
				if (task.getResource1() == null) {
					task.setResource1(matchResult.getResource().getEmpName());
					task.setTaskType1(taskType.getTaskLabel(matchResult.getResource().getTaskType()));					
				} else if (task.getResource2() == null) {
					task.setResource2(matchResult.getResource().getEmpName());
					task.setTaskType2(taskType.getTaskLabel(matchResult.getResource().getTaskType()));
				} else if (task.getResource3() == null) {
					task.setResource3(matchResult.getResource().getEmpName());
					task.setTaskType3(taskType.getTaskLabel(matchResult.getResource().getTaskType()));
				} else if (task.getResource4() == null) {
					task.setResource4(matchResult.getResource().getEmpName());
					task.setTaskType4(taskType.getTaskLabel(matchResult.getResource().getTaskType()));
				}
			} 
		}

		return task;
	}
}
