package com.ibm.ac.ghs.tasks;

public class BelowTheWingArrival extends AbstractTask {

	private String bags;
	private String hcBags;
	private String wchcPax;

	//PRJ1329 CR13 - Support for Resources and Task Types
	private String resource1;
	private String resource2;
	private String resource3;
	private String resource4;
	private String taskType1;
	private String taskType2;
	private String taskType3;
	private String taskType4;	
	
	private String aircraftFin;
	private String arrival;
	private String eta;
	private String sta;
	private String gate;
	private String load;
	
	public BelowTheWingArrival() {
		this.setCategory(CategoryType.btw_arr);
	}

	public String getBags() {
		return bags;
	}

	public void setBags(String bags) {
		this.bags = bags;
	}

	public String getHcBags() {
		return hcBags;
	}

	public void setHcBags(String hcBags) {
		this.hcBags = hcBags;
	}

	public String getWchcPax() {
		return wchcPax;
	}

	public void setWchcPax(String wchcPax) {
		this.wchcPax = wchcPax;
	}

	public String getAircraftFin() {
		return aircraftFin;
	}

	public void setAircraftFin(String aircraftFin) {
		this.aircraftFin = aircraftFin;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	public String getSta() {
		return sta;
	}

	public void setSta(String sta) {
		this.sta = sta;
	}
	
	public String getGate() {
		return gate;
	}

	public void setGate(String gate) {
		this.gate = gate;
	}

	public String getLoad() {
		return load;
	}

	public void setLoad(String load) {
		this.load = load;
	}

	public String getResource1() {
		return resource1;
	}

	public void setResource1(String resource1) {
		this.resource1 = resource1;
	}

	public String getResource2() {
		return resource2;
	}

	public void setResource2(String resource2) {
		this.resource2 = resource2;
	}

	public String getResource3() {
		return resource3;
	}

	public void setResource3(String resource3) {
		this.resource3 = resource3;
	}

	public String getResource4() {
		return resource4;
	}

	public void setResource4(String resource4) {
		this.resource4 = resource4;
	}

	public String getTaskType1() {
		return taskType1;
	}
	
	public void setTaskType1(String taskType1) {
		this.taskType1 = taskType1;
	}

	public String getTaskType2() {
		return taskType2;
	}
	
	public void setTaskType2(String taskType2) {
		this.taskType2 = taskType2;
	}

	public String getTaskType3() {
		return taskType3;
	}

	public void setTaskType3(String taskType3) {
		this.taskType3 = taskType3;
	}

	public String getTaskType4() {
		return taskType4;
	}

	public void setTaskType4(String taskType4) {
		this.taskType4 = taskType4;
	}	
}
