package com.ibm.ac.ghs.tasks.sabre.builder;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ibm.ac.ghs.tasks.AboveTheWingArrival;
import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.Connection;
import com.sabre.streamline.FlightInformationInboundFlight;
import com.sabre.streamline.TaskInformation;

/**
 * Builder responsible for instantiating and population the client-side object
 * which represents a Above The Wing - Departure category task.
 * 
 * @author M.Kurabi
 */
public class AboveTheWingArrivalBuilder extends AbstractBuilder {

	/*
	 * (non-Javadoc)
	 * @see com.ibm.ac.ghs.tasks.sabre.builder.AbstractBuilder#build(com.sabre.streamline.TaskInformation)
	 */
	public AbstractTask build(TaskInformation taskInformation) {
		AboveTheWingArrival atwArr = new AboveTheWingArrival();

		// setup the default task information
		initialize(atwArr, taskInformation);

		// ensure we have flight information
		if (taskInformation.getFlightInformation() != null) {
			
			// ensure we have outbound flight information
			if (taskInformation.getFlightInformation().getInboundFlight() != null) {
				FlightInformationInboundFlight inFlt = taskInformation.getFlightInformation().getInboundFlight();
				
				atwArr.setEta(convertTime(inFlt.getEstimatedArrivalTime())); // set the ETD
				atwArr.setFrom(StringUtils.defaultString(inFlt.getUplineStation())); // set the destination
				atwArr.setGate(inFlt.getGateInformation() != null ? StringUtils.defaultString(inFlt.getGateInformation().getGate()) : "");
				atwArr.setLoad(inFlt.getLoadFactor().toPlainString());
				
				//PRJ1329 CR13 - Show ETD/A on Task List Screen
				//atwArr.setEstimatedTime(atwArr.getEta());
				
				// ensure we have pax information
				if (inFlt.getPaxInformation() != null) {
					
					// set the pax information
					//PY-Change:start
					if (inFlt.getPaxInformation().getPaxFClass() != null) {
						atwArr.setPaxF(inFlt.getPaxInformation().getPaxFClass().toString());
					}
					//PY-Change:end
					
					if (inFlt.getPaxInformation().getPaxCClass() != null) {
						atwArr.setPaxC(inFlt.getPaxInformation().getPaxCClass().toString());
					}
					if (inFlt.getPaxInformation().getPaxYClass() != null) {
						atwArr.setPaxY(inFlt.getPaxInformation().getPaxYClass().toString());
					}
					if (inFlt.getPaxInformation().getTotalUMCount() != null) {
						atwArr.setUm(inFlt.getPaxInformation().getTotalUMCount().toString());
					}
					if (inFlt.getPaxInformation().getTotalBlndCount() != null) {
						atwArr.setBlind(inFlt.getPaxInformation().getTotalBlndCount().toString());
					}
				}
				
				// ensure we have misc information
				if (inFlt.getMiscInformation() != null) {
					// set the wheel chair info
					if (inFlt.getMiscInformation().getTotalWheelChairs() != null) {
						atwArr.setWchr(inFlt.getMiscInformation().getTotalWheelChairs().toString());
					}
					if (inFlt.getMiscInformation().getTotalCarryOnOffWheelChairs() != null) {
						atwArr.setWchc(inFlt.getMiscInformation().getTotalCarryOnOffWheelChairs().toString());
					}
				}
				
				// setup the hot connections
				atwArr.setHc("#");
				List<Connection> connectionList = buildConnectionList(taskInformation);
				if (connectionList != null) {
					atwArr.setHc(connectionList);
				} 				
			}
			
			// ensure we have equipment information
			if (taskInformation.getFlightInformation().getEquipmentInformation() != null) {
				// set the aircraft type and fin
				atwArr.setAircraftFin(taskInformation.getFlightInformation().getEquipmentInformation().getAircraftType() + "/"
						+ StringUtils.defaultString(taskInformation.getFlightInformation().getEquipmentInformation().getTailNumber()));
			}
		}
		
		// ensure we have resource information
		if (taskInformation.getTask().getNumberofTasks() != null) {
			// set the number of resource
			atwArr.setNumAgents(taskInformation.getTask().getNumberofTasks().toString());	
		}
		
		return atwArr;
	}
	
}
