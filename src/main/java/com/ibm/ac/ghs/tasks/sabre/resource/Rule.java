package com.ibm.ac.ghs.tasks.sabre.resource;

import com.sabre.streamline.Resource;
import com.sabre.streamline.TaskCategory;

public interface Rule {

	public String getName();
	
	public ResourceMatchResult matches(TaskCategory taskCategory, String taskType, Resource resource);
}
