package com.ibm.ac.ghs.tasks.sabre.builder;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.Cabin;
import com.ibm.ac.ghs.tasks.sabre.resource.ResourceMatchResult;
import com.ibm.ac.ghs.tasks.sabre.resource.TaskTypeResource;
import com.sabre.streamline.FlightInformationInboundFlight;
import com.sabre.streamline.FlightInformationOutboundFlight;
import com.sabre.streamline.TaskInformation;

/**
 * Builder responsible for instantiating and population the client-side object
 * which represents a cabin category task.
 * 
 * @author Jeremy Koch, M.Kurabi
 */
public class CabinBuilder extends AbstractBuilder {

	private Logger log = LogManager.getLogger(getClass());

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ibm.ac.ghs.tasks.sabre.builder.AbstractBuilder#build(com.sabre.streamline
	 * .TaskInformation)
	 */
	public AbstractTask build(TaskInformation taskInformation) {
		Cabin cabin = new Cabin();
		TaskTypeResource taskType = new TaskTypeResource();

		// setup the default task information
		initialize(cabin, taskInformation);

		// set the num crew
		if (taskInformation.getTask().getNumberofTasks() != null) {
			cabin.setNumCrew(taskInformation.getTask().getNumberofTasks().intValue());
		}

		// ensure we have flight info
		if (taskInformation.getFlightInformation() != null) {

			// ensure we have equipment info
			if (taskInformation.getFlightInformation().getEquipmentInformation() != null) {
				// set the aircraft type and fin
				cabin.setAircraftFin(taskInformation.getFlightInformation().getEquipmentInformation().getAircraftType() + "/"
						+ StringUtils.defaultString(taskInformation.getFlightInformation().getEquipmentInformation().getTailNumber()));

				// set the ground time
				String groundTime = formatGroundTime(taskInformation.getFlightInformation().getEquipmentInformation().getAircraftGroundTime());
				cabin.setGroundTime(StringUtils.defaultString(groundTime));
			}

			// setup the inbound flight
			FlightInformationInboundFlight inFlt = taskInformation.getFlightInformation().getInboundFlight();
			if (inFlt != null) {
				cabin.setArrFlightNum(inFlt.getFlightNumber());
				cabin.setArrFrom(StringUtils.defaultString(inFlt.getUplineStation()));
				cabin.setArrGate(inFlt.getGateInformation() != null ? StringUtils.defaultString(inFlt.getGateInformation().getGate()) : "");
				cabin.setArrETA(convertTime(inFlt.getEstimatedArrivalTime()));
				cabin.setArrLoad(inFlt.getLoadFactor() != null ? inFlt.getLoadFactor().toPlainString() : "");
				if (inFlt.getMiscInformation() != null && inFlt.getMiscInformation().getTotalCarryOnOffWheelChairs() != null) {
					cabin.setArrWchcPax(inFlt.getMiscInformation().getTotalCarryOnOffWheelChairs().toString());
				}
				//PRJ1329 CR13 - Sort the data by Flight ETD or ETA instead of Start Time
				//**cabin.setSort((inFlt.getEstimatedArrivalTime().toGregorianCalendar().getTime().getTime() / 1000));
				
				//PRJ1329 CR13 - Show ETD/A on Task List Screen
				//cabin.setEstimatedTime(cabin.getArrETA());
			}

			// setup the outbound flight
			FlightInformationOutboundFlight outFlt = taskInformation.getFlightInformation().getOutboundFlight();
			if (outFlt != null) {
				cabin.setDepFlightNum(outFlt.getFlightNumber());
				cabin.setDepTo(StringUtils.defaultString(outFlt.getDownlineStation()));
				cabin.setDepGate(outFlt.getGateInformation() != null ? StringUtils.defaultString(outFlt.getGateInformation().getGate()) : "");
				cabin.setDepETD(convertTime(outFlt.getEstimatedDepartureTime()));
				cabin.setDepLoad(outFlt.getLoadFactor() != null ? outFlt.getLoadFactor().toPlainString() : "");
				if (outFlt.getMiscInformation() != null && outFlt.getMiscInformation().getTotalCarryOnOffWheelChairs() != null) {
					cabin.setDepWchcPax(outFlt.getMiscInformation().getTotalCarryOnOffWheelChairs().toString());
				}
				//PRJ1329 CR13 - Show ETD/A on Task List Screen
				//if (cabin.getEstimatedTime().length() == 0) {
					//cabin.setEstimatedTime(cabin.getDepETD());	
				//}
				
			}
		}
		//PRJ1329 CR13 - ensure we have resource information
		List<ResourceMatchResult> matches = detectResources(taskInformation);
		for (ResourceMatchResult matchResult : matches) {
			if ("Cabin".equals(matchResult.getRule().getName())) {
				if (cabin.getResource1() == null) {
					cabin.setResource1(matchResult.getResource().getEmpName());
					cabin.setTaskType1(taskType.getTaskLabel(matchResult.getResource().getTaskType()));					
				} else if (cabin.getResource2() == null) {
					cabin.setResource2(matchResult.getResource().getEmpName());
					cabin.setTaskType2(taskType.getTaskLabel(matchResult.getResource().getTaskType()));
				} else if (cabin.getResource3() == null) {
					cabin.setResource3(matchResult.getResource().getEmpName());
					cabin.setTaskType3(taskType.getTaskLabel(matchResult.getResource().getTaskType()));
				} else if (cabin.getResource4() == null) {
					cabin.setResource4(matchResult.getResource().getEmpName());
					cabin.setTaskType4(taskType.getTaskLabel(matchResult.getResource().getTaskType()));
				}
			} 
		}

		return cabin;
	}
}
