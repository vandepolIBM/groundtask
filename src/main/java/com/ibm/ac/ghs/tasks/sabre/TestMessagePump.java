package com.ibm.ac.ghs.tasks.sabre;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;
import org.springframework.jms.core.JmsTemplate;

public class TestMessagePump {

	private Map<String, TimerTask> timerMap = new HashMap<String, TimerTask>();
	
	private JmsTemplate jmsTemplate;

	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void start(final String employeeNumber, final String alertText, int interval) {
		TimerTask task = new TimerTask() {
			int i;
			String xml_add = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><StaffManagerMobileMessage xsi:noNamespaceSchemaLocation=\"../src/xsd/staffmanager-message.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">	<MessageReference>		<Timestamp>2001-12-17T09:35:47.0Z</Timestamp>		<CreatorReference>123</CreatorReference>	</MessageReference>	<TaskInformation>		<Task>			<SMInstanceId>123</SMInstanceId>			<TaskType>InFlt747</TaskType>			<StartTime>2008-09-20T09:30:00.0Z</StartTime>			<EndTime>2008-09-20T10:30:00.0Z</EndTime>			<TaskStatus>Acknowledged</TaskStatus>			<TaskPosition>A2</TaskPosition>			<TaskCategory>Cabin</TaskCategory>			<NumberofTasks>2</NumberofTasks>			<TransactionType>Add</TransactionType>		</Task>		<FlightInformation>			<InboundFlight>				<FlightNumber>AC467</FlightNumber>				<UplineStation>YYC</UplineStation>				<EstimatedArrivalTime>2008-09-20T09:20:00.0Z</EstimatedArrivalTime>				<FlightStatus>OnGround</FlightStatus>				<GateInformation>					<Gate>A2</Gate>					<Stand>A100</Stand>				</GateInformation>				<MiscInformation>					<TotalCarryOnOffWheelChairs>3</TotalCarryOnOffWheelChairs>				</MiscInformation>				<LoadFactor>40.0</LoadFactor>			</InboundFlight>			<OutboundFlight>				<FlightNumber>AC356</FlightNumber>				<DownlineStation>YYC</DownlineStation>				<EstimatedDepartureTime>2008-09-20T11:30:00.0Z</EstimatedDepartureTime>				<FlightStatus>Scheduled</FlightStatus>				<GateInformation>					<Gate>A2</Gate>					<Stand>A100</Stand>				</GateInformation>				<LoadFactor>0.0</LoadFactor>			</OutboundFlight>			<EquipmentInformation>				<AircraftType>747</AircraftType>				<TailNumber>B45</TailNumber>				<AircraftGroundTime>90</AircraftGroundTime>			</EquipmentInformation>		</FlightInformation>	</TaskInformation>	<AlertInformation>		<Alert>			<AlertID>0</AlertID>			<AlertString>				<EnglishString>New Task - Gate Changed From A1 to A2</EnglishString>			</AlertString>			<AlertSeverity>Critical</AlertSeverity>			<DisplayAlert>Yes</DisplayAlert>		</Alert>	</AlertInformation>	<EmployeeInformation>		<EmpNumber>111</EmpNumber>		<EmpName>Alex Smith</EmpName>		<EmpPhoneNumber>8765432391</EmpPhoneNumber>	</EmployeeInformation></StaffManagerMobileMessage>";
			String xml_delete = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><StaffManagerMobileMessage xsi:noNamespaceSchemaLocation=\"../src/xsd/staffmanager-message.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">	<MessageReference>		<Timestamp>2001-12-17T09:35:47.0Z</Timestamp>		<CreatorReference>123</CreatorReference>	</MessageReference>	<TaskInformation>		<Task>			<SMInstanceId>123</SMInstanceId>			<TaskType>InFlt747</TaskType>			<StartTime>2008-09-20T09:30:00.0Z</StartTime>			<EndTime>2008-09-20T10:30:00.0Z</EndTime>			<TaskStatus>Acknowledged</TaskStatus>			<TaskPosition>A2</TaskPosition>			<TaskCategory>Cabin</TaskCategory>			<NumberofTasks>2</NumberofTasks>			<TransactionType>Delete</TransactionType>		</Task>		<FlightInformation>			<InboundFlight>				<FlightNumber>AC467</FlightNumber>				<UplineStation>YYC</UplineStation>				<EstimatedArrivalTime>2008-09-20T09:20:00.0Z</EstimatedArrivalTime>				<FlightStatus>OnGround</FlightStatus>				<GateInformation>					<Gate>A2</Gate>					<Stand>A100</Stand>				</GateInformation>				<MiscInformation>					<TotalCarryOnOffWheelChairs>3</TotalCarryOnOffWheelChairs>				</MiscInformation>				<LoadFactor>40.0</LoadFactor>			</InboundFlight>			<OutboundFlight>				<FlightNumber>AC356</FlightNumber>				<DownlineStation>YYC</DownlineStation>				<EstimatedDepartureTime>2008-09-20T11:30:00.0Z</EstimatedDepartureTime>				<FlightStatus>Scheduled</FlightStatus>				<GateInformation>					<Gate>A2</Gate>					<Stand>A100</Stand>				</GateInformation>				<LoadFactor>0.0</LoadFactor>			</OutboundFlight>			<EquipmentInformation>				<AircraftType>747</AircraftType>				<TailNumber>B45</TailNumber>				<AircraftGroundTime>90</AircraftGroundTime>			</EquipmentInformation>		</FlightInformation>	</TaskInformation>	<AlertInformation>		<Alert>			<AlertID>0</AlertID>			<AlertString>				<EnglishString>Delete Task</EnglishString>			</AlertString>			<AlertSeverity>Critical</AlertSeverity>			<DisplayAlert>Yes</DisplayAlert>		</Alert>	</AlertInformation>	<EmployeeInformation>		<EmpNumber>111</EmpNumber>		<EmpName>Alex Smith</EmpName>		<EmpPhoneNumber>8765432391</EmpPhoneNumber>	</EmployeeInformation></StaffManagerMobileMessage>";

			public void run() {
				try {
					String xml;
					if (i % 2 == 0) {
						xml=xml_add;
					} else {
						xml=xml_delete;
					}
					// modify the message
					xml = StringUtils.replace(xml, "<AlertID>0</AlertID>", "<AlertID>" + (++i) + "</AlertID>");
					xml = StringUtils.replace(xml, "<EmpNumber>111", "<EmpNumber>" + employeeNumber);
					if (alertText != null) {
						xml = StringUtils.replace(xml, "<EnglishString>Gate Changed From A1 to A2", "<EnglishString>" + alertText);
					}
					
					// send it!
					jmsTemplate.convertAndSend(xml);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(task, 0, interval);
		timerMap.put(employeeNumber, task);
	}
	
	public void stop(String employeeNumber) {
		timerMap.remove(employeeNumber).cancel();
	}
	
	public void stopAll() {
		for (String employeeNumber : timerMap.keySet()) {	
			stop(employeeNumber);
		}
	}
}
