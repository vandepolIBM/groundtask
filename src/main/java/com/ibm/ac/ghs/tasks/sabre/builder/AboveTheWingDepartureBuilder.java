package com.ibm.ac.ghs.tasks.sabre.builder;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ibm.ac.ghs.tasks.AboveTheWingDeparture;
import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.sabre.resource.ResourceMatchResult;
import com.sabre.streamline.FlightInformationOutboundFlight;
import com.sabre.streamline.TaskInformation;

/**
 * Builder responsible for instantiating and population the client-side object
 * which represents a Above The Wing - Departure category task.
 * 
 * @author M.Kurabi
 */
public class AboveTheWingDepartureBuilder extends AbstractBuilder {

	private Logger log = LogManager.getLogger(getClass());

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ibm.ac.ghs.tasks.sabre.builder.AbstractBuilder#build(com.sabre.streamline
	 * .TaskInformation)
	 */
	public AbstractTask build(TaskInformation taskInformation) {
		AboveTheWingDeparture atwDep = new AboveTheWingDeparture();

		// setup the default task information
		initialize(atwDep, taskInformation);

		// set the num of GT_SPATT
		if (taskInformation.getTask().getNumberofTasks() != null) {
			atwDep.setNumOfGT_SPATT(taskInformation.getTask().getNumberofTasks().toString());
		}

		// ensure we have flight information
		if (taskInformation.getFlightInformation() != null) {

			// ensure we have outbound flight information
			if (taskInformation.getFlightInformation().getOutboundFlight() != null) {
				FlightInformationOutboundFlight outFlt = taskInformation.getFlightInformation().getOutboundFlight();

				atwDep.setEtd(convertTime(outFlt.getEstimatedDepartureTime())); // set
				atwDep.setTo(StringUtils.defaultString(outFlt.getDownlineStation())); // set
				atwDep.setGate(outFlt.getGateInformation() != null ? StringUtils.defaultString(outFlt.getGateInformation().getGate()) : "");
				atwDep.setLoad(outFlt.getLoadFactor().toPlainString());

				//PRJ1329 CR13 - Show ETD/A on Task List Screen
				//atwDep.setEstimatedTime(atwDep.getEtd());				
				
				// ensure we have pax information
				if (outFlt.getPaxInformation() != null) {

					//PY-Change:start
					if (outFlt.getPaxInformation().getPaxFClass() != null) {
						atwDep.setPaxF(outFlt.getPaxInformation().getPaxFClass().toString());
					}
					//PY-Change:end
					
					if (outFlt.getPaxInformation().getPaxCClass() != null) {
						atwDep.setPaxC(outFlt.getPaxInformation().getPaxCClass().toString());
					}
					if (outFlt.getPaxInformation().getPaxYClass() != null) {
						atwDep.setPaxY(outFlt.getPaxInformation().getPaxYClass().toString());
					}
					if (outFlt.getPaxInformation().getTotalUMCount() != null) {
						atwDep.setUm(outFlt.getPaxInformation().getTotalUMCount().toString());
					}
					if (outFlt.getPaxInformation().getTotalBlndCount() != null) {
						atwDep.setBlind(outFlt.getPaxInformation().getTotalBlndCount().toString());
					}

				}

				// ensure we have misc information
				if (outFlt.getMiscInformation() != null) {

					// set the wheel chair info
					if (outFlt.getMiscInformation().getTotalWheelChairs() != null) {
						atwDep.setWchr(outFlt.getMiscInformation().getTotalWheelChairs().toString());
					}
					if (outFlt.getMiscInformation().getTotalCarryOnOffWheelChairs() != null) {
						atwDep.setWchc(outFlt.getMiscInformation().getTotalCarryOnOffWheelChairs().toString());
					}
				}

			}

			// ensure we have equipment information
			if (taskInformation.getFlightInformation().getEquipmentInformation() != null) {
				// set the aircraft type and fin
				atwDep.setAircraftFin(taskInformation.getFlightInformation().getEquipmentInformation().getAircraftType() + "/"
						+ StringUtils.defaultString(taskInformation.getFlightInformation().getEquipmentInformation().getTailNumber()));
			}
		}

		// ensure we have resource information
		List<ResourceMatchResult> matches = detectResources(taskInformation);
		for (ResourceMatchResult matchResult : matches) {
			if ("ATWDep_Control".equals(matchResult.getRule().getName())) {
				if (atwDep.getControlAgent() == null) {
					atwDep.setControlAgent(matchResult.getResource().getEmpName());
				}
			} else if ("ATWDep_Assist".equals(matchResult.getRule().getName())) {
				if (atwDep.getAssistAgent1() == null) {
					atwDep.setAssistAgent1(matchResult.getResource().getEmpName());
				} else if (atwDep.getAssistAgent2() == null) {
					atwDep.setAssistAgent2(matchResult.getResource().getEmpName());
				} else if (atwDep.getAssistAgent3() == null) {
					atwDep.setAssistAgent3(matchResult.getResource().getEmpName());
				}
			}
		}

		return atwDep;
	}
}
