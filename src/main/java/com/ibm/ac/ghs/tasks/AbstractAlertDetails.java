package com.ibm.ac.ghs.tasks;

import java.io.Serializable;

public class AbstractAlertDetails implements Serializable {

	protected String id;
	protected CategoryType category;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CategoryType getCategory() {
		return category;
	}
	
	public void setCategory(CategoryType category) {
		this.category = category;
	}
}
