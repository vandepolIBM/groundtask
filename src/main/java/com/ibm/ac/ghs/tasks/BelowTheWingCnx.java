package com.ibm.ac.ghs.tasks;

import java.util.List;

public class BelowTheWingCnx extends AbstractTask {

	private boolean partialUpdate;
	private Object hc;
	private String hcBags;

	private String aircraftFin;
	private String arrival;
	private String eta;
	private String sta;
	private String gate;
	
	public BelowTheWingCnx() {
		this.setCategory(CategoryType.btw_cnx);
	}
	
	public boolean isPartialUpdate() {
		return partialUpdate;
	}

	public void setPartialUpdate(boolean partialUpdate) {
		this.partialUpdate = partialUpdate;
	}

	public Object getHc() {
		return hc;
	}

	public void setHc(Object hc) {
		this.hc = hc;
	}

	public String getHcBags() {
		return hcBags;
	}

	public void setHcBags(String hcBags) {
		this.hcBags = hcBags;
	}

	public String getAircraftFin() {
		return aircraftFin;
	}

	public void setAircraftFin(String aircraftFin) {
		this.aircraftFin = aircraftFin;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	public String getSta() {
		return sta;
	}

	public void setSta(String sta) {
		this.sta = sta;
	}

	public String getGate() {
		return gate;
	}

	public void setGate(String gate) {
		this.gate = gate;
	}
	
}
