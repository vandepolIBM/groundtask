package com.ibm.ac.ghs.tasks.sabre;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeFactory;

import com.sabre.streamline.GetTasks;
import com.sabre.streamline.MessageReference;
import com.sabre.streamline.ObjectFactory;
import com.sabre.streamline.RegisterDevice;
import com.sabre.streamline.StaffManagerMobileDeviceRegister;
import com.sabre.streamline.StaffManagerMobileDeviceTaskStatusUpdate;
import com.sabre.streamline.StaffManagerTaskRequest;
import com.sabre.streamline.UpdateTaskStatus;
import com.sabre.streamline.StaffManagerMobileDeviceRegister.MobileRequest;
import com.sabre.streamline.StaffManagerMobileDeviceRegister.MobileRequest.RegisterRequest;
import com.sabre.streamline.StaffManagerMobileDeviceRegister.MobileRequest.UnRegisterRequest;
import com.sabre.streamline.StaffManagerMobileDeviceTaskStatusUpdate.MobileTaskStatusUpdate;

public class SabreWebServiceRequestFactory {
	
	private static final TimeZone DEFAULT_TIMEZONE = TimeZone.getTimeZone("Canada/Eastern");
	
	private ObjectFactory factory = new ObjectFactory();

	private MessageReference createMessageReference() {
		MessageReference messageReference = new MessageReference();
		messageReference.setCreatorReference(Long.toHexString(new Date().getTime()));
		try {
			messageReference.setTimestamp(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar(DEFAULT_TIMEZONE)));
			messageReference.getTimestamp().setTimezone(60*-5);
		} catch (Exception e) {
		}
		return messageReference;
	}

	public GetTasks createGetTasks(String id) {
		GetTasks req = factory.createGetTasks();
		req.setStaffManagerTaskRequest(new StaffManagerTaskRequest());
		req.getStaffManagerTaskRequest().setMessageReference(createMessageReference());
		req.getStaffManagerTaskRequest().setEmpNumber(id);
		return req;
	}
	
	public RegisterDevice createRegisterDevice(String id, boolean register) {
		RegisterDevice req = factory.createRegisterDevice();
		req.setStaffManagerMobileDeviceRegister(new StaffManagerMobileDeviceRegister());
		req.getStaffManagerMobileDeviceRegister().setMessageReference(createMessageReference());
		req.getStaffManagerMobileDeviceRegister().setMobileRequest(new MobileRequest());
		if (register) {
			req.getStaffManagerMobileDeviceRegister().getMobileRequest().setRegisterRequest(new RegisterRequest());
			req.getStaffManagerMobileDeviceRegister().getMobileRequest().getRegisterRequest().setEmpNumber(id);
			
			// we aren't currently using a phone number on the device, but sabre requires this field to be populated
			req.getStaffManagerMobileDeviceRegister().getMobileRequest().getRegisterRequest().setPhoneNumber(id);
		} else {
			req.getStaffManagerMobileDeviceRegister().getMobileRequest().setUnRegisterRequest(new UnRegisterRequest());
			req.getStaffManagerMobileDeviceRegister().getMobileRequest().getUnRegisterRequest().setEmpNumber(id);
		}
		return req;
	}

	public UpdateTaskStatus createUpdateTaskStatusRequest(String empId) {
		UpdateTaskStatus req = factory.createUpdateTaskStatus();
		req.setStaffManagerMobileDeviceTaskStatusUpdate(new StaffManagerMobileDeviceTaskStatusUpdate());
		req.getStaffManagerMobileDeviceTaskStatusUpdate().setMessageReference(createMessageReference());
		req.getStaffManagerMobileDeviceTaskStatusUpdate().setMobileTaskStatusUpdate(new MobileTaskStatusUpdate());
		req.getStaffManagerMobileDeviceTaskStatusUpdate().getMobileTaskStatusUpdate().setEmpNumber(empId);
		return req;
	}
}
