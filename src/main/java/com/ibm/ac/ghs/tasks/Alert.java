package com.ibm.ac.ghs.tasks;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class Alert implements Serializable {

	private static final long serialVersionUID = -5410506910460891498L;

	public static final String TYPE_NEW = "n";
	public static final String TYPE_EDIT = "e";
	public static final String TYPE_DELETE = "d";
	public static final String TYPE_DIRECT_MESSAGE = "m";
	public static final String TYPE_OTHER = "o";

	private String id;
	private AbstractAlertDetails task;
	private String type;
	private Message message;
	private int severity;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public AbstractAlertDetails getTask() {
		return task;
	}

	public void setTask(AbstractAlertDetails task) {
		this.task = task;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public int getSeverity() {
		return severity;
	}

	public void setSeverity(int severity) {
		this.severity = severity;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	public static class Message implements Serializable {
		
		private static final long serialVersionUID = 4335932905706486297L;

		private String en;
		private String fr;

		public Message() {
			this("", "");
		}
		
		public Message(String en, String fr) {
			this.en = en;
			this.fr = fr;
		}
		
		public String getEn() {
			return en;
		}

		public void setEn(String en) {
			this.en = en;
		}

		public String getFr() {
			return fr;
		}

		public void setFr(String fr) {
			this.fr = fr;
		}
		
		@Override
		public String toString() {
			return new StringBuilder().append(en).append('|').append(fr).toString();
		}
	}
}
