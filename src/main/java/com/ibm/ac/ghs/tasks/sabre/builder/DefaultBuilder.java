package com.ibm.ac.ghs.tasks.sabre.builder;

import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.DefaultTask;
import com.sabre.streamline.TaskInformation;

public class DefaultBuilder extends AbstractBuilder {

	public AbstractTask build(TaskInformation taskInformation) {
		DefaultTask task = new DefaultTask();
		initialize(task, taskInformation);
		return task;
	}
}
