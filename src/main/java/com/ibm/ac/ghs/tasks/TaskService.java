package com.ibm.ac.ghs.tasks;

import java.util.List;

public interface TaskService {

	public void login(String employeeNumber);
	
	public void logoff(String employeeNumber);
	
	public List<AbstractTask> getTasks(String employeeNumber);
	
	public void acknowledgeTask(String employeeNumber, String taskId);
	
	public void startTask(String employeeNumber, String taskId);
	
	public void stopTask(String employeeNumber, String taskId);
}