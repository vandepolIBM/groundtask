package com.ibm.ac.ghs.tasks.sabre.resource;

import com.sabre.streamline.Resource;

public class ResourceMatchResult {

	private Rule rule;
	private Resource resource;

	public ResourceMatchResult(Rule rule, Resource resource) {
		this.rule = rule;
		this.resource = resource;
	}

	public Rule getRule() {
		return rule;
	}

	public Resource getResource() {
		return resource;
	}
}
