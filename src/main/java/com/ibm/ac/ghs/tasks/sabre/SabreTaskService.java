package com.ibm.ac.ghs.tasks.sabre;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.TaskService;
import com.ibm.ac.ghs.tasks.sabre.builder.TaskFactory;
import com.sabre.streamline.GetTasks;
import com.sabre.streamline.GetTasksResponse;
import com.sabre.streamline.RegisterDevice;
import com.sabre.streamline.RegisterDeviceResponse;
import com.sabre.streamline.TaskInformation;
import com.sabre.streamline.TaskStatus;
import com.sabre.streamline.UpdateTaskStatus;
import com.sabre.streamline.UpdateTaskStatusResponse;

public class SabreTaskService implements TaskService {

	private Logger log = LogManager.getLogger(getClass());

	private WebServiceTemplate webServiceTemplate;
	private SabreWebServiceMessageCallback messageCallback;
	private SabreWebServiceRequestFactory sabreWebServiceRequestFactory = new SabreWebServiceRequestFactory();
	private TaskFactory taskFactory;
	
	public SabreTaskService() {
		this("", "");
	}

	public SabreTaskService(String username, String password) {
		this.messageCallback = new SabreWebServiceMessageCallback(username, password);
	}

	public void setWebServiceTemplate(WebServiceTemplate webServiceTemplate) {
		this.webServiceTemplate = webServiceTemplate;
	}

	public void setTaskFactory(TaskFactory taskFactory) {
		this.taskFactory = taskFactory;
	}
	
	public List<AbstractTask> getTasks(String employeeNumber) {
		// log
		if (log.isDebugEnabled()) {
			log.debug("Sending GetTasks request, employeeNumber=" + employeeNumber);
		}

		try {
			// create the request
			GetTasks req = sabreWebServiceRequestFactory.createGetTasks(employeeNumber);

			// send the request
			GetTasksResponse resp = (GetTasksResponse) webServiceTemplate.marshalSendAndReceive(req, messageCallback);

			// log response details
			log.debug("GetTasksResponse: employee="
					+ employeeNumber
					+ ", # tasks="
					+ (resp.getStaffManagerInformation().getTasksCollection() != null ? resp.getStaffManagerInformation().getTasksCollection().getTaskInformation().size()
							: 0));

			// iterate through the list of tasks and create domain objects
			List<AbstractTask> tasks = new ArrayList<AbstractTask>();
			for (TaskInformation taskInformation : resp.getStaffManagerInformation().getTasksCollection().getTaskInformation()) {
				AbstractTask task = taskFactory.createTask(taskInformation);
				if (task != null) {
					log.debug("GetTasksResponse: employee=" + employeeNumber + ", task=" + task);
					tasks.add(task);
				}
			}
			return tasks;

		} catch (Exception e) {
			log.error("Error building task list, emp=" + employeeNumber, e);
			e.printStackTrace();
			return new ArrayList<AbstractTask>();
		}
	}

	public void login(String employeeNumber) {
		// log
		if (log.isDebugEnabled()) {
			log.debug("Sending RegisterDevice login request, employeeNumber=" + employeeNumber);
		}

		try {
			// create the request
			RegisterDevice req = sabreWebServiceRequestFactory.createRegisterDevice(employeeNumber, true);

			// send the request
			RegisterDeviceResponse resp = null;
			try {
			 resp = (RegisterDeviceResponse) webServiceTemplate.marshalSendAndReceive(req, messageCallback);
			} catch (Exception e) {
				log.debug("DVP - exception in login" + e.getMessage());
				e.printStackTrace();
				throw e;
			}
			
			
			
			if (resp.getStaffManagerMobileDeviceRegister().getMobileResponse().getAcknowledgement() != null) {
				log.debug("RegisterDeviceResponse - Acknowledged");
			} else {
				log.debug("RegistedDeviceResonse - NotActioned, reason="
						+ resp.getStaffManagerMobileDeviceRegister().getMobileResponse().getNotActioned().getReason());
			}

		} catch (Exception e) {
			log.error("Error sending login, emp=" + employeeNumber, e);
		}
	}

	public void logoff(String employeeNumber) {
		// log
		if (log.isDebugEnabled()) {
			log.debug("Sending RegisterDevice logoff request, employeeNumber=" + employeeNumber);
		}

		try {
			// create the request
			RegisterDevice req = sabreWebServiceRequestFactory.createRegisterDevice(employeeNumber, false);

			// send the request
			RegisterDeviceResponse resp = (RegisterDeviceResponse) webServiceTemplate.marshalSendAndReceive(req, messageCallback);

			if (resp.getStaffManagerMobileDeviceRegister().getMobileResponse().getAcknowledgement() != null) {
				log.debug("RegisterDeviceResponse - Acknowledged");
			} else {
				log.debug("RegistedDeviceResonse - NotActioned, reason="
						+ resp.getStaffManagerMobileDeviceRegister().getMobileResponse().getNotActioned().getReason());
			}

		} catch (Exception e) {
			log.error("Error sending logoff, emp=" + employeeNumber, e);
		}
	}

	public void acknowledgeTask(String employeeNumber, String taskId) {
		// log
		if (log.isDebugEnabled()) {
			log.debug("Sending UpdateTaskStatus ACK request, employeeNumber=" + employeeNumber + ", taskId=" + taskId);
		}

		try {
			// create the request and populate
			UpdateTaskStatus req = sabreWebServiceRequestFactory.createUpdateTaskStatusRequest(employeeNumber);
			req.getStaffManagerMobileDeviceTaskStatusUpdate().getMobileTaskStatusUpdate().setSMTaskInstanceId(taskId);
			req.getStaffManagerMobileDeviceTaskStatusUpdate().getMobileTaskStatusUpdate().setTaskStatus(TaskStatus.ACKNOWLEDGED);

			// send the request
			UpdateTaskStatusResponse resp = (UpdateTaskStatusResponse) webServiceTemplate.marshalSendAndReceive(req, messageCallback);
			
			if (resp.getStaffManagerMobileDeviceTaskStatusUpdate().getAckowledgementOfStatusUpdate().getAcknowledgement() != null){
					log.debug("UpdateTaskStatusResponse, ACK request, employeeNumber=" + employeeNumber + ", taskId=" + taskId + " -- Acknowledged");
			} else { 
				log.debug("UpdateTaskStatusResponse, ACK request, employeeNumber=" + employeeNumber + ", taskId=" + taskId + " -  Not Actioned - SABRE did not receive update for some reason.  Should not happen, reason="
						+ resp.getStaffManagerMobileDeviceTaskStatusUpdate().getAckowledgementOfStatusUpdate().getNotActioned().getReason());

			}			
		} catch (Exception e) {
			log.error("Error sending acknowledge task, emp=" + employeeNumber + ", id=" + taskId, e);
		}
	}

	public void startTask(String employeeNumber, String taskId) {
		// log
		if (log.isDebugEnabled()) {
			log.debug("Sending UpdateTaskStatus START request, employeeNumber=" + employeeNumber + ", taskId=" + taskId);
		}
		try {
			// create the request and populate
			UpdateTaskStatus req = sabreWebServiceRequestFactory.createUpdateTaskStatusRequest(employeeNumber);
			req.getStaffManagerMobileDeviceTaskStatusUpdate().getMobileTaskStatusUpdate().setSMTaskInstanceId(taskId);
			req.getStaffManagerMobileDeviceTaskStatusUpdate().getMobileTaskStatusUpdate().setTaskStatus(TaskStatus.STARTED);

			// send the request
			UpdateTaskStatusResponse resp = (UpdateTaskStatusResponse) webServiceTemplate.marshalSendAndReceive(
					req, messageCallback);

			if (resp.getStaffManagerMobileDeviceTaskStatusUpdate().getAckowledgementOfStatusUpdate().getAcknowledgement() != null){
				log.debug("UpdateTaskStatusResponse, START request, employeeNumber=" + employeeNumber + ", taskId=" + taskId + " -- Acknowledged");
			} else { 
				log.debug("UpdateTaskStatusResponse, START request, employeeNumber=" + employeeNumber + ", taskId=" + taskId + " -  Not Actioned - SABRE did not receive update for some reason.  Should not happen, reason="
					+ resp.getStaffManagerMobileDeviceTaskStatusUpdate().getAckowledgementOfStatusUpdate().getNotActioned().getReason());
			}
		} catch (Exception e) {
			log.error("Error sending start task, emp=" + employeeNumber + ", id=" + taskId, e);
		}
	}

	public void stopTask(String employeeNumber, String taskId) {
		// log
		if (log.isDebugEnabled()) {
			log.debug("Sending UpdateTaskStatus STOP request, employeeNumber=" + employeeNumber + ", taskId=" + taskId);
		}
		try {
			// create the request and populate
			UpdateTaskStatus req = sabreWebServiceRequestFactory.createUpdateTaskStatusRequest(employeeNumber);
			req.getStaffManagerMobileDeviceTaskStatusUpdate().getMobileTaskStatusUpdate().setSMTaskInstanceId(taskId);
			req.getStaffManagerMobileDeviceTaskStatusUpdate().getMobileTaskStatusUpdate().setTaskStatus(TaskStatus.COMPLETED);

			// send the request
			UpdateTaskStatusResponse resp = (UpdateTaskStatusResponse) webServiceTemplate.marshalSendAndReceive(
					req, messageCallback);

			if (resp.getStaffManagerMobileDeviceTaskStatusUpdate().getAckowledgementOfStatusUpdate().getAcknowledgement() != null){
				log.debug("UpdateTaskStatusResponse, STOP request, employeeNumber=" + employeeNumber + ", taskId=" + taskId + " -- Acknowledged");
			} else { 
				log.debug("UpdateTaskStatusResponse, STOP request, employeeNumber=" + employeeNumber + ", taskId=" + taskId + " -  Not Actioned - SABRE did not receive update for some reason.  Should not happen, reason="
					+ resp.getStaffManagerMobileDeviceTaskStatusUpdate().getAckowledgementOfStatusUpdate().getNotActioned().getReason());
			}

		} catch (Exception e) {
			log.error("Error sending stop task, emp=" + employeeNumber + ", id=" + taskId, e);
		}
	}

}
