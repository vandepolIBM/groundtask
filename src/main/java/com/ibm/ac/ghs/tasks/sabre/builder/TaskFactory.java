package com.ibm.ac.ghs.tasks.sabre.builder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.sabre.resource.ResourceManager;
import com.sabre.streamline.TaskInformation;

@Component
public class TaskFactory {

	private ResourceManager resourceManager;

	@Autowired
	public void setResourceManager(ResourceManager resourceManager) {
		this.resourceManager = resourceManager;
	}

	public AbstractTask createTask(TaskInformation taskInformation) {

		AbstractBuilder builder;
		
		switch (taskInformation.getTask().getTaskCategory()) {
		case CABIN:
			builder = new CabinBuilder();
			break;
		case ATW_ARR:
			builder = new AboveTheWingArrivalBuilder();
			break;
		case ATW_DEP:
			builder = new AboveTheWingDepartureBuilder();
			break;
		case BTW_ARR:
			builder = new BelowTheWingArrivalBuilder();
			break;
		case BTW_DEP:
			builder = new BelowTheWingDepartureBuilder();
			break;
		case BTW_CNX:
			builder = new BelowTheWingCnxBuilder();
			break;
		case BTW_LAV_WTR:
			builder = new BelowTheWingLavWaterBuilder();
			break;
		case BTW_TOW:
			builder = new BelowTheWingTowBuilder();
			break;			
		case BTW_TURN:
			builder = new BelowTheWingTurnBuilder();
			break;
		default:
			builder = new DefaultBuilder();
			break;
		}

		builder.setResourceManager(resourceManager);
		return builder.build(taskInformation);
	}

}
