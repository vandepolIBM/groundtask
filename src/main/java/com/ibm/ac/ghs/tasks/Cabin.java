package com.ibm.ac.ghs.tasks;


public class Cabin extends AbstractTask {

	// Task Information
	private int numCrew;

	//PRJ1329 CR13 - Support for Resources and Task Types
	private String resource1;
	private String resource2;
	private String resource3;
	private String resource4;
	private String taskType1;
	private String taskType2;
	private String taskType3;
	private String taskType4;			
	
	// Flight Information
	private String aircraftFin;
	private String groundTime;
	// Arrival
	private String arrFlightNum;
	private String arrFrom;
	private String arrGate;
	private String arrETA;
	private String arrSTA;
	private String arrLoad;
	private String arrWchcPax;
	// Departure
	private String depFlightNum;
	private String depTo;
	private String depGate;
	private String depETD;
	private String depSTD;
	private String depLoad;
	private String depWchcPax;

	public Cabin() {
		setCategory(CategoryType.cabin);
	}

	public int getNumCrew() {
		return numCrew;
	}

	public void setNumCrew(int numCrew) {
		this.numCrew = numCrew;
	}

	public String getAircraftFin() {
		return aircraftFin;
	}

	public void setAircraftFin(String aircraftFin) {
		this.aircraftFin = aircraftFin;
	}

	public String getGroundTime() {
		return groundTime;
	}

	public void setGroundTime(String groundTime) {
		this.groundTime = groundTime;
	}

	public String getArrFlightNum() {
		return arrFlightNum;
	}

	public void setArrFlightNum(String arrFlightNum) {
		this.arrFlightNum = arrFlightNum;
	}

	public String getArrFrom() {
		return arrFrom;
	}

	public void setArrFrom(String arrFrom) {
		this.arrFrom = arrFrom;
	}

	public String getArrGate() {
		return arrGate;
	}

	public void setArrGate(String arrGate) {
		this.arrGate = arrGate;
	}

	public String getArrETA() {
		return arrETA;
	}

	public void setArrETA(String arrETA) {
		this.arrETA = arrETA;
	}
	
	public String getArrSTA() {
		return arrSTA;
	}

	public void setArrSTA(String arrSTA) {
		this.arrSTA = arrSTA;
	}

	public String getArrLoad() {
		return arrLoad;
	}

	public void setArrLoad(String arrLoad) {
		this.arrLoad = arrLoad;
	}
	
	public String getArrWchcPax() {
		return arrWchcPax;
	}

	public void setArrWchcPax(String arrWchcPax) {
		this.arrWchcPax = arrWchcPax;
	}

	public String getDepFlightNum() {
		return depFlightNum;
	}

	public void setDepFlightNum(String depFlightNum) {
		this.depFlightNum = depFlightNum;
	}

	public String getDepTo() {
		return depTo;
	}

	public void setDepTo(String depTo) {
		this.depTo = depTo;
	}

	public String getDepGate() {
		return depGate;
	}

	public void setDepGate(String depGate) {
		this.depGate = depGate;
	}

	public String getDepETD() {
		return depETD;
	}

	public void setDepETD(String depETD) {
		this.depETD = depETD;
	}
	
	public String getDepSTD() {
		return depSTD;
	}

	public void setDepSTD(String depSTD) {
		this.depSTD = depSTD;
	}

	public String getDepLoad() {
		return depLoad;
	}

	public void setDepLoad(String depLoad) {
		this.depLoad = depLoad;
	}
	
	public String getDepWchcPax() {
		return depWchcPax;
	}

	public void setDepWchcPax(String depWchcPax) {
		this.depWchcPax = depWchcPax;
	}
	public String getResource1() {
		return resource1;
	}

	public void setResource1(String resource1) {
		this.resource1 = resource1;
	}

	public String getResource2() {
		return resource2;
	}

	public void setResource2(String resource2) {
		this.resource2 = resource2;
	}

	public String getResource3() {
		return resource3;
	}

	public void setResource3(String resource3) {
		this.resource3 = resource3;
	}

	public String getResource4() {
		return resource4;
	}

	public void setResource4(String resource4) {
		this.resource4 = resource4;
	}

	public String getTaskType1() {
		return taskType1;
	}
	
	public void setTaskType1(String taskType1) {
		this.taskType1 = taskType1;
	}

	public String getTaskType2() {
		return taskType2;
	}
	
	public void setTaskType2(String taskType2) {
		this.taskType2 = taskType2;
	}

	public String getTaskType3() {
		return taskType3;
	}

	public void setTaskType3(String taskType3) {
		this.taskType3 = taskType3;
	}

	public String getTaskType4() {
		return taskType4;
	}

	public void setTaskType4(String taskType4) {
		this.taskType4 = taskType4;
	}
}
