package com.ibm.ac.ghs.tasks.sabre;

import java.io.StringReader;
import java.util.Date;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.ibm.ac.ghs.persistence.AlertManager;
import com.ibm.ac.ghs.tasks.sabre.builder.AlertFactory;
import com.sabre.streamline.StaffManagerMobileMessage;

public class AlertMdp implements MessageListener {

	private Logger log = LogManager.getLogger(getClass());

	private JAXBContext jaxbContext;
	
	private AlertManager alertManager;
	private AlertFactory alertFactory;
	
	public AlertMdp() {
		try {
			jaxbContext = JAXBContext.newInstance("com.sabre.streamline");
		} catch (Exception e) {
			log.error("Error creating unmarshaller.", e);
		}
	}

	@Autowired
	public void setAlertManager(AlertManager alertManager) {
		this.alertManager = alertManager;
	}

	@Autowired
	public void setAlertFactory(AlertFactory alertFactory) {
		this.alertFactory = alertFactory;
	}
	
	public void onMessage(Message message) {
		String text = null;
		try {
			// get the message text
			text = ((TextMessage) message).getText();
			
			// log raw message
			if (log.isTraceEnabled()) {
				log.trace("Recieved Sabre message, text=" + text);
			}

			// unmarshall the message text
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StaffManagerMobileMessage mobileMessage = (StaffManagerMobileMessage) unmarshaller.unmarshal(new StringReader(text));

			String id = mobileMessage.getAlertInformation().getAlert().getAlertID();
			Date timestamp = new Date();
			String employeeNumber = mobileMessage.getEmployeeInformation().getEmpNumber();

			// store in database
			alertManager.addAlert(id, timestamp, employeeNumber, alertFactory.createAlert(mobileMessage));

		} catch (Exception e) {
			log.error("Unable to handle Sabre message, text=" + text, e);
		}
	}
}
