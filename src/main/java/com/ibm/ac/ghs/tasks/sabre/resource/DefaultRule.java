package com.ibm.ac.ghs.tasks.sabre.resource;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.sabre.streamline.Resource;
import com.sabre.streamline.TaskCategory;

public class DefaultRule implements Rule {

	private static Logger log = LogManager.getLogger(DefaultRule.class);

	private Pattern wildcardPattern = Pattern.compile("\\$([0-9])");

	private String name;
	private Pattern taskCatagoryPattern;
	private Pattern taskTypePattern;
	private Pattern sabreResourcePattern;

	public DefaultRule(String name, String taskCatagoryMatcher, String taskTypeMatcher, String sabreResourceMatcher) {
		this.name = name;
		this.taskCatagoryPattern = Pattern.compile(taskCatagoryMatcher, Pattern.CASE_INSENSITIVE);
		this.taskTypePattern = Pattern.compile(taskTypeMatcher, Pattern.CASE_INSENSITIVE);
		this.sabreResourcePattern = Pattern.compile(sabreResourceMatcher, Pattern.CASE_INSENSITIVE);
	}

	public String getName() {
		return name;
	}
	
	public ResourceMatchResult matches(TaskCategory taskCategory, String taskType, Resource resource) {
		// ensure valid parameters
		if (taskCategory == null || taskType == null || resource == null || resource.getTaskType() == null) {
			return null;
		}

		// ensure the task category matches
		if (taskCatagoryPattern.matcher(taskCategory.value()).matches()) {

			// try to match task type rule
			Matcher taskTypeMatcher = taskTypePattern.matcher(taskType);
			if (taskTypeMatcher.find()) {

				// store the original sabre matching pattern
				String replacementStr = sabreResourcePattern.pattern();

				// replace $1 $2 in sabre matching pattern with task type
				// matches
				Matcher wildcardMatcher = wildcardPattern.matcher(sabreResourcePattern.pattern());
				while (wildcardMatcher.find()) {
					String strVar = wildcardMatcher.group(0);
					int strNum = Integer.parseInt(wildcardMatcher.group(1));
					strVar = strVar.replaceAll("\\$", "\\\\\\$");
					replacementStr = replacementStr.replaceAll(strVar, taskTypeMatcher.group(strNum));
				}

				// try to match the sabre task type
				if (Pattern.compile(replacementStr, Pattern.CASE_INSENSITIVE).matcher(resource.getTaskType()).matches()) {

					// log new search regex
					if (log.isDebugEnabled()) {
						log.debug("Matched resource, taskType=" + taskType + ", resource=" + resource.getEmpName());
					}

					return new ResourceMatchResult(this, resource);
				}
			}
		}

		// no match
		return null;
	}
}
