package com.ibm.ac.ghs.tasks;

public class AboveTheWingDeparture extends AbstractTask {

	// Task Information
	private String paxF;//PY-Change
	private String paxC;
	private String paxY;
	private String wchr;
	private String wchc;
	private String um;
	private String blind;
	private String controlAgent;
	private String assistAgent1;
	private String assistAgent2;
	private String assistAgent3;
	private String numOfGT_SPATT;
	
	// Flight Information
	private String aircraftFin;
	private String to;
	private String gate;
	private String etd;
	private String std;
	private String load;
	
	public AboveTheWingDeparture() {
		setCategory(CategoryType.atw_dep);
	}
	
	//PY-Change:start
	public String getPaxF() {
		return paxF;
	}
	public void setPaxF(String paxF) {
		this.paxF = paxF;
	}
	//PY-Change:end


	public String getPaxC() {
		return paxC;
	}
	public void setPaxC(String paxC) {
		this.paxC = paxC;
	}
	public String getPaxY() {
		return paxY;
	}
	public void setPaxY(String paxY) {
		this.paxY = paxY;
	}
	public String getWchr() {
		return wchr;
	}
	public void setWchr(String wchr) {
		this.wchr = wchr;
	}
	public String getWchc() {
		return wchc;
	}
	public void setWchc(String wchc) {
		this.wchc = wchc;
	}
	public String getUm() {
		return um;
	}
	public void setUm(String um) {
		this.um = um;
	}
	public String getBlind() {
		return blind;
	}
	public void setBlind(String blind) {
		this.blind = blind;
	}
	public String getControlAgent() {
		return controlAgent;
	}
	public void setControlAgent(String controlAgent) {
		this.controlAgent = controlAgent;
	}
	public String getAssistAgent1() {
		return assistAgent1;
	}
	public void setAssistAgent1(String assistAgent1) {
		this.assistAgent1 = assistAgent1;
	}
	public String getAssistAgent2() {
		return assistAgent2;
	}
	public void setAssistAgent2(String assistAgent2) {
		this.assistAgent2 = assistAgent2;
	}
	public String getAssistAgent3() {
		return assistAgent3;
	}
	public String getNumOfGT_SPATT() {
		return numOfGT_SPATT;
	}
	public void setNumOfGT_SPATT(String numOfGT_SPATT) {
		this.numOfGT_SPATT = numOfGT_SPATT;
	}
	public void setAssistAgent3(String assistAgent3) {
		this.assistAgent3 = assistAgent3;
	}
	public String getAircraftFin() {
		return aircraftFin;
	}
	public void setAircraftFin(String aircraftFin) {
		this.aircraftFin = aircraftFin;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getGate() {
		return gate;
	}
	public void setGate(String gate) {
		this.gate = gate;
	}
	public String getEtd() {
		return etd;
	}
	public void setEtd(String etd) {
		this.etd = etd;
	}
	public String getStd() {
		return std;
	}
	public void setStd(String std) {
		this.std = std;
	}
	public String getLoad() {
		return load;
	}
	public void setLoad(String load) {
		this.load = load;
	}


}
