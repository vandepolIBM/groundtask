package com.ibm.ac.ghs.tasks.sabre.builder;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.BelowTheWingCnx;
import com.ibm.ac.ghs.tasks.Connection;
import com.sabre.streamline.FlightInformationInboundFlight;
import com.sabre.streamline.TaskInformation;

/**
 * Builder responsible for instantiating and population the client-side object
 * which represents a Below the Wing - Arrival category task.
 * 
 * @author J.Koch
 */
public class BelowTheWingCnxBuilder extends AbstractBuilder {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ibm.ac.ghs.tasks.sabre.builder.AbstractBuilder#build(com.sabre.streamline
	 * .TaskInformation)
	 */
	public AbstractTask build(TaskInformation taskInformation) {
		BelowTheWingCnx task = new BelowTheWingCnx();

		// setup the default task information
		initialize(task, taskInformation);

		// ensure we have flight information
		if (taskInformation.getFlightInformation() != null) {

			// ensure we have inbound flight information
			if (taskInformation.getFlightInformation().getInboundFlight() != null) {

				// grab the flight
				FlightInformationInboundFlight flt = taskInformation.getFlightInformation().getInboundFlight();
				
				// ensure we have equipment info
				if (taskInformation.getFlightInformation().getEquipmentInformation() != null) {
					// set the aircraft type and fin
					task.setAircraftFin(taskInformation.getFlightInformation().getEquipmentInformation().getAircraftType() + "/"
							+ StringUtils.defaultString(taskInformation.getFlightInformation().getEquipmentInformation().getTailNumber()));
				}
				
				// set flight details
				task.setArrival(StringUtils.defaultString(flt.getUplineStation()));
				task.setGate(flt.getGateInformation() != null ? StringUtils.defaultString(flt.getGateInformation().getGate()) : "");
				task.setEta(convertTime(flt.getEstimatedArrivalTime()));

				//PRJ1329 CR13 - Show ETD/A on Task List Screen
				//task.setEstimatedTime(task.getEta());
				
				// setup the hot connections
				task.setHc("#");
				task.setHcBags("#");
				List<Connection> connectionList = buildConnectionList(taskInformation);
				if (connectionList != null) {
					// set the connection
					task.setHc(connectionList);
					
					// set the total pax and bags
					int pax = 0, bags = 0;
					for (Connection connection : connectionList) {
						pax += connection.getPax() != null ? Integer.parseInt(connection.getPax()) : 0;
						bags += connection.getBags() != null ? Integer.parseInt(connection.getBags()) : 0;
					}
					task.setHcBags("" + bags);
				} 	
				
				// TODO 
				task.setSta("");
			}
		}

		return task;
	}
}
