package com.ibm.ac.ghs.tasks;

public enum TaskStatusType {

	DEFAULT,
	STARTED,
	COMPLETED;
}
