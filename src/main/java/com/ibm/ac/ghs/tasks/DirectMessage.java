package com.ibm.ac.ghs.tasks;

public class DirectMessage extends AbstractAlertDetails {

	private String from;
	private String time;
	private String message;

	public DirectMessage() {
		setCategory(CategoryType.message);
	}
	
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;	
	}
	
	public String toString() {
		return "DirectMessage[from=" + from + ",msg=" + message + "]"; 
	}
}
