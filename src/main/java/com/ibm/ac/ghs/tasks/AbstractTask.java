package com.ibm.ac.ghs.tasks;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public abstract class AbstractTask extends AbstractAlertDetails {

	private String type;

	private long sort;
	private String startTime;
	private String endTime;
	
	//PRJ1329 CR13 - Show ETD/A on Task List Screen
	//private String estimatedTime;

	private String city;
	private String location;
	private String flightNum;

	private TaskStatusType status = TaskStatusType.DEFAULT;
	private boolean ackd;

	private String comments;
	private String opsRemarksArr;
	private String opsRemarksDep;

	// show start/stop buttons indicator
	private boolean showSS;

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getOpsRemarksArr() {
		return opsRemarksArr;
	}

	public void setOpsRemarksArr(String opsRemarksArr) {
		this.opsRemarksArr = opsRemarksArr;
	}

	public String getOpsRemarksDep() {
		return opsRemarksDep;
	}

	public void setOpsRemarksDep(String opsRemarksDep) {
		this.opsRemarksDep = opsRemarksDep;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getSort() {
		return sort;
	}

	public void setSort(long sort) {
		this.sort = sort;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
	public String getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(String estimatedTime) {
		this.estimatedTime = estimatedTime;
	}
	**/
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getFlightNum() {
		return flightNum;
	}

	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}

	public TaskStatusType getStatus() {
		return status;
	}

	public void setStatus(TaskStatusType status) {
		this.status = status;
	}

	public boolean isAckd() {
		return ackd;
	}

	public void setAckd(boolean ackd) {
		this.ackd = ackd;
	}

	public boolean isShowSS() {
		return showSS;
	}

	public void setShowSS(boolean showSS) {
		this.showSS = showSS;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
