package com.ibm.ac.ghs.jndi;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceEditor;

/**
 * A simple JNDI initial context which loads the JNDI namespace from a
 * spring.xml configuration file. The spring.xml configuration file can be
 * specified by the {@link Context#PROVIDER_URL} property which can be any
 * spring resource string (classpath://foo.xml, or file://foo/bar.xml or a URL)
 * otherwise the jndi.xml file is found on the classpath.
 */
public class SpringInitialContextFactory implements InitialContextFactory {
	
	private static final transient Logger log = LogManager.getLogger(SpringInitialContextFactory.class);

	private static Map<Object, Object> cache = new HashMap<Object, Object>();

	private static Context singleton;

   /**
    * A factory method which can be used to initialise a singleton JNDI context from inside a Spring.xml
    * such that future calls to new InitialContext() will reuse it
    */
   public static Context makeInitialContext() {
       singleton = new DefaultContext();
       return singleton;
   }
   
	@SuppressWarnings("unchecked")
	public Context getInitialContext(Hashtable environment) throws NamingException {
		System.out.println("DVP : getInitialContext" );
		InitialContext ic = new InitialContext();
		return ic;
		// if (singleton != null) {
		// 	return singleton;
		// }
		// Resource resource = null;
		// InitialContext ic = new InitialContext();
		// System.out.println(ic.getClass());
		// Object value = environment.get(Context.PROVIDER_URL);
		// String key = "jndi.xml";
		// if (value == null) {
		// 	resource = new ClassPathResource(key);
		// } else {
		// 	if (value instanceof Resource) {
		// 		resource = (Resource) value;
		// 	} else {
		// 		ResourceEditor editor = new ResourceEditor();
		// 		key = value.toString();
		// 		editor.setAsText(key);
		// 		resource = (Resource) editor.getValue();
		// 	}
		// }
		// BeanFactory context = loadContext(resource, key);
		// Context answer = (Context) context.getBean("jndi");
		// if (answer == null) {
		// 	log.error("No JNDI context available in JNDI resource: " + resource);
        //  answer = new DefaultContext(environment, new ConcurrentHashMap());
		// }
		// return answer;
	}

	protected BeanFactory loadContext(Resource resource, String key) {
		synchronized (cache) {
			BeanFactory answer = (BeanFactory) cache.get(key);
			if (answer == null) {
				answer = createContext(resource);
				cache.put(key, answer);
			}
			return answer;
		}
	}

	protected BeanFactory createContext(Resource resource) {
		log.info("Loading JNDI context from: " + resource);
		return new XmlBeanFactory(resource);
	}
}
