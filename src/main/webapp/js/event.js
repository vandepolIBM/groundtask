/**
 * 
 * Events
 * 
 * @author M.Kurabi
 */
 
/**
 * Declare an object to which we can add real functions.
 */
if (ghs.event == null) { ghs.event = {}; }

// Events
ghs.event.TASK_RECEIVED = 'TR';
ghs.event.TASK_LIST_UPDATED = 'TLU';
ghs.event.ALERT_LIST_UPDATED = 'ALU';