/**
 *
 * UI Management functions
 *
 * @author M.Kurabi
 */

/**
 * Declare an object to which we can add real functions.
 */
//DVPalert("ui.js loaded");
if (ghs.ui == null) { ghs.ui = {}; } // Init ui object

ghs.ui._backButtonTop = 'uBB';
ghs.ui._backButtonBot = 'lBB';
ghs.ui.lang = ghs.util.getURLParam('lang') || 'en'; // English is default language
ghs.ui.empId = ghs.util.getURLParam('empl'); // Employee Id
ghs.ui.enableBrock = ghs.util.getURLParam('ralert') || ''; // Brock Alert System Integration Enabler Indicator
ghs.ui.isOffline = true;
ghs.ui.isTaskTableMore=false;
ghs.ui.isAlertTableMore=false;
ghs.ui.isViewingTaskDetail = false;

ghs.ui.BATTERY_POLL_INTERVAL = 70000;
ghs.ui.DETAIL_SCREEN_TIMEOUT_DELAY = 30000;
ghs.ui.TIME_POLL_INTERVAL = 30000;
ghs.ui.REG_TIME_TIMER_ID = null;
ghs.ui.REG_DETAIL_SCREEN_TIMOUT_ID = null;

ghs.ui.ALERT_DUR = 2000;
ghs.ui.ALERT_SOUND = "\\windows\\Alarm1.wav";

ghs.ui.VIB_NUM = 6;
ghs.ui.VIB_ON_DUR = 1000;
ghs.ui.VIB_OFF_DUR = 500;
ghs.ui.VIB_COUNT = 3;

ghs.ui.LED_NUM_REG = '0';
ghs.ui.LED_NUM_SEV = '1';
ghs.ui.LED_ON_DUR = 1000;
ghs.ui.LED_OFF_DUR = 1000;
ghs.ui.LED_COUNT = 20;

ghs.ui.imageDirectory = '/gc/images/';
ghs.ui.cacheList = ['heading/task_hands-on.png', 'heading/flightInfo.png', 'heading/remark.png',
                    'heading/title_alert_tip.png', ghs.ui.lang + '/btn_startTask_off.png',
                    ghs.ui.lang + '/btn_completeTask_off.png', ghs.ui.lang + '/btn_startTask_disabled.png',
                    ghs.ui.lang + '/btn_completeTask_disabled.png',	'online.png', 'online-yel.png',
                    'icons/leaf.png', 'icons/caution.png', 'icons/alert_tip.png',
                    ghs.ui.lang + '/btn_view_off.png', 'bkgrd/bkgrd_alert_yellow.gif',
                    'bkgrd/bkgrd_alert_green.gif'
				   ];

/**
 * @public initializing function
 */
ghs.ui.init = function() {
  //DVPalert("UI init!");
  if (ghs.util.getURLParam('alertdebug') == "true"){
    alert("[ghs.ui.init] Initializing application...");
  }
  ghs.log.info('[ghs.ui.init] Initializing application...');
  // Reset any LEDs/Sounds
  try {	MobileX.stopAll(); } catch (ex) { /* No ActiveX Installed */ };
  // Activate Battery Polling
  if (ghs.gears.BATTERY_TIMER) {
  	ghs.ui.setBatteryLevel(true);
  	// Clear interval if it exists
  	if (ghs.gears.BATTERY_TIMER_ID) {
  	  ghs.gears.BATTERY_TIMER.clearInterval(ghs.gears.BATTERY_TIMER_ID);
  	}
  	ghs.gears.BATTERY_TIMER_ID = ghs.gears.BATTERY_TIMER.setInterval(ghs.ui.setBatteryLevel, ghs.ui.BATTERY_POLL_INTERVAL);
  }
  // Activate Time Polling
  if (ghs.gears.TIME_TIMER) {
  	//ghs.ui.setTime(true);
  	// Clear interval if it exists
  	if (ghs.gears.TIME_TIMER_ID) {
  	  ghs.gears.TIME_TIMER.clearInterval(ghs.gears.TIME_TIMER_ID);
  	}
  	ghs.gears.TIME_TIMER_ID = ghs.gears.TIME_TIMER.setInterval(ghs.ui.setTime, ghs.ui.TIME_POLL_INTERVAL);
  }
  // Instantiate Layout
  ghs.util.addRows(taskTableHeader(), 0, headerTaskOptions);
  ghs.ui._setHeadersAndFooters();
  //DVPalert("UI login!");
  if (ghs.util.getURLParam('alertdebug') == "true"){
    alert("[ghs.ui.init] calling login");
  } 
  if (ghs.ui.empId) { 
  	ghs.srvc.login(ghs.ui.empId, ghs.ui.lang);
  } else {
  	ghs.ui._hideLoading();
  	ghs.ui.setStatus('No Employee ID Found.');
  }
  ghs.ui.cacheImages(ghs.ui.cacheList);
};

/**
 * Shuts down the application gracefully and sends exit command to mBrowse
 */
ghs.ui.exit = function() {
  if (confirm(ghs.ui.getMessage('exitPrompt'))) {
    ghs.log.info('[ghs.ui.exit] Exiting application...');
    try { MobileX.stopAll(); } catch (ex) { /*Do Nothing*/ };
    ghs.srvc.shutdown(); // Shut down service queue
    document.location.href = 'http://mLaunch/exit';
  }
};

/**
 * @public Updates the task list
 */
ghs.ui.updateTaskList = function() {
  ghs.ui._deleteAllTaskRows(false);
  taskRowCount = ghs.util.addRows(ghs.srvc.taskList, 0, addTaskOptions);
  ghs.ui._setTaskHeaderAndFooter();
  ghs.log.info('[ghs.ui.updateTaskList] Task list updated.');
};

/**
 * @public Updates the alert list
 */
ghs.ui.updateAlertList = function(optns) {
  ghs.ui._deleteAllAlertRows(false);
  if (optns.alertCount > 0) {
    ghs.ui.updateTaskList(); // Update TaskList
  }
  alertRowCount = ghs.util.addRows([].concat(ghs.srvc.alertList).reverse(), 0, addAlertOptions);
  ghs.ui._setAlertHeaderAndFooter();
  // Check Severity
  if (optns.severity > 0) {
  	ghs.ui.back(true); // Go to Home Page
  }
  // Activate LEDS/Sound
  try {
  	if (optns.alertCount > 0) {
  	  if (optns.severity > 0) {
  	  	MobileX.stopAll();
  	  	MobileX.activateLED_PLS(11, ghs.ui.LED_NUM_REG, ghs.ui.LED_ON_DUR, ghs.ui.LED_OFF_DUR, ghs.ui.LED_COUNT);
  	    MobileX.playSound(ghs.ui.ALERT_SOUND, optns.alertCount, ghs.ui.ALERT_DUR);
  	    MobileX.activateVibrate(ghs.ui.VIB_ON_DUR, ghs.ui.VIB_OFF_DUR, ghs.ui.VIB_COUNT);
  	  }
  	}
  } catch (ex) {
  	ghs.log.warn('[ghs.ui.updateAlertList] Exception thrown (no ActiveX support possibly):' + ex.message);
  }
  ghs.log.info('[ghs.ui.updateAlertList] Alert list updated.');
};

/**
 * @public Delgates events to the intrested functions
 */
ghs.ui.eventHandler = function(event, optns) {
  if (event == ghs.event.TASK_LIST_UPDATED) {
  	ghs.ui.updateTaskList();
  	return;
  }
  if (event == ghs.event.ALERT_LIST_UPDATED) {
  	ghs.ui.updateAlertList(optns);
  	return;
  }
};

/**
 * @public Back Button function
 */
ghs.ui.back = function(goToHome) {
  ghs.ui.isViewingTaskDetail = false;
  goToHome = goToHome ? true : false;
  var detailContent = $('detailContent');
  if (detailContent.style.display == '' && !goToHome) {
  	ghs.ui._clearDetailScreenTimeout(); // Clear detail screen timeout
    detailContent.style.display = 'none';
	$('homeContent').style.display='';
	if ($('taskContainer').style.display != 'none' && $('alertContainer').style.display != 'none') {
	  ghs.ui._backButtonDisplay(false); // Hide back buttons if Task/Alert list is not expanded
	}
  } else {
  	// Show Home Screen
  	if (detailContent.style.display == '' && goToHome) {
  	  ghs.ui._clearDetailScreenTimeout(); // Clear detail screen timeout
  	  detailContent.style.display = 'none';
	  $('homeContent').style.display='';
  	}
    $('taskTableMore').style.display = 'none';
	$('alertTableMore').style.display = 'none';
	$('taskContainer').style.display = '';
	$('alertContainer').style.display = '';
	ghs.ui._setFooters();
	ghs.ui._backButtonDisplay(false);
	if (ghs.ui.isTaskTableMore) {
	  ghs.ui.isTaskTableMore = false;
	  ghs.ui._setTaskHeader();
	}
	if (ghs.ui.isAlertTableMore) {
	  ghs.ui.isAlertTableMore = false;
	  ghs.ui._setAlertHeader();
	}
  }
};

/**
 * @public displays the item selected on page
 */
ghs.ui.itemSelected = function(data) {
  if (ghs.ui.isViewingTaskDetail){
  	return; // Bail if we are already here (need this because device is slow)
  }
  try {	MobileX.stopAll(); } catch (ex) { /* No ActiveX Installed */ };
  var type = data.category;
  var detailContent = $('detailContent');
  detailContent.innerHTML = ghs.util.populateTemplate(ghs.i8n.labels[ghs.ui.lang], data,
		  ghs.detail[type].template, ghs.detail[type].itemRenderers);
  ghs.detail[type].init(data);
  if (type != 'message') {
	if (!data.ackd) {
	  ghs.srvc.acknowledgeTask(ghs.ui.empId, data.id);
	  data.ackd = true;
	}
  	if (data.properties && data.status != 'COMPLETED') {
  	  data.properties.highlight = '';
  	  ghs.util.removeHighlight('id_' + data.id, ghs.config.HLGHT_CLASS);
  	}
  }
  if (data.properties) {
  	ghs.util.highlightTemplateFields(data.properties.modifiedFields, ghs.detail[type].hlghtMap, ghs.config.HLGHT_CLASS);
  	data.properties.modifiedFields = [];
  }
  $('homeContent').style.display = 'none';
  ghs.ui._backButtonDisplay(true);
  detailContent.style.display = '';
  ghs.srvc.removeAlert(ghs.ui.empId, data.id);
  ghs.log.info('[ghs.ui.itemSelected] Viewing task#' + data.id);
  ghs.ui.isViewingTaskDetail = true;
  ghs.ui._setDetailScreenTimeout(ghs.ui.back, ghs.ui.DETAIL_SCREEN_TIMEOUT_DELAY);

  // This sloution is only for Chrome
  // window.chrome ? ghs.srvc.alertsRecieved(ghs.ui.empId, [data.id], true) : '';

};

/**
 * @public displays more tasks on page
 */
ghs.ui.seeMoreTasks = function() {
  $('alertContainer').style.display = 'none';
  $('taskTableMore').style.display = '';
  $('taskFooter').style.display = 'none';
  ghs.ui.isTaskTableMore = true;
  ghs.ui._setTaskHeader();
  ghs.ui._backButtonDisplay(true);
};

/**
 * @public displays more alerts on page
 */
ghs.ui.seeMoreAlerts = function() {
  $('taskContainer').style.display = 'none';
  $('alertTableMore').style.display = '';
  $('alertFooter').style.display = 'none';
  ghs.ui.isAlertTableMore = true;
  ghs.ui._setAlertHeader();
  ghs.ui._backButtonDisplay(true);
};

/**
 * @public Set the Battery level thru ActiveX
 */
ghs.ui.setBatteryLevel = function(firstTime) {
  var power;
  try {
  	power = MobileX.getBatteryPower();
  	power = parseInt(power);
  } catch (ex) {
  	/* No activeX installed or non-numeric value returned */
  	power = -1;
  }
  if (firstTime) { $('battCont').style.visibility = 'visible'; }
  if (power >= 0) {
    $('battLvlT').innerHTML = power + '%';
  } else if (firstTime) {
  	$('battLvlT').innerHTML = ghs.ui.getMessage('na');
  	power = 101;
  } else {
  	return;
  }
  var battLvl = $('battLvl');
  battLvl.style.width = (power / 5) + 'px';
  battLvl.style.backgroundColor = ghs.util.getBatteryColour(power);
};

/**
 * @public Set the Battery level thru ActiveX
 */
ghs.ui.setTime = function(firstTime) {
  var time = new Date();
  time = String("0" + time.getHours()).slice(-2) + ":" + String("0" + time.getMinutes()).slice(-2);
  if (firstTime) {
    return time;
  }
  $("time").innerHTML = time;
};


/**
 * @public Set the Offline or Online signal
 */
ghs.ui.setSignalLevel = function(bool) {
  if (bool && ghs.ui.isOffline) {
    // Set Online orange
    ghs.ui.isOffline = false;
    var image = $('signalImg');
    var imageName = 'online-yel.png';
    if (image.src.indexOf(imageName) < 0 ||
        image.style.visibility == 'hidden') {
      image.src = ghs.ui.imageDirectory + imageName;
      image.style.visibility = '';
    }
  } else if (bool && !ghs.ui.isOffline) {
  	// Set Online green
  	ghs.ui.isOffline = false;
  	var image = $('signalImg');
  	var imageName = 'online.png';
  	if (image.src.indexOf(imageName) < 0 ||
        image.style.visibility == 'hidden') {
      image.src = ghs.ui.imageDirectory + imageName;
      image.style.visibility = '';
  	}
  } else if (!bool && !ghs.ui.isOffline) {
    // Set Offline
    ghs.ui.isOffline = true;
    var image = $('signalImg');
    if (image.style.visibility == '') {
      image.style.visibility = 'hidden';
    }
  }
};

/**
 * @public preloads needed images into memory
 */
ghs.ui.cacheImages = function(list) {
 if (document.images) {
    for (var i = 0; i < list.length; i++) {
      this[i+1] = new Image();
      this[i+1].src = ghs.ui.imageDirectory + list[i];
    }
  }
};

/**
 * UI Status Message
 * @param {Object} string
 */
ghs.ui.setStatus = function(string) {
  var ele = $("status");
  ele.innerHTML = string;
};

/**
 * @private sets the detail screen timout
 */
ghs.ui._setDetailScreenTimeout = function(func, timeout) {
  ghs.ui._clearDetailScreenTimeout();
  if (ghs.gears.GENERIC_TIMER) {
  	ghs.gears.DETAIL_SCREEN_TIMEOUT_ID = ghs.gears.DETAIL_SCREEN_TIMEOUT.setTimeout(func, timeout);
  } else {
  	ghs.ui.REG_DETAIL_SCREEN_TIMOUT_ID = window.setTimeout(func, timeout);
  }
};

ghs.ui._clearDetailScreenTimeout = function() {
  try {
    if (ghs.gears.DETAIL_SCREEN_TIMEOUT) {
  	  if (ghs.gears.DETAIL_SCREEN_TIMEOUT_ID) { ghs.gears.DETAIL_SCREEN_TIMEOUT.clearTimeout(ghs.gears.DETAIL_SCREEN_TIMEOUT_ID); }
    } else {
  	  if (ghs.ui.REG_DETAIL_SCREEN_TIMOUT_ID) { window.clearTimeout(ghs.ui.REG_DETAIL_SCREEN_TIMOUT_ID); }
    }
  } catch (ex) {
  	ghs.log.warn('[ghs.ui._clearDetailScreenTimeout] Cannot clear detail screen timeout. Error:' + ex);
  }
};

/**
 * Gets i8n message
 * @param {Object} string
 */
ghs.ui.getMessage = function(message) {
  return ghs.i8n.labels[ghs.ui.lang][message];
};

/**
 * @public deletes all tasks from task tables
 */
ghs.ui._deleteAllTaskRows = function(setHeaderFooter) {
  ghs.util.deleteAllRows("taskTableMore", false);
  ghs.util.deleteAllRows("taskTable", true);
  if (setHeaderFooter) {
  	ghs.ui._setTaskHeaderAndFooter();
  }
};

/**
 * @public deletes all tasks from task tables
 */
ghs.ui._deleteAllAlertRows = function(setHeaderFooter) {
  ghs.util.deleteAllRows("alertTableMore", false);
  ghs.util.deleteAllRows("alertTable", false);
  if (setHeaderFooter) {
  	ghs.ui._setAlertHeaderAndFooter();
  }
};

/**
 * @private Helper that sets all headers and footers
 */
ghs.ui._setHeadersAndFooters = function() {
  ghs.ui._setHeaders();
  ghs.ui._setFooters();
};

/**
 * @private Helper that sets tasks header and footer
 */
ghs.ui._setTaskHeaderAndFooter = function() {
  ghs.ui._setTaskHeader();
  ghs.ui._setTaskFooter();
};

/**
 * @private Helper that sets alerts header and footer
 */
ghs.ui._setAlertHeaderAndFooter = function() {
  ghs.ui._setAlertHeader();
  ghs.ui._setAlertFooter();
};

/**
 * @private Helper that sets tasks and alerts header
 */
ghs.ui._setHeaders = function() {
  ghs.ui._setTaskHeader();
  ghs.ui._setAlertHeader();
};

/**
 * @private Helper that sets task header
 */
ghs.ui._setTaskHeader = function() {
  ghs.util.setValue("taskHeader", taskHeaderDivFunc);
};

/**
 * @private Helper that sets alerts header
 */
ghs.ui._setAlertHeader = function() {
  ghs.util.setValue("alertHeader", alertHeaderDivFunc);
};

/**
 * @private Helper that sets tasks & alerts footers
 */
ghs.ui._setFooters = function() {
  ghs.ui._setTaskFooter();
  ghs.ui._setAlertFooter();
};

/**
 * @private Helper that sets the task footer
 */
ghs.ui._setTaskFooter = function() {
  ghs.util.setValue('taskFooter', taskFooterDivFunc, true);
};

/**
 * @private Helper that sets the alert footer
 */
ghs.ui._setAlertFooter = function() {
  ghs.util.setValue('alertFooter', alertFooterDivFunc, true);
};

/**
 * @private Helper function that hides the back buttons on the UI
 */
ghs.ui._backButtonDisplay = function(bool) {
  var uBB = $(ghs.ui._backButtonTop);
  var lBB = $(ghs.ui._backButtonBot);
  if (bool) {
  	uBB.style.display = 'none';
	lBB.style.display = '';
  } else {
  	uBB.style.display = 'none';
    lBB.style.display = 'none';
  }
};

/**
 * @private show loading graphic
 */
ghs.ui._showLoading = function() {
  ghs.log.info('[ghs.srvc._getTasksCallback] showing loading.');
  $('loader').style.display='';
  $('pageContent').style.display='none';
};

/**
 * @private hide loading graphic
 */
ghs.ui._hideLoading = function() {
  $('loader').style.display='none';
  $('pageContent').style.display='';

};
