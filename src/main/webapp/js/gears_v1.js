/**
 * 
 * Gears File for AC GHS
 * Manages Gears related functionality
 * 
 * @author M.Kurabi
 * 
 */

/**
 * Declare an object to which we can add gears functions in.
 */
if (ghs.gears == null) { ghs.gears = {}; }

// Static Varibles
ghs.gears.STORE_NAME = "AC_GHS";
ghs.gears.MANIFEST_FILENAME = "manifest";
ghs.gears.LOCALSERVER;
ghs.gears.STORE;
ghs.gears.currentVersion;

// Gear Timers
ghs.gears.BATTERY_TIMER;
ghs.gears.TIME_TIMER;
ghs.gears.SIGNAL_TIMER;
ghs.gears.GENERIC_TIMER;
ghs.gears.POLL_TIMEOUT_TIMER;
ghs.gears.SRVC_QUEUE_TIMER;
ghs.gears.DETAIL_SCREEN_TIMEOUT;

// Gear Timers IDs
ghs.gears.BATTERY_TIMER_ID;
ghs.gears.TIME_TIMER_ID;
ghs.gears.SIGNAL_TIMER_ID;
ghs.gears.GENERIC_TIMER_ID;
ghs.gears.POLL_TIMEOUT_TIMER_ID;
ghs.gears.SRVC_QUEUE_TIMER_ID;
ghs.gears.DETAIL_SCREEN_TIMEOUT_ID;

ghs.gears.UPDATE_INTERVAL = 2000;

// Gear based logger
ghs.gears.logStatus = false;
ghs.gears.logger = null;

/**
 * Checks if gears is installed and
 * initializes the managed store.
 */
ghs.gears.init = function() {
  if (!window.google || !google.gears) {
    // ghs.gears.out("Running Live!");
    ghs.gears.out("v3.84");
  } else if (google.gears.factory.hasPermission || google.gears.factory.getPermission('GHS - Ground-Task', '', '')) {
    ghs.gears.LOCALSERVER = google.gears.factory.create("beta.localserver");
    // Create and/or update Managed Store
    ghs.gears.STORE = ghs.gears.LOCALSERVER.createManagedStore(ghs.gears.STORE_NAME);
    ghs.gears.currentVersion = ghs.gears.STORE.currentVersion;
    ghs.log.gears.init();
    ghs.gears.updateStore();
   return;
   
  } else {
    // Run Locally
    ghs.gears.out("Running Live! No permission.");
  }
  ghs.ui.setStatus("");
  ghs.ui.init();
};

/**
 * Update the Managed Store
 */
ghs.gears.updateStore = function() {
  ghs.log.info("[ghs.gears.updateStore] Current manifest version is " + ghs.gears.currentVersion + ". Checking for updates...");
  ghs.gears.STORE.manifestUrl = ghs.gears.MANIFEST_FILENAME;
  ghs.gears.STORE.enabled = ghs.config.gearsEnabled; // Set to true to enable gears caching
  ghs.gears.STORE.checkForUpdate();
  ghs.gears.GENERIC_TIMER = google.gears.factory.create('beta.timer');
  ghs.gears.GENERIC_TIMER_ID = ghs.gears.GENERIC_TIMER.setTimeout(ghs.gears._updateStore, ghs.gears.UPDATE_INTERVAL * 2);
};

/**
 * @private helper function for ghs.gears.updateStore
 * @return launch GHS application once ready
 * 
 */
ghs.gears._updateStore = function() {
  var ready = false;
  switch(ghs.gears.STORE.updateStatus) {
  	case 0:
  	  ghs.gears.out(ghs.gears.STORE.currentVersion);
      ready = true;
  	  break;
  	case 2:
  	  ghs.gears.GENERIC_TIMER_ID = ghs.gears.GENERIC_TIMER.setTimeout(ghs.gears._updateStore, ghs.gears.UPDATE_INTERVAL);
  	  ghs.ui.setStatus("Updating Ground-Task...");
  	  break;
  	case 3:
  	  ghs.log.warn("[ghs.gears._updateStore] Gears Update Error: " + ghs.gears.STORE.lastErrorMessage + ". Retrying...");
	  ghs.ui.setStatus("Cannot connect to server. Retrying...");
	  // TODO TO DELETE JUST FOR TESTING
	  /*try {
	  	var image = new Image();
	  	image.src = "/gc/images/toDelete.gif?id=" + Math.floor(Math.random()*1000000);
	  } catch (e) {	ghs.log.warn("[non_cached_image] Cached Image Error"); }*/
	  ghs.gears.STORE.checkForUpdate();
	  ghs.gears.GENERIC_TIMER_ID = ghs.gears.GENERIC_TIMER.setTimeout(ghs.gears._updateStore, ghs.gears.UPDATE_INTERVAL * 2);
  	  break;
  	default:
  	  ghs.gears.GENERIC_TIMER_ID = ghs.gears.GENERIC_TIMER.setTimeout(ghs.gears._updateStore, ghs.gears.UPDATE_INTERVAL);
  	  break;
  }
  // Refresh application if new gears version is found, otherwise launch ground-task
  if (ready) {
  	if (ghs.gears.currentVersion != ghs.gears.STORE.currentVersion) {
  	  ghs.log.info("[ghs.gears._updateStore] Update Downloaded, refreshing application.");
  	  ghs.gears.GENERIC_TIMER.clearTimeout(ghs.gears.GENERIC_TIMER_ID); // Clear the timer used for update
  	  document.location.href = document.location.href;
  	  return;
  	} else {
  	  // Init Timers
  	  try {
  	    ghs.gears.STORE.manifestUrl = "";
  	  } catch (ex) {
  	  	ghs.log.error("[ghs.gears._updateStore] Gears manifest url change to empty failed:" + ex.message);
  	  }
      ghs.gears.activateTimers();
      ghs.ui.setStatus("");
      ghs.ui.init();
  	}
  }
};

/**
 * Activates gears based timers
 */
ghs.gears.activateTimers = function() {
  ghs.gears.GENERIC_TIMER.clearTimeout(ghs.gears.GENERIC_TIMER_ID); // Clear the timer used for update
  // Instantiate gear based timers
  ghs.gears.BATTERY_TIMER = google.gears.factory.create('beta.timer');
  ghs.gears.TIME_TIMER = google.gears.factory.create('beta.timer');
  ghs.gears.SIGNAL_TIMER = google.gears.factory.create('beta.timer');
  ghs.gears.GENERIC_TIMER = google.gears.factory.create('beta.timer');
  ghs.gears.POLL_TIMEOUT_TIMER = google.gears.factory.create('beta.timer');
  ghs.gears.SRVC_QUEUE_TIMER = google.gears.factory.create('beta.timer');
  ghs.gears.DETAIL_SCREEN_TIMEOUT = google.gears.factory.create('beta.timer');
};

ghs.gears.deactivateTimers = function() {
  try {
  	// Instantiate gear based timers
  	ghs.gears.BATTERY_TIMER.clearTimeout(ghs.gears.BATTERY_TIMER_ID);
  	ghs.gears.TIME_TIMER.clearTimeout(ghs.gears.BATTERY_TIMER_ID);
  	ghs.gears.SIGNAL_TIMER.clearTimeout(ghs.gears.SIGNAL_TIMER_ID);
  	ghs.gears.GENERIC_TIMER.clearTimeout(ghs.gears.GENERIC_TIMER_ID);
  	ghs.gears.POLL_TIMEOUT_TIMER.clearTimeout(ghs.gears.POLL_TIMEOUT_ID);
  	ghs.gears.SRVC_QUEUE_TIMER.clearTimeout(ghs.gears.SRVC_QUEUE_TIMER_ID);
  	ghs.gears.DETAIL_SCREEN_TIMEOUT.clearTimeout(ghs.gears.DETAIL_SCREEN_TIMEOUT_ID);
  } catch (ex) {
  	ghs.log.warn("[ghs.gears.deactivateTimers] Error in deactivating gear timers:" + ex);
  }
};

/**
 * Standard output for status/error msgs for gears
 * @param {Object} string
 */
ghs.gears.out = function(string) {
  var ele = $("gearsStdOut");
  ele.innerHTML = string;
};
