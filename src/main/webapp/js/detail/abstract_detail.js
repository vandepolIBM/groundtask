/**
 * @author Administrator
 *
 * M.Kurabi
 */

/*
 #############################################
 	Cabin Properties
 ############################################# 
 */

if (ghs.detail == null) { ghs.detail = {}; } // Init ui detail object

ghs.detail.startTask = function(taskId) {
  // Disable start task button
  var startTask = $('startTask');
  if (startTask.style.visibility != 'hidden') {
  	startTask.style.visibility = 'hidden';
  	// Enable stop task button
  	var stopTask = $('stopTask');
  	stopTask.style.visibility = '';
  	var taskPos = ghs.srvc.getTaskPos(taskId);
  	if (taskPos > -1) {
  	  ghs.srvc.taskList[taskPos].status="STARTED";
  	}
  	// Make ajax call
  	ghs.srvc.startTask(ghs.ui.empId, taskId);
  }
};

ghs.detail.stopTask = function(taskId) {
  // Disable stop task button
  var stopTask = $('stopTask');
  if (stopTask.style.visibility != 'hidden') {
  	// Enable start task button, this is removed since you can only start stop once.
  	//var startTask = $('startTask');
  	//startTask.style.visibility ='';
  	var taskPos = ghs.srvc.getTaskPos(taskId);
  	if (taskPos > -1) {
  	  stopTask.style.visibility = 'hidden';
  	  ghs.srvc.taskList[taskPos].status="COMPLETED";
  	  ghs.srvc.taskList[taskPos].properties.highlight = ghs.config.COMPLETE_HLGHT;
  	  ghs.util.highlight("id_" + ghs.srvc.taskList[taskPos].id , ghs.config.COMPLETE_HLGHT);
  	  ghs.ui.back(true);
  	  // Make ajax call
  	  ghs.srvc.completeTask(ghs.ui.empId, taskId);
  	}
  }
};
