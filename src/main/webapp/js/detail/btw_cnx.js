/**
 * @author Administrator
 *
 * M.Kurabi
 */

/*
 #############################################
 	BTW-Arrival Properties
 ############################################# 
 */

/* Declare an object to which we can add real functions. */
if (ghs.detail.btw_cnx == null) { ghs.detail.btw_cnx = {}; };

// Initializing function that activates the templates dynamic content
ghs.detail.btw_cnx.init = function(data) {
  if (data.showSS) {
  	if (data.status == 'DEFAULT') {
  	  ghs.util.setStyleVisibility('startTask', true);
  	} else if (data.status == 'STARTED') {
  	  ghs.util.setStyleVisibility('stopTask', true);
  	}
  }
  if (!data.hcBags || data.hcBags == "#") { $('_hcBags').style.display = 'none'; }
  if (!data.sta) { $('_sta').style.display = 'none'; }
  if (!data.hc || data.hc == "#") { $('_connectionsSection').style.display = 'none'; }
  if (!data.comments || (data.comments.length <= 0)) { $('comments').style.display = 'none'; }
  if (!data.opsRemarksArr) {
	  $('opsRemarks').style.display = 'none';
  }
  if (window.navigator.userAgent.indexOf("Chrome") != -1){
	 if (data.comments || (data.comments.length > 0)) { $('comments').style.paddingTop = '20px'; }
	 if (data.opsRemarksArr) { $('opsRemarks').style.paddingTop = '20px'; }
  }
};

// List of headings to populate into the template (language specific)
if (ghs.detail.btw_cnx.headings == null) { ghs.detail.btw_cnx.headings = {}; };

ghs.detail.btw_cnx.hlghtMap = {
  type:"_type",
  startTime:'_time', endTime:'_time',
  hcPax:"_hcBags", hcBags:"_hcBags",
  aircraftFin:'_aircraftFin',
  flightNum:"_flightNum",
  arrival:"_arrival",
  eta:"_eta",
  sta:"_sta",
  gate:'_gate',
  hc:'_connections',
  comments:'_comments',
  opsRemarks:'_opsRemarks'
};
 

ghs.detail.btw_cnx.itemRenderers =
	{hc:"<div id='empty' class='row'>" + 
			"<div class='col ctr' style='width: 22%;'><p>${flightNum}</p></div>" +
			"<div class='col ctr' style='width: 15%;'><p>${fin}</p></div>" +
			"<div class='col ctr' style='width: 18%;'><p>${etd}</p></div>" +
			"<div class='col ctr' style='width: 16%;'><p>${gate}</p></div>" +
			"<div class='col ctr' style='width: 16%;'><p>${dest}</p></div>" +
			"<div class='col ctr' style='width: 13%;'><p>${bags}</p></div>" +
		"</div>"
	};

//Generic template that takes in headings and data
ghs.detail.btw_cnx.template =
				"<div class='widthContainer row pad2Top pad3Bot display_${showSS}'>" +
					"<div class='col np ctr startTask' style='width:43%;background-image:url(\"/gc/images/" + ghs.ui.lang + "/btn_startTask_disabled.png\");background-repeat: no-repeat;'>" +
						"<a id='startTask' style='visibility:hidden;' href='#' onclick='javascript:ghs.detail.startTask(\"${id}\");return false;'>" +
							"<p><img src='/gc/images/" + ghs.ui.lang + "/btn_startTask_off.png' /></p>" +
						"</a>" +
					"</div>" +
					"<div class='col' style='width:2%'></div>" +
					"<div class='col np ctr stopTask' style='width:55%;background-image:url(\"/gc/images/" + ghs.ui.lang + "/btn_completeTask_disabled.png\");background-repeat: no-repeat;'>" +
						"<a id='stopTask' style='visibility:hidden;' href='#' onclick='javascript:ghs.detail.stopTask(\"${id}\");return false;'>" +
							"<p><img src='/gc/images/" + ghs.ui.lang + "/btn_completeTask_off.png' /></p>" +
						"</a>" +
					"</div>" +
				"</div>" +
				"<div class='row nm heading'>" +
					"<div class='col np headingIcon'><img src='/gc/images/heading/task_hands-on.png' /></div>" +
					"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_one}</p></div>" +
				"</div>" +
				"<div id='_type' class='row'>" +
					"<div class='col' style='width:49%'><p class='pad5lft'>${_type}</p></div>" +
					"<div class='col' style='width:50%'><p>${type}</p></div>" +
				"</div>" +
				"<div id='_time' class='row'>" +
					"<div class='col' style='width:49%'><p class='pad5lft'>${_startTime}&nbsp;&#47;&nbsp;${_endTime}</p></div>" +
					"<div class='col' style='width:50%'><p>${startTime}&nbsp;&#45;&nbsp;${endTime}</p></div>" +
				"</div>" +
				"<div id='_hcBags' class='row'>" +
					"<div class='col' style='width:49%'><p class='pad5lft'>${_hcBags}</p></div>" +
					"<div class='col' style='width:50%'><p>${hcBags}</p></div>" +
				"</div>" +
				"<div class='row nm heading'>" +
					"<div class='col np headingIcon'><img src='/gc/images/heading/flightInfo.png' /></div>" +
					"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_two}</p></div>" +
				"</div>" +
				"<div id='_aircraftFin' class='row'>" +
					"<div class='col' style='width:49%'><p class='pad5lft'>${_aircraftFin}</p></div>" +
					"<div class='col' style='width:50%'><p>${aircraftFin}</p></div>" +
				"</div>" +
				"<div id='_flightNum' class='row'>" +
					"<div class='col' style='width:49%'><p class='pad5lft'>${_flightNum}</p></div>" +
					"<div class='col' style='width:50%'><p>${flightNum}</p></div>" +
				"</div>" +
				"<div id='_arrival' class='row'>" +
					"<div class='col' style='width:49%'><p class='pad5lft'>${_arrival}</p></div>" +
					"<div class='col' style='width:50%'><p>${arrival}</p></div>" +
				"</div>" +
				"<div id='_gate' class='row'>" +
					"<div class='col' style='width:49%'><p class='pad5lft'>${_gate}</p></div>" +
					"<div class='col' style='width:50%'><p>${gate}</p></div>" +
				"</div>" +
				"<div id='_eta' class='row'>" +
					"<div class='col' style='width:49%'><p class='pad5lft'>${_eta}</p></div>" +
					"<div class='col' style='width:50%'><p>${eta}</p></div>" +
				"</div>" +
				"<div id='_sta' class='row'>" +
					"<div class='col' style='width:49%'><p class='pad5lft'>${_sta}</p></div>" +
					"<div class='col' style='width:50%'><p>${sta}</p></div>" +
				"</div>" +
				"<div id='_connectionsSection'>" +
					"<div class='row nm heading'>" +
						"<div class='col np headingIcon'><img src='/gc/images/heading/connection.png' /></div>" +
						"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_five}</p></div>" +
					"</div>" +
					"<div class='row b'>" + 
						"<div class='col ctr' style='width: 22%;'><p>${_cnxFlight}</p></div>" +
						"<div class='col ctr' style='width: 15%;'><p>${_cnxFin}</p></div>" +
						"<div class='col ctr' style='width: 18%;'><p>${_cnxEtd}</p></div>" +
						"<div class='col ctr' style='width: 16%;'><p>${_cnxGate}</p></div>" +
						"<div class='col ctr' style='width: 16%;'><p>${_cnxDestination}</p></div>" +
						"<div class='col ctr' style='width: 13%;'><p>${_cnxBags}</p></div>" +
					"</div>" +
					"<div id='_connections'>" +
						"${hc}" +	
					"</div>" +
				"</div>" +
				"<div id='comments'>" +
					"<div class='row nm heading'>" +
						"<div class='col np headingIcon'><img src='/gc/images/heading/remark.png' /></div>" +
						"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_three}</p></div>" +
					"</div>" +
					"<div id='_comments' class='pad5lft pad2Top pad3Bot'><p>${comments}</p></div>" +
				"</div>" +
				"<div id='opsRemarks'>" +
					"<div class='row nm heading'>" +
						"<div class='col np headingIcon'><img src='/gc/images/heading/remark.png' /></div>" +
						"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_four}</p></div>" +
					"</div>" +
					"<div id='_opsRemarks' class='pad5lft pad2Top'><p>${opsRemarksArr}</p></div>" +
				"</div>";
