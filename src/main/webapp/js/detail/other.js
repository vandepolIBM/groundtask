/**
 * @author M.Kurabi
 */

/*
 #############################################
 	Other Properties
 ############################################# 
 */

/* Declare an object to which we can add real functions. */
if (ghs.detail.other == null) { ghs.detail.other = {}; };

// Initializing function that activates the templates dynamic content	
ghs.detail.other.init = function(data) {
  if (!data.comments || (data.comments.length <= 0)) { $('comments').style.display = 'none'; }
  if (window.navigator.userAgent.indexOf("Chrome") != -1){
	 if (data.comments || (data.comments.length > 0)) { $('comments').style.paddingTop = '20px'; }
  }
};

// List of headings to populate into the template (language specific)
if (ghs.detail.other.headings == null) { ghs.detail.other.headings = {}; };

ghs.detail.other.hlghtMap = {
  type:"_type", startTime:'_time', endTime:'_time', opsRemarks:'_opsRemarks'
};
 
// Generic template that takes in headings and data
ghs.detail.other.template =
				"<div class='row nm heading'>" +
					"<div class='col np headingIcon'><img src='/gc/images/heading/check.png' /></div>" +
					"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_one}</p></div>" +
				"</div>" +
				"<div id='_type' class='row'>" +
					"<div class='col' style='width:45%'><p class='pad5lft'>${_type}</p></div>" +
					"<div class='col' style='width:55%'><p>${type}</p></div>" +
				"</div>" +
				"<div id='_time' class='row'>" +
					"<div class='col' style='width:45%'><p class='pad5lft'>${_startTime}&nbsp;&#47;&nbsp;${_endTime}</p></div>" +
					"<div class='col' style='width:55%'><p>${startTime}&nbsp;&#45;&nbsp;${endTime}</p></div>" +
				"</div>" +
				"<div id='comments'>" +
					"<div class='row nm heading'>" +
						"<div class='col np headingIcon'><img src='/gc/images/heading/remark.png' /></div>" +
						"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_three}</p></div>" +
					"</div>" +
					"<div id='_opsRemarks' class='pad5lft pad2Top'><p>${comments}</p></div>"
				+ "</div>";
