/**
 * @author Administrator
 *
 * M.Kurabi
 */

/*
 #############################################
 	BTW-Arrival Properties
 ############################################# 
 */

/* Declare an object to which we can add real functions. */
if (ghs.detail.btw_arr == null) { ghs.detail.btw_arr = {}; };

// Initializing function that activates the templates dynamic content
ghs.detail.btw_arr.init = function(data) {
  ghs.util.setLoadFactor('loadFactor', data.load, 110, true);
  if (data.showSS) {
  	if (data.status == 'DEFAULT') {
  	  ghs.util.setStyleVisibility('startTask', true);
  	} else if (data.status == 'STARTED') {
  	  ghs.util.setStyleVisibility('stopTask', true);
  	}
  }
  if (!data.bags) { $('_bags').style.display = 'none'; }
  if (!data.hcBags || data.hcBags == "#") { $('_hcBags').style.display = 'none'; }
  if (!data.wchcPax) { $('_wchcPax').style.display = 'none'; }
  if (!data.comments || (data.comments.length <= 0)) { $('comments').style.display = 'none'; }
  if (!data.opsRemarksArr) {
	  $('opsRemarks').style.display = 'none';
  }
  if (!data.resource1) { $('_resource1').style.display = 'none'; }
  if (!data.resource2) { $('_resource2').style.display = 'none'; }
  if (!data.resource3) { $('_resource3').style.display = 'none'; }
  if (!data.resource4) { $('_resource4').style.display = 'none'; }
  
  if (window.navigator.userAgent.indexOf("Chrome") != -1){
	 if (data.comments || (data.comments.length > 0)) { $('comments').style.paddingTop = '20px'; }
	 if (data.opsRemarksArr) { $('opsRemarks').style.paddingTop = '20px'; }
  }
};

// List of headings to populate into the template (language specific)
if (ghs.detail.btw_arr.headings == null) { ghs.detail.btw_arr.headings = {}; };

ghs.detail.btw_arr.hlghtMap = {
  type:"_type", startTime:'_time', endTime:'_time', bags:'_bags', hcBags:"_hcBags",
  wchcPax:"_wchcPax", 
  
  resource1:["_resource1","_resource1_2"], 
  resource2:["_resource2","_resource2_2"], 
  resource3:["_resource3","_resource3_2"],
  resource4:["_resource4","_resource4_2"],

  aircraftFin:'_aircraftFin', flightNum:"_flightNum", arrival:"_arrival",
  eta:"_eta", sta:"_sta", gate:'_gate', load:'_load', comments:'_comments', opsRemarks:'_opsRemarks'
};
 
// Generic template that takes in headings and data
ghs.detail.btw_arr.template =
				"<div class='widthContainer row pad2Top pad3Bot display_${showSS}'>" +
					"<div class='col np ctr startTask' style='width:43%;background-image:url(\"/gc/images/" + ghs.ui.lang + "/btn_startTask_disabled.png\")no-repeat;'>" +
						"<a id='startTask' style='visibility:hidden;' href='#' onclick='javascript:ghs.detail.startTask(\"${id}\");return false;'>" +
							"<p><img src='/gc/images/" + ghs.ui.lang + "/btn_startTask_off.png' /></p>" +
						"</a>" +
					"</div>" +
					"<div class='col' style='width:2%'></div>" +
					"<div class='col np ctr stopTask' style='width:55%;background-image:url(\"/gc/images/" + ghs.ui.lang + "/btn_completeTask_disabled.png\")no-repeat;'>" +
						"<a id='stopTask' style='visibility:hidden;' href='#' onclick='javascript:ghs.detail.stopTask(\"${id}\");return false;'>" +
							"<p><img src='/gc/images/" + ghs.ui.lang + "/btn_completeTask_off.png' /></p>" +
						"</a>" +
					"</div>" +
				"</div>" +
				"<div class='row nm heading'>" +
					"<div class='col np headingIcon'><img src='/gc/images/heading/task_hands-on.png' /></div>" +
					"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_one}</p></div>" +
				"</div>" +
				"<div id='_type' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_type}</p></div>" +
					"<div class='col' style='width:50%'><p>${type}</p></div>" +
				"</div>" +
				"<div id='_time' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_startTime}&nbsp;&#47;&nbsp;${_endTime}</p></div>" +
					"<div class='col' style='width:50%'><p>${startTime}&nbsp;&#45;&nbsp;${endTime}</p></div>" +
				"</div>" +
				"<div id='_bags' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_bags}</p></div>" +
					"<div class='col' style='width:50%'><p>${bags}</p></div>" +
				"</div>" +
				"<div id='_hcBags' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_hcBags}</p></div>" +
					"<div class='col' style='width:50%'><p>${hcBags}</p></div>" +
				"</div>" +
				"<div id='_wchcPax' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_wchcPax}</p></div>" +
					"<div class='col' style='width:50%'><p>${wchcPax}</p></div>" +
				"</div>" +				
				"<div id='_resource1' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${taskType1}</p></div>" +
					"<div class='col' id='_resource1_2' style='width:50%'><p>${resource1}</p></div>" +
				"</div>" +
				"<div id='_resource2' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${taskType2}</p></div>" +
					"<div class='col' id='_resource2_2' style='width:50%'><p>${resource2}</p></div>" +
				"</div>" +
				"<div id='_resource3' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${taskType3}</p></div>" +
					"<div class='col' id='_resource3_2' style='width:50%'><p>${resource3}</p></div>" +
				"</div>" +
				"<div id='_resource4' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${taskType4}</p></div>" +
					"<div class='col' id='_resource4_2' style='width:50%'><p>${resource4}</p></div>" +
				"</div>" +												
				"<div class='row nm heading'>" +
					"<div class='col np headingIcon'><img src='/gc/images/heading/flightInfo.png' /></div>" +
					"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_two}</p></div>" +
				"</div>" +
				"<div id='_aircraftFin' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_aircraftFin}</p></div>" +
					"<div class='col' style='width:50%'><p>${aircraftFin}</p></div>" +
				"</div>" +
				"<div id='_flightNum' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_flightNum}</p></div>" +
					"<div class='col' style='width:50%'><p>${flightNum}</p></div>" +
				"</div>" +
				"<div id='_arrival' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_arrival}</p></div>" +
					"<div class='col' style='width:50%'><p>${arrival}</p></div>" +
				"</div>" +
				"<div id='_gate' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_gate}</p></div>" +
					"<div class='col' style='width:50%'><p>${gate}</p></div>" +
				"</div>" +
				"<div id='_eta' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_eta}</p></div>" +
					"<div class='col' style='width:50%'><p>${eta}</p></div>" +
				"</div>" +
				"<!--div id='_sta' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_sta}</p></div>" +
					"<div class='col' style='width:50%'><p>${sta}</p></div>" +
				"</div-->" +
				"<div id='_load' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_load}</p></div>" +
					"<div id='_loadFactor' class='col np' style='width:50%;height:100%'>" +
						"<div class='loadBarShell mrg3Top' style='width:110px;'>" +
							"<div id='loadFactor' class='loadBar' style='width:110px;'>" +
       							"<p>${load}%</p>" +
							"</div>" +
						"</div>" +
					"</div>" +
				"</div>" +
				"<div id='comments'>" +
					"<div class='row nm heading'>" +
						"<div class='col np headingIcon'><img src='/gc/images/heading/remark.png' /></div>" +
						"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_three}</p></div>" +
					"</div>" +
					"<div id='_comments' class='pad5lft pad2Top pad3Bot'><p>${comments}</p></div>" +
				"</div>" +
				"<div id='opsRemarks'>" +
					"<div class='row nm heading'>" +
						"<div class='col np headingIcon'><img src='/gc/images/heading/remark.png' /></div>" +
						"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_four}</p></div>" +
					"</div>" +
					"<div id='_opsRemarks' class='pad5lft pad2Top'><p>${opsRemarksArr}</p></div>" +
				"</div>";
