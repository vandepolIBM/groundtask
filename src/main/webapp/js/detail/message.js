/**
 * @author Administrator
 *
 * M.Kurabi
 */

/*
 #############################################
 	Message Properties
 ############################################# 
 */

/* Declare an object to which we can add real functions. */
if (ghs.detail.message == null) ghs.detail.message = {};

// Initializing function that activates the templates dynamic content	
ghs.detail.message.init = function(data) {}

// List of headings to populate into the template (language specific)
if (ghs.detail.message.headings == null) ghs.detail.message.headings = {};
 
// Generic template that takes in headings and data
ghs.detail.message.template =
				"<div class='row nm heading grn'>" +
					"<div class='col np headingIcon'><img src='/gc/images/heading/title_alert_tip.png' /></div>" +
					"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_direct_message}</p></div>" +
				"</div>" +
				"<div class='rowGTop pad3Top'>" +
					"<div class='col' style='width:38%'><p class='rht'>${_from}</p></div>" +
					"<div class='col' style='width:2%'></div>" +
					"<div class='col' style='width:58%'><p>${from}</p></div>" +
				"</div>" +
				"<div class='rowGBot'>" +
					"<div class='col' style='width:38%'><p class='rht'>${_time}</p></div>" +
					"<div class='col' style='width:2%'></div>" +
					"<div class='col' style='width:58%'><p>${time}</p></div>" +
				"</div>" +
				"<div class='row break'></div>" +
				"<div><p class='msgTxt'>${message}</p></div>";
