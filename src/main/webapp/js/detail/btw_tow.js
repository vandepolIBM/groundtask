/**
 * @author Administrator
 *
 * M.Kurabi
 */

/*
 #############################################
 	BTW-Tow Properties
 ############################################# 
 */

/* Declare an object to which we can add real functions. */
if (ghs.detail.btw_tow == null) { ghs.detail.btw_tow = {}; };

// Initializing function that activates the templates dynamic content
ghs.detail.btw_tow.init = function(data) {
  if (data.showSS) {
  	if (data.status == 'DEFAULT') {
  	  ghs.util.setStyleVisibility('startTask', true);
  	} else if (data.status == 'STARTED') {
  	  ghs.util.setStyleVisibility('stopTask', true);
  	}
  }
  if (!data.comments || (data.comments.length <= 0)) { $('comments').style.display = 'none'; }
  if (!data.opsRemarksArr && !data.opsRemarksDep) {
	  $('opsRemarks').style.display = 'none';
  } else {
	  if (!data.opsRemarksArr) { $('opsRemarksArr').style.display = 'none'; }
	  if (!data.opsRemarksDep) { $('opsRemarksDep').style.display = 'none'; }
  }
  if (window.navigator.userAgent.indexOf("Chrome") != -1){
	 if (data.comments || (data.comments.length > 0)) { $('comments').style.paddingTop = '20px'; }
	 if (data.opsRemarksArr || data.opsRemarksDep) { $('opsRemarks').style.paddingTop = '20px'; }
  }
};

// List of headings to populate into the template (language specific)
if (ghs.detail.btw_tow.headings == null) { ghs.detail.btw_tow.headings = {}; };

ghs.detail.btw_tow.hlghtMap = {
  type:"_type",
  startTime:'_time', endTime:'_time',
  location:'_location',
  aircraftFin:'_fin',
  groundTime:'_groundTime',
  arrFlightNum:['_flightNum', '_arrFlightNum'], depFlightNum:['_flightNum', '_depFlightNum'],
  arrFrom:['_arrDep', '_arrFrom'], depTo:['_arrDep', '_depTo'],
  arrGate:['_gates', '_arrGate'], depGate:['_gates', '_depGate'],
  arrETA:['_etaEtd', '_arrETA'], depETD:['_etaEtd', '_depETD'],
  arrSTA:['_staStd', '_arrSTA'], depSTA:['_staStd', '_depSTA'],
  comments:'_comments',
  opsRemarks:'_opsRemarks'
};
 
// Generic template that takes in headings and data
ghs.detail.btw_tow.template =
				"<div class='widthContainer row pad2Top pad3Bot display_${showSS}'>" +
					"<div class='col np ctr startTask' style='width:43%;background-image:url(\"/gc/images/" + ghs.ui.lang + "/btn_startTask_disabled.png\");background-repeat: no-repeat;'>" +
						"<a id='startTask' style='visibility:hidden;' href='#' onclick='javascript:ghs.detail.startTask(\"${id}\");return false;'>" +
							"<p><img src='/gc/images/" + ghs.ui.lang + "/btn_startTask_off.png' /></p>" +
						"</a>" +
					"</div>" +
					"<div class='col' style='width:2%'></div>" +
					"<div class='col np ctr stopTask' style='width:55%;background-image:url(\"/gc/images/" + ghs.ui.lang + "/btn_completeTask_disabled.png\");background-repeat: no-repeat;'>" +
						"<a id='stopTask' style='visibility:hidden;' href='#' onclick='javascript:ghs.detail.stopTask(\"${id}\");return false;'>" +
							"<p><img src='/gc/images/" + ghs.ui.lang + "/btn_completeTask_off.png' /></p>" +
						"</a>" +
					"</div>" +
				"</div>" +
				"<div class='row nm heading'>" +
					"<div class='col np headingIcon'><img src='/gc/images/heading/task_hands-on.png' /></div>" +
					"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_one}</p></div>" +
				"</div>" +
				"<div id='_type' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_type}</p></div>" +
					"<div class='col' style='width:50%'><p>${type}</p></div>" +
				"</div>" +
				"<div id='_time' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_startTime}&nbsp;&#47;&nbsp;${_endTime}</p></div>" +
					"<div class='col' style='width:50%'><p>${startTime}&nbsp;&#45;&nbsp;${endTime}</p></div>" +
				"</div>" +
				"<div id='_location' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_location}</p></div>" +
					"<div class='col' style='width:50%'><p>${location}</p></div>" +
				"</div>" +
				"<div class='row nm heading'>" +
					"<div class='col np headingIcon'><img src='/gc/images/heading/flightInfo.png' /></div>" +
					"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_two}</p></div>" +
				"</div>" +
				"<div id='_fin' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_aircraftFin}</p></div>" +
					"<div class='col' style='width:50%'><p>${aircraftFin}</p></div>" +
				"</div>" +
				"<div id='_groundTime' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_groundTime}</p></div>" +
					"<div class='col' style='width:50%'><p>${groundTime}</p></div>" +
				"</div>" +
				"<div class='rowTop'>" +
					"<div class='col' style='width:47%'></div>" +
					"<div class='col np gry ctr' style='width:25%;height:100%'><p><img src='/gc/images/catagory/atw_arr.png' /></p></div>" +
					"<div class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p><img src='/gc/images/catagory/atw_dep.png' /></p></div>" +
				"</div>" +
				"<div id='_flightNum' class='rowMid'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_flightNum}</p></div>" +
					"<div id='_arrFlightNum' class='col np gry ctr' style='width:25%;height:100%'><p class='pad4Top'>${arrFlightNum}</p></div>" +
					"<div id='_depFlightNum' class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p class='pad4Top'>${depFlightNum}</p></div>" +
				"</div>" +
				"<div id='_arrDep' class='rowMid'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_arrDep}</p></div>" +
					"<div id='_arrFrom' class='col np gry ctr' style='width:25%;height:100%'><p class='pad4Top'>${arrFrom}</p></div>" +
					"<div id='_depTo' class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p class='pad4Top'>${depTo}</p></div>" +
				"</div>" +
				"<div id='_gates' class='rowMid'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_gates}</p></div>" +
					"<div id='_arrGate' class='col np gry ctr' style='width:25%;height:100%'><p class='pad4Top'>${arrGate}</p></div>" +
					"<div id='_depGate' class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p class='pad4Top'>${depGate}</p></div>" +
				"</div>" +
				"<div id='_etaEtd' class='rowBot'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_etaEtd}</p></div>" +
					"<div id='_arrETA' class='col np gry ctr' style='width:25%;height:100%'><p class='pad4Top'>${arrETA}</p></div>" +
					"<div id='_depETD' class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p class='pad4Top'>${depETD}</p></div>" +
				"</div>" +
				"<!--div id='_staStd' class='rowBot'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_staStd}</p></div>" +
					"<div id='_arrSTA' class='col np gry ctr' style='width:25%;height:100%'><p class='pad4Top'>${arrSTA}</p></div>" +
					"<div id='_depSTD' class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p class='pad4Top'>${depSTD}</p></div>" +
				"</div-->" +
				"<div id='comments'>" +
					"<div class='row nm heading'>" +
						"<div class='col np headingIcon'><img src='/gc/images/heading/remark.png' /></div>" +
						"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_three}</p></div>" +
					"</div>" +
					"<div id='_comments' class='pad5lft pad2Top pad3Bot'><p>${comments}</p></div>" +
				"</div>" +
				"<div id='opsRemarks'>" +
					"<div class='row nm heading'>" +
						"<div class='col np headingIcon'><img src='/gc/images/heading/remark.png' /></div>" +
						"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_four}</p></div>" +
					"</div>" +
					"<div id='opsRemarksArr' class='pad5lft pad2Top'><p>${_opsRemarksArr}</p><p>${opsRemarksArr}</p></div>" +
					"<div id='opsRemarksDep' class='pad5lft pad2Top'><p>${_opsRemarksDep}</p><p>${opsRemarksDep}</p></div>" +
				"</div>";
