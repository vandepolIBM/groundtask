/**
 * @author Administrator
 *
 * M.Kurabi
 */

/*
 #############################################
 	BTW-Turn Properties
 ############################################# 
 */

/* Declare an object to which we can add real functions. */
if (ghs.detail.btw_turn == null) { ghs.detail.btw_turn = {}; };

// Initializing function that activates the templates dynamic content
ghs.detail.btw_turn.init = function(data) {
  ghs.util.setLoadFactor('arrLoad', data.arrLoad, 46, true);
  ghs.util.setLoadFactor('depLoad', data.depLoad, 46, true);
  if (data.showSS) {
  	if (data.status == 'DEFAULT') {
  	  ghs.util.setStyleVisibility('startTask', true);
  	} else if (data.status == 'STARTED') {
  	  ghs.util.setStyleVisibility('stopTask', true);
  	}
  }
  if (!data.arrBags && !data.depBags) { $('_bags').style.display = 'none'; }
  if (!data.arrHcBags && !data.depHcBags) { $('_hcBags').style.display = 'none'; }
  if (!data.arrSTA && !data.depSTD) { $('_staStd').style.display = 'none'; }
  if (!data.comments || (data.comments.length <= 0)) { $('comments').style.display = 'none'; }
  if (!data.opsRemarksArr && !data.opsRemarksDep) {
	  $('opsRemarks').style.display = 'none';
  } else {
	  if (!data.opsRemarksArr) { $('opsRemarksArr').style.display = 'none'; }
	  if (!data.opsRemarksDep) { $('opsRemarksDep').style.display = 'none'; }
  }
  if (!data.resource1) { $('_resource1').style.display = 'none'; }
  if (!data.resource2) { $('_resource2').style.display = 'none'; }
  if (!data.resource3) { $('_resource3').style.display = 'none'; }
  if (!data.resource4) { $('_resource4').style.display = 'none'; }  
  
  if (window.navigator.userAgent.indexOf("Chrome") != -1){
	 if (data.comments || (data.comments.length > 0)) { $('comments').style.paddingTop = '20px'; }
	 if (data.opsRemarksArr || data.opsRemarksDep) { $('opsRemarks').style.paddingTop = '20px'; }
  }
};

// List of headings to populate into the template (language specific)
if (ghs.detail.btw_turn.headings == null) { ghs.detail.btw_turn.headings = {}; };

ghs.detail.btw_turn.hlghtMap = {
  type:"_type",
  startTime:'_time', endTime:'_time',  
  
  resource1:["_resource1","_resource1_2"], 
  resource2:["_resource2","_resource2_2"], 
  resource3:["_resource3","_resource3_2"],
  resource4:["_resource4","_resource4_2"],  
  
  arrBags:['_bags', '_arrBags'], depBags:['_bags', '_depBags'],
  arrHcBags:['_hcBags', '_arr_hcBags'], depHcBags:['_hcBags', '_dep_hcBags'],
  arrWchcPax:['_wchcPax', '_arrWchcPax'], depWchcPax:['_wchcPax', '_depWchcPax'],
  aircraftFin:'_fin',
  groundTime:'_groundTime',
  arrFlightNum:['_flightNum', '_arrFlightNum'], depFlightNum:['_flightNum', '_depFlightNum'],
  arrFrom:['_arrDep', '_arrFrom'], depTo:['_arrDep', '_depTo'],
  arrETA:['_etaEtd', '_arrETA'], depETD:['_etaEtd', '_depETD'],
  arrSTA:['_staStd', '_arrSTA'], depSTA:['_staStd', '_depSTA'],
  arrGate:['_gate', '_arrGate'], depGate:['_gate', '_depGate'],
  arrLoad:['_load', '_arrLoad'], depLoad:['_load', '_depLoad'],
  comments:'_comments',
  opsRemarks:'_opsRemarks'
};
 
// Generic template that takes in headings and data
ghs.detail.btw_turn.template =
				"<div class='widthContainer row pad2Top pad3Bot display_${showSS}'>" +
					"<div class='col np ctr startTask' style='width:43%;background-image:url(\"/gc/images/" + ghs.ui.lang + "/btn_startTask_disabled.png\")no-repeat;'>" +
						"<a id='startTask' style='visibility:hidden;' href='#' onclick='javascript:ghs.detail.startTask(\"${id}\");return false;'>" +
							"<p><img src='/gc/images/" + ghs.ui.lang + "/btn_startTask_off.png' /></p>" +
						"</a>" +
					"</div>" +
					"<div class='col' style='width:2%'></div>" +
					"<div class='col np ctr stopTask' style='width:55%;background-image:url(\"/gc/images/" + ghs.ui.lang + "/btn_completeTask_disabled.png\")no-repeat;'>" +
						"<a id='stopTask' style='visibility:hidden;' href='#' onclick='javascript:ghs.detail.stopTask(\"${id}\");return false;'>" +
							"<p><img src='/gc/images/" + ghs.ui.lang + "/btn_completeTask_off.png' /></p>" +
						"</a>" +
					"</div>" +
				"</div>" +
				"<div class='row nm heading'>" +
					"<div class='col np headingIcon'><img src='/gc/images/heading/task_hands-on.png' /></div>" +
					"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_one}</p></div>" +
				"</div>" +
				"<div id='_type' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_type}</p></div>" +
					"<div class='col' style='width:50%'><p>${type}</p></div>" +
				"</div>" +
				"<div id='_time' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_startTime}&nbsp;&#47;&nbsp;${_endTime}</p></div>" +
					"<div class='col' style='width:50%'><p>${startTime}&nbsp;&#45;&nbsp;${endTime}</p></div>" +
				"</div>" +
				"<div id='_resource1' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${taskType1}</p></div>" +
					"<div class='col' id='_resource1_2' style='width:50%'><p>${resource1}</p></div>" +
				"</div>" +
				"<div id='_resource2' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${taskType2}</p></div>" +
					"<div class='col' id='_resource2_2' style='width:50%'><p>${resource2}</p></div>" +
				"</div>" +
				"<div id='_resource3' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${taskType3}</p></div>" +
					"<div class='col' id='_resource3_2' style='width:50%'><p>${resource3}</p></div>" +
				"</div>" +
				"<div id='_resource4' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${taskType4}</p></div>" +
					"<div class='col' id='_resource4_2' style='width:50%'><p>${resource4}</p></div>" +
				"</div>" +												
				"<div class='rowTop'>" +
					"<div class='col' style='width:47%'></div>" +
					"<div class='col np gry ctr' style='width:25%;height:100%'><p><img src='/gc/images/catagory/atw_arr.png' /></p></div>" +
					"<div class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p><img src='/gc/images/catagory/atw_dep.png' /></p></div>" +
				"</div>" +
				"<div id='_bags' class='rowMid'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_bags}</p></div>" +
					"<div id='_arrBags' class='col np gry ctr' style='width:25%;height:100%'><p class='pad4Top'>${arrBags}</p></div>" +
					"<div id='_depBags' class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p class='pad4Top'>${depBags}</p></div>" +
				"</div>" +
				"<div id='_hcBags' class='rowMid'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_hcBags}</p></div>" +
					"<div id='_arr_hcBags' class='col np gry ctr' style='width:25%;height:100%'><p class='pad4Top'>${arrHcBags}</p></div>" +
					"<div id='_dep_hcBags' class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p class='pad4Top'>${depHcBags}</p></div>" +
				"</div>" +
				"<div id='_wchcPax' class='rowBot'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_wchcPax}</p></div>" +
					"<div id='_arrWchcPax' class='col np gry ctr' style='width:25%;height:100%'><p class='pad4Top'>${arrWchcPax}</p></div>" +
					"<div id='_depWchcPax' class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p class='pad4Top'>${depWchcPax}</p></div>" +
				"</div>" +
				
				"<div class='row nm heading'>" +
					"<div class='col np headingIcon'><img src='/gc/images/heading/flightInfo.png' /></div>" +
					"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_two}</p></div>" +
				"</div>" +
				"<div id='_fin' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_aircraftFin}</p></div>" +
					"<div class='col' style='width:50%'><p>${aircraftFin}</p></div>" +
				"</div>" +
				"<div id='_groundTime' class='row'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_groundTime}</p></div>" +
					"<div class='col' style='width:50%'><p>${groundTime}</p></div>" +
				"</div>" +
				"<div class='rowTop'>" +
					"<div class='col' style='width:47%'></div>" +
					"<div class='col np gry ctr' style='width:25%;height:100%'><p><img src='/gc/images/catagory/atw_arr.png' /></p></div>" +
					"<div class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p><img src='/gc/images/catagory/atw_dep.png' /></p></div>" +
				"</div>" +
				"<div id='_flightNum' class='rowMid'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_flightNum}</p></div>" +
					"<div id='_arrFlightNum' class='col np gry ctr' style='width:25%;height:100%'><p class='pad4Top'>${arrFlightNum}</p></div>" +
					"<div id='_depFlightNum' class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p class='pad4Top'>${depFlightNum}</p></div>" +
				"</div>" +
				"<div id='_arrDep' class='rowMid'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_arrDep}</p></div>" +
					"<div id='_arrFrom' class='col np gry ctr' style='width:25%;height:100%'><p class='pad4Top'>${arrFrom}</p></div>" +
					"<div id='_depTo' class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p class='pad4Top'>${depTo}</p></div>" +
				"</div>" +
				"<div id='_gate' class='rowMid'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_gate}</p></div>" +
					"<div id='_arrGate' class='col np gry ctr' style='width:25%;height:100%'><p class='pad4Top'>${arrGate}</p></div>" +
					"<div id='_depGate' class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p class='pad4Top'>${depGate}</p></div>" +
				"</div>" +
				"<div id='_etaEtd' class='rowMid'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_etaEtd}</p></div>" +
					"<div id='_arrETA' class='col np gry ctr' style='width:25%;height:100%'><p class='pad4Top'>${arrETA}</p></div>" +
					"<div id='_depETD' class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p class='pad4Top'>${depETD}</p></div>" +
				"</div>" +
				"<div id='_staStd' class='rowMid'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_staStd}</p></div>" +
					"<div id='_arrSTA' class='col np gry ctr' style='width:25%;height:100%'><p class='pad4Top'>${arrSTA}</p></div>" +
					"<div id='_depSTD' class='col np gry mrg4Lft ctr' style='width:25%;height:100%'><p class='pad4Top'>${depSTD}</p></div>" +
				"</div>" +
				"<div id='_load' class='rowBot'>" +
					"<div class='col' style='width:47%'><p class='pad5lft'>${_load}</p></div>" +
					"<div id='_arrLoad' class='col np gry' style='width:25%;height:100%'>" +
						"<div class='loadBarShell mrg4Lft mrg3Top' style='width:46px;'>" +
							"<div id='arrLoad' class='loadBar' style='width:46px;'>" +
	   							"<p>${arrLoad}%</p>" +
							"</div>" +
						"</div>" +
					"</div>" +
					"<div id='_depLoad' class='col np gry mrg4Lft' style='width:25%;height:100%'>" +
						"<div class='loadBarShell mrg4Lft mrg3Top' style='width:46px;'>" +
							"<div id='depLoad' class='loadBar' style='width:46px;'>" +
	   							"<p>${depLoad}%</p>" +
							"</div>" +
						"</div>" +
					"</div>" +
				"</div>" +
				"<div id='comments'>" +
					"<div class='row nm heading'>" +
						"<div class='col np headingIcon'><img src='/gc/images/heading/remark.png' /></div>" +
						"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_three}</p></div>" +
					"</div>" +
					"<div id='_comments' class='pad5lft pad2Top pad3Bot'><p>${comments}</p></div>" +
				"</div>" +
				"<div id='opsRemarks'>" +
					"<div class='row nm heading'>" +
						"<div class='col np headingIcon'><img src='/gc/images/heading/remark.png' /></div>" +
						"<div class='col np pad4Top cW' style='width:85%'><p class='f12 b'>${_heading_four}</p></div>" +
					"</div>" +
					"<div id='opsRemarksArr' class='pad5lft pad2Top'><p>${_opsRemarksArr}</p><p>${opsRemarksArr}</p></div>" +
					"<div id='opsRemarksDep' class='pad5lft pad2Top'><p>${_opsRemarksDep}</p><p>${opsRemarksDep}</p></div>" +
				"</div>";
