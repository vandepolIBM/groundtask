/**
 *
 * Services
 * GHS Client Services that talk with UI & Server
 *
 * @author M.Kurabi
 */

/**
 * Declare an object to which we can add service objects to
 */
//DVPalert("srvc init");
if (ghs.srvc == null) { ghs.srvc = {}; }

ghs.srvc.taskList = []; // List of tasks
ghs.srvc.alertList = []; // List of alerts
ghs.srvc.lastAlertStatusSent = false;
ghs.srvc.allAlertsReceived = []; // keep records of all received alerts

ghs.srvc.alertsReceivedList = []; // List Queue of alerts received
ghs.srvc.tasksAcknowledged = []; // List Queue of tasks acknowledged
ghs.srvc.tasksStarted = []; // List Queue of tasks started
ghs.srvc.tasksCompleted = []; // List Queue of tasks completed

// Timeout lengths
ghs.srvc.POLL_INTERVAL = 10000; // Poll every 10 seconds when disconnected
ghs.srvc.GENERIC_TIMEOUT = 22000;
ghs.srvc.AJAX_TIMEOUT = 60000;
ghs.srvc.RAJAX_POLL_TIMEOUT = 75000;
ghs.srvc.QUEUE_INTERVAL = 25000;

// Static fields
ghs.srvc.ALERT_TYPE_ADD = 'n';
ghs.srvc.ALERT_TYPE_EDT = 'e';
ghs.srvc.ALERT_TYPE_DEL = 'd';
ghs.srvc.ALERT_TYPE_MSG = 'm';

ghs.srvc.SEVERITY_LEVEL = 1;

// Timers for non-gear supported browsers
ghs.srvc.REG_POLL_TIMEOUT_TIMER_ID = null;
ghs.srvc.REG_SRVC_QUEUE_TIMER_ID = null;
ghs.srvc.REG_SIGNAL_TIMER_ID = null;
ghs.srvc.REG_GENERIC_TIMER_ID = null;

ghs.srvc.networkStatus = true;

ghs.srvc.getAlertsLock = false;

// ghs.srvc.isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

/**
 * Server calls to indicate to client to get updates
 */
ghs.srvc.getTaskUpdates = function() {
  ghs.log.info('[ghs.srvc.getTaskUpdates] (Server Request) Server is requesting to get tasks from server....');
  ghs.srvc.getTasks(ghs.ui.empId, false);
};
/**
 * Server calls to indicate to client to get updates
 */
ghs.srvc.getAlertUpdates = function() {
  ghs.log.info('[ghs.srvc.getAlertUpdates] (Server Request) Server is requesting to get alerts from server...');
  ghs.srvc.getAlerts(ghs.ui.empId);
};

/** @private receiveAlerts Callback */
ghs.srvc.receiveAlerts = function(data) {
  ghs.log.info('[ghs.srvc.receiveAlerts] (Server Request) Receiving alerts...(Delegating to [ghs.srvc._getAlertsCallback])');
  ghs.srvc._getAlertsCallback(data);
};

/**
 * Server calls to indicate to client to get updates
 */
ghs.srvc.remoteProcedureCall = function(data) {
  ghs.log.info('[ghs.srvc.remoteExecute] Executing remote procedure call..');
  try {
  	var result = eval(data);
  	ghs.log.info('[ghs.srvc.remoteExecute] RPC Result: ' + result);
  } catch (ex) {
  	ghs.log.info('[ghs.srvc.remoteExecute] RPC Exception Occured: ' + ex.message);
  }
};

/**
 * @public Called by client upon logging in
 * @param empId Employee ID
 * @param lang user prefered language
 */
ghs.srvc.login = function(empId, lang) {
  //alert("login1");
  ghs.log.info('[ghs.srvc.login] Logging in Employee#' + empId + "...");
  ghs.srvc._setGenericTimeout(ghs.srvc._login, ghs.srvc.GENERIC_TIMEOUT); // Start Generic Timeout
  ghs.log.info('[ghs.srvc.login] Logging in Employee#' + empId + " 1");

  ghs.log.info("login2b: " + dwr.engine._scriptSessionId);
  ghs.log.info('[ghs.srvc.login] Logging in Employee#' + empId + " scriptSessionId = " + dwr.engine._scriptSessionId);
  if (!dwr.engine._scriptSessionId) {
    //DVP alert("login2c: " + dwr.engine._pathToDwrServlet);
  	dwr.engine._execute("/gc/dwr", '__System', 'pageLoaded', [ function() {
      dwr.engine._ordered = false;
  	}]);
    //DVP alert("login2d: ");
  	return;
  }
  // add any login options if required
  var options = new Array();
  if (ghs.ui.enableBrock == 'true') {
	  options[0] = 'ralert';
  }
  ghs.log.info("login calling TaskCreator.loginWithOptions -START");
  TaskCreator.loginWithOptions(empId, options, {callback:ghs.srvc._loginCallback,
  							errorHandler:ghs.srvc._loginErrorHandler,
  							timeout:ghs.srvc.AJAX_TIMEOUT});
  ghs.log.info("login calling TaskCreator.loginWithOptions - DONE");
};
/**
 * @private Helper function when login ajax calls fails
 */
ghs.srvc._login = function() {
  ghs.log.info('[ghs.srvc._loginErrorHandler] Cannot connect to server. Retrying...');
  ghs.ui.setStatus(ghs.ui.getMessage('statusContactingServer'));
  ghs.srvc.login(ghs.ui.empId, ghs.ui.lang);
};
/** @private login Callback */
ghs.srvc._loginCallback = function(data) {
  ghs.srvc._clearGenericTimeout(); // Clear timeout
  ghs.log.info('[ghs.srvc._loginCallback] Login Succeeded.');
  ghs.ui.setStatus("");
  ghs.srvc.getTasks(ghs.ui.empId, true);
};
/** @private login Error Handler */
ghs.srvc._loginErrorHandler = function(msg, ex) {
  ghs.log.warn('[ghs.srvc._loginErrorHandler] Ajax error occoured:' + msg + ":" + ex.message);
};

/**
 * @public called to request updated alert list
 * @param empld Employee ID
 */
ghs.srvc.getAlerts = function(empId) {
  if (ghs.srvc.getAlertsLock) {
  	ghs.log.info('[ghs.srvc.getAlerts] Get Alerts request is already in queue or is processing.');
  	return;
  }
  ghs.srvc.getAlertsLock = true; // Close getAlert lock
  ghs.srvc._getAlerts(empId);
};

/**
 * @private getAlerts Helper function
 */
ghs.srvc._getAlerts = function(empId) {
  if (ghs.srvc.alertsReceivedList.length <= 0) {
  	ghs.log.info('[ghs.srvc._getAlerts] Retrieving alerts...');
  	ghs.srvc._activateQueues(); // Reset Service queues
    TaskCreator.getAlerts(empId, {callback:ghs.srvc._getAlertsCallback,
  							      errorHandler:ghs.srvc._getAlertsErrorHandler,
  							      timeout:ghs.srvc.AJAX_TIMEOUT});
  } else {
  	ghs.log.info('[ghs.srvc._getAlerts] Alerts Received list has alerts to acknowledge, cannot get alerts.');
  }
};
/** @private getAlerts Callback */
ghs.srvc._getAlertsCallback = function(data) {
  var isArray = ghs.util._isArray(data);
  if (!isArray) {
  	data = [data]; // Convert data to an array if it isn't
  }

  // if (window.chrome && data.length) {
  //   var filteredData = [];
  //   // filter Alerts
  //   data.map(newAlert => {
  //     // find if new alert was Received before
  //     var flag = ghs.srvc.allAlertsReceived.some(savedAlert => newAlert.id === savedAlert.id);
  //     // if alert is not Received before add it
  //     if(!flag) { filteredData.push(newAlert); }
  //   });
  //   // keep record of all Received Alerts
  //   ghs.srvc.allAlertsReceived = ghs.srvc.allAlertsReceived.concat(filteredData);
  //   data = filteredData;
  // }

  var originalLength = ghs.srvc.alertList.length;
  var processedAlerts = 0;
  var severityCount = 0;
  var receivedAlerts = new Array();
  var i = 0;
  for (i = 0; i < data.length; i++) {
  	try {
  	  switch (data[i].type) {
  	    case ghs.srvc.ALERT_TYPE_ADD:
  	      // Add Task
  	  	  if ( ghs.srvc._addEvent(data[i])) {
  	  	    processedAlerts++;
  	  	    severityCount = ghs.srvc._isSeverityOne(data[i].severity) ? ++severityCount : severityCount;
  	  	  }
  	   	  break;
  	    case ghs.srvc.ALERT_TYPE_EDT:
  	      // Edit Task, add/merge alert
  	  	  if ( ghs.srvc._editEvent(data[i])) {
  	  	  	processedAlerts++;
  	  	  	severityCount = ghs.srvc._isSeverityOne(data[i].severity) ? ++severityCount : severityCount;
  	  	  }
  	  	  break;
  	    case ghs.srvc.ALERT_TYPE_DEL:
  	  	  // Delete Task, add alert
  	  	  if ( ghs.srvc._deleteEvent(data[i])) {
  	  	    processedAlerts++;
  	  	    severityCount = ghs.srvc._isSeverityOne(data[i].severity) ? ++severityCount : severityCount;
  	  	  }
  	   	  break;
		case ghs.srvc.ALERT_TYPE_MSG:
  	  	  // Add direct msg
  	  	  if ( ghs.srvc._messageEvent(data[i]) ) {
  	  	    processedAlerts++;
  	  	    severityCount = ghs.srvc._isSeverityOne(data[i].severity) ? ++severityCount : severityCount;
  	  	  }
  	   	  break;
  	  }
  	  receivedAlerts.push(data[i].id);
  	} catch (ex) {
  	  ghs.log.warn('[ghs.srvc._getAlertsCallback] Exception occoured:' + ex.message);
  	  continue;
  	}
  }
  // If original array length before insert is 0 and result of push results in array length of > 0,
  // then call the alertsExist function
  if ((originalLength == 0) && (ghs.srvc.alertList.length > 0)) {
	  ghs.srvc.alertsExist(ghs.ui.empId, true);
  }
  ghs.srvc.getAlertsLock = false; // Open getAlert lock
  ghs.log.info('[ghs.srvc._getAlertsCallback] Alert(s) received: [' + receivedAlerts + '].');
  ghs.log.info('[ghs.srvc._getAlertsCallback] ' + processedAlerts + ' alerts processed.');
  // if not chrome run ghs.srvc.alertsRecieved
  // window.chrome ? '' : ghs.srvc.alertsRecieved(ghs.ui.empId, receivedAlerts, true); // Send Confirmation of alerts
  ghs.srvc.alertsRecieved(ghs.ui.empId, receivedAlerts, true); // Send Confirmation of alerts

  if (processedAlerts > 0) {
  	// Sort Task List
  	ghs.srvc.taskList.sort(ghs.srvc._sortByTime);
  	// Fire-off event to indicate new alert(s) arrived
  	ghs.ui.eventHandler(ghs.event.ALERT_LIST_UPDATED, { alertCount:processedAlerts, severity:severityCount });
  }
};
/** @private getAlerts Error Handler */
ghs.srvc._getAlertsErrorHandler = function(msg, ex) {
  ghs.log.warn('[ghs.srvc._getAlertsErrorHandler] Ajax error occoured:' + msg + ":" + ex.message);
};

/**
 * @public called to request updated task list
 * @param empld Employee ID
 */
ghs.srvc.getTasks = function(empId, firstTime) {
  ghs.log.info('[ghs.srvc.getTasks] Retrieving tasks...');
  if (firstTime) {
    ghs.srvc._setGenericTimeout(ghs.srvc._getTasks, ghs.srvc.GENERIC_TIMEOUT);
  }
  TaskCreator.getTasks(empId, {callback:ghs.srvc._getTasksCallback,
  							   arg:firstTime,
  							   errorHandler:ghs.srvc._getTasksErrorHandler,
  							   timeout:ghs.srvc.AJAX_TIMEOUT});
};
/**
 * @private Helper function when getTasks ajax calls fails
 */
ghs.srvc._getTasks = function() {
  ghs.log.info('[ghs.srvc._getTasksErrorHandler] Task retrieval failed. Retrying...');
  ghs.ui.setStatus(ghs.ui.getMessage('statusGettingTaskList'));
  ghs.srvc.getTasks(ghs.ui.empId, true);
};
/** @private getTasks Callback */
ghs.srvc._getTasksCallback = function(data, firstTime) {
  if (firstTime) { ghs.srvc._clearGenericTimeout(); };
  ghs.log.info('[ghs.srvc._getTasksCallback] Tasks recieved.');
  ghs.ui.setStatus("");
  ghs.srvc.taskList = data;
  ghs.srvc.taskList.sort(ghs.srvc._sortByTime);
  ghs.srvc._highlightNonAcknowledged(ghs.srvc.taskList);
  ghs.ui.eventHandler(ghs.event.TASK_LIST_UPDATED);
  if (firstTime) {
    dwr.engine.setActiveReverseAjax(true); // Activate R-AJAX if not already active
    ghs.srvc._activateQueues(); // activate service queues if not already active
    ghs.ui._hideLoading(); // Hide loading if not already hidden
    ghs.log.info('[ghs.srvc._activateQueues] Service queues activated.');
    ghs.log.info('[ghs.ui.init] Application initialization complete!');
  }
};
/** @private getTasks Error Handler */
ghs.srvc._getTasksErrorHandler = function(msg, ex) {
  ghs.log.warn('[ghs.srvc._getTasksErrorHandler] Ajax error occoured:' + msg + ":" + ex.message);
};

/**
 * @public used to set start status of a single task
 * @param empld Employee ID
 * @param taskId the task id to set the start status for
 */
ghs.srvc.startTask = function(empId, taskId) {
  taskId = [taskId];
  ghs.srvc.startTasks(empId, taskId, true); // Convert taskId to an array and send
};
/**
 * @public used to set start status of multiple tasks
 * @param empld Employee ID
 * @param taskIds the array of task ids to set to start status
 */
ghs.srvc.startTasks = function(empId, taskIds, addToQueue) {
  ghs.log.info('[ghs.srvc.startTasks] Sending start status of task(s) [' + taskIds + ']...');
  if (addToQueue) {
    ghs.srvc.tasksStarted = ghs.srvc.tasksStarted.concat(taskIds);
  }
  ghs.srvc._activateQueues(); // Reset Queue
  TaskCreator.startTasks(empId, taskIds, {callback:ghs.srvc._startTasksCallback,
  										  arg:taskIds,
    									  errorHandler:ghs.srvc._startTasksErrorHandler,
    									  timeout:ghs.srvc.AJAX_TIMEOUT});
};
/** @private startTasks Callback */
ghs.srvc._startTasksCallback = function(data, taskIds) {
  try {
  	ghs.log.info('[ghs.srvc._startTasksCallback] Task(s) [' + taskIds + '] were set to start status.');
  	// Remove task IDs from received task IDs queue if they were queued
  	ghs.srvc.tasksStarted.subtract(taskIds);
  } catch (ex) {
  	ghs.log.warn('[ghs.srvc._startTasksCallback] Exception occoured:' + ex.message);
  }
};
/** @private startTasks Error Handler */
ghs.srvc._startTasksErrorHandler = function(msg, ex, taskIds) {
  ghs.log.warn('[ghs.srvc._startTasksErrorHandler] Ajax error occoured:' + msg + ":" + ex.message);
};

/**
 * @public used to set complete status of a single task
 * @param empld Employee ID
 * @param taskId the task id to set the complete status for
 */
ghs.srvc.completeTask = function(empId, taskId) {
  taskId = [taskId];
  ghs.srvc.completeTasks(empId, taskId, true); // Convert taskId to an array and send
};
/**
 * @public used to set complete status of multiple tasks
 * @param empld Employee ID
 * @param taskIds the array of task ids to set to complete status
 */
ghs.srvc.completeTasks = function(empId, taskIds, addToQueue) {
  ghs.log.info('[ghs.srvc.completeTasks] Sending complete status of task(s) [' + taskIds + ']...');
  if (addToQueue) {
    ghs.srvc.tasksCompleted = ghs.srvc.tasksCompleted.concat(taskIds);
  }
  ghs.srvc._activateQueues(); // Reset Queue
  TaskCreator.stopTasks(empId, taskIds, {callback:ghs.srvc._completeTasksCallback,
  										  arg:taskIds,
    									  errorHandler:ghs.srvc._completeTasksErrorHandler,
    									  timeout:ghs.srvc.AJAX_TIMEOUT});
};
/** @private completeTasks Callback */
ghs.srvc._completeTasksCallback = function(data, taskIds) {
  try {
  	ghs.log.info('[ghs.srvc._completeTasksCallback] Task(s) [' + taskIds + '] were set to complete status.');
  	// Remove task IDs from received task IDs queue if they were queued
  	ghs.srvc.tasksCompleted.subtract(taskIds);
  } catch (ex) {
  	ghs.log.warn('[ghs.srvc._completeTasksCallback] Exception occoured:' + ex.message);
  }
};
/** @private completeTasks Error Handler */
ghs.srvc._completeTasksErrorHandler = function(msg, ex, taskIds) {
  ghs.log.warn('[ghs.srvc._completeTasksErrorHandler] Ajax error occoured:' + msg + ":" + ex.message);
};

/**
 * @public used to acknowledge a single task
 * @param empld Employee ID
 * @param taskId the task id to acknowledge
 */
ghs.srvc.acknowledgeTask = function(empId, taskId) {
  taskId = [taskId];
  ghs.srvc.acknowledgeTasks(empId, taskId, true); // Convert taskId to an array and send
};
/**
 * @public used to acknowledge multiple tasks
 * @param empld Employee ID
 * @param taskIds the array of task ids to acknowledge
 */
ghs.srvc.acknowledgeTasks = function(empId, taskIds, addToQueue) {
  ghs.log.info('[ghs.srvc.acknowledgeTasks] Acknowledging task(s) [' + taskIds + ']...');
  if (taskIds.length == 0) { return; } // Bail out if there is nothing to confirm
  if (addToQueue) {
    ghs.srvc.tasksAcknowledged = ghs.srvc.tasksAcknowledged.concat(taskIds);
  }
  ghs.srvc._activateQueues(); // Reset Queue
  TaskCreator.acknowledgeTasks(empId, taskIds, {callback:ghs.srvc._acknowledgeTasksCallback,
  												arg:taskIds,
    											errorHandler:ghs.srvc_acknowledgeTasksErrorHandler,
    											timeout:ghs.srvc.AJAX_TIMEOUT});
};
/** @private acknowledgeTasks Callback */
ghs.srvc._acknowledgeTasksCallback = function(data, taskIds) {
  try {
  	ghs.log.info('[ghs.srvc._acknowledgeTasksCallback] Task(s) [' + taskIds + '] were acknowledged.');
  	// Remove task IDs from received task IDs queue if they were queued
  	ghs.srvc.tasksAcknowledged.subtract(taskIds);
  } catch (ex) {
  	ghs.log.warn('[ghs.srvc._acknowledgeTasksCallback] Exception occoured:' + ex.message);
  }
};
/** @private acknowledgeTasks Error Handler */
ghs.srvc._acknowledgeTasksErrorHandler = function(msg, ex, taskIds) {
  ghs.log.warn('[ghs.srvc._acknowledgeTasksErrorHandler] Ajax error occoured:' + msg + ":" + ex.message);
};


/**
 * @public used to confirm recipt of multiple alerts
 * @param empld Employee ID
 * @param alertIds the array of alert ids to acknowledge
 */
ghs.srvc.alertsRecieved = function(empId, alertIds, addToQueue) {
  ghs.log.info('[ghs.srvc.alertsRecieved] Acknowledging receipt of alert(s) [' + alertIds + ']...');
  if (alertIds.length == 0) { return; } // Bail out if there is nothing to confirm
  if (addToQueue) {
  	ghs.srvc.alertsReceivedList = ghs.srvc.alertsReceivedList.concat(alertIds);
  }
  ghs.srvc._activateQueues(); // Reset Queue
  TaskCreator.alertsReceived(empId, alertIds, {callback:ghs.srvc._alertsRecievedCallback,
    										   arg:alertIds,
    										   errorHandler:ghs.srvc._alertsRecievedErrorHandler,
    										   timeout:ghs.srvc.AJAX_TIMEOUT});
};
/** @private alertsRecieved Callback */
ghs.srvc._alertsRecievedCallback = function(data, alertIds) {
  try {
  	ghs.log.info('[ghs.srvc._alertsRecievedCallback] Alert(s): [' + alertIds + '] were acknowledged.');
  	// Remove alert IDs from received alert IDs queue if they were queued
  	ghs.srvc.alertsReceivedList.subtract(alertIds);
  	// Check if getAlerts call is waiting to be executed
  	if (ghs.srvc.getAlertsLock) {
  	  ghs.srvc._getAlerts(ghs.ui.empId);
  	}
  } catch (ex) {
  	ghs.log.warn('[ghs.srvc._alertsRecievedCallback] Exception occoured:' + ex.message);
  }
};
/** @private alertsRecieved Error Handler */
ghs.srvc._alertsRecievedErrorHandler = function(msg, ex, alertIds) {
  ghs.log.warn('[ghs.srvc._alertsRecievedErrorHandler] Ajax error occoured:' + msg + ":" + ex.message);
};

/**
 * @private returns bool indicating if severity one is met
 */
ghs.srvc._isSeverityOne = function(severityLevel) {
  // Check Alert Severity Level
  if (severityLevel == ghs.srvc.SEVERITY_LEVEL) {
    return true;
  }
  return false;
};

/**
 * @private Adds the 'new' task to the task list
 * @param alertObj: the alert that contains the task
 * @return boolean indicating if operation is successful
 */
ghs.srvc._addEvent = function(alertObj) {
  // Add alert if no duplicate add alert exists
  if (ghs.srvc.getTaskPos(alertObj.task.id) < 0) {
  	alertObj.task.properties = { highlight:ghs.config.HLGHT_CLASS, modifiedFields:[] };
  	ghs.srvc.taskList = ghs.srvc.taskList.concat([alertObj.task]);
  	ghs.srvc._addAlert(alertObj, false);
  	return true;
  } else {
  	ghs.log.warn('[ghs.srvc._addEvent] Task already exists, will not add task.');
  	return false;
  }
};

/**
 * @private Adds an 'edit' alert to the alert list, along with the alert
 * 			Merges edit alerts with the same task id
 * @param alertObj: the alert that contains the task
 * @return boolean indicating of alert has been added
 */
ghs.srvc._editEvent = function(alertObj) {
  // Merge Tasks
  var taskPos = ghs.srvc.getTaskPos(alertObj.task.id);
  if (taskPos < 0) {
  	ghs.log.warn('[ghs.srvc._editEvent] Task does not exist, cannot add this edited task. Ignoring this alert.');
  	return false;
  }
  ghs.srvc.taskList[taskPos] = ghs.srvc._mergeTasks(ghs.srvc.taskList[taskPos], alertObj.task);
  ghs.srvc._addAlert(alertObj, true);
  return true;
};

/**
 * @private Deletes
 * @param alertObj: the alert that contains the task
 * @return boolean indicating of alert has been added
 */
ghs.srvc._deleteEvent = function(alertObj) {
  // Get task pos of task to delete
  var taskPos = ghs.srvc.getTaskPos(alertObj.task.id);
  if (taskPos < 0) {
  	ghs.log.warn('[ghs.srvc._deleteEvent] Task does not exist, cannot delete. Ignoring this alert.');
  	return false;
  }
  ghs.srvc._removeAlerts(alertObj.task.id);
  ghs.srvc._addAlert(alertObj, false);
  ghs.srvc.taskList.remove(taskPos);
  return true;
};

/**
 * @private Adds alert to alert list if it qualifies
 * @param alertObj: the alert that contains the task
 * @param isMergable
 */
ghs.srvc._addAlert = function(alertObj, isMergable) {
  if (alertObj.severity == ghs.srvc.SEVERITY_LEVEL) {
  	// Check to see if there is any alert to merge with, add otherwise
  	var otherAlert = isMergable ? ghs.srvc.getAlertByTaskId(alertObj.task.id) : -1;
  	if (isMergable && otherAlert > -1 && alertObj.id != ghs.srvc.alertList[otherAlert].id) {
  	  // Merge Alerts
  	  return ghs.srvc._mergeAlerts(ghs.srvc.alertList[otherAlert], alertObj);
    } else {
      // Add an alert
      ghs.srvc.alertList.push(alertObj);
    }
  }
};

/**
 * @private
 * @param alertObj: the alert that contains the task
 * @return boolean indicating if alertObj is a direct message
 */
ghs.srvc._messageEvent = function(alertObj) {
  ghs.srvc.alertList.push(alertObj);
  return true;
};

/**
 * @private starts a generic timeout that will call a function with a specific timeout
 */
ghs.srvc._setGenericTimeout = function(func, timeout) {
  ghs.srvc._clearGenericTimeout();
  if (ghs.gears.GENERIC_TIMER) {
  	ghs.gears.GENERIC_TIMER_ID = ghs.gears.GENERIC_TIMER.setTimeout(func, timeout);
  } else {
  	ghs.srvc.REG_GENERIC_TIMER_ID = window.setTimeout(func, timeout);
  }
};

/**
 * Clears the generic timeout
 */
ghs.srvc._clearGenericTimeout = function() {
  try {
    if (ghs.gears.GENERIC_TIMER) {
  	  if (ghs.gears.GENERIC_TIMER_ID) { ghs.gears.GENERIC_TIMER.clearTimeout(ghs.gears.GENERIC_TIMER_ID); }
    } else {
  	  if (ghs.srvc.REG_GENERIC_TIMER_ID) { window.clearTimeout(ghs.srvc.REG_GENERIC_TIMER_ID); }
    }
  } catch (ex) {
  	ghs.log.warn('[ghs.srvc.clearGenericTimeout] Cannot clear generic timeout. Error:' + ex);
  }
};

/**
 * @private activates poll timeout
 */
ghs.srvc._startReverseAjaxTimeout = function () {
  ghs.srvc._clearReverseAjaxTimeout(); // Clear timeouts before starting them
  if (ghs.gears.POLL_TIMEOUT_TIMER) {
  	// Set gears timeout
  	ghs.gears.POLL_TIMEOUT_TIMER_ID = ghs.gears.POLL_TIMEOUT_TIMER.setTimeout(ghs.srvc._abortReverseAjaxPoll, ghs.srvc.RAJAX_POLL_TIMEOUT);
  } else {
  	// Set regular timeout
  	ghs.srvc.REG_POLL_TIMEOUT_TIMER_ID = window.setTimeout(ghs.srvc._abortReverseAjaxPoll, ghs.srvc.RAJAX_POLL_TIMEOUT);
  }
};

/**
 * @private clears reverse ajax timeout
 */
ghs.srvc._clearReverseAjaxTimeout = function () {
  if (ghs.gears.POLL_TIMEOUT_TIMER) {
  	// Clear timout if active
  	if (ghs.gears.POLL_TIMEOUT_TIMER_ID) {
  	  ghs.gears.POLL_TIMEOUT_TIMER.clearTimeout(ghs.gears.POLL_TIMEOUT_TIMER_ID);
  	}
  } else {
  	// Clear timout if active
  	if (ghs.srvc.REG_POLL_TIMEOUT_TIMER_ID) {
  	  window.clearTimeout(ghs.srvc.REG_POLL_TIMEOUT_TIMER_ID);
  	}
  }
};

/**
 * @private Aborts the DWR reverse ajax poll, this causes DWR to start polling again.
 */
ghs.srvc._abortReverseAjaxPoll = function(){
  if (dwr.engine._pollReq) {
    ghs.log.warn('[ghs.srvc._abortReverseAjaxPoll] Reverse Ajax timed out. Aborting poll...');
	ghs.srvc._setGenericTimeout(ghs.srvc._pollAbortTimeout, ghs.srvc.GENERIC_TIMEOUT / 2);
    dwr.engine._pollReq.abort();
  } else {
    ghs.log.warn('[ghs.srvc._abortReverseAjaxPoll] PollReq is null. Calling poll error handler...');
    ghs.srvc._setGenericTimeout(ghs.srvc._pollAbortTimeout, ghs.srvc.GENERIC_TIMEOUT / 2);
    dwr.engine._pollErrorHandler();
  }
};

/**
 * @private function invoked when DWR poll abort times out
 */
ghs.srvc._pollAbortTimeout = function() {
  ghs.log.warn('[ghs.srvc._startReverseAjaxTimeout] DWR Poll Abort Failed, calling [dwr.engine._pollErrorHandler] manually...');
  dwr.engine._pollErrorHandler();
};

/**
 * @private activate service queues
 */
ghs.srvc._activateQueues = function () {
  ghs.srvc._deactivateQueues(); // De-activate queues before starting them
  if (ghs.gears.SRVC_QUEUE_TIMER) {
  	ghs.gears.SRVC_QUEUE_TIMER_ID = ghs.gears.SRVC_QUEUE_TIMER.setTimeout(ghs.srvc.processServiceQueues, ghs.srvc.QUEUE_INTERVAL);
  } else {
  	ghs.srvc.REG_SRVC_QUEUE_TIMER_ID = window.setTimeout(ghs.srvc.processServiceQueues, ghs.srvc.QUEUE_INTERVAL);
  }
};

/**
 * @private decactivate service queues
 */
ghs.srvc._deactivateQueues = function () {
  try {
    if (ghs.gears.SRVC_QUEUE_TIMER) {
  	  if (ghs.gears.SRVC_QUEUE_TIMER_ID) { ghs.gears.SRVC_QUEUE_TIMER.clearTimeout(ghs.gears.SRVC_QUEUE_TIMER_ID); }
    } else {
  	  if (ghs.srvc.REG_SRVC_QUEUE_TIMER_ID) { window.clearTimeout(ghs.srvc.REG_SRVC_QUEUE_TIMER_ID); }
    }
  } catch (ex) {
  	ghs.log.warn('[ghs.srvc._deactivateQueues] Exception occured: ' + ex.message);
  }
};

/**
 * @public Process Alerts Received Queue
 */
ghs.srvc.processServiceQueues = function() {
  ghs.log.info('[ghs.srvc.processServiceQueues] Checking if alive...');
  if (dwr.engine._pollReq != null) {
  	ghs.log.info('[ghs.srvc.processServiceQueues] PollReq exists.');
  	//ghs.srvc.setNetworkStatus(true);
  }
  if (ghs.srvc.isOnline()) {
  	if (ghs.srvc.getAlertsLock) {
  	  ghs.srvc._getAlerts(ghs.ui.empId);
  	}
  	if (ghs.srvc.alertsReceivedList.length > 0) {
  	  ghs.srvc.alertsRecieved(ghs.ui.empId, ghs.srvc.alertsReceivedList, false);
    }
  	if (ghs.srvc.tasksAcknowledged.length > 0) {
  	  ghs.srvc.acknowledgeTasks(ghs.ui.empId, ghs.srvc.tasksAcknowledged, false);
    }
    if (ghs.srvc.tasksStarted.length > 0) {
  	  ghs.srvc.startTasks(ghs.ui.empId, ghs.srvc.tasksStarted, false);
    }
    if (ghs.srvc.tasksCompleted.length > 0) {
  	  ghs.srvc.completeTasks(ghs.ui.empId, ghs.srvc.tasksCompleted, false);
    }
    if ((ghs.srvc.alertList.length > 0) && (ghs.srvc.lastAlertStatusSent == false)) {
      ghs.srvc.alertsExist(ghs.ui.empId, true);
    } else if ((ghs.srvc.alertList.length == 0) && (ghs.srvc.lastAlertStatusSent == true)) {
      ghs.srvc.alertsExist(ghs.ui.empId, false);
    }
  }
  ghs.srvc._activateQueues();
};

/**
 * @public shuts down the timers used by the service
 */
ghs.srvc.shutdown = function() {
  try {
    ghs.gears.shutdown(); // shut gears down
    ghs.gears.deactivateTimers();
  	window.clearTimeout(ghs.srvc.REG_POLL_TIMEOUT_TIMER_ID);
	window.clearTimeout(ghs.srvc.REG_SRVC_QUEUE_TIMER_ID);
	window.clearTimeout(ghs.srvc.REG_SIGNAL_TIMER_ID);
	window.clearTimeout(ghs.srvc.REG_GENERIC_TIMER_ID);
  } catch (ex) {
  	ghs.log.warn('[ghs.srvc.shutdown] Error clearing timeouts. Error:' + ex);
  }
};

/**
 * @public returns the index of the task in the taskList
 */
ghs.srvc.getTaskPos = function(taskID) {
  for (i=0; i < ghs.srvc.taskList.length; i++) {
  	if (ghs.srvc.taskList[i].id == taskID) {
  		return i;
  	}
  }
  return -1;
};

/**
 * @public returns the index of the alert in the alertList
 */
ghs.srvc.getAlertPos = function(alertID) {
  for (i=0; i < ghs.srvc.alertList.length; i++) {
  	if (ghs.srvc.alertList[i].id == alertID) {
  		return i;
  	}
  }
  return -1;
};

/**
 * @public returns the index of the alert in the alertList
 * if alert exists
 */
ghs.srvc.getAlertByTaskId = function(taskID) {
  var i = 0;
  for (i=0; i < ghs.srvc.alertList.length; i++) {
  	if (ghs.srvc.alertList[i].task.id == taskID) {
  		return i;
  	}
  }
  return -1;
};

/**
 * @public Remove Alert if it exists
 * @param empId: Employee ID
 * @param taskId
 */
ghs.srvc.removeAlert = function(empId, taskId) {
  var rowNum = ghs.srvc.getAlertByTaskId(taskId);
  if (rowNum < 0) { return; }
  var originalLength = ghs.srvc.alertList.length;
  ghs.srvc.alertList.remove(rowNum);
  ghs.srvc._removeAlerts(taskId);
  if ((ghs.srvc.alertList.length == 0) && (originalLength > 0)) {
	  ghs.srvc.alertsExist(ghs.ui.empId, false);
  }
  ghs.ui.eventHandler(ghs.event.ALERT_LIST_UPDATED, {alertCount:0});
};

/**
 * @private Helper for removeAlert function
 * @param taskId
 */
ghs.srvc._removeAlerts = function(taskId) {
  var i = ghs.srvc.alertList.length - 1;
  for (i; i >= 0; i--) {
  	if (ghs.srvc.alertList[i].task.id == taskId) {
  	  ghs.srvc.alertList.remove(i);
  	}
  }
};

/**
 * @public alertsExist: Called to notify server/Brock of alert list state
 * @param empId: Employee ID
 * @param exist: boolean indicating whether unviewed alerts exist
 */
ghs.srvc.alertsExist = function(empId, exist) {
	if (ghs.ui.enableBrock == 'true') {
		ghs.srvc._activateQueues(); // Reset Queue
		ghs.log.info('[ghs.srvc.alertsExist] Notifying server of alert list status...');
		TaskCreator.alertsExist(empId, exist, {callback:ghs.srvc._alertsExistCallback,
											   errorHandler:ghs.srvc._alertsExistErrorHandler,
											   timeout:ghs.srvc.AJAX_TIMEOUT});
	}
};

/** @private alertsExist Callback */
ghs.srvc._alertsExistCallback = function(data) {
	ghs.log.info('[ghs.srvc._alertsExistCallback] Alerts Exist Status Sent to Server.');
	// Set the lastAlertStatusSent flag
	ghs.srvc.lastAlertStatusSent = data;
};

/** @private alertsExist Error Handler */
ghs.srvc._alertsExistErrorHandler = function(msg, ex) {
	ghs.log.warn('[ghs.srvc._alertsExistErrorHandler] Ajax error occoured:' + msg + ":" + ex.message);
};

/**
 * @private sort function used by Array.sort method
 */
ghs.srvc._sortByTime = function(TaskA, TaskB) {
  try {
    if (TaskA.sort > TaskB.sort) {
      return 1;
    } else {
      return -1;
    }
  } catch (ex) {
  	return -1;
  }
};

/**
 * @private Highlights non acknowledged tasks
 */
ghs.srvc._highlightNonAcknowledged = function(tasks) {
  for (var i = 0; i < tasks.length; i++) {
  	if (tasks[i]) {
  		if (tasks[i].status == "COMPLETED") {
  			tasks[i].properties = { highlight:ghs.config.COMPLETE_HLGHT, modifiedFields:[] };
  		} else if (!tasks[i].ackd) {
  			tasks[i].properties = { highlight:ghs.config.HLGHT_CLASS, modifiedFields:[] };
  		}
  	}
  }
};

/**
 * @private Merges Alert2 into Alert1, return new Alert1
 */
ghs.srvc._mergeAlerts = function(alertOld, alertNew) {
  alertOld.id = alertNew.id;
  alertOld.message[ghs.ui.lang] += "<br />" + alertNew.message[ghs.ui.lang];
  return true;
  // TODO in the future, merge the alert ids into an array
};

/**
 * @private function that merges two tasks togather
 * @param old task and new task
 * @return new modified task
 */
ghs.srvc._mergeTasks = function(oldTask, newTask) {
  // Get a list of the modified fields and set them to the new task
  var modifiedFields = ghs.srvc._compareTasks(oldTask, newTask);
  newTask.properties = { highlight: newTask.status == 'COMPLETED' ? ghs.config.COMPLETE_HLGHT : ghs.config.HLGHT_CLASS,
  						 modifiedFields:[]
  					   };
  newTask.properties.modifiedFields = modifiedFields;
  // Get a list of the previous modified fields if any, and add them to the new
  // if duplicates do not exist
  var modifiedFieldsOld = oldTask.properties ? oldTask.properties.modifiedFields : [];
  for (var n in modifiedFieldsOld) {
  	if ( (typeof(modifiedFieldsOld[n]) != 'function')
  	  && newTask.properties.modifiedFields.indexOf(modifiedFieldsOld[n]) < 0) {
  		newTask.properties.modifiedFields.push(modifiedFieldsOld[n]);
  	}
  }
  // Maintain existing user managed properties
  ghs.srvc._maintainSelfManagedProperties(oldTask, newTask);

  return newTask;
};

/**
 * @private Replaces new tasks properties with the more update properties
 * that the usr manages, this is due to the reason that they changes occour here
 * first before they are changed on the server.
 */
ghs.srvc._maintainSelfManagedProperties = function(oldTask, newTask) {
  try {
  	// Keep old status flag
    if (oldTask.status &&
     ghs.srvc.tasksStarted.indexOf(newTask.id) > -1 &&
     ghs.srvc.tasksCompleted.indexOf(newTask.id) > -1) {
  	  newTask.status = oldTask.status;
    }
    // Keep old ackd flag
    if (oldTask.ackd &&
     ghs.srvc.tasksAcknowledged.indexOf(newTask.id) > -1) {
  	  newTask.ackd = oldTask.ackd;
    }
  } catch (ex) {
  	ghs.log.warn("[ghs.srvc._maintainSelfManagedProperties] Error: " + ex.name + ", " + ex.message);
  }
};

/**
 * @private Compares task2 to task1
 * @return an array of fields contained in task2 that are different in task1
 */
ghs.srvc._compareTasks = function(task1, task2) {
  // Get array of different fields
  var diffFields = [];
  for (var n in task2) {
	if ( task2[n] == '#' ) { task2[n] = task1[n]; }
    if ( (typeof(task2[n]) == 'function') || task1[n] == task2[n]) { continue; };
    diffFields.push(n);
  }
  return diffFields;
};

/**
 * @public returns true if network status is active
 */
ghs.srvc.isOnline = function() {
 return ghs.srvc.networkStatus;
};

/**
 * @public sets the network status bool
 */
ghs.srvc.setNetworkStatus = function(bool) {
  ghs.srvc.networkStatus = bool;
  ghs.ui.setSignalLevel(bool);
};

/**
 * The default message handler for DWR
 * @param {String} message The text of the error message
 * @param {Object} ex An error object containing at least a name and message
 */
dwr.engine.setWarningHandler(ghs.srvc.defaultAjaxErrorHandler);
dwr.engine.setErrorHandler(ghs.srvc.defaultAjaxErrorHandler);
ghs.srvc.defaultDWRErrorHandler = function(message, ex) {
  ghs.log.error("[dwr.engine.setErrorHandler] Error: " + ex.name + ", " + ex.message, true);
  if (message == null || message == "") {
    ghs.log.error("[dwr.engine.setErrorHandler] A server error has occurred.");
  } else if (message.indexOf("0x80040111") != -1) {
  	// Ignore NS_ERROR_NOT_AVAILABLE if Mozilla is being narky
  	ghs.log.error('[dwr.engine.setErrorHandler] ' + message);
  } else {
  	ghs.log.error('[dwr.engine.setErrorHandler] ' + message);
  }
};


/*
 *
 * LOG Services
 *
 */

/**
 * Init log services placeholder
 */
ghs.srvc.logs = {};
/**
 * Handler
 */
ghs.srvc.logs.handler = function(msg, isError) {
  ghs.log.info('[ghs.srvc.logs.handler] Sending log request...');
  TaskCreator.sendLogs(ghs.ui.empId, msg, isError, {callback:ghs.srvc.logs._handlerCallback,
  												   errorHandler:ghs.srvc.logs._handlerErrorHandler,
  												   timeout:(ghs.srvc.AJAX_TIMEOUT * 2)});
};
/** @private logs.handler Callback */
ghs.srvc.logs._handlerCallback = function(data) {
  ghs.log.info('[ghs.srvc.logs._handlerCallback] Log request sent.');
};
/** @private logs.handler Error Handler */
ghs.srvc.logs._handlerErrorHandler = function(msg, ex) {
  ghs.log.warn('[ghs.srvc.logs._handlerErrorHandler] Ajax error occoured: Log request resulted in error:' + msg + ':' + ex.message);
};

/**
 * Gets the the number of minitues requested
 *
 * Example:
 * 		I. 5 returns the last 5 minitue worth of entries.
 *
 * @param {numOfMins} number of mins to retrive
 * @return {String} of log entries
 */
ghs.srvc.logs.get = function(numOfMins) {
  if (ghs.gears.logger != null) {
    ghs.gears.logger.send(ghs.log.output.gears.GET_CMD, numOfMins);
    ghs.log.info('[ghs.srvc.logs.get] Getting ' + numOfMins + ' minitues from log file.');
  } else {
  	ghs.srvc.logs.handler('[ghs.srvc.logs.getAll] Gears logger is not active.', false);
  }
};
/**
 * Gets all logs
 *
 * @return {String} of all log entries
 */
ghs.srvc.logs.getAll = function() {
  if (ghs.gears.logger != null) {
    ghs.gears.logger.send(ghs.log.output.gears.GET_ALL_CMD);
    ghs.log.info('[ghs.srvc.logs.getAll] Getting all data from log file.');
  } else {
  	ghs.srvc.logs.handler('[ghs.srvc.logs.getAll] Gears logger is not active.', false);
  }
};
/**
 * Deletes all logs
 *
 * @return String indicating if successfull
 */
ghs.srvc.logs.deleteAll = function() {
  if (ghs.gears.logger != null) {
    ghs.gears.logger.send(ghs.log.output.gears.DELETE_ALL_CMD);
    ghs.log.info('[ghs.srvc.logs.deleteAll] Deleting all logs.');
  } else {
  	ghs.srvc.logs.handler('[ghs.srvc.logs.getAll] Gears logger is not active.', false);
  }
};


/*
 *
 * DWR Override
 *
 */
/**
 * @override dwr.engine._pollErrorHandler
 */
dwr.engine._pollErrorHandler = function(msg, ex) {
  ghs.srvc._clearReverseAjaxTimeout(); // Clear AJAX timeout
  ghs.srvc._clearGenericTimeout(); // Clear DWR abort timeout
  try { ghs.srvc.setNetworkStatus(false); } catch (ex) { /* Ignore */ } // Set Signal level to false
  ghs.log.info('[dwr.engine._pollErrorHandler] Connection lost. Polling retry #' + dwr.engine._pollRetries +
  			   " in " + (ghs.srvc.POLL_INTERVAL / 1000) + 'seconds.');
  try {
    var image = new Image();
	image.src = "/gc/images/toDelete.gif?id=" + Math.floor(Math.random()*1000000);
  } catch (e) {	ghs.log.warn("[non_cached_image] Cached Image Error"); }
  ghs.srvc.startPollTimeout(ghs.srvc.POLL_INTERVAL); // Poll again in 10seconds
};

/**
 * Begin polling
 */
ghs.srvc.startPollTimeout = function(pollInterval) {
  ghs.srvc.clearPollTimeout(); // Clear timers before we start
  if (ghs.gears.SIGNAL_TIMER) {
    // If gears exists
    ghs.log.info('[ghs.srvc.startPollTimeout] GEARS POLLING in 10!');
    ghs.gears.SIGNAL_TIMER_ID = ghs.gears.SIGNAL_TIMER.setTimeout(dwr.engine._poll, pollInterval);
  } else {
    // If gears does not exist
    ghs.log.info('[ghs.srvc.startPollTimeout] NON-GEARS POLLING in 10!');
    ghs.srvc.REG_SIGNAL_TIMER_ID = window.setTimeout(dwr.engine._poll, pollInterval);
  }
  dwr.engine._pollRetries++; // Increase poll count
};

ghs.srvc.clearPollTimeout = function() {
  if (ghs.gears.SIGNAL_TIMER) {
    // If gears exists
    if (ghs.gears.SIGNAL_TIMER_ID) {
      ghs.gears.SIGNAL_TIMER.clearTimeout(ghs.gears.SIGNAL_TIMER_ID);
    }
  } else {
    // If gears does not exist
    if (ghs.srvc.REG_SIGNAL_TIMER_ID) {
      window.clearTimeout(ghs.srvc.REG_SIGNAL_TIMER_ID);
    }
  }
};

/**
 * @override dwr.engine.transport.xhr.send
 *
 * Setup a batch for transfer through XHR
 * @param {Object} batch The batch to alter for XHR transmit
 */
dwr.engine.transport.xhr.send = function(batch) {
  try { ghs.srvc.setNetworkStatus(true); } catch (ex) { /* Ignore */ } // Set Signal level to false
  if (batch.isPoll) {
    batch.map.partialResponse = dwr.engine._partialResponseYes;
  }
  // Do proxies or IE force us to use early closing mode?
  if (batch.isPoll && dwr.engine._pollWithXhr == "true") {
    batch.map.partialResponse = dwr.engine._partialResponseNo;
  }
  if (batch.isPoll && dwr.engine.isIE) {
    batch.map.partialResponse = dwr.engine._partialResponseNo;
  }
  if (window.XMLHttpRequest) {
    batch.req = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    batch.req = dwr.engine.util.newActiveXObject(dwr.engine.transport.xhr.XMLHTTP);
  }
  // Proceed using XMLHttpRequest
  if (batch.async == true) {
    batch.req.onreadystatechange = function() {
      if (typeof dwr != 'undefined') {
        dwr.engine.transport.xhr.stateChange(batch);
      }
    };
  }
  // If we're polling, record this for monitoring
  if (batch.isPoll) {
    dwr.engine._pollReq = batch.req;
    // In IE XHR is an ActiveX control so you can't augment it like this
    if (!dwr.engine.isIE) { batch.req.batch = batch; }
  }
  httpMethod = dwr.engine.transport.xhr.httpMethod;
  // Workaround for Safari 1.x POST bug
  var indexSafari = navigator.userAgent.indexOf("Safari/");
  if (indexSafari >= 0) {
    var version = navigator.userAgent.substring(indexSafari + 7);
    if (parseInt(version, 10) < 400) {
      if (dwr.engine._allowGetForSafariButMakeForgeryEasier == "true") {
        httpMethod = "GET";
      } else {
        dwr.engine._handleWarning(batch, {
          name: "dwr.engine.oldSafari",
          message: "Safari GET support disabled. See getahead.org/dwr/server/servlet and allowGetForSafariButMakeForgeryEasier."
        });
      }
    }
  }
  batch.mode = batch.isPoll ? dwr.engine._ModePlainPoll : dwr.engine._ModePlainCall;
  var request = dwr.engine.batch.constructRequest(batch, httpMethod);
  try {
    batch.req.open(httpMethod, request.url, batch.async);
    try {
      for (var prop in batch.headers) {
        var value = batch.headers[prop];
        if (typeof value == "string") {
          batch.req.setRequestHeader(prop, value);
        }
      }
      if (!batch.headers["Content-Type"]) {
        batch.req.setRequestHeader("Content-Type", "text/plain");
      }
    } catch (ex) {
      dwr.engine._handleWarning(batch, ex);
    }
    if (batch.isPoll) {
    	ghs.log.info('[dwr.engine.transport.xhr.send] Sending new reverse ajax poll.');
    	ghs.srvc._startReverseAjaxTimeout(); // timeout in XX seconds to make sure we have a pulse
    }
    batch.req.send(request.body);
    if (batch.async == false) {
      dwr.engine.transport.xhr.stateChange(batch);
    }
  } catch (ex) {
    dwr.engine._handleError(batch, ex);
  }
  if (batch.isPoll && batch.map.partialResponse == dwr.engine._partialResponseYes) {
    dwr.engine.transport.xhr.checkCometPoll();
  }
  // This is only of any use in sync mode to return the reply data
  return batch.reply;
};

dwr.engine.batch.createPoll = function() {
      var batch = {
        async:true,
        charsProcessed:0,
        handlers:[{
          callback:function(pause) {
          	ghs.srvc._clearReverseAjaxTimeout(); // Clear AJAX timeout
  			ghs.srvc._clearGenericTimeout(); // Clear DWR abort timeout
          	dwr.engine._pollRetries = 0;
          	ghs.srvc.setNetworkStatus(true);
          	ghs.log.info('[dwr.engine.batch.createPoll] pause is:' + pause);

            setTimeout(dwr.engine._poll, pause);
          }
        }],
        isPoll:true,
        map:{ windowName:window.name },
        paramCount:0,
        path:dwr.engine._pathToDwrServlet,
        preHooks:[],
        postHooks:[],
        timeout:0,
        windowName:window.name,
        errorHandler:dwr.engine._pollErrorHandler,
        warningHandler:dwr.engine._pollErrorHandler,
        textHtmlHandler:dwr.engine._textHtmlHandler
      };

      dwr.engine.batch.populateHeadersAndParameters(batch);
      return batch;
    };

// /** @private Generate a new standard batch */
// dwr.engine._createBatch = function() {
//   var batch = {
//     map:{
//       callCount:0,
//       page:window.location.pathname + window.location.search,
//       httpSessionId:dwr.engine._getJSessionId(),
//       scriptSessionId:dwr.engine._getScriptSessionId()
//     },
//     charsProcessed:0, paramCount:0,
//     parameters:{}, headers:{},
//     isPoll:false, handlers:{}, preHooks:[], postHooks:[],
//     rpcType:dwr.engine._rpcType,
//     httpMethod:dwr.engine._httpMethod,
//     async:dwr.engine._async,
//     timeout:dwr.engine._timeout,
//     errorHandler:dwr.engine._errorHandler,
//     warningHandler:dwr.engine._warningHandler,
//     textHtmlHandler:dwr.engine._textHtmlHandler
//   };
//   if (dwr.engine._preHook) batch.preHooks.push(dwr.engine._preHook);
//   if (dwr.engine._postHook) batch.postHooks.push(dwr.engine._postHook);
//   var propname, data;
//   if (dwr.engine._headers) {
//     for (propname in dwr.engine._headers) {
//       data = dwr.engine._headers[propname];
//       if (typeof data != "function") batch.headers[propname] = data;
//     }
//   }
//   if (dwr.engine._parameters) {
//     for (propname in dwr.engine._parameters) {
//       data = dwr.engine._parameters[propname];
//       if (typeof data != "function") batch.parameters[propname] = data;
//     }
//   }
//   return batch;
// };
/** @private What is our session id? */
// getJSessionId =  function() {
//   var cookies = document.cookie.split(';');
//   for (var i = 0; i < cookies.length; i++) {
//     var cookie = cookies[i];
//     console.log(i + " : " + cookie);
//     while (cookie.charAt(0) == ' ') cookie = cookie.substring(1, cookie.length);
//     if (cookie.indexOf(dwr.engine._sessionCookieName + "=") == 0) {
      
//       return cookie.substring(dwr.engine._sessionCookieName.length + 1, cookie.length);
//     }
//   }
//   return "";
// };

// dwr.engine.batch.create = function() {
 
//     var batch = {
//       async:dwr.engine._async,
//       charsProcessed:0,
//       handlers:[],
//       isPoll:false,
//       map:{ callCount:0, windowName:window.name, httpSessionId:getJSessionId() },
//       paramCount:0,
//       preHooks:[],
//       postHooks:[],
//       timeout:dwr.engine._timeout,
//       errorHandler:dwr.engine._errorHandler,
//       warningHandler:dwr.engine._warningHandler,
//       textHtmlHandler:dwr.engine._textHtmlHandler
//     };

//     if (dwr.engine._preHook) {
//       batch.preHooks.push(dwr.engine._preHook);
//     }
//     if (dwr.engine._postHook) {
//       batch.postHooks.push(dwr.engine._postHook);
//     }

//     dwr.engine.batch.populateHeadersAndParameters(batch);
//     return batch;
//   };