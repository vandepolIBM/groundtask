/**
 * 
 * Logger Utilities
 * 
 * @author M.Kurabi
 * 
 */

/**
 * Declare an object to which we can add logger objects to
 */
ghs.log = {};

// Static debuger options, do not touch
ghs.log.TEXT = ['DEBUG','INFO','WARN','ERROR'];
ghs.log.DEBUG = 0;
ghs.log.INFO = 1;
ghs.log.WARN = 2;
ghs.log.ERROR = 3;

/** LOGGER CONFIGURATIONS **/
// Log level
ghs.log.level = ghs.log.DEBUG;
// Debug Output Settings
ghs.log.output = { gears:true, console:true};

ghs.log.gears = {};
ghs.log.gears.init = function() {
  /**
   * @private Outputs log to gears if enabled
   * @param {level} log level
   * @param {String} msg
   * @param {Object} stacktrace
   */
  if (ghs.log.output.gears) {

    // Initialize logger
    ghs.gears.logger = new Logger();
    ghs.gears.logger.init();
  
    // Output log
    ghs.log.output.gears = function(level, msg, stacktrace) {
      ghs.gears.logger.send(ghs.log.output.gears.INSERT_CMD, ghs.log._getTimeStamp() + ' ' + ghs.log.TEXT[level] + ' ' + msg);
    };
  
    // Logger commands
    ghs.log.output.gears.INSERT_CMD = '{insert}';
    ghs.log.output.gears.GET_CMD = '{get}';
    ghs.log.output.gears.GET_ALL_CMD = '{getAll}';
    ghs.log.output.gears.DELETE_ALL_CMD = '{deleteAll}';
    ghs.log.output.gears.PURGE_OLD_LOGS_CMD = '{purgeOldLogs}';

  };
};

/**
 * @private Outputs log to console if enabled
 * @param {level} log level
 * @param {String} msg
 * @param {Object} stacktrace
 */
if (ghs.log.output.console) {
  ghs.log.output.console = function(level, msg, stacktrace) {
    try {
      if (window.console) {
        if (window.console.trace) {
          window.console.log(ghs.log._getTimeStamp() + ' ' + ghs.log.TEXT[level] + ' ' + msg);
		}
		if (stacktrace) { window.console.trace(); }
      }
	} catch (ex) { /* Ignore */ }
  }
};

/**
 * @public used to debug code
 * @param {String} msg
 * @param {Object} stacktrace
 */
ghs.log.debug = function(msg, stacktrace) {
  ghs.log._debugOutput(ghs.log.DEBUG, msg, stacktrace);
};
ghs.log.info = function(msg, stacktrace) {
  ghs.log._debugOutput(ghs.log.INFO, msg, stacktrace);
};
ghs.log.warn = function(msg, stacktrace) {
  ghs.log._debugOutput(ghs.log.WARN, msg, stacktrace);
};
ghs.log.error = function(msg, stacktrace) {
  ghs.log._debugOutput(ghs.log.ERROR, msg, stacktrace);
};

/**
 * @private helper function to delegate output to desired place
 * @param {String} message
 * @param {Object} stacktrace
 */
ghs.log._debugOutput = function(level, msg, stacktrace) {
  if (ghs.log.level <= level) {
  	for (var n in ghs.log.output) {
      if (typeof(ghs.log.output[n]) == 'function') { 
      	ghs.log.output[n](level, msg, stacktrace);
      }
  	}
  }
};

/**
 * @private returns the current time stamp in "YYYY-MM-DD HH:MM:SS" format
 * @param {UTC} boolean indicating to return time in UTC timezone
 */
ghs.log._getTimeStamp = function(UTC) {
	var time = new Date();
	if (!UTC) {
		return time.getFullYear() + '-' + String("0" + (time.getMonth()+1)).slice(-2) + '-' + String("0" + time.getDate()).slice(-2) + ' ' +
			   time.getHours() + ':' + String("0" + time.getMinutes()).slice(-2) + ':' + String("0" + time.getSeconds()).slice(-2);
	} else {
		return time.getUTCFullYear() + '-' + String("0" + (time.getUTCMonth()+1)).slice(-2) + '-' + String("0" + time.getUTCDate()).slice(-2) + ' ' +
			   time.getUTCHours() + ':' + String("0" + time.getUTCMinutes()).slice(-2) + ':' + String("0" + time.getUTCSeconds()).slice(-2);
	}
}


function Logger() {
  
  var wp_id;
  var wp;
  var PURGE_TIMEOUT = 100000; // 100 seconds
  
  // Worker Commands
  var SEND_CMD = '{send}';
  var SEND_ERROR_CMD = '{send.error}'; 
  var PURGE_OLD_LOGS_CMD = '{purgeOldLogs}';

  /**
   * Initialize the Logging Worker + Database
   */
  this.init = function() {
  	
    try {
      wp = google.gears.factory.create('beta.workerpool', '1.0');
    } catch(ex) {
      return false;
    }
    
    wp_id = wp.createWorker(String(LoggerWorker) + ';LoggerWorker()');
            
    wp.onmessage = function(msgText, senderId, msgObj) {
      if (senderId == wp_id) {
		if (msgText.indexOf(SEND_CMD) > -1) {
		  var msg = msgText.substring(SEND_CMD.length);
		  ghs.srvc.logs.handler(msg, false);
		  return true;
		}
		if (msgText.indexOf(SEND_ERROR_CMD) > -1) {
		  var msg = msgText.substring(SEND_ERROR_CMD.length);
		  ghs.srvc.logs.handler(msg, true);
		  return true;
		}
      }
    }
    
    // Purge data older than 28 days
    var timer = google.gears.factory.create('beta.timer', '1.0');
    timer.setTimeout(purgeOldLogs, PURGE_TIMEOUT); // Purge old data in 100 seconds
    
    return true;
  }
  
  purgeOldLogs = function() {
  	wp.sendMessage(PURGE_OLD_LOGS_CMD, wp_id);
  }
  
  this.send = function(command, param) {
  	wp.sendMessage(command + param, wp_id);
  }
        
  function LoggerWorker() {
    
    var wp = google.gears.workerPool;
    var parentId;
    var db;
    
    // Commands understood by worker
    var INSERT_CMD = '{insert}';
    var GET_CMD = '{get}';
  	var GET_ALL_CMD = '{getAll}';
  	var DELETE_ALL_CMD = '{deleteAll}';
  	var PURGE_OLD_LOGS_CMD = '{purgeOldLogs}';
    
    // Get DB instance, create one if none exists
    try {
      db = google.gears.factory.create('beta.database');
      if (db) {
        db.open('database-logs');
        db.execute('create table if not exists Logs' + ' (Phrase varchar(255), Timestamp int)');
      }
    } catch (ex) {
      sendError('Could not create database: ' + ex.message);
    }
            
    wp.onmessage = function(msgText, senderId, msgObj) {
      parentId = msgObj.sender;
      if (msgText.indexOf(INSERT_CMD) > -1) {
      	insert( msgText.substring(INSERT_CMD.length) );
      	return true;
	  } 
	  if (msgText.indexOf(GET_CMD) > -1) {
	  	get( msgText.substring(GET_CMD.length) );
	  	return true;
	  }
	  if (msgText.indexOf(GET_ALL_CMD) > -1) {
	  	getAll();
	  	return true;
	  }
	  if (msgText.indexOf(DELETE_ALL_CMD) > -1) {
	  	deleteAll();
	  	return true;
	  }
	  if (msgText.indexOf(PURGE_OLD_LOGS_CMD) > -1) {
	  	purgeOldData();
	  	return true;
	  }
	  sendError('Worker command not found.');
	  return false;
    }
    
	/**
	 * Inserts log row into Logs table
	 */
    insert = function(row) {
    	wp.sendMessage('here', parentId);
      var currTime = new Date().getTime();
      // Insert the new item.
      // The Gears database automatically escapes/unescapes inserted values.
      db.execute('insert into Logs values (?, ?)', [row, currTime]);
	}
	
	/**
	 * Gets all rows in Logs table
	 */
	get = function(range) {
	  try {
	  	range = parseInt(range);
	  } catch (ex) {
	  	sendError('Range is invalid. Exception: ' + ex.message );
	  	return;
	  }
	  var rows;
	  if (range > 0) {
	  	var currTime = new Date();
	    var MINS_PRIOR = 60000 * range;
	    rows = db.execute('select * from (select * from Logs where Timestamp > ' +
	    	   (currTime.getTime() - MINS_PRIOR) + ') order by Timestamp asc');
	  } else if (range < 0) {
	  	sendError('Range is invalid. Cannot be a negative number.');
	  }
	  convertAndSend(rows);
	}
	
	/**
	 * Gets all rows in Logs table
	 */
	getAll = function() {
	  var rows = db.execute('select * from Logs order by Timestamp asc');
	  convertAndSend(rows);
	}
	
	/**
	 * Deletes all rows in Logs table
	 */
	deleteAll = function() {
	  db.execute('delete from Logs');
	  send('Success. Logs table is now empty.');
	}
	
	/**
	 * Converts results into a big text string and sends it
	 */
	convertAndSend = function(rows) {
	  var logString = '';
      while (rows.isValidRow()) {
		logString += rows.field(0) + '\n';
        rows.next();
      } 
      rows.close(); // Clean up
      // Send
      send(logString);
    }
    
    /**
	 * Deletes all rows in Logs table
	 */
	purgeOldData = function() {
	  var currTime = new Date();
	  var SEVEN_DAYS = 86400000 * 7;
	  db.execute('delete from Logs where Timestamp < ' + (currTime.getTime() - SEVEN_DAYS));
	}
    
    /**
	 * Converts results into a big text string and sends it
	 */
	send = function(msg) {
      wp.sendMessage('{send}' + msg, parentId);
    }
    
    /**
	 * Converts results into a big text string and sends it
	 */
	sendError = function(msg) {
      wp.sendMessage('{send.error}' + msg, parentId);
    }

  }
}