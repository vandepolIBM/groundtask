
/**
 * 
 * UI Properties File for AC GHS
 * 
 * @author M.Kurabi
 * 
 */
 
 /*
 #############################################
 	Misc Properties
 ############################################# 
 */
 
/**
 * Declare an object to which we can add real functions.
 */
if (ghs == null) { var ghs = {}; }
if (ghs.config == null) { ghs.config = {}; }

ghs.config.gearsEnabled = true; // Value to turn on/off gears

ghs.config.RED = '#FF0000';
ghs.config.ORANGE = '#FF6A00';
ghs.config.YELLOW = '#FFD800';
ghs.config.GREEN = '#66FF99';
ghs.config.GREY = '#cccccc';

ghs.config.HLGHT_CLASS = 'hlght';
ghs.config.COMPLETE_HLGHT = 'complete';

/**
 * Calculates the highght of the alert box based on
 * length of words
 * @param minHeight, msg
 */
ghs.config.alertRowHeight = function(minHeight, msg) {
  var charPerRow = 18;
  var pixelsPerRow = 14;
  var buffer = 6;
  var lineBreakCount = msg.split("<br />").length - 1;
  var height = ((Math.ceil((msg.length / charPerRow)) + lineBreakCount) * pixelsPerRow) + buffer;
  return minHeight >= height ? minHeight : height;
};

/**
 * @private Default row creation function
 */
ghs.config.defaultRowCreator = function(options) {
  var row = new Node('div');
  if (options.rowProp) {
  	row.id = options.rowData.id ? 'id_' + options.rowData.id : 'empty';
  	if (options.rowData.type == 'm') {
  		row.className = options.rowProp.msgClassName ? options.rowProp.msgClassName : '';
  	} else {
  		row.className = options.rowProp.className ? options.rowProp.className : '';	
  	}
	if (options.rowData.properties) {
		row.className += options.rowData.properties.highlight ? ' ' + options.rowData.properties.highlight : '';
	}
	row.style = options.rowProp.style ? options.rowProp.style : '';
	if (options.rowProp.dynamicHeightMin) {
	  var height = ghs.config.alertRowHeight(options.rowProp.dynamicHeightMin, options.rowData.message[ghs.ui.lang]);
	  row.style += 'height:' + height + 'px;';
	}
  }
  return row;
};

/**
 * @private Default cell creation function
 */
ghs.config.defaultCellCreator = function(options) {
  var col = new Node('div');
  var cellProp = options.cellProp[options.cellNum];
  if (cellProp) {
  	col.className=cellProp.className ? cellProp.className : '';
  	col.style=cellProp.style ? cellProp.style : '';
  }
  return col;
};
 
/*
 #############################################
 	Task and Alert related Properties
 ############################################# 
 */
 
// Static varibles
var alertRowCount = 0; // Current # of tasks
var taskRowCount = 0; // Current # of alerts
var MAX_DISPLAY_TASKS = 3; // Maximum # of tasks to be displayed on main screen
var MAX_DISPLAY_ALERTS = 3; // Maximum # of alerts to be displayed on main screen
	
/*
 #############################################
 	Task Properties
 ############################################# 
 */

// Task Header Data
var taskTableHeader = function() {
	return [[ghs.ui.getMessage('start'),
	         ghs.ui.getMessage('type'),
	         ghs.ui.getMessage('gate'),
	         ghs.ui.getMessage('city'),
	         ghs.ui.getMessage('flight')
	       ]];
	
};
// Row format properties
var taskRowPropHeader = [ {className:'row b'} ]; // For Header row
var taskRowPropBody = [ {className:'row'} ]; // For non-header rows
var taskRowPropHighlight = [ {style:'row hlght'} ]; // For highlighted rows !!!!!!!!!!!!!!!!!!!!!!!!!!!!TO BE DELETED!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// Cell format properties
var taskCellProp = [
	{className:'col ctr', style:'width:18%'},
	{className:'col ctr', style:'width:31%'},
	{className:'col ctr', style:'width:16%'},
	{className:'col ctr', style:'width:14%'},
	{className:'col ctr', style:'width:21%'}
];

// Header Cell functions
var taskHeaderCellFuncs = [
	function(data) { return '<p>' + (data[0] ? data[0] : "") + '</p>'; },
	function(data) { return '<p>' + (data[1] ? data[1] : "") + '</p>'; },
	function(data) { return '<p>' + (data[2] ? data[2] : "") + '</p>'; },
	function(data) { return '<p>' + (data[3] ? data[3] : "") + '</p>'; },
	function(data) { return '<p>' + (data[4] ? data[4] : "") + '</p>'; }
];

// Body Cell functions
var taskBodyCellFuncs = [
	function(data, optn) { return '<p>' + (data.startTime ? data.startTime : '') + '</p>'; },
	function(data, optn) { return "<p><a href='javascript:ghs.ui.itemSelected(ghs.srvc.taskList["
									+ ghs.srvc.getTaskPos(data.id) + "])'>" + data.type + '</a></p>'; },
	function(data, optn) { return '<p>' + (data.location ? data.location : '') + '</p>'; },
	function(data, optn) { return '<p>' + (data.city ? data.city : '') + '</p>'; },
	function(data, optn) { return '<p>' + (data.flightNum ? data.flightNum : '') + '</p>'; }
];

var addTaskOptions = { table:'taskTable', tableOverflow:'taskTableMore',
				       tableMax:MAX_DISPLAY_TASKS, rowProp:taskRowPropBody[0],
				       cellProp:taskCellProp, cellFuncs:taskBodyCellFuncs,
				       escapeHtml:false };

var headerTaskOptions = { table:'taskTable', tableOverflow:null,
				       tableMax:null, rowProp:taskRowPropHeader[0],
				       cellProp:taskCellProp, cellFuncs:taskHeaderCellFuncs,
				       escapeHtml:false };	



// Header Function
var taskHeaderDivFunc = function(data) {
	var countMsg = '';
	if (!ghs.ui.isTaskTableMore && (taskRowCount >= MAX_DISPLAY_TASKS)) {
		countMsg = MAX_DISPLAY_TASKS + " " + ghs.ui.getMessage('of') + " " + taskRowCount;
	} else if (ghs.ui.isTaskTableMore || (taskRowCount < MAX_DISPLAY_TASKS) && (taskRowCount != 0)) {
		countMsg = taskRowCount + ' ' + ghs.ui.getMessage('of') + ' ' + taskRowCount;
	}
	if (window.navigator.userAgent.indexOf("Chrome") != -1){
	
	//return "<div class='rowNew nm'><div class='col  pad5lft' style='width:60%; padding: 0px 0 3px 5px;'><p1><b>" + ghs.ui.getMessage('tasks') + "</b></p1></div><div class='col' style='width:34%; padding: 0px 0 3px 0;'><p1 style='text-align:right; display:block'><b>" + countMsg + "</b></p1></div></div>";
	return "<div class='row nm'><div class='col  pad5lft' style='width:60%; padding: 1px 0 3px 5px;'><p><b>" + ghs.ui.getMessage('tasks') + "</b></p></div><div class='col' style='width:34%; padding: 1px 0 3px 0;'><p style='text-align:right; display:block'><b>" + countMsg + "</b></p></div></div>";
	}
	else{
	
	return "<div class='row nm'><div class='col pad3Top pad5lft' style='width:60%'><p class='f12'><b>" + ghs.ui.getMessage('tasks') + "</b></p></div><div class='col' style='width:34%'><p style='text-align:right'>" + countMsg + "</p></div></div>";
	}
};

// Footer function
var taskFooterDivFunc = function() {
	if (taskRowCount > MAX_DISPLAY_TASKS && $('taskTableMore').style.display == 'none') {
	
	return "<p class='pad3lft ctr'><a href='javascript:ghs.ui.seeMoreTasks();'>" + ghs.ui.getMessage('seeAll') + " " + taskRowCount + " " + ghs.ui.getMessage('tasks') + "</a></p>";
		
	} else if (taskRowCount <= 0){
		$('taskTable').style.display='none';
		$('taskTableMore').style.display='none';
		
		return "<p class='pad3lft ctr'>" + ghs.ui.getMessage('noTasksAvailable') + "</p>";
		
	} else {
		return '';
	}	
};	   


/*
 #############################################
 	Alert Properties
 ############################################# 
 */

// Row Properties
var alertRowPropData = [ {className:'row alert', msgClassName:'row message', dynamicHeightMin:36} ];

// Cell Properties
var alertCellProp;

if (window.navigator.userAgent.indexOf("Chrome") != -1){
alertCellProp = [
	{className:'col ctr', style:'width:15%;padding-top:4px;'},
	{className:'col lft', style:'width:65%;padding-top: 7px;'},
	{className:'col ctr f11', style:'width:20%;padding-top:7px;'}
];
}
else{
alertCellProp = [
	{className:'col ctr', style:'width:15%;padding-top:4px;'},
	{className:'col lft', style:'width:65%;'},
	{className:'col ctr f11', style:'width:20%;padding-top:7px;'}
];
}

// Cell Functions
var alertCellFuncs = [
	function(data) {
		
		
		if (data.type == 'm') {
		if (window.navigator.userAgent.indexOf("Chrome") != -1){
			return '<p>' + '<img src="../images/icons/alert_tip.png" width="40"/>' + '</p>';
		}
		else{
		return '<p>' + '<img src="../images/icons/alert_tip.png"/>' + '</p>';
		}
			
		}
		
		if (window.navigator.userAgent.indexOf("Chrome") != -1){
		return '<p>' + '<img src="../images/icons/caution.png" width="40"/>' + '</p>'; 
		}
		else{
		return '<p>' + '<img src="../images/icons/caution.png"/>' + '</p>'; 
		}
		
		
		},
	function(data) { return '<p>' + data.message[ghs.ui.lang] + '</p>'; },
	function(data, optn) {
		if (data.type == 'd') {
		  return "<p><a href='javascript:ghs.srvc.removeAlert(\""+ ghs.ui.empId + "\",\"" + data.task.id + "\");'><img src='../images/" + ghs.ui.lang + "/btn_ok_off.png' /></a></p>";
		} else if (data.type == 'm') {
		  return "<p><a href='javascript:ghs.ui.itemSelected(ghs.srvc.alertList[" + ghs.srvc.getAlertPos(data.id) + "].task);'><img src='../images/" + ghs.ui.lang + "/btn_view_off.png' /></a></p>";
		}
		return "<p><a href='javascript:ghs.ui.itemSelected(ghs.srvc.taskList[" + ghs.srvc.getTaskPos(data.task.id) + "]);'><img src='../images/" + ghs.ui.lang + "/btn_view_off.png' /></a></p>";
	}
];

// Header Function
var alertHeaderDivFunc = function(data) {
	var countMsg = '';
	if (!ghs.ui.isAlertTableMore && alertRowCount >= MAX_DISPLAY_ALERTS) {
		countMsg = MAX_DISPLAY_ALERTS + " " + ghs.ui.getMessage('of') + " " + alertRowCount;
	} else if (ghs.ui.isAlertTableMore || (alertRowCount < MAX_DISPLAY_ALERTS) && (alertRowCount != 0)) {
		countMsg = alertRowCount + " " + ghs.ui.getMessage('of') + " " + alertRowCount;
	}
	if (window.navigator.userAgent.indexOf("Chrome") != -1){
	return "<div class='row nm'><div class='col pad1Top pad5lft' style='width:60%'><p><b>" + ghs.ui.getMessage('alerts') + "</b></p></div><div class='col pad1Top' style='width:34%'><p style='text-align:right; display:block'><b>" + countMsg + "</b></p></div></div>";
	}
	else{
	return "<div class='row nm'><div class='col pad3Top pad5lft' style='width:60%'><p class='f12'><b>" + ghs.ui.getMessage('alerts') + "</b></p></div><div class='col' style='width:34%'><p style='text-align:right'>" + countMsg + "</p></div></div>";
	}
};

// Footer Function
var alertFooterDivFunc = function() {
	if (alertRowCount > MAX_DISPLAY_ALERTS && $('alertTableMore').style.display == 'none') {
		
		return "<p class='pad3lft ctr'><a href='javascript:ghs.ui.seeMoreAlerts();'>See All " + alertRowCount + " " + ghs.ui.getMessage('alerts') + "</a></p>";
		
	} else if (alertRowCount <= 0) {
		$('alertTable').style.display='none';
		$('alertTableMore').style.display='none';
		
		return "<p class='pad3lft ctr'>" + ghs.ui.getMessage('noAlertsAvailable') + "</p>";
		
	} else {
		return '';
	}
};

var addAlertOptions = { table:'alertTable', tableOverflow:'alertTableMore',
				       tableMax:MAX_DISPLAY_ALERTS, rowProp:alertRowPropData[0],
				       cellProp:alertCellProp, cellFuncs:alertCellFuncs,
				       escapeHtml:false };


