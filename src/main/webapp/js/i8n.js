

/**
 * 
 * UI Properties File for AC GHS
 * 
 * @author M.Kurabi
 * 
 */
 
 /*
 #############################################
 	i8n Properties
 ############################################# 
 */
 
/**
 * Declare an object to which we can add real functions.
 */
if (ghs.i8n == null) { ghs.i8n = {}; }

// Labels object
if (ghs.i8n.labels == null) { ghs.i8n.labels = {}; }

// English Labels
if (ghs.i8n.labels.en == null) { ghs.i8n.labels.en = {}; }

ghs.i8n.labels.en = {
_aircraftFin:"Aircraft/Fin #:",
_arrDep:"Arrive/Depart:",
_arrival:"Arrival:",
_assistAgent1:"Assist Agent #1:",
_assistAgent2:"Assist Agent #2:",
_assistAgent3:"Assist Agent #3:",
_bags:"Bags:",
_cnxBags:"Bags",
_cnxDestination:"Dest.",
_cnxEtd:"ETD",
_cnxFin:"Fin",
_cnxFlight:"Flight",
_cnxGate:"Gate",
_cnxPax:"Pax",
_controlAgent:"Control Agent:",
_departure:"Departure:",
_endTime:"End:",
_eta:"ETA:",
_etaEtd:"ETA/ETD:",
_etd:"ETD:",
_flightNum:"Flight #:",
_from:"From:",
_gate:"Gate:",
_gates:"Gates:",
_groundTime:"Ground Time:",
_hcBags:"HOT CNX Bag:",
_heading_comments:"Comments",
_heading_direct_message:"Direct Message",
_heading_five:"Connections",
_heading_four:"Ops/Task Remarks",
_heading_one:"Task Information",
_heading_three:"Comments",
_heading_two:"Flight Information",
_load:"Load Factor:",
_location:"Location:",
_numAgents:"# of Agents:",
_numCrew:"# of Crews:",
_numOfGT_SPATT:"# Gate Spat Agts:",
_opsRemarksArr:"<b>Arrival Flight Remarks:</b",
_opsRemarksDep:"<b>Departure Flight Remarks:</b>",
_pax:"PAX:",
_sta:"STA:",
_startTime:"Start",
_staStd:"STA/STD:",
_std:"STD:",
_time:"Date/Time:",
_to:"Departure:",
_type:"Type:",
_um_blnd:"UM/BLND:",
_wchcPax:"WCHC Pax:",
_wchr_wchc:"WCHR/WCHC:",
alerts:"Alerts",
back:"Back",
city:"City",
flight:"Flight",
gate:"Gate",
id:"ID:",
na:"N/A",
noAlertsAvailable:"No alerts available",
noTasksAvailable:"No tasks available",
of:"of",
pleaseWait:"Please Wait...",
seeAll:"See All",
start:"Start",
tasks:"Tasks",
time:"Time:",
lastUpdate:"Last update:",
refreshTime: "5",
type:"Type",
exitPrompt:"If you would like to exit, press OK.",
statusGettingTaskList:"Retrieving Task List...",
statusContactingServer:"Contacting Server..."
};

// French Labels
if (ghs.i8n.labels.fr == null) { ghs.i8n.labels.fr = {}; }

ghs.i8n.labels.fr = {
_aircraftFin:"App.:",
_arrDep:"Arriv�e/D�part:",
_arrival:"Arriv�e:",
_assistAgent1:"Agent d'assistance no 1:",
_assistAgent2:"Agent d'assistance no 2:",
_assistAgent3:"Agent d'assistance no 3:",
_bags:"Bag.:",
_cnxBags:"Bag.",
_cnxDestination:"Dest.",
_cnxEtd:"ETD",
_cnxFin:"App.",
_cnxFlight:"Vol",
_cnxGate:"Porte",
_cnxPax:"Pass.",
_controlAgent:"Agent de contr�le:",
_departure:"D�part:",
_endTime:"Fin:",
_eta:"ETA:",
_etaEtd:"ETA/ETD:",
_etd:"ETD:",
_flightNum:"No de vol:",
_from:"Origine:",
_gate:"No de porte:",
_gates:"Portes:",
_groundTime:"Temps d�escale:",
_hcBags:"HCNX Bag:",
_heading_comments:"Commentaires",
_heading_direct_message:"Message direct",
_heading_five:"Correspondances",
_heading_four:"Remarque OPS sur les t�che",
_heading_one:"Information sur la t�che",
_heading_three:"Commentaires",
_heading_two:"Information sur le vol",
_load:"Coeff. d�occ.:",
_location:"Lieu:",
_numAgents:"Nombre d'agents:",
_numCrew:"Nbre memb. d'�quip.:",
_numOfGT_SPATT:"Nb d'agents de porte � SPAT:",
_opsRemarksArr:"<b>Remarques � Vol entrant :</b",
_opsRemarksDep:"<b>Remarques � Vol sortant :</b>",
_pax:"Pass.:",
_sta:"STA:",
_startTime:"D�but",
_staStd:"STA/STD:",
_std:"STD:",
_time:"Date/heure:",
_to:"D�part:",
_type:"Type:",
_um_blnd:"UM/BLND:",
_wchcPax:"Pass. faut. roulant:",
_wchr_wchc:"WCHR/WCHC:",
alerts:"Alertes",
back:"Pr�c�dent",
city:"Ville",
flight:"Vol",
gate:"Porte",
id:"Matr.:",
na:"S.O.",
noAlertsAvailable:"Aucune alerte n'est disponible",
noTasksAvailable:"Aucune t�che n'est disponible",
of:"de",
pleaseWait:"Veuillez attendre...",
seeAll:"Voir tout",
start:"D�but",
tasks:"T�ches",
time:"Heure:",
lastUpdate:"",
refreshTime: "5",
type:"Type",
exitPrompt:"Si vous souhaitez quitter, appuyez sur OK.",
statusGettingTaskList:"R�cup�rer la liste des t�ches...",
statusContactingServer:"Contacte Server..."
};
