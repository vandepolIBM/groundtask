/**
 * 
 * Utilities Functions
 * 
 * @author M.Kurabi
 */

/**
 * Declare an object to which we can add real functions.
 */
if (ghs.util == null) { ghs.util = {}; }

/** @private The flag we use to decide if we should escape html */
ghs.util._escapeHtml = true;

/**
 * @public The colour to return depending on the load factor
 */
ghs.util.getLoadColour = function(loadFactor) {
  loadFactor = parseInt(loadFactor);
  if (loadFactor >= 100) { return ghs.config.RED; }
  if (loadFactor >=  75) { return ghs.config.ORANGE; }
  if (loadFactor >=  50) { return ghs.config.YELLOW; }
  if (loadFactor >=   0) { return ghs.config.GREEN; }
};

/**
 * @public The colour to return depending on the battery level
 */
ghs.util.getBatteryColour = function(loadFactor) {
  loadFactor = parseInt(loadFactor);
  if (loadFactor >  100) { return ghs.config.GREY; }
  if (loadFactor >=  40) { return ghs.config.GREEN; }
  if (loadFactor >=  20) { return ghs.config.YELLOW; }
  if (loadFactor >=  15) { return ghs.config.ORANGE; }
  if (loadFactor >=   0) { return ghs.config.RED; }
};

/**
 * Sets the Load factor on a LoadBar CSS class object
 * @param id of LoadBar div
 * @param load percentrage in String format
 * @param width of div
 * @param hide if null
 */
ghs.util.setLoadFactor = function(id, load, width, hideNull) {
  var loadFactor = $(id);
  if (loadFactor) {
  	if (load) {
  	  loadFactor.style.backgroundPosition = parseInt(load / (100 / width)) + 'px 0px';
      loadFactor.style.backgroundColor = ghs.util.getLoadColour(load);
  	} else if (hideNull) {
  		loadFactor.parentNode.style.visibility = 'hidden';
  	}
  }
}

/**
 * Highlight ele with style highlight
 */
ghs.util.highlight = function(ele, highlight) {
  ele = ghs.util._getElementById(ele, "setValue");
  if (ele != null && ele.className.indexOf(highlight) < 0) {
  	ele.className = ele.className ? ele.className + ' ' + highlight : highlight;
  }
};

/**
 * Remove highlight from ele
 */
ghs.util.removeHighlight = function(ele, highlight) {
  ele = ghs.util._getElementById(ele, "setValue");
  if (ele) {
    ele.className = ele.className.replace(highlight, '');
  }
};

/**
 * @public Sets the value of the ele param with the data gathered
 * from eleFunc param. If hideIfEmpty is true and ele is empty, object
 * is hidden
 */
ghs.util.setValue = function(ele, eleFunc, hideIfEmpty) {
  ele = ghs.util._getElementById(ele, "setValue");
  if (ele == null) { return; }
  var content = eleFunc();
  ele.innerHTML = content;
  !content && hideIfEmpty ? ele.style.display = 'none' : ele.style.display='';
};

/**
 * @public Sets the style properties of the element
 */
ghs.util.setStyle = function(ele, styleProp) {
  ele = ghs.util._getElementById(ele, "setValue");
  if (ele == null) { return; }
  var style = styleProp.style;
  ele.style = style;
};

/**
 * @public Sets the visibility style of an element based on param
 */
ghs.util.setStyleVisibility = function(ele, bool) {
  ele = ghs.util._getElementById(ele, "ghs.util.setStyleVisibility");
  if (ele == null) { return; }
  ele.style.visibility = bool ? '' : 'hidden';
}

/**
 * @public Returns table row count, hasHeader substracts 1 from count
 */
ghs.util.getTableRowCount = function (ele, hasHeader) {
  var header = hasHeader ? 1 : 0;
  ele = ghs.util._getElementById(ele, "getTableRowCount()");
  var count = ele.childNodes.length;
  return count ? count - header : 0;
};

/**
 * @public Populate a JSON object into the template string
 */
ghs.util.populateTemplate = function(labels, data, template, itemRenderers) {
  var reg = /\$\{((_)?\w+)\}/gi;
  var match = null;
  while (match = reg.exec(template)) {
    if (itemRenderers && itemRenderers[match[1]]) {
    	template = template.replace(match[0], ghs.util.generateItemRendererContent(data[match[1]], itemRenderers[match[1]]));
    } else {
	  if (match[2]) {
		  template = template.replace(match[0], labels[match[1]] != null ? labels[match[1]] : '&nbsp;' );
	  } else {
		  template = template.replace(match[0], (data[match[1]] != null && data[match[1]] != "#") ? data[match[1]] : '&nbsp;' );
	  }
    }
  }
  return template;
};

ghs.util.generateItemRendererContent = function(array, itemRenderer) {
  var i, reg;
  var content = "";
  var itemContent = "";
  for (i = 0; i < array.length; i++) {
	itemContent = itemRenderer;
	for (var n in array[i]) {
	  if (typeof(array[i][n]) == 'function') { continue; }
      reg = new RegExp('\\$\{' + n + '\}', "gi");
      itemContent = itemContent.replace(reg, array[i][n]);
      try{
	      if(array[i][n] === true){
	    	  itemContent = itemContent.replace("row","row rowRed");
	      }
      }catch(e){  /*Do Nothing*/ } 
	}
	content += itemContent;
  }
  return content;
};

ghs.util.replaceWithAsciiCharacters = function(text) {
  return text.replace(/\r|\n|\r\n/g, '<br />');
};

/**
 * @public Highlight Fields in template
 */
ghs.util.highlightTemplateFields = function(fields, map, highlight) {
  var i = 0;
  for (i = 0; i < fields.length; i++) {
  	if (map[fields[i]] != null) {
  	  if ( ghs.util._isArray(map[fields[i]]) ) {
  	  	j = 0;
  	  	for (j = 0; j < map[fields[i]].length; j++) {
  	  	  ghs.util.highlight(map[fields[i]][j], highlight);
  	  	}
  	  } else {
  		ghs.util.highlight(map[fields[i]], highlight);
  	  }
  	}
  }
}

/**
 * @public delete all rows in a table
 */
ghs.util.deleteAllRows = function(ele, hasHeader) {
  ele = ghs.util._getElementById(ele, "deleteAllRows");
  var child = ele.childNodes;
  var length = child.length;
  var count = 0;
  if (hasHeader && length > 0) {
  	count = 1;
  }
  while (count < length) {
    ele.removeChild(child[--length]);
  }
};

/**
 * @public Create rows/col inside a DIV ele
 */
ghs.util.addRows = function(data, eleCount, options) {
  var ele = ghs.util._getElementById(options.table, "addRows()");
  var ele2;
  options.tableOverflow ? ele2 = ghs.util._getElementById(options.tableOverflow, "addRows()") : null;
  if (ele == null) { return; }
  var isEle2;
  if (ele2 == null && options.tableMax != null) {
  	return;
  } else {
  	isEle2 = true;
  }
  ele.style.display='';
  if (!options) { options = {}; }
  if (!options.rowCreator) { options.rowCreator = ghs.config.defaultRowCreator; }
  if (!options.cellCreator) { options.cellCreator = ghs.config.defaultCellCreator; }
  var tr, rowNum;
  if (ghs.util._isArray(data)) {
    for (rowNum = 0; rowNum < data.length; rowNum++) {
      options.rowData = data[rowNum];
      options.rowNum = rowNum;
      options.data = null;
      options.cellNum = -1;
      tr = ghs.util._addRowInner(options);
      if (tr != null) {
	  	eleCount++;
	  	if (ele2 && eleCount > options.tableMax) {
			(tr.toHtml) ? ele2.innerHTML += tr.toHtml() : ele2.appendChild(tr);
		} else {
			(tr.toHtml) ? ele.innerHTML += tr.toHtml() : ele.appendChild(tr);
		}
	  }
    }
  }
  return eleCount;
};

/**
 * @private Internal function to draw a single row of a table.
 */
ghs.util._addRowInner = function(options) {
  var tr = options.rowCreator(options);
  if (tr == null) { return null; }
  for (var cellNum = 0; cellNum < options.cellFuncs.length; cellNum++) {
    var func = options.cellFuncs[cellNum];
    if (typeof func == 'function') {
      options.data = func(options.rowData, options);
    } else { 
      options.data = func || "";
    }
    options.cellNum = cellNum;
    var td = options.cellCreator(options);
    if (td != null) {
      if (options.data != null) {
        if (ghs.util._shouldEscapeHtml(options) && typeof(options.data) == "string") {
		  td.innerHTML = ghs.util.escapeHtml(options.data);
		} else {
		  td.innerHTML += options.data;
		}
      }
	  tr.appendChild(td);
    }
  }
  return tr;
};

/**
 * @private Array detector. Note: instanceof doesn't work with multiple frames.
 */
ghs.util._isArray = function(data) {
  return (data && data.join);
};

/**
 * @public Compare 2 non-complex Arrays.
 */
ghs.util.areArraysEqual = function(arr1, arr2) {
  if (arr1.length != arr2.length) { return false; }
  arr1.sort();
  arr2.sort();
  if (arr1.toString() == arr2.toString()) { return true; }
  return false;
};

/** @private Work out from an options list and global settings if we should be esccaping */
ghs.util._shouldEscapeHtml = function(options) {
  if (options && options.escapeHtml != null) {
    return options.escapeHtml;
  }
  return ghs.util._escapeHtml;
};

/**
 * @public Return a string with &, <, >, ' and " replaced with their entities
 */
ghs.util.escapeHtml = function(original) {
  var div = document.createElement('div');
  var text = document.createTextNode(original);
  div.appendChild(text);
  return div.innerHTML;
};

/**
 * @private Helper to turn a string into an element with an error message
 */
ghs.util._getElementById = function(ele, source) {
  //var orig = ele;
  ele = ghs.util.byId(ele);
  /*if (ele == null) {
    ghs.util._debug(source + " can't find an element with id: " + orig + ".");
  }*/
  return ele;
};

/**
 * @public Find the element in the current HTML document with the given id or ids
 */
if (document.getElementById) {
  ghs.util.byId = function() {
    var elements = new Array();
    for (var i = 0; i < arguments.length; i++) {
      var element = arguments[i];
      if (typeof element == 'string') {
        element = document.getElementById(element);
      }
      if (arguments.length == 1) {
        return element;
      }
      elements.push(element);
    }
    return elements;
  };
} else if (document.all) {
  ghs.util.byId = function(){
    var elements = new Array();
    for (var i = 0; i < arguments.length; i++) {
      var element = arguments[i];
      if (typeof element == 'string') {
        element = document.all[element];
      }
      if (arguments.length == 1) {
        return element;
      }
      elements.push(element);
    }
    return elements;
  };
};

/**
 * Alias $ to dwr.util.byId
 */
var $;
if (!$) {
  $ = ghs.util.byId;
}

ghs.util.getURLParam = function( name ) {
	ghs.log.info('[ghs.util.getURLParam] original url...'+window.location.href);
  if (typeof name == 'undefined'){
    alert("name undefined")
    var err = new Error().stack;
    ghs.log.info(err);
  } else {
    ghs.log.info('[ghs.util.getURLParam] original name defined');
    //DVPalert('[ghs.util.getURLParam] original name...'+name);
  }
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var url = window.location.href;
  var url2 = url.replace(/&amp;/g, "&");
  ghs.log.info('[ghs.util.getURLParam] transformed url...'+url2);
  var results = regex.exec( url2 );
  if( results == null ) { return ""; } else { return results[1]; }
};

ghs.util.getURLParamNoLog = function( name ) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var url = window.location.href;
  var url2 = url.replace(/&amp;/g, "&");
  var results = regex.exec( url2 );
  if( results == null ) { return ""; } else { return results[1]; }
};

/**
 * Node Object
 * This node object is used due to broken document.appendChild() in IE mobile
 */
function Node(tagName) {
  this.tag = tagName;
}

/**
 * append Node object to another node object
 */
function appendChild(child) {
  this.innerHTML += child.toHtml();
};

/**
 * Remove method to remove elments from array
 */
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

/**
 * Remove method to remove elments from array
 */
Array.prototype.subtract = function(array) {
  var originalArray = this.concat();
  for (var i = this.length - 1; i >= 0 ; i--) {
  	for (var j = 0; j < array.length; j++) {
  		if (originalArray[i] == array[j]) {
  			this.remove(i);
  		}
  	}
  }
};

//Override IE Array Method due to Array.indexOf method does not work in IE!
if(!Array.indexOf){
  Array.prototype.indexOf = function(obj){
  for(var i=0; i<this.length; i++){
    if(this[i]==obj){ return i; }
  }
  return -1;
  }
};

/**
 * Get the HTML for that Node object
 */
function toHtml() {
  var string = "<" + this.tag;
  this.id ? string += " id='" + this.id + "'" : '';
  this.className ? string += " class='" + this.className + "'" : '';
  this.style ? string += " style='" + this.style + "'" : '';
  string += ">";
  this.innerHTML ? string += this.innerHTML : '';
  string += "</" + this.tag + ">";
  return string;
};

/**
 * Node prototype Properties
 */
Node.prototype.tag='';
Node.prototype.className='';
Node.prototype.id='';
Node.prototype.style=''
Node.prototype.innerHTML='';
Node.prototype.appendChild=appendChild;
Node.prototype.toHtml=toHtml;
