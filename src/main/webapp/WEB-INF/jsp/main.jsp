<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="MobileOptimized" content="240" />
		
		<title>Air Canada - StreamLine</title>
				
		<link id="linkID" rel="stylesheet" href="<c:url value='/css/style.css' />" type="text/css" />		
		<script>
       // if Chrome
			if (window.navigator.userAgent.indexOf("Chrome") != -1){
			
    		var link = document.getElementById('linkID');
    		link.href = "<c:url value='/css/style_Android.css' />";
    		
    		
    		}
			
		</script>
						
		
		
		
		<script src="<c:url value='/js/https_cert_hack.js' />" type="text/javascript"></script>
		
		<script src="<c:url value='/dwr/interface/TaskCreator.js' />" type="text/javascript"></script>
		 <!-- <script src="<c:url value='/dwr/engine.js' />" type="text/javascript"></script>  -->
		 <script src="<c:url value='/js/engine_dvp.js' />" type="text/javascript"></script> 
		<script>dwr.engine.setErrorHandler(function(){return false;});</script> <!-- Override error handler ASAP -->
		
		
		<!-- Configurations -->
		<script src="<c:url value='/js/config_v1.js' />" type='text/javascript'></script>
		
		<!-- Gears -->
		<script src="<c:url value='/js/gears_init.js' />" type='text/javascript'></script>
		<!-- <script src="<c:url value='/js/gears_v2.js' />" type='text/javascript'></script> -->

		<script src="<c:url value='/js/gears_v1.js' />" type='text/javascript'></script>
		
		<!-- i8n -->
		<script src="<c:url value='/js/i8n.js' />" type='text/javascript'></script>
		
		<!-- Logger Utility -->
		<script src="<c:url value='/js/logger.js' />" type='text/javascript'></script>
		
		<!-- Application Utilities -->
		<script src="<c:url value='/js/util.js' />" type='text/javascript'></script>
		
		<!-- UI & UI Events -->
		<script src="<c:url value='/js/ui.js' />" type='text/javascript'></script>
		<script src="<c:url value='/js/event.js' />" type='text/javascript'></script>
		
		<!-- Services -->
		
		<!--script src="<c:url value='/js/engine_static.js' />" type="text/javascript"></script-->
		<script src="<c:url value='/js/srvc.js' />" type='text/javascript'></script>
		
		<!-- Detail Screens -->
		<script src="<c:url value='/js/detail/abstract_detail.js' />" type='text/javascript'></script>
		
		<script src="<c:url value='/js/detail/cabin.js' />" type='text/javascript'></script>
		
		<script src="<c:url value='/js/detail/atw_dep.js' />" type='text/javascript'></script>
		<script src="<c:url value='/js/detail/atw_arr.js' />" type='text/javascript'></script>
		
		<script src="<c:url value='/js/detail/btw_dep.js' />" type='text/javascript'></script>
		<script src="<c:url value='/js/detail/btw_arr.js' />" type='text/javascript'></script>
		<script src="<c:url value='/js/detail/btw_tow.js' />" type='text/javascript'></script>
		<script src="<c:url value='/js/detail/btw_lav.js' />" type='text/javascript'></script>
		<script src="<c:url value='/js/detail/btw_turn.js' />" type='text/javascript'></script>
		<script src="<c:url value='/js/detail/btw_cnx.js' />" type='text/javascript'></script>
		
		<script src="<c:url value='/js/detail/other.js' />" type='text/javascript'></script>
		
		<script src="<c:url value='/js/detail/message.js' />" type='text/javascript'></script>

	</head>
	<body onload='ghs.gears.init();'>
		
		<object id="MobileX" classid="clsid:019BB827-4327-4D66-9B8D-F2ECBAB915CD" width="0" height="0"></object>
		
		<div id='pageHeader' class='pageHeader container'>
			<div class='row nm f11 titleHeader'>
				<div class='col np' style='width:20px;background-image:url(<c:url value="/images/offline.png" />);height:28px;'>
					<p><img id='signalImg' src='<c:url value="/images/online-yel.png" />' /></p>
				</div>
				<div class='col np' style='padding-left:1px;width:32px'><p><img src='<c:url value="/images/icons/leaf.png" />' /></p></div>
				<div class='col pad2Top rht' style='width:40px;padding: 0px 0 0px 0;'>
					<p class='f10' id="idField">
						<b>
							<script>document.write(ghs.ui.getMessage('id'));</script>&nbsp;
							<br />
							<script>window.navigator.userAgent.indexOf("Chrome") == -1 ? document.write(ghs.ui.getMessage('time')) : "";</script>&nbsp;
						</b>
					</p>
				</div>
				<div class='col pad2Top' style='width:50px; padding: 0px 0 0px 0;'>
					<p id='empId' class='f10'><script>document.write(ghs.ui.empId ? ghs.ui.empId : ghs.ui.getMessage('na'));</script></p>
					<p id='time' class='f10'><script>window.navigator.userAgent.indexOf("Chrome") == -1 ?document.write(ghs.ui.setTime(true)) : "";</script></p>
				</div>
				<div class='col ctr' style='width:27px;'>
					<div id='battCont' 'class='battCont' style='visibility:hidden;'>
						<div class='battBG'><div class='batt' id='battLvl'><!--  --></div></div>
						<div class='ctr'><p class='f8' id='battLvlT'>---%</p></div>
					</div>
				</div>
				<div id="colRight" class='col np ctr pad3Top' style='width:51px'>
					<a href='#' onclick='javascript:ghs.ui.exit();return false;' tabindex='-1'>
						<script>document.write("<p><img src='/gc/images/" + ghs.ui.lang + "/btn_exit_off.png' /></p>");</script>
					</a>
				</div>
				<div class='col np ctr' style='width:100%'></div>
			</div>
		</div>
		
		<div id='loader' class='loader container f10'><img src='<c:url value="/images/loader.gif" />' />
			<br />
			<script>document.write(ghs.ui.getMessage('pleaseWait'));</script>
			<br />
			<br />
		</div>
		
		<div id='pageContent' style='display:none;'>

			<div id='uBB' class='container uBB f11 pad6' style='display:none;'>
				<div style='width:35%;height:18px;' onclick='javascript:ghs.ui.back();'>
					<p class='pad3lft'>&laquo; <a href='javascript:ghs.ui.back();'><script>document.write(ghs.ui.getMessage('back'));</script></a></p>
				</div>
			</div>
		
			<div id='homeContent' class='homeContent container'>
			
				<div id='taskContainer'>
					<div id='taskHeader' class='heading cW'></div>
					<div id='taskTable' class='widthContainer'></div>
					<div id='taskTableMore' class='widthContainer' style='display:none;'></div>
					<div id='taskFooter' class='container footer f11 pad6 ctr'></div>
				</div>
			
				<div id='alertContainer'>
					<div id='alertHeader' class='heading cW'></div>
					<div id='alertTable' class='table'></div>
					<div id='alertTableMore' class='table'  style='display:none;padding-top:4px;'></div>
					<div id='alertFooter' class='container footer f11 pad6 ctr'></div>
				</div>
			
			</div>
		
			<div id='detailContent' class='detailContent container' style='display:none;'></div>
		
			<div id='lBB' class='container lBB f11 pad6 lBB mrg3Top' style='display:none;'>
				<div style='width:35%;height:18px;' onclick='javascript:ghs.ui.back();'>
					<p class='pad3lft'>&laquo; <a href='javascript:ghs.ui.back();'><script>document.write(ghs.ui.getMessage('back'));</script></a></p>
				</div>
			</div>
			
		</div>
		<div class='container ctr'>
			<p class='f8' id='status'></p>
		</div>
		<div class='container ctr'>
			<p class='f8' id='gearsStdOut'></p>
		</div>
		<script>
       // if Chrome
			if (window.navigator.userAgent.indexOf("Chrome") != -1){
			
			document.getElementById("colRight").style.display = "none";
		    document.getElementById("idField").className = 'f18';
		    document.getElementById("empId").className = 'f18'
		    document.getElementById("time").className = 'f18'
		    document.getElementById("taskHeader").classList.add("rowTop");
		    document.getElementById("alertHeader").classList.add("rowTop");
			var container = document.querySelectorAll(".container");
			var widthContainer =  document.querySelectorAll(".widthContainer");
			addWidth(container, "maxWidth");
			addWidth(widthContainer, "width");
			document.getElementById("taskFooter").style["padding-top"] = "30px";
			document.getElementById("alertFooter").style["padding-top"] = "30px";
			document.getElementById("lBB").style["padding-top"] = "20px";
			}

			function addWidth(elm, cssPrpo) {
				for (var i = 0;  i < elm.length; i++) {
							elm[i].style[cssPrpo] = "100%"
						}
			}
		</script>
	</body>
</html>
