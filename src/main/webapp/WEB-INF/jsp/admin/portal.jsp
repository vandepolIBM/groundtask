<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<title>Ground Task - Admin Portal</title>
		<style>
			
			body {
			    font-family: Arial, Helvetica, sans-serif;;
			    font-size: .8em;
			}
			
			form {
			    padding: 0px;
			    margin: 0px;
			}
			
			h3 {
			    margin: 15px 0px 0px 0px;
			}
			
			a:link {
			    color: blue;
			}
			
			a:visited {
			    color: blue;
			}
			
			#details label {
			    position: absolute;
			    margin-top: 8px;
			    text-align: right;
			    width: 130px;
			}
			
			#details input, #details textarea, #details select {
			    margin-left: 140px;
			    margin-top: 5px;
			}
			
			.selected {
			    background-color: #AACCFF;
			    font-weight: bold;
			}
			
			#tasks {
			    display: none;
			}
			
			#alerts {
			    display: none;
			}
			
			#rpc {
				display: none;
			}
			
			#header {
			    margin: 5px 0px 5px 5px;
			}
			
			#pane {
			    float: left;
			    width: 180px;
			    padding: 5px 0px 5px 5px;
			    margin: 5px 0px 5px 5px;
			}
			
			#view {
			    position: relative;
			    margin: 0px 30px 0px 185px;
			    height: 400px;
			    border: solid;
			    border-width: 5px;
			    border-color: #AACCFF;
			}
			
			#buttonsBar {
			    background-color: #AACCFF;
			    padding: 5px 0px 5px 5px;
			}
			
			#details {
			    margin: 20px 0px 0px 0px;
			}
		</style>
		<script src="<c:url value='/js/prototype.js' />" type="text/javascript"></script>
		<script src="<c:url value='/dwr/interface/TaskCreatorAdmin.js' />" type="text/javascript"></script>
		<script src="<c:url value='/dwr/engine.js' />" type="text/javascript"></script>
		<script src="<c:url value='/dwr/util.js' />" type="text/javascript"></script>
		<script type="text/javascript">
			
			var emp = "";
			var taskId = "";
			
			function initialize(){
			    TaskCreatorAdmin.getEmployees(function(data){
			        var val = "";
			        for (i = 0; i < data.length; i++) {
			            var e = document.createElement("a");
			            e.setAttribute("href", "#emp/" + data[i]);
			            e.setAttribute("id", "employee_" + data[i]);
			            e.innerHTML = data[i];
			            var div = document.createElement("div");
			            div.onclick = employeeSelected;
			            div.setAttribute("class", "unselected");
			            div.appendChild(e);
			            $("employeeList").appendChild(div);
			        }
			    });
			}
			
			function loadTasks(employee){
			    TaskCreatorAdmin.getCurrentTasks(employee, function(data){
			        $("taskList").innerHTML = "";
			        for (i = 0; i < data.length; i++) {
			            var e = document.createElement("a");
			            e.setAttribute("href", "#emp/" + employee + "/task/" + data[i].id);
			            e.setAttribute("id", "task_" + data[i].id);
			            e.innerHTML = data[i].type;
			            var div = document.createElement("div");
			            div.onclick = taskSelected;
			            div.setAttribute("class", "unselected");
			            div.appendChild(e);
			            $("taskList").appendChild(div);
			        }
			    });
			}
			
			function loadAlerts(employee){
			    TaskCreatorAdmin.getAlerts(employee, function(data){
			        $("alertList").innerHTML = "";
			        for (i = 0; i < data.length; i++) {
			            var e = document.createElement("a");
			            e.setAttribute("id", "alert_" + data[i].id);
			            e.innerHTML = data[i].message;
			            var div = document.createElement("div");
			            div.setAttribute("class", "unselected");
			            div.appendChild(e);
			            $("alertList").appendChild(div);
			        }
			    });
			}
			
			function employeeSelected(){
			    for (i = 0; i < $("employeeList").childNodes.length; i++) {
			        $("employeeList").childNodes[i].className = 'unselected';
			    }
			    this.className = 'selected';
			    $("tasks").style.display = 'inline';
			    $("alerts").style.display = 'inline';
			    $("rpc").style.display = 'inline';
			    $("content").innerHTML = "";
			    emp = this.childNodes[0].id.substring(this.childNodes[0].id.lastIndexOf('_') + 1);
			    loadTasks(emp);
			    loadAlerts(emp);
			}
			
			function taskSelected(){
			    for (i = 0; i < $("taskList").childNodes.length; i++) {
			        $("taskList").childNodes[i].className = 'unselected';
			    }
			    this.className = 'selected';
			    
			    taskId = this.childNodes[0].id.substring(this.childNodes[0].id.lastIndexOf('_') + 1);
			    
			    var ajax = new Ajax.Updater({
			        success: 'content'
			    }, '../task/cabin', {
			        method: 'get',
			        parameters: 'emp=' + emp + '&taskId=' + taskId
			    });
			}
			
			function createTaskSelected(){
				taskId = "";
			    for (i = 0; i < $("taskList").childNodes.length; i++) {
			        $("taskList").childNodes[i].className = 'unselected';
			    }
			    var ajax = new Ajax.Updater({
			        success: 'content'
			    }, '../task/cabin', {
			        method: 'get',
			        parameters: 'emp=' + emp
			    });
			}
			
			function makeRemoteCall(){
				taskId = "";
			    for (i = 0; i < $("taskList").childNodes.length; i++) {
			        $("taskList").childNodes[i].className = 'unselected';
			    }
			    var ajax = new Ajax.Updater({
			        success: 'content'
			    }, '../rpc', {
			        method: 'get',
			        parameters: 'emp=' + emp
			    });
			}
			
			function deleteTask() {
				window.location.href = '<c:url value="/ctrl/task/delete" />' + '?emp=' + emp + '&taskId=' + taskId;
			}
						
			function createAlert(){
			
			}
			
		</script>
	</head>
	<body onload="initialize();">
		<div id="header">
			<h1>Task Manager</h1>
		</div>
		<div id="pane">
			<div id="employees">
				<h3>Employees</h3>
				<div id="employeeList">
				</div>
			</div>
			<div id="tasks">
				<h3>Tasks</h3>
				<div id="taskList">
				</div>
				<a href="#" onclick="createTaskSelected();">Create Task</a>
			</div>
			<div id="alerts">
				<h3>Alerts</h3>
				<div id="alertList">
				</div>
			</div>
			<div id="rpc">
				<h3>Remote Call</h3>
				<a href="#" onclick="makeRemoteCall();">Make remote call</a>
			</div>
		</div>
		<div id="view">
			<div style="background-color:#AACCFF; font-weight: bold; padding: 3px;">
				Select an employee, then select or create a task/alert.
			</div>
			<div id="content">
			</div>
		</div>
	</body>
</html>
