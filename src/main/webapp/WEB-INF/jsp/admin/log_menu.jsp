<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Logging Console</title>
		<script src="<c:url value="/js/admin/jquery-1.4.3.min.js" />" type="text/javascript"></script>
		<script src="<c:url value="/js/admin/jquery.address-1.3.1.min.js" />" type="text/javascript"></script>
		<script src="<c:url value="/js/admin/sdf.js" />" type="text/javascript"></script>
<style>
body {
	margin: 0;
	padding: 0;
	background-color: white;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 11pt;
	color: #343432;
}		
	
a {
	color: #105CB6;
	cursor: pointer;
	text-decoration: none;
}

#tabs {
	overflow: hidden;
}

#tabs ul {
	list-style: none;
	text-align: left;
	padding: 0;
	margin: 0;
}

#tabs li,#tabs a {
	float: left;
}	

#tabs a {
	border-bottom-left-radius: 5px 5px;
	border-bottom-right-radius: 5px 5px;
	border-top-left-radius: 5px 5px;
	border-top-right-radius: 5px 5px;
	color: #343432;
	font-size: 1.1em;
	font-weight: bold;
	margin-right: 10px;
	padding: 1px 17px 2px;
	position: relative;
}

#tabs .active,#tabs .active:hover {
	background: white;
	color: #343432;
	text-decoration: underline;	
}


#tabs li#note {
	padding: 3px 10px 4px;
}

#stats {
}

h3 {
	margin: 2px auto;
	font-size: 1.1em;
	background-color: #00A0C6;
	color: white;
	padding: 2px 7px 4px;
	margin-bottom: 7px;
}

legend {
	background-color: #00A0C6;
	border: 1px solid #B2B2B2;
	padding: 0px 10px;
	color: white;
	font-size: 0.8em
}

input,button,select {
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
}

#container {
	margin: 20px;
}

.row {
	margin-top: 3px;
	padding: 3px 0px;
	border-top: 1px solid #B2B2B2
}

label {
	display: inline;
	margin-right: 10px;
}

input[type=text] {
	font-size: 1.0em;
	width: 250px;
	padding: 7px 9px 6px;
	border: 1px solid #B2B2B2;
	margin-right: 10px;
}

input[type=checkbox] {
	margin-right: 5px;
}		

select {
	font-size: 1.0em;
	border: 1px solid #888A85;
	background: #81A745 -webkit-gradient(linear, 0% 0%, 0% 100%, from(#E5E5E5), to(#F4F4F4) );
	color: black;
	padding: 7px;
}

button {
	font-size: .9em;
	font-weight: bold;
	border: 1px solid #B2B2B2;
	background: #00A0C6 -webkit-gradient(linear, 0% 0%, 0% 100%, from(#00B0C6), to(#00A0C6) );
	border-bottom-left-radius: 5px 5px;
	border-bottom-right-radius: 5px 5px;
	border-top-left-radius: 5px 5px;
	border-top-right-radius: 5px 5px;	
	color: white;
	padding: 3px;
	margin-right: 10px;
	min-width: 80px;
}
button:active { 
	background: #00A0C6 -webkit-gradient(linear, 0% 0%, 0% 100%, from(#0090C6), to(#0090C6) );
}

fieldset {
	margin: 0px;
	padding: 5px 0px;
	border: 0px;
}

fieldset legend {
	font-weight: bold;
}

pre {
	margin: 0px;
}

table {
	margin: 15px 0px 15px 30px;
	background-color: #CDCDCD;
	color: #333;
	
}


th, td {
	padding: 2px 4px;
	background-color: white;
}

th {
	text-align: left;
	background-color: #00A0C6;
	color: white;
}

tr {
	font-size: 0.9em
}

td#empnum {
	cursor: pointer;
}

#placeholder {
	padding-top: 12px;
	font-size: 10px;	
}

#loading {
	border-bottom-left-radius: 5px 5px;
	border-bottom-right-radius: 5px 5px;
	border-top-left-radius: 5px 5px;
	border-top-right-radius: 5px 5px;
	position: fixed;
	top: 20px;
	right: 20px;
	background-color: red;
	font-size: 1.1em;
	font-weight: bold;
	color: white;
	padding: 1px 17px 2px;
}
</style>
		
<script type="text/javascript">
var clients = {};

function updateClient() {
	$.ajax({
		  type: 'GET',
		  url: "<c:url value='/ctrl/message/sessions' />",
		  global: false,
		  success: function(data) {
			  clients = data;
			  $('span#client-count').html(data.length);
			  
				var table = $('table#clients'); 
				$( "table#clients tbody tr" ).each( function(){
					  this.parentNode.removeChild( this ); 
					});
				for (var key in clients) {
				  if (clients.hasOwnProperty(key)) {
					var client = clients[key];
					var now = new Date();
					var diff = Math.floor((now.getTime() - client.lastAccessedTime) / 1000);
					var tr = $('<tr><td id=\"empnum\">' + client.userId + 
							     '</td><td>' + client.pendingAlerts + 
							     '</td><td>' + new SimpleDateFormat('MM-dd-yyyy HH:mm:ssZ').format(new Date(client.creationTime)) + 
							     '</td><td>' + Math.floor(diff/60) + "m" + diff%60 + 's</td></tr>');
					table.append(tr);
					tr.find('td#empnum').click(function() {
						var submit = $('button#submit-task-log');
						submit.closest('form').find('input').val($(this).html().trim());
						$('button#submit-task-log').trigger('click');
					});
				  }
				}	
		  }
		});	
}


$(function($) {

	// setup the tabs
	if (document.location.hostname.indexOf('450') != -1 || document.location.hostname.indexOf('451') != -1) {
		$('#tabs #t1 a').attr("href", "https://acsayul33450.aircanada.ca/gc/ctrl/logging");
		$('#tabs #t2 a').attr("href", "https://acsayul33451.aircanada.ca/gc/ctrl/logging");
		if (document.location.hostname.indexOf('450') != -1) {
			$('#tabs #t1 a').attr("class", "active");
		} else {
			$('#tabs #t2 a').attr("class", "active");
		}
	} else if (document.location.hostname.indexOf('620') != -1 || document.location.hostname.indexOf('621') != -1) {
		$('#tabs #t1 a').attr("href", "https://acsayul33620.aircanada.ca/gc/ctrl/logging");
		$('#tabs #t2 a').attr("href", "https://acsayul33621.aircanada.ca/gc/ctrl/logging");
		if (document.location.hostname.indexOf('620') != -1) {
			$('#tabs #t1 a').attr("class", "active");
		} else {
			$('#tabs #t2 a').attr("class", "active");
		}
	} else {
		$('#tabs #t1 a').attr("href", "#t1");
		$('#tabs #t2 a').attr("href", "#t2");
		$('#tabs #t1 a').attr("class", "active");
	}
	
	$('#loading')
    .hide()  // hide it initially
    .ajaxStart(function() {
        $(this).show();
    })
    .ajaxStop(function() {
        $(this).hide();
    });
	
	$('button#submit-event-employee').bind('click', function(event) {
		var empNum = $(this).closest('form').find('input[type="text"]').val();

		function appendRegexForWebService(e, tagName, dest, optional) {
			if (optional === undefined) {
				optional = "";
			}
			
			if ($(e).closest('form').find('#include-ws').is(':checked')) {
				dest = appendRegex(dest, "Sent request .+?" + tagName + ".*?\\<EmpNumber\\>" + empNum + "\\<" + optional);
				dest = appendRegex(dest, "Received response .+?" + tagName + ".*?\\<EmpNumber\\>" + empNum + "\\<" + optional);
			} 
			return dest;
		}
		
		function appendRegex(dest, src) {
			src = '(?:' + src + ')';
			if (dest.length > 0) {
				return dest + '|' + src;
			} else {
				return src;
			}
		}		
		var regex = "";
		if ($(this).closest('form').find('#include-alerts').is(':checked')) {
			regex = appendRegex(regex, "Received alert broadcast, empl=" + empNum);
		}
		if ($(this).closest('form').find('#include-connect').is(':checked')) {
			regex = appendRegex(regex, "Employee session registered, cont=\\[employeeNumber=" + empNum);
			regex = appendRegex(regex, "Employee session deregistered, cont=\\[employeeNumber=" + empNum);
		} 
		if ($(this).closest('form').find('#include-login').is(':checked')) {
			regex = appendRegexForWebService(this, "RegisterRequest", regex);
			regex = appendRegex(regex, "Sending RegisterDevice login request, employeeNumber=" + empNum);
		}
		if ($(this).closest('form').find('#include-tasks').is(':checked')) {
			regex = appendRegexForWebService(this, "GetTasks", regex);
			regex = appendRegex(regex, "GetTasksResponse: employee=" + empNum);
		}
		if ($(this).closest('form').find('#include-ack').is(':checked')) {
			regex = appendRegexForWebService(this, "UpdateTaskStatus", regex, ".*?Acknowledged\\<");
			regex = appendRegex(regex, "Sending UpdateTaskStatus ACK request, employeeNumber=" + empNum);
		}
		if ($(this).closest('form').find('#include-start').is(':checked')) {
			regex = appendRegexForWebService(this, "UpdateTaskStatus", regex, ".*?Started\\<");
			regex = appendRegex(regex, "Sending UpdateTaskStatus START request, employeeNumber=" + empNum);
		}
		if ($(this).closest('form').find('#include-stop').is(':checked')) {
			regex = appendRegexForWebService(this, "UpdateTaskStatus", regex, ".*?Completed\\<");
			regex = appendRegex(regex, "Sending UpdateTaskStatus STOP request, employeeNumber=" + empNum);
		}
				
		$(this).closest('form').find('input[type="hidden"]').val(regex);

		$.ajax({
			  type: 'GET',
			  url: $(this.form).attr('action'),
			  data: $(this.form).serialize(),
			  success: function(data) {
				  $("#placeholder").html('<pre>'+ data.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;") + '</pre>');
			  }
			});				
		$.address.value('display/search?' + $(this.form).serialize());  
		return false;
	});

	$('#include-all').click(function () {
		$(this).parents('fieldset:eq(1)').find(':checkbox').attr('checked', this.checked);
	});
	
	$('button#submit-task-log').bind('click', function(event) {
		$.ajax({
			  type: 'GET',
			  url: $(this.form).attr('action'),
			  data: $(this.form).serialize(),
			  success: function(data) {
				  $("#placeholder").html('<pre>'+ data + '</pre>');
			  }
			});		
		$.address.value('display/search?' + $(this.form).serialize());  
		return false;
	});	

	$('button#submit-regex').bind('click', function(event) {
		$.ajax({
			  type: 'GET',
			  url: $(this.form).attr('action'),
			  data: $(this.form).serialize(),
			  success: function(data) {
				  $("#placeholder").html('<pre>'+ data.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;") + '</pre>');
			  }
			});		
		$.address.value($(this.form).serialize());  
		return false;
	});	

	$('button#show-clients').bind('click', function(event) {
		var div = $('#client-view'); 
		if( div.is(':visible') ) {
		    div.slideUp();
		    $(this).text('Show');
		} else {
		    div.slideDown();
		    $(this).text('Hide');
		}
		
		return false;			
	});
	
	$('li#mode button').bind('click', function(event) {
		var div = $('#advanced'); 
		if( div.is(':visible') ) {
		    div.slideUp();
		    $(this).text('Advanced Mode');
		} else {
			div.slideDown();
		    $(this).text('Simple Mode');
		}
		
		return false;			
	});
	
	$('button#refresh-clients').bind('click', function(event) {
		updateClient();
		return false;			
	});
	
	
	$.address.change(function(event) {  
		if (event.value == '/') {
			$('#placeholder').html('');			
		} else {
		}
	}); 	
	
	$('#client-view').hide();
	$('#advanced').hide();
	updateClient();
	setInterval("updateClient()", 60000);
});
</script>
		
	</head>
	<body>
		<div id="container">
			<div id="loading">Loading...</div>
			<div id="tabs">
				<ul>
					<!-- 
					<li id="t1"><a href="#">Node 1</a></li>
				    <li id="t2"><a href="#">Node 2</a></li> 
					-->
					<li id="note"><span>Note: Log times are GMT (except for the <i>Report</i> which is specified by <i>Local Time Zone</i>)</span></li>
					<li id="mode"><span><button>Advanced Mode</button></span></li>
				</ul>
			</div>
			<div class="row" id="stats">
				<h3>Current Status</h3>
				<div><span>Active Client Connections:</span>&nbsp<span style="display: inline-block; width: 20px" id="client-count">
					</span> &nbsp&nbsp&nbsp&nbsp
					<button id="refresh-clients">Refresh</button>
					<button id="show-clients" type="submit">Show</button>
				</div>
				<div id="client-view">
					<table cellpadding="0" cellspacing="1" border="0" id="clients"> 
						<thead> 
							<tr> 
								<th width="130px";">Employee Number</th> 
								<th width="100px";">Pending Alerts</th> 
								<th width="200px">Last Login</th> 
								<th width="150px">Time Since Last Login</th> 
							</tr> 
						</thead> 
						<tbody> 
						</tbody> 
					</table> 				
				</div>
			</div>
			<div class="row">
				<form action="logging/display/task_log" method="get">
					<h3>Report by Employee Number</h3>
					<fieldset>
						<label>Employee Number (eg. 123456)</label>
						<input type="text" name="employee"/>
						<button id="submit-task-log" type="submit">Generate</button>
						<label>Local Time Zone</label>
						<select name="timezone">
							<option value="PST">Pacific</option>
							<option value="MST">Mountain</option>
							<option value="CST">Central</option>
							<option value="EST" selected="selected">Eastern</option>
							<option value="NST">Newfoundland</option>
							<option value="AST">Atlantic</option>
						</select>						
					</fieldset>
				</form>
			</div>
			<div id="advanced">
				<div class="row">
					<form action="logging/display/search" method="get">
						<h3>Search for Events by Employee Number</h3>
						<fieldset>
							<label>Employee Number (eg. 123456)</label>
							<input type="text" name="employee"/>
							<button id="submit-event-employee" type="submit">Search</button>
							<label>Max. Size</label>
							<select name="size">
								<option selected="selected">20</option>
								<option>50</option>
								<option>100</option>
								<option>500</option>
								<option>1000</option>
							</select>
							<label>KB</label>
							<input type="hidden" name="regex"/>
							<fieldset>
								<legend>General Filters</legend>
								<input id="include-alerts" type="checkbox" /><label>Alerts</label>
								<input id="include-connect" type="checkbox" /><label>Connectivity</label>
							</fieldset>						
							<fieldset>
								<legend>Web Service Filters</legend>
								<input id="include-login" type="checkbox" /><label>Login</label>
								<input id="include-tasks" type="checkbox" /><label>Tasks</label>
								<input id="include-ack" type="checkbox" /><label>Acknowledge</label>
								<input id="include-start" type="checkbox" /><label>Start</label>
								<input id="include-stop" type="checkbox" /><label>Stop</label>
								<input id="include-ws" type="checkbox" /><label>XML (for selected filters)</label>
							</fieldset>
							<fieldset>
								<legend>Global</legend>
								<input id="include-all" type="checkbox" /><label>Select All</label>
							</fieldset>
						</fieldset>
					</form>
				</div>	
				<div class="row">
					<form action="logging/display/search" method="get">
						<h3>Search by Text</h3>
						<fieldset>
							<label>Search Text</label>
							<input type="text" name="regex"/>
							<button id="submit-regex" type="submit">Search</button>
							<label>Max. Size</label>
							<select name="size">
								<option selected="selected">20</option>
								<option>50</option>
								<option>100</option>
								<option>500</option>
								<option>1000</option>
							</select>
							<label>KB</label>
						</fieldset>
					</form>
				</div>
				<div class="row">
					<form action="logging/download" method="get">
						<h3>Download</h3>
						<fieldset>
							<label>Today's Log (ZIP)</label>
							<button id="submit-download" type="submit">Download</button>
						</fieldset>
					</form>
				</div>
			</div>
			<div id="results" class="row">
	 			<div id="placeholder"></div>
			</div>
		</div>
	</body>
</html>
