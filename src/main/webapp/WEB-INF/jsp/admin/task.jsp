<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>


<f:form commandName="cabin">
	<div id="buttonsBar">
	<input type="submit" value="Save"/>
	<button onclick="deleteTask(); return false;">Delete</button>
	</div>
	<div id="details">
		<f:hidden path="id"/>
		<input name="silent" value="true" type="checkbox"/>
		<label>
			Silent (update only)
		</label>
		<br/>
		<label>
			Type
		</label>
		<f:select path="type">
			<f:option value="Turn_1"/><f:option value="Turn_2"/><f:option value="Turn_3"/><f:option value="O/N_1"/><f:option value="SEC"/><f:option value="CLN_1"/><f:option value="PROV_2"/><f:option value="RON_2"/><f:option value="DEEP_1"/><f:option value="CLN+TB_1"/><f:option value="SO"/>
			<f:option value="GT_CTRL"/><f:option value="GT_SPAT"/><f:option value="GT_ASST"/>
			<f:option value="ARR_MEET"/><f:option value="ARR_SPAT"/>
			<f:option value="LUNCH"/>
		</f:select>
		<br/>
		<label>
			Start Time
		</label>
		<f:input path="startTime"/>
		<br/>
		<label>
			End Time
		</label>
		<f:input path="endTime"/>
		<br/>
		<label>
			Location
		</label>
		<f:input path="location"/>
		<br/>
		<label>
			Flight Number 
		</label>
		<f:input path="flightNum"/>
		<br/>
		<label>
			Aircraft/Fin
		</label>
		<f:input path="aircraftFin"/>
		<br/>
	</div>
</f:form>
