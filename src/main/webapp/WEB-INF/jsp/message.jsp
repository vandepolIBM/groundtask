<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
	
	<head>
		<title>AJAX Master 4000</title>
		<script type='text/javascript' src='../dwr/interface/AjaxManager.js'></script>
		<script type='text/javascript' src='../dwr/engine.js'></script>
		<!--script type='text/javascript' src='../dwr/util.js'></script -->
		<script language="JavaScript"><!--
			/**
			 * Find the element in the current HTML document with the given id or ids
			 * @see http://getahead.org/dwr/browser/util/$
			 */
			 if (document.all) {
			  byId = function() {
			    var elements = new Array();
			    for (var i = 0; i < arguments.length; i++) {
			      var element = arguments[i];
			      if (typeof element == 'string') {
			        element = document.all[element];
			      }
			      if (arguments.length == 1) {
			        return element;
			      }
			      elements.push(element);
			    }
			    return elements;
			  };
			}
			
			/**
			 * Alias $ to dwr.util.byId
			 * @see http://getahead.org/dwr/browser/util/$
			 */
			var $;
			if (!$) {
			  $ = byId;
			}

			var timer = null;
			var ajaxReply = function(data) {
				$('chatWindow').innerHTML += data + "<br />"; 
			}
			
			function sendMsg() {
				var msgValue = $('msg').value;
				var userNameValue = $('userName').value;
				if ( (userNameValue != '') && ( msgValue != '') ) {
					AjaxManager.sendMsg(userNameValue, msgValue);
					$('userName').disabled=true;
					$('msg').value='';
					$('msg').focus();
				} else {
					alert('Please enter a valid name and/or a message!');
				}
			}
			
			function sendAlert() {
				var alertValue = $('alert').value;
				if (  alertValue != '' ) {
					AjaxManager.sendAlert(alertValue);
					$('alert').value='';
					$('alert').focus();
				} else {
					alert('Please enter a valid alert msg!');
				}
			}
			
			function clearChatWindow() {
				$('chatWindow').innerHTML = '';
				$('msg').focus();
			}
		--></script>
 	</head>
	
	<body onload="dwr.engine.setActiveReverseAjax(true);$('userName').focus();">
		<h1>AJAX Master 4000</h1>
 		<!--label>
			<input type="submit" name="timeBtn" id="timeBtn" value="Get Time!" onClick="AjaxManager.getTimeActive();"/>
		</label>
		<div>	
			<span id="result">Server Times:</span>
		</div-->
		<div>	
			<span id="nameLabel">Send Alert:</span>
			<br />
			<input type="text" name="alert" id="alert"/>
			<br />
			<input type="submit" name="alertSubmit" id="alertSubmit" value="Send Alert!" onclick="sendAlert();"/>
			<br />
			<br />
		</div>
		<div>	
			<span id="nameLabel">Your Name:</span>
			<br />
			<input type="text" name="userName" id="userName"/>
		</div>
		<div>	
			<span id="msgLabel">Message:</span>
			<br />
			<input type="text" name="msg" id="msg" />
			<br />
			<input type="submit" name="submit" id="submit" value="Send it!" onclick="sendMsg();"/>
		</div>
		<div>
			<br />	
			<span id="chatWindowLabel"><b>Chat Window:</b></span>
			<br />
			<span id="chatWindow"></span>
			<br />
			<input type="reset" name="clear" id="clear" value="Clear Chat Window!" onclick="clearChatWindow();"/>
		</div>
		
		
	</body>

</html>