<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="MobileOptimized" content="240" />
		
		<title>Air Canada - StreamLine</title>	
		
		<link rel="stylesheet" href="<c:url value='/css/style.css' />" type="text/css" />
		
		<script type="text/javascript">
			function login() {
				document.location = "<c:url value='/ctrl/main.html?empl=' />" + document.getElementById('id').value;
			}
		</script>
	</head>	
	<body>
		<label>Employee Number: </label><input type="text" id="id" size="10"/><button onclick="login();">Login</button>
	</body>
</html>

