package com.ibm.ac.ghs.persistence.jdbc;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.ibm.ac.ghs.persistence.Employee;
import com.ibm.ac.ghs.persistence.ServerAlert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/daos.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
@Transactional
public class JdbcEmployeeDaoTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private JdbcServerAlertDao alertDao;
	
	@Autowired
	private JdbcEmployeeDao employeeDao;

	@Before
	public void setupTestData() {
	}

	@Test
	public void testInsertAndUpdateEmployee() throws Exception {
		Assert.assertNull(employeeDao.findByEmployeeNumber("1"));
		
		employeeDao.insert(createEmployee("1", new Date()));
		Assert.assertNotNull(employeeDao.findByEmployeeNumber("1"));
		
		employeeDao.update(createEmployee("1", new Date()));
		Assert.assertNotNull(employeeDao.findByEmployeeNumber("1"));
	}
	
	@Test
	public void testFindAll() throws Exception {
		alertDao.insert(createAlert("alertid_1", "1"));
		alertDao.insert(createAlert("alertid_2", "1"));
		alertDao.insert(createAlert("alertid_3", "1"));
		alertDao.insert(createAlert("alertid_4", "2"));
		alertDao.insert(createAlert("alertid_5", "2"));
		alertDao.insert(createAlert("alertid_6", "999"));
		
		employeeDao.insert(createEmployee("1", new Date(1000)));
		employeeDao.insert(createEmployee("2", new Date(2000)));
		employeeDao.insert(createEmployee("3", new Date(3000)));
		
		Assert.assertEquals(3, employeeDao.findEmployeesWithQueuedAlerts().size());
	}
	
	@Test
	public void testFindEmployeesWithQueuedAlerts() throws Exception {
		employeeDao.insert(createEmployee("1", new Date()));
		employeeDao.insert(createEmployee("2", new Date()));
		employeeDao.insert(createEmployee("3", new Date()));		
	}
	
	private Employee createEmployee(String employeeNumber, Date lastLogin) {
		Employee employee = new Employee();
		employee.setEmployeeNumber(employeeNumber);
		employee.setLastRegistered(lastLogin);
		return employee;
	}
	
	private ServerAlert createAlert(String id, String employeeNumber) {
		ServerAlert serverAlert = new ServerAlert();
		serverAlert.setId(id);
		serverAlert.setEmployee(new Employee());
		serverAlert.getEmployee().setEmployeeNumber(employeeNumber);
		return serverAlert;
	}	
}
