package com.ibm.ac.ghs.persistence.jdbc;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.ibm.ac.ghs.persistence.Employee;
import com.ibm.ac.ghs.persistence.ServerAlert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/daos.xml" })
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = true)
@Transactional
public class JdbcServerAlertDaoTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private JdbcServerAlertDao alertDao;

	@Autowired
	private JdbcEmployeeDao employeeDao;
	
	private Date lastRegistered = new Date();
	
	@Before
	public void setupTestData() {
		Employee employee = new Employee();
		employee.setEmployeeNumber("1");
		employee.setLastRegistered(lastRegistered);
		employeeDao.insert(employee);
	}

	@Test
	public void testInsertAlert() throws Exception {
		alertDao.insert(createAlert("1", "1"));
		Assert.assertNotNull(alertDao.findById("1"));
		Assert.assertEquals(lastRegistered, alertDao.findById("1").getEmployee().getLastRegistered());
	}
	
	@Test
	public void testFindByEmployee() throws Exception {
		alertDao.insert(createAlert("1", "007341"));
		alertDao.insert(createAlert("2", "007342"));
		alertDao.insert(createAlert("3", "007343"));
		alertDao.insert(createAlert("4", "1"));
		Assert.assertEquals(1, alertDao.findByEmployeeNumber("007343").size());
		Assert.assertEquals(1, alertDao.findByEmployeeNumber("1").size());
	}
	
	@Test
	public void testDeleteOlderThan() throws Exception {
		alertDao.insert(createAlert("1", "007341"));
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
		}
		alertDao.insert(createAlert("2", "007341"));
		
		alertDao.deleteByOlderThan(1);
		Assert.assertEquals(1, alertDao.findByEmployeeNumber("007341").size());		
	}
	
	@Test 
	public void testInsertDeleteAndCount() throws Exception {
		alertDao.insert(createAlert("1", "007341"));
		Assert.assertEquals(1, alertDao.getNumAlerts());
		
		alertDao.insert(createAlert("2", "007341"));
		alertDao.insert(createAlert("3", "007341"));
		Assert.assertEquals(3, alertDao.getNumAlerts());
		
		alertDao.delete("3");
		Assert.assertEquals(2, alertDao.getNumAlerts());
		
		alertDao.deleteByOlderThan(0);
		Assert.assertEquals(0, alertDao.getNumAlerts());		
	}
	
	private ServerAlert createAlert(String id, String employeeNumber) {
		ServerAlert serverAlert = new ServerAlert();
		serverAlert.setId(id);
		serverAlert.setEmployee(new Employee());
		serverAlert.getEmployee().setEmployeeNumber(employeeNumber);
		serverAlert.setTimestamp(new Date());
		return serverAlert;
	}
}
