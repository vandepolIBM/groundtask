package com.ibm.ac.ghs.tasks.sabre;

import java.util.List;

import junit.framework.TestCase;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapVersion;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import com.ibm.ac.ghs.tasks.AbstractTask;

public class SabreWebServiceClientTest extends TestCase {

	private SabreTaskService webClient;
	
	public SabreWebServiceClientTest() throws Exception {
		SaajSoapMessageFactory messageFactory = new SaajSoapMessageFactory();
		messageFactory.setSoapVersion(SoapVersion.SOAP_12);
		messageFactory.afterPropertiesSet();
		
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("com.sabre.streamline");
		marshaller.afterPropertiesSet();
		
		WebServiceTemplate template = new WebServiceTemplate(messageFactory);
		template.setDefaultUri("http://cdsserv.mkm.can.ibm.com:8223/ACGHS-200804-MobileInterfaceWebService/Mobile.asmx");
		template.setMarshaller(marshaller);
		template.setUnmarshaller(marshaller);
		
		webClient = new SabreTaskService();
		webClient.setWebServiceTemplate(template);
	}
	
	public void testLogin() throws Exception {
		webClient.login("1234");
	}
	
	public void testGetTasks() throws Exception {
		List<AbstractTask> tasks = webClient.getTasks("1234");
		System.out.println(tasks);
	}
}
