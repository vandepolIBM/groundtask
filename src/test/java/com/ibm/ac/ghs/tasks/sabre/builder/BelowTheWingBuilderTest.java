package com.ibm.ac.ghs.tasks.sabre.builder;

import junit.framework.Assert;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.junit.Test;

import com.ibm.ac.ghs.tasks.BelowTheWingCnx;
import com.ibm.ac.ghs.tasks.BelowTheWingDeparture;
import com.ibm.ac.ghs.tasks.BelowTheWingArrival;
import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.BelowTheWingLavWater;
import com.ibm.ac.ghs.tasks.BelowTheWingTow;
import com.ibm.ac.ghs.tasks.BelowTheWingTurn;
import com.ibm.ac.ghs.tasks.Cabin;
import com.ibm.ac.ghs.tasks.sabre.resource.ResourceManager;
import com.sabre.streamline.StaffManagerMobileMessage;

public class BelowTheWingBuilderTest extends AbstractBuilderTest {

	private ResourceManager resourceManager = new ResourceManager();

	@Test
	public void testBelowTheWingArrival() throws Exception {
		BelowTheWingArrivalBuilder b = new BelowTheWingArrivalBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-ARR BelowTheWingArrival - Inbound");
		StaffManagerMobileMessage msg = unmarshal("btw-arr-tow-inbound.xml");
		BelowTheWingArrival task = (BelowTheWingArrival) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("CRA/711", task.getAircraftFin());
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertTrue(task.getOpsRemarksArr().contains("TOW-remark"));
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getEta=" + task.getEta());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());
		
		if (task.getResource1() != null) {
			System.out.println("getResource1=" + task.getResource1());
			System.out.println("getTaskType1=" + task.getTaskType1());
			Assert.assertEquals("SHAFIQ AHMAD", task.getResource1());
			Assert.assertEquals("OFF/L Crew:", task.getTaskType1());
			
		} 
		if (task.getResource2() != null) {
			System.out.println("getResource2=" + task.getResource2());
			System.out.println("getTaskType2=" + task.getTaskType2());
			Assert.assertEquals("STARK WILLIAM", task.getResource2());
			Assert.assertEquals("OFF/L Assist Crew:", task.getTaskType2());			
		} 
		if (task.getResource3() != null) {
			System.out.println("getResource3=" + task.getResource3());
			System.out.println("getTaskType3=" + task.getTaskType3());
			Assert.assertEquals("ANGELA FORD", task.getResource3());
			Assert.assertEquals("Cabin Crew #1:", task.getTaskType3());			
		} 
		if (task.getResource4() != null) {
			System.out.println("getResource4=" + task.getResource4());
			System.out.println("getTaskType4=" + task.getTaskType4());
			Assert.assertEquals("JENNIFER GINA", task.getResource4());
			Assert.assertEquals("Cabin Crew #1:", task.getTaskType4());			
		} 
		
		System.out.println("");
		
	}

	@Test
	public void testBelowTheWingArrivalNull() throws Exception {
		BelowTheWingArrivalBuilder b = new BelowTheWingArrivalBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-ARR BelowTheWingArrival - Inbound - Empty Remarks");
		StaffManagerMobileMessage msg = unmarshal("btw-arr-tow-inbound-null.xml");
		BelowTheWingArrival task = (BelowTheWingArrival) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("CRA/711", task.getAircraftFin());
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertEquals(null,task.getOpsRemarksArr());	
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getEta=" + task.getEta());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());		
		System.out.println("");
	}
	
	@Test
	public void testBelowTheWingDeparture() throws Exception {
		BelowTheWingDepartureBuilder b = new BelowTheWingDepartureBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-DEP BelowTheWingDeparture - Outbound");
		StaffManagerMobileMessage msg = unmarshal("btw-dep-tow-outbound.xml");
		BelowTheWingDeparture task = (BelowTheWingDeparture) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("DH1/802", task.getAircraftFin());
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertTrue(task.getOpsRemarksDep().contains("TOW-remark"));
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getEtd=" + task.getEtd());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());		
		
		if (task.getResource1() != null) {
			System.out.println("getResource1=" + task.getResource1());
			System.out.println("getTaskType1=" + task.getTaskType1());
			Assert.assertEquals("HOLDER SEAN ANTHONY", task.getResource1());
			Assert.assertEquals("ON/L Assist Crew:", task.getTaskType1());
			
		} 
		if (task.getResource2() != null) {
			System.out.println("getResource2=" + task.getResource2());
			System.out.println("getTaskType2=" + task.getTaskType2());
			Assert.assertEquals("KLAJIC MILOS", task.getResource2());
			Assert.assertEquals("Dep WCHC Crew:", task.getTaskType2());			
		} 
		if (task.getResource3() != null) {
			System.out.println("getResource3=" + task.getResource3());
			System.out.println("getTaskType3=" + task.getTaskType3());
			Assert.assertEquals("ALBERT BART", task.getResource3());
			Assert.assertEquals("ON/L Crew:", task.getTaskType3());			
		} 
		if (task.getResource4() != null) {
			System.out.println("getResource4=" + task.getResource4());
			System.out.println("getTaskType4=" + task.getTaskType4());
			Assert.assertEquals("SAM ZELOS", task.getResource4());
			Assert.assertEquals("Dep Dispatch Crew:", task.getTaskType4());			
		} 
		
		System.out.println("");		
	}
	
	@Test
	public void testBelowTheWingDepartureNull() throws Exception {
		BelowTheWingDepartureBuilder b = new BelowTheWingDepartureBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-DEP BelowTheWingDeparture - Outbound - Empty Remarks");
		StaffManagerMobileMessage msg = unmarshal("btw-dep-tow-outbound-null.xml");
		BelowTheWingDeparture task = (BelowTheWingDeparture) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("DH1/802", task.getAircraftFin());
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertEquals(null,task.getOpsRemarksDep());
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getEtd=" + task.getEtd());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());		
		System.out.println("");		
	}
	
	@Test
	public void testBelowTheWingLavWater() throws Exception {
		BelowTheWingLavWaterBuilder b = new BelowTheWingLavWaterBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-LAV BelowTheWingLavWater - InBound");
		StaffManagerMobileMessage msg = unmarshal("btw-lav-tow-inbound.xml");
		BelowTheWingLavWater task = (BelowTheWingLavWater) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("CRJ/167", task.getAircraftFin());	
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertTrue(task.getOpsRemarksArr().contains("TOW-remark"));		
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getArrETA=" + task.getArrETA());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());		
		System.out.println("");		
	}

	@Test
	public void testBelowTheWingLavWaterNull() throws Exception {
		BelowTheWingLavWaterBuilder b = new BelowTheWingLavWaterBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-LAV BelowTheWingLavWater - InBound - Empty Remarks");
		StaffManagerMobileMessage msg = unmarshal("btw-lav-tow-inbound-null.xml");
		BelowTheWingLavWater task = (BelowTheWingLavWater) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("CRJ/167", task.getAircraftFin());	
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertEquals(null,task.getOpsRemarksArr());			
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getArrETA=" + task.getArrETA());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());				
		System.out.println("");		
	}
	
	@Test
	public void testBelowTheWingLavWater2() throws Exception {
		BelowTheWingLavWaterBuilder b = new BelowTheWingLavWaterBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-LAV BelowTheWingLavWater - OutBound");
		StaffManagerMobileMessage msg = unmarshal("btw-lav-tow-outbound.xml");
		BelowTheWingLavWater task = (BelowTheWingLavWater) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("CRJ/167", task.getAircraftFin());
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertTrue(task.getOpsRemarksDep().contains("TOW-remark"));		
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getArrETD=" + task.getDepETD());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());				
		System.out.println("");
		
	}
	
	@Test
	public void testBelowTheWingLavWater2Null() throws Exception {
		BelowTheWingLavWaterBuilder b = new BelowTheWingLavWaterBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-LAV BelowTheWingLavWater - OutBound - Empty Remarks");
		StaffManagerMobileMessage msg = unmarshal("btw-lav-tow-outbound-null.xml");
		BelowTheWingLavWater task = (BelowTheWingLavWater) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("CRJ/167", task.getAircraftFin());
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertEquals(null,task.getOpsRemarksDep());				
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getArrETD=" + task.getDepETD());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());				
		System.out.println("");
		
	}

	@Test
	public void testBelowTheWingCnx() throws Exception {
		BelowTheWingCnxBuilder b = new BelowTheWingCnxBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-CNX BelowTheWingCnx - Inbound");
		StaffManagerMobileMessage msg = unmarshal("btw-cnx-tow-inbound.xml");
		BelowTheWingCnx task = (BelowTheWingCnx) b.build(msg.getTaskInformation());
		
		Assert.assertNotNull(task.getHc());
		Assert.assertEquals("23", task.getHcBags());
		//System.out.println(task.toString());
		Assert.assertTrue(task.getOpsRemarksArr().contains("TOW-remark"));
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getEta=" + task.getEta());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());		
		System.out.println("");
		
	}	
		
	@Test
	public void testBelowTheWingCnxNull() throws Exception {
		BelowTheWingCnxBuilder b = new BelowTheWingCnxBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-CNX BelowTheWingCnx - Inbound - Empty Remarks");
		StaffManagerMobileMessage msg = unmarshal("btw-cnx-tow-inbound-null.xml");
		BelowTheWingCnx task = (BelowTheWingCnx) b.build(msg.getTaskInformation());
		
		Assert.assertNotNull(task.getHc());
		Assert.assertEquals("23", task.getHcBags());
		//System.out.println(task.toString());
		Assert.assertEquals(null,task.getOpsRemarksArr());
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getEta=" + task.getEta());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());		
		System.out.println("");
		
	}	
	
	@Test
	public void testBelowTheWingTow() throws Exception {
		BelowTheWingTowBuilder b = new BelowTheWingTowBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-TOW BelowTheWingTow - Inbound and Outbound");
		StaffManagerMobileMessage msg = unmarshal("btw-tow-tow-inbound-outbound.xml");
		BelowTheWingTow task = (BelowTheWingTow) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("320/408", task.getAircraftFin());
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertTrue(task.getOpsRemarksArr().contains("TOW-remark"));
		Assert.assertTrue(task.getOpsRemarksDep().contains("TOW-remark"));		
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getArrETA=" + task.getArrETA());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());		
		System.out.println("");
		
	}

	@Test
	public void testBelowTheWingTowNull() throws Exception {
		BelowTheWingTowBuilder b = new BelowTheWingTowBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-TOW BelowTheWingTow - Inbound and Outbound - Empty Remarks");
		StaffManagerMobileMessage msg = unmarshal("btw-tow-tow-inbound-outbound-null.xml");
		BelowTheWingTow task = (BelowTheWingTow) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("320/408", task.getAircraftFin());
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertEquals(null,task.getOpsRemarksArr());
		Assert.assertEquals(null,task.getOpsRemarksDep());		
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getArrETA=" + task.getArrETA());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());		
		System.out.println("");
		
	}
	
	@Test
	public void testBelowTheWingTurn() throws Exception {
		BelowTheWingTurnBuilder b = new BelowTheWingTurnBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-TURN BelowTheWingTurn - OutBound");
		StaffManagerMobileMessage msg = unmarshal("btw-turn-tow-outbound.xml");
		BelowTheWingTurn task = (BelowTheWingTurn) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("CRA/715", task.getAircraftFin());
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertTrue(task.getOpsRemarksDep().contains("TOW-remark"));
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());	
		//System.out.println("getArrETD=" + task.getDepETD());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());		
		System.out.println("");
		
	}

	@Test
	public void testBelowTheWingTurnNull() throws Exception {
		BelowTheWingTurnBuilder b = new BelowTheWingTurnBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-TURN BelowTheWingTurn - OutBound - Empty Remarks");
		StaffManagerMobileMessage msg = unmarshal("btw-turn-tow-outbound-null.xml");
		BelowTheWingTurn task = (BelowTheWingTurn) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("CRA/715", task.getAircraftFin());
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertEquals(null,task.getOpsRemarksDep());
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getArrETD=" + task.getDepETD());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());		
		System.out.println("");
		
	}
	
	@Test
	public void testBelowTheWingTurn2() throws Exception {
		BelowTheWingTurnBuilder b = new BelowTheWingTurnBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-TURN BelowTheWingTurn - InBound");
		StaffManagerMobileMessage msg = unmarshal("btw-turn-tow-inbound.xml");
		BelowTheWingTurn task = (BelowTheWingTurn) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("DH3/313", task.getAircraftFin());
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertTrue(task.getOpsRemarksArr().contains("TOW-remark"));
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getArrETA=" + task.getArrETA());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());		

		if (task.getResource1() != null) {
			System.out.println("getResource1=" + task.getResource1());
			System.out.println("getTaskType1=" + task.getTaskType1());
			Assert.assertEquals("SHAFIQ AHMAD", task.getResource1());
			Assert.assertEquals("Turn Crew:", task.getTaskType1());
			
		} 
		if (task.getResource2() != null) {
			System.out.println("getResource2=" + task.getResource2());
			System.out.println("getTaskType2=" + task.getTaskType2());
			Assert.assertEquals("STARK WILLIAM", task.getResource2());
			Assert.assertEquals("ON/L Assist Crew:", task.getTaskType2());			
		} 
		if (task.getResource3() != null) {
			System.out.println("getResource3=" + task.getResource3());
			System.out.println("getTaskType3=" + task.getTaskType3());
			Assert.assertEquals("ANGELA FORD", task.getResource3());
			Assert.assertEquals("Cabin Crew #1:", task.getTaskType3());			
		} 
		if (task.getResource4() != null) {
			System.out.println("getResource4=" + task.getResource4());
			System.out.println("getTaskType4=" + task.getTaskType4());
			Assert.assertEquals("JENNIFER GINA", task.getResource4());
			Assert.assertEquals("Arr WCHC Crew:", task.getTaskType4());			
		} 
				
		System.out.println("");
		
	}

	@Test
	public void testBelowTheWingTurn2Null() throws Exception {
		BelowTheWingTurnBuilder b = new BelowTheWingTurnBuilder();
		b.setResourceManager(resourceManager);

		System.out.println("BTW-TURN BelowTheWingTurn - InBound - Empty Remarks");
		StaffManagerMobileMessage msg = unmarshal("btw-turn-tow-inbound-null.xml");
		BelowTheWingTurn task = (BelowTheWingTurn) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("DH3/313", task.getAircraftFin());
		//Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println(task.toString());
		Assert.assertEquals(null,task.getOpsRemarksArr());
		System.out.println("OpsRemarksArr=" + task.getOpsRemarksArr());
		System.out.println("getOpsRemarksDep=" + task.getOpsRemarksDep());
		//System.out.println("getArrETA=" + task.getArrETA());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());		
		System.out.println("");
		
	}
	
}
