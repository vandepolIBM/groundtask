package com.ibm.ac.ghs.tasks.sabre.resource;

import org.junit.Test;

import com.sabre.streamline.Resource;
import com.sabre.streamline.Task;
import com.sabre.streamline.TaskCategory;
import com.sabre.streamline.TaskInformation;
import com.sabre.streamline.TaskInformation.OtherResources;


public class ResourceManagerTest {

	@Test
	public void testManager() {
		ResourceManager rm = new ResourceManager();
		
		TaskInformation ti = new TaskInformation();
		ti.setTask(new Task());
		ti.getTask().setTaskType("CLN_2");
		ti.getTask().setTaskCategory(TaskCategory.CABIN);
		ti.setOtherResources(new OtherResources());

		ti.getOtherResources().getResource().add(getResource("Cabin_CLN_1"));
		ti.getOtherResources().getResource().add(getResource("Cabin_CLN_2"));
		ti.getOtherResources().getResource().add(getResource("Cabin_CLN_3"));
		ti.getOtherResources().getResource().add(getResource("Cabin_CLN_4"));
		
		System.out.println(rm.matchResources(ti));
	}
	
	private Resource getResource(String type) {
		Resource resource = new Resource();
		resource.setTaskType(type);
		return resource;
	}
}
