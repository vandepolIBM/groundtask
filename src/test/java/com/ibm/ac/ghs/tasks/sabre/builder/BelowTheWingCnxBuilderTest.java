package com.ibm.ac.ghs.tasks.sabre.builder;

import org.junit.Assert;
import org.junit.Test;

import com.ibm.ac.ghs.tasks.BelowTheWingCnx;
import com.ibm.ac.ghs.tasks.sabre.resource.ResourceManager;
import com.sabre.streamline.StaffManagerMobileMessage;

public class BelowTheWingCnxBuilderTest extends AbstractBuilderTest {

	private ResourceManager resourceManager = new ResourceManager();

	@Test
	public void testResources() throws Exception {
		BelowTheWingCnxBuilder b = new BelowTheWingCnxBuilder();
		b.setResourceManager(resourceManager);

		StaffManagerMobileMessage msg = unmarshal("cnx.xml");
		BelowTheWingCnx task = (BelowTheWingCnx) b.build(msg.getTaskInformation());
		
		Assert.assertNotNull(task.getHc());
		Assert.assertEquals("45", task.getHcBags());
	}
}
