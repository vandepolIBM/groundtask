package com.ibm.ac.ghs.tasks.sabre.builder;

import junit.framework.Assert;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.junit.Test;

import com.ibm.ac.ghs.tasks.AboveTheWingDeparture;
import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.Cabin;
import com.ibm.ac.ghs.tasks.sabre.resource.ResourceManager;
import com.sabre.streamline.StaffManagerMobileMessage;

public class AboveTheWingDepartureBuilderTest extends AbstractBuilderTest {

	private ResourceManager resourceManager = new ResourceManager();

	@Test
	public void testResources() throws Exception {
		AboveTheWingDepartureBuilder b = new AboveTheWingDepartureBuilder();
		b.setResourceManager(resourceManager);

		StaffManagerMobileMessage msg = unmarshal("atw-dep.xml");
		AboveTheWingDeparture task = (AboveTheWingDeparture) b.build(msg.getTaskInformation());
		
		Assert.assertEquals("XUE LIANG CAI", task.getControlAgent());
		Assert.assertEquals("PERRUZZA BIANCA MARIA", task.getAssistAgent1());
		//System.out.println("getEtd=" + task.getEtd());
		//System.out.println("getEstimatedTime=" + task.getEstimatedTime());
	}
}
