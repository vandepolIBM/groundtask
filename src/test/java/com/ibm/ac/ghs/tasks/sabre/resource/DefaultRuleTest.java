package com.ibm.ac.ghs.tasks.sabre.resource;

import org.junit.Assert;
import org.junit.Test;

import com.sabre.streamline.Resource;
import com.sabre.streamline.TaskCategory;


public class DefaultRuleTest {

	@Test
	public void testMatch() {

		Assert.assertNull(new DefaultRule(null, "(.+)", "(.+)", "(.+)").matches(null, null, null));
		Assert.assertNotNull(new DefaultRule(null, "(.+)", "(.+)", "(.+)").matches(TaskCategory.CABIN, "CLN_1", getResource("Cabin_CLN_4")));
		Assert.assertNull(new DefaultRule(null, "ATW_Dep", "(.+)", "(.+)").matches(TaskCategory.CABIN, "CLN_1", getResource("Cabin_CLN_4")));
		
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_1", getResource("Cabin_CLN_1")));
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_2", getResource("Cabin_CLN_1")));
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_3", getResource("Cabin_CLN_1")));
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_4", getResource("Cabin_CLN_1")));

		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_1", getResource("Cabin_CLN_2")));
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_2", getResource("Cabin_CLN_2")));
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_3", getResource("Cabin_CLN_2")));
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_4", getResource("Cabin_CLN_2")));

		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_1", getResource("Cabin_CLN_3")));
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_2", getResource("Cabin_CLN_3")));
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_3", getResource("Cabin_CLN_3")));
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_4", getResource("Cabin_CLN_3")));

		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_1", getResource("Cabin_CLN_4")));
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_2", getResource("Cabin_CLN_4")));
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_3", getResource("Cabin_CLN_4")));
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "CLN_(.+)", "Cabin_CLN_(.+)").matches(TaskCategory.CABIN, "CLN_4", getResource("Cabin_CLN_4")));

		Assert.assertNotNull(new DefaultRule(null, "Cabin", "(.+)_(.+)", "Cabin_$1_$2").matches(TaskCategory.CABIN, "JER_1", getResource("Cabin_JER_1")));
		Assert.assertNotNull(new DefaultRule(null, "Cabin", "(.+)_(.+)", "Cabin_$1_(.+)").matches(TaskCategory.CABIN, "JER_1", getResource("Cabin_JER_2")));

		Assert.assertNull(new DefaultRule(null, "Cabin", "(.+)_(.+)", "Cabin_$1_$2").matches(TaskCategory.CABIN, "FRED_2", getResource("Cabin_JER_1")));

		Assert.assertNotNull(new DefaultRule("ATWDep_Assist", "ATW-Dep", "(.+)", "(ATWDep_GT_CTRL|ATWDep_RMP_DIR_D)").matches(TaskCategory.ATW_DEP, "ANY", getResource("ATWDep_GT_CTRL")));
		Assert.assertNotNull(new DefaultRule("ATWDep_Assist", "ATW-Dep", "(.+)", "(ATWDep_GT_CTRL|ATWDep_RMP_DIR_D)").matches(TaskCategory.ATW_DEP, "ANY", getResource("ATWDep_RMP_DIR_D")));
	}
	
	private Resource getResource(String type) {
		Resource resource = new Resource();
		resource.setTaskType(type);
		return resource;
	}
}