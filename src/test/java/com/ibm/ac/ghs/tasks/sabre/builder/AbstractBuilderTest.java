package com.ibm.ac.ghs.tasks.sabre.builder;

import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.junit.Assert;
import org.springframework.util.ResourceUtils;

import com.ibm.ac.ghs.tasks.sabre.resource.ResourceManager;
import com.ibm.ac.ghs.tasks.sabre.resource.ResourceMatchResult;
import com.sabre.streamline.StaffManagerMobileMessage;
import com.sabre.streamline.TaskInformation;

public abstract class AbstractBuilderTest {

	private JAXBContext jaxbContext;
	
	public AbstractBuilderTest() {
		try {
			jaxbContext = JAXBContext.newInstance("com.sabre.streamline");
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	
	public StaffManagerMobileMessage unmarshal(String xmlFilename) throws Exception {
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		return (StaffManagerMobileMessage) unmarshaller.unmarshal(ResourceUtils.getFile("classpath:xml/" + xmlFilename));
	}
	
	private ResourceManager resourceManager;

	public void setResourceManager(ResourceManager resourceManager) {
		this.resourceManager = resourceManager;
	}
	
	/**
	 * Searches list of resource returned from Sabre against the rules defined
	 * in the resource rule chain.
	 * 
	 * @param taskInformation
	 * @return
	 */
	protected List<ResourceMatchResult> detectResources(TaskInformation taskInformation) {
		return resourceManager.matchResources(taskInformation);
	}
	
}
