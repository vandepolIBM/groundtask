package com.ibm.ac.ghs.tasks.sabre.builder;

import junit.framework.Assert;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.junit.Test;

import com.ibm.ac.ghs.tasks.AbstractTask;
import com.ibm.ac.ghs.tasks.Cabin;
import com.ibm.ac.ghs.tasks.sabre.resource.ResourceManager;
import com.sabre.streamline.StaffManagerMobileMessage;

public class CabinBuilderTest extends AbstractBuilderTest {

	private ResourceManager resourceManager = new ResourceManager();

	@Test
	public void testResources() throws Exception {
		CabinBuilder b = new CabinBuilder();
		b.setResourceManager(resourceManager);

		StaffManagerMobileMessage msg = unmarshal("cabin.xml");
		Cabin task = (Cabin) b.build(msg.getTaskInformation());
		
//		Assert.assertEquals("<p>CABRERA CARLOS</p>", task.getLcscaName());
//		System.out.println(task.getLcscaName());
//		System.out.println(task.toString());

		if (task.getResource1() != null) {
			System.out.println("getResource1=" + task.getResource1());
			System.out.println("getTaskType1=" + task.getTaskType1());
			Assert.assertEquals("CABRERA CARLOS", task.getResource1());
			Assert.assertEquals("Cabin Crew #2:", task.getTaskType1());
			
		} 
		if (task.getResource2() != null) {
			System.out.println("getResource2=" + task.getResource2());
			System.out.println("getTaskType2=" + task.getTaskType2());
			Assert.assertEquals("SHAFIQ AHMAD", task.getResource2());
			Assert.assertEquals("OFF/L Crew:", task.getTaskType2());			
		} 
		if (task.getResource3() != null) {
			System.out.println("getResource3=" + task.getResource3());
			System.out.println("getTaskType3=" + task.getTaskType3());
			Assert.assertEquals("STARK WILLIAM", task.getResource3());
			Assert.assertEquals("Turn Crew:", task.getTaskType3());			
		} 
		if (task.getResource4() != null) {
			System.out.println("getResource4=" + task.getResource4());
			System.out.println("getTaskType4=" + task.getTaskType4());
			Assert.assertEquals("ANGELA FORD", task.getResource4());
			Assert.assertEquals("Cabin Crew #1:", task.getTaskType4());			
		} 
		
	}
}
